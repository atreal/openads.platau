**********************************
openads.platau history / changelog
**********************************

8.0.0 (unreleased)
=====================

Features
--------

* Refactor iDE'AU forms retrieval to support on-host config
  Each middleware deployment can specify form to get dfrom iDE'AU


7.2.0-rc1 (2024-10-10)
======================

Features
--------

* feat: IDE'AU / Support CERFA integral
  https://gitlab.com/atreal/openads.platau/-/issues/131


7.1.0 (2024-10-04)
==================

Fixes
-----

* Correct temporay file not rendering
  https://gitlab.com/atreal/openads.platau/-/issues/119

Features
--------

* Add actor.uid in Message task
  Needed to support parallel tasks integration in openADS
  https://gitlab.com/atreal/openads.platau/-/issues/123

* Improved logging while testing.


7.0.0 (2024-06-17)
==================

Features
--------

* Provide fallback if poolbox is unavailable

* Support Plat'AU v10
  + Apply new specs
  + Few new mandatory properties are now used while handling Actor
    and implied improvement in `openads_driver` and `platau_cli`


6.8.0 (2024-06-10)
==================

Fixes
-----

* Avoid error on AttributsAmenagement drop
  Following #101, we needed to ensure AttributsAmenagement exists before
  dropping it
  https://gitlab.com/atreal/openads.platau/-/issues/101
  https://gitlab.com/atreal/openads.platau/-/issues/84

* Properly set `noLocal` for ADAU Dossier

* Avoid setuptool error while building offline package

* Publik : workaround to avoid null Type_piece
  We don't know why, but we retrieved Demandes without Type_piece value.
  This is a workaround with a fallback to 111 - (Autre piece) value.


Features
--------

* Update PISTE base urls (aife.economie.gouv.fr -> piste.gouv.fr)

* Improve log when retrieving tasks

* Publik : improve Demande retrieval
  To avoid some strange behavours on Publik (same Demande retrieved
  various time), we restricted the set applying a filter on state
  (DEMANDE_INIT_STATUS)
  We still don't know the root cause on Publik side

* Always update `dtDepot` on successful Piece upload
  https://gitlab.com/atreal/openads.platau/-/issues/81

* Support updating Piece
  https://gitlab.com/atreal/openads.platau/-/issues/112

* Support dropping Piece
  https://gitlab.com/atreal/openads.platau/-/issues/95

* Backport few Nomenclatures from v10


Tests
-----

* Improve dataset build

* Improve speed & avoid useless tests :
  - drop custom waiting time following notification-based step in
    `base_test._build_dataset`
  - drop notification assertation, as it's not in openads.platau scope


6.7.0 (2024-01-23)
==================


Fixes
-----

* Support non-ASCII char in Syncplicity upload
  Improved string normalization to support non-ASCII char in Syncplicity in pre-upload method
  https://gitlab.com/atreal/openads.platau/-/issues/94
  https://gitlab.com/atreal/openads.platau/-/issues/103

* Issue while mapping bad infoFiscales data
  https://gitlab.com/atreal/openads.platau/-/issues/102

* Catch uncaught exc. on bad params from openADS

* Allow Demande mapping without Piece
  https://gitlab.com/atreal/openads.platau/-/issues/99

* Ignore platau_acteur_ROLE_EXTRAPROPERTY on absent actor

* Adresse and Email are properly mapped on PM
  + When mapping Personne Morale Adresse & Coordonnées, we used to lose
    some data, we fixed that
  + Add mapping tests
  Ext Ref. MC0046315
  https://gitlab.com/atreal/openads.platau/-/issues/97

* Empty Demandeur type on Modificatif
  While mapping Deamndeur on Modificatif, 'type_demandeur' was lost.
  https://gitlab.com/atreal/openads.platau/-/issues/104

* Error while mapping AmenagementAttributs
  In some edge case (Ammenagement without any related Attributs),
  Attributs templates were not properly cleaned before mapping.
  https://gitlab.com/atreal/openads.platau/-/issues/101


Features
--------

* Update PISTE base url

* Add noTeledemarcheADAU in create_DI payload

* platau-cli: add 'history' command
  Allow to retrieve every events that occured on Dossier

* Add DPMI JSON payloads

* Support upload threshold method from env

* Support syncing generic parameters
  + openADS actor's parameters are now mapped to Actor, in a generic way
  + 'platau_acteur_guichet_unique_depot_adau' is synced from openADS toPlat'AU
  https://gitlab.com/atreal/openads.platau/-/issues/96


Tests
-----

* Deactivate uniquify on Piece creation

* Add SyncplicityConnector tests
  https://gitlab.com/atreal/openads.platau/-/issues/94

* Add special Piece test case in CT interation tests
  https://gitlab.com/atreal/openads.platau/-/issues/94


6.6.1 (2023-10-16)
==================

Fixes
-----

* Erase bad formated SIRET
  https://gitlab.com/atreal/openads.platau/-/issues/87

* Update noLocal on AD'AU first update
  noLocal update is not handled by /dossiersPut endpoint.
  A specific endpoint should be called (/dossiersNumerotationPUT)
  https://gitlab.com/atreal/openads.platau/-/issues/90


6.6.0 (2023-10-10)
==================

Feature
-------

* Support TS DIA in iDE'AU
  https://gitlab.com/atreal/openads.platau/-/issues/45


* Update Plat'AU base url
  https://gitlab.com/atreal/openads.platau/-/issues/92


6.5.0 (2023-09-06)
==================

Feature
-------

* Allow 100% flexibility in wokflow
  + State rationalization : we kept only one "Instruction" state, allowing to handle
    different type of document at any time
  + Provide necessary data at each WF state
  https://gitlab.com/atreal/openads.platau/-/issues/18
  https://gitlab.com/atreal/openads.platau/-/issues/82

* Multi nofication support
  + Provide Mandataire data to openADS
  + Provide email recipients list to be notified to Publik
  https://gitlab.com/atreal/openads.platau/-/issues/53


6.4.1 (2023-08-09)
==================

Fixes
-----

* Fix offline buildout dependencies foolowing VDM feedback


6.4.0 (2023-07-26)
==================

Feature
-------

* Provide Publik tracking code to openADS
  https://gitlab.com/atreal/openads.platau/-/issues/54

* Better handling on Piece downloading fail.
  Improve log and post-treatment when failing to download Piece from Plat'AU

* Give each batch an unique identifier
  https://gitlab.com/atreal/openads.platau/-/issues/80

* Update all dependencies


Fixes
-----

* Avoid to generate inconsistent Amenagement
  https://gitlab.com/atreal/openads.platau/-/issues/89


6.3.0 (2023-06-08)
==================

Fixes
-----

* Piece Complementaire nature properly set
  Publik: Piece nature set on task 'ajout_piece' was always set to 'Initiale'.
  Now, we are setting nature according to TYPE_DEMANDE :
    'dps' -> 1 (Initiale)
    'dpc' -> 2 (Complemantaire)
  https://gitlab.com/atreal/openads.platau/-/issues/78

* Cleanup experimental offset implementation
  Initially, handling specific offset would drop next events, causing data loss.
  Until now, offset operation has been used only on staging env, without consequences.
  We needed an offset based operation on production.

* Secure mapping on Amenagements sub-properties


Features
--------

* Better unique filename composition
  https://gitlab.com/atreal/openads.platau/-/issues/79


6.2.3 (2023-04-03)
==================

Fixes
-----

* Ignore in|completude event on CT mode.
  https://gitlab.com/atreal/openads.platau/-/issues/71

* On Tacite Decision, task state set to 'done'
  https://gitlab.com/atreal/openads.platau/-/issues/73


6.2.2 (2023-03-30)
==================

Fixes
-----

* Properly handle edge case introduced by a bad error handling in openADS
  See https://adullact.net/tracker/?func=detail&aid=10041&group_id=390&atid=2085


6.2.1 (2023-03-30)
==================

Fixes
-----

* Forget to cancel 'envoi_CL' if Dossier compentency is set to 'ETAT'


6.2.0 (2023-03-30)
==================

Features
--------

* Support Piece free description
  On both Publik and Plat'AU, this data waw not mapped.
  https://gitlab.com/atreal/openads.platau/-/issues/57

* Cancel `decision_DI` if not our competency
  If Dossier compentency is set to 'ETAT', its not our duty to apply the
  decision. Task should be canceled.
  https://gitlab.com/atreal/openads.platau/-/issues/69

* Support envoi_CL fro ANNUL & PRO
  https://gitlab.com/atreal/openads.platau/-/issues/68


6.1.0 (2023-03-22)
==================

Features
--------

* Secure timeout handling
  Two timeout are defined :
    + one for requests calls
    + one for PISTE calls
  Both can be overrided by env var.
  https://gitlab.com/atreal/openads.platau/-/issues/49
  https://gitlab.com/atreal/openads.platau/-/issues/59


6.0.0 (2023-03-15)
==================

Features
--------

* Support PeC with attachment in CT mode
  https://gitlab.com/atreal/openads.platau/-/issues/42

* Cancel avis_decision task if missing data
  If no nature or type is provided, we cancel the task. This is often the
  case when a change on Instruction has been done.
  https://gitlab.com/atreal/openads.platau/-/issues/50

* Add START & END treatment tags in log
  https://gitlab.com/atreal/openads.platau/-/issues/44

* Refactor task type filtering
  Filtering is now made in __init__, to support canceling every handler
  https://gitlab.com/atreal/openads.platau/-/issues/51

* Mapped CERFA january, september 2022 and january 2023
  Update Nomenclature
  https://gitlab.com/atreal/openads.platau/-/issues/58

* Update to CDS 9.6 & merge done
  https://gitlab.com/atreal/openads.platau/-/issues/58

* Support input PeC with document (CT)
  https://gitlab.com/atreal/openads.platau/-/issues/42

* Unified PACKAGE_VERSION for both conductors
  PACKAGE_VERSION is now build in __init__ and imported in conductors

* Avoid running tasks when not necessary
  https://gitlab.com/atreal/openads.platau/-/issues/51

* Support lettre_aux_petitionnaires stream

  + Add necessary methods to handle both `lettre_majoration` and `lettre_incompletude`
  + Refactor getting AC actor by comptency to support a `LETTRE_PETITIONNAIRE_ROLE_BY_COMPETENCY` mapping
  + Add `set_date_limite_intruction` to update temporary the date before
    adding the `lettre_majoration`
  https://gitlab.com/atreal/openads.platau/-/issues/62

* Adapt DPC/S DOC DAACT PM DT
  Dropped Identite and Terrain handling on some Demande, following VDM feedback
  https://gitlab.com/atreal/openads.platau/-/issues/64
  Ref: FM24683

* Re-activate in|completude streams
  Following fixes in openADS 5.16.
  https://gitlab.com/atreal/openads.platau/-/issues/10

* Improved 'ajout_piece' handling
  To avoid useless file upload, we first check that Dossier UID is available in payload

* Support ANNUL & PRO CT outpuit streams

  + Refactor platau.get_decisions
  + Add platau.evolution_decision
  + Complete paltau_task.handle_decision_DI

  https://gitlab.com/atreal/openads.platau/-/issues/65

* Support Retrait(openADS) / Abandon(Plat'AU)
  On Retrait Decision, endpoint `/dossiers/abandon` is called
  https://gitlab.com/atreal/openads.platau/-/issues/65

* Support output streams for DOC & DAACT

  + Attachments are out of the picture (MTE is analysing the IRL usecase)
  + Streams useless are canceled, only `creation_DI` and `depot_DI` are handled
  https://gitlab.com/atreal/openads.platau/-/issues/65


Fixes
-----
* Properly fail on null Piece fields
  If a Demande is retrieved with a null pirecs_raw fields, let the mapper handle the fail properly.
  The issue was caugth on a Publik schema update

* Stream Creation_DI state move to error not canceled
  Task should move to state `error`. This is not a nominal case and should trigger a human action if the error persists
  https://gitlab.com/atreal/openads.platau/-/issues/56

* Support event absent from TYPE_EVENEMENT
  When unkown events are retreived:
    + add a fallback on type and label mapping
    + properly log it on InputTask building


5.2.1 (2022-12-02)
==================

Fixes
-----

* provide Signataire actor's UID on Decision
  See https://gitlab.com/atreal/openads.platau/-/issues/48

* better error handling on Consultation stream
  + Exception is properly logged
  + User message corrected
  See https://gitlab.com/atreal/openads.platau/-/issues/48


5.2.0 (2022-11-03)
==================

Features
--------

* Support PeC with attachment in SC mode
  https://gitlab.com/atreal/openads.platau/-/issues/41

* Precise message on avis output task.
  When sending avis_consultation output stream, do no mention an attachment if there
  isn't.
  See [MC0043617] Libellé du tacite - Matrix42 2647

* Support actors deletion

* Support Transfert & Modficatifs Dossier type
  https://gitlab.com/atreal/openads.platau/-/issues/43

* Log actor UID if found in headers

Fixes
-----

* handle_envoi_CL on EPCI competency Dossier

* Provide a ' ' for `txAvis` when no attachment
  `tXAvis` is required value and cannot be empty


5.1.1 (2022-10-25)
==================

Fixes
-----

* Support actors Autorite Competente for EPCI
  https://gitlab.com/atreal/openads.platau/-/issues/40

* Notifications for *_epci actors not retrieved


5.1.0 (2022-10-07)
==================

Features
--------

* *platau-conductor* : allow logging in extra paths
  https://gitlab.com/atreal/openads.platau/issues/21

* Support *Qualification & Decision* for EPCI actors
  https://gitlab.com/atreal/openads.platau/issues/40

* Support *Dossier AD'AU* retrieving
  For current XP AD'AU, we had to be able to check Dossier AD'AU data.
  Method 'get_dossier_ADAU_by_uid' is now fully functionnal, and may be used
  to download ADAU archive.

Fixes
-----

* Avoid breaking process when PISTE auth fails
  Wrap the first platau.start() in try/except block to allow process to end properly.


5.0.0 (2022-09-15)
==================

Features
--------

* **Breaking change** : upgrade to *Platau v8*

* Update nomenclatures to 8.3

* Support actor email property on config load.
  When loading openads config (actors), we now support a platau_acteur_YOURROLE_email.
  It will be mapped to actor email property. Needed for envoi_CL.

* *platau-cli* : actor parameters can be synced from openADS to Plat'AU

* Support outdated nomenclature
  When outdated Nomenclature item are used,  Plat'AU rejects the request.
  We simply dropped them from the current mapping, waiting CERFA in openADS.
  https://gitlab.com/atreal/openads.platau/issues/31

* *platau-conductor* : allow running output stream for ONE task


Fixes
-----

* Ignore not mapped properties
  See MC0043357

* fix: support bad number input
  https://gitlab.com/atreal/openads.platau/issues/35

* fix: 'nomenclatures' operationId is now lowercase

* fix: specific handler to support handler in PeC
  https://gitlab.com/atreal/openads.platau/issues/38

* fx: bad merging for AlgoHash property
  https://gitlab.com/atreal/openads.platau/issues/39



4.3.0 (2022-07-14)
==================

Features
--------

* Handle redundant Piece
  https://gitlab.com/atreal/openads.platau/issues/27

* Better file name renaming.
  When receiving file from Publik, file name are sanitized and uniquified
  https://gitlab.com/atreal/openads.platau/issues/2

* Refactor generic method to get Dossier & Evenement

* Move task to 'canceled' if UIDs are missing.
  We found few Dossiers from migration with a 'modification_DI' task pending, but with
  no DA / DI in Plat'AU. These usecases should not happen in openADS (fix is on his way).
  We have no reason to try to transmit these streams : task will be canceled, warning
  will be issued.

* Handle Piece downloading timeout.
  When a timoeut is caught while downloading a Piece, current Piece is kept in the list,
  but with an attachment set to None. When calling 'get_piece_binaries', if a Piece is
  missing, a message is added on the Dossier.
  https://gitlab.com/atreal/openads.platau/issues/28

* Adapt dataset builder.
  Following completude mapping implementation, adapt the builder to provide the right
  values.

* Redundant files handling functionnal:
    * with INT tests
    * few updates in build_dataset
    * HISTORY udpated
  https://gitlab.com/atreal/openads.platau/issues/27

* Support downloading multiples files as Zip
    * rewrite content-type handling
    * changed filename extension
  https://gitlab.com/atreal/openads.platau/issues/34

* Log message when retrieving Avis with service diffenrent than guichet_unique.
  In this case, Avis creation is ignored


Fixes
-----

* Support contact with critical empty values
  When retrieving Personne Morale, some critical values for openADS where
  missing. Consequently, tasks could not be created or were failing on
  openADS on Dossier creation.
  When meeting these usecases, we provide a "N/C" value or properly handle empty
  values in the middleware.

* Make ADAU Dossier reception working again on Plat'AU v7


4.2.1 (2022/06/28)
==================

Fixes
-----

* Properly handle failing output tasks. When retrieving output tasks from openADS, a timeout or a lost
  connection could cause all the process to fail.
  Exception or empty results are now supported

* Retrieve OAUTH scope dynamically
  https://gitlab.com/atreal/openads.platau/issues/30

* Properly handle failed mapping
  https://gitlab.com/atreal/openads.platau/issues/29


4.2.0 (2022/06/14)
==================

Feature
-------

* Actor update functionnal

* Display package info at start & stop
  https://gitlab.com/atreal/openads.platau/issues/24

* Platau-cli: allow logging in output file

* Better output on resync_binary_tasks

* Platau-cli/resync_pieces move success-pending tasks to done

* Properly handle 'ajout_piece' with bad type_piece

* Allow multiple config files

* Platau-cli | add Piece resync action

Fixes
-----

* Support Avis with a nomNatureAvisRendu at None
  https://gitlab.com/atreal/openads.platau/issues/19

* Add requests timeout

* Make replay_event working following InputTask signature update

* Handle forgotten mappings in 7.5
  https://gitlab.com/atreal/openads.platau/issues/25

* Input mapping / allow name's list with null or "" values

* Input mapping / allow empty list and empty list with null on input mapping

* platau-cli/resync_pieces better error handling


4.1.0 (2022/05/16)
==================

Feature
-------

* Avis are sent with role 'guichet_unique'. Avoid double notifications on Avis and allow
  Consultation even when Service Instructeur is DDT

* in|completude_DI: move tasks to 'canceled'
  https://gitlab.com/atreal/openads.platau/-/issues/10

* Handle NATURE_PIECE not mapped : moved current task to state `canceled` if Nature
  does not exist in Plat'AU
  https://gitlab.com/atreal/openads.platau/-/issues/16

* Provide feeback to user on bad consultation
  https://gitlab.com/atreal/openads.platau/-/issues/17

* Finalize Syncplicity new filename format header

* Correctly handle inconsistent task

* platau-cli : allow resyncing events and tasks. If for any reason, output tasks with
  binary did not receive ACK, and stand as `pending`, we provide a resync action.
  When triggering, Dossier history will be check, and openADS tasks updated accordingly.

* platau-cli : added `dry-run` mode.

* More criterions on output task search
  https://gitlab.com/atreal/openads.platau/-/issues/12

Fixes
-----

* Properly handle code 429 returns

* Support no document on avis
  https://gitlab.com/atreal/openads.platau/-/issues/11

4.0.0 (2022/04/20)
==================

Feature
-------

* Plat'AU 7.5 compatibility

* Update and map new nomenclatures

* Implement correct ACK for docuement (Consultation, Avis, Prescrititon, etc)

* Improve CDS update docs.

* Interactive platau without ipdb

* Support Syncplicity new filename format header while downloading

* `service_consulte` mode : properly handle failure while retreiving Piece on Consultation


3.5.0 (2022/04/12)
==================

Feature
-------

* Implement Piece Depot on success ACK

* Drop Piece Depot to avoid false negative

* Use Garbage Collector to avoid high memory usage

* Handle 'consultation' tasks before 'qualification'

* Set 'direct' upload method by default

* Take care of false-positive 'ajout_piece' task

* Implement correct ACK by Piece

* Restrict tasks search with to 'platau' category

* Avoid multiple searches while replaying event

* Properly catch 'Too many request' error

* Allow Syncplicty to be use in sandbox env

* Properly handle '429 too many request' errors


Fix
---

* Correct bad app path when using sandbox env

* Avoid 'decison_DI' to be successfully transmitted

* Avoid KeyError on not always provided 'donnees_techniques.am_exist_agrand'

* Update task to 'canceled' on currently not supported functionality

* Correct 'spec_overrides' PISTE connector property baddly handled

* Avoid undefined 'platau_notifications' variable


3.4.1 (2022/03/11)
==================

Fix
---

* Notification logged only for last actor / MC0042300
  [tiazma]


3.4.0 (2022/02/07)
==================

Feature
-------

* Clean temporary files after use
  [tiazma]

* Support notification from app|platau (no Demande in pblk)
  [tiazma]

* Better error handling in various places
  [tiazma]

* Ignore Decision not supported by Plat'AU
  [tiazma]


Fix
---

* Fix 'nesaipas' boolean mapping to None
  [tiazma]


3.3.0 (2022/02/02)
==================

Feature
-------


* Keep safe mapping from null or not provided data / MC0041525
  [tiazma]

* Avoid logging binary data on failed task creation
  [tiazma]


Fix
---

* Do not stop on failed 'add_piece' task creation
  [tiazma]

* Produce a well-formed JSON while logging notifications
  [tiazma]


3.2.0 (2022/01/14)
==================

Feature
-------

* platau-cli : 'iplatau' action, an ipdb loaded with a Plat'AU connection
  [tiazma]

* Update get_DDT method following Plat'AU upgrade
  [tiazma]

* Test : provide an easy way to connect to Plat'AU
  [tiazma]

* Publik : support multi CT mode
  [tiazma]

* Publik : add a fallback to 111 type_piece if a bad type is provided
  [tiazma]

* Publik : support initializing openADS driver without loaddin all actors from backend
  [tiazma]

* Publik: better logging if connection to openADS failed
  [tiazma]

* platau-cli : add 'status' action
  [tiazma]

* Allow Plat'AU notification specific log
  [tiazma]


Fix
---

* Bad attachment handling in handle_decision_DI
  [tiazma]

* Increase verbosity on error
  [tiazma]

* Plat'Au > openADS : ignore Personne with no role provided - FM0041275
  [tiazma]



3.1.0 (2021/12/15)
==================

Feature
-------

* Filename from Publik are now unique, avoiding backend conflict
  (see https://gitlab.com/atreal/openads.platau/-/issues/2)
  [tiazma]

Fix
---

* Dropped useless error log on Piece notification
 (see https://gitlab.com/atreal/openads.platau/-/issues/4)
 [tiazma]


3.0.0 (2021/12/15)
==================

Feature
-------

* CT : Publik transferts
  [tiazma]


2.4.1 (2021/12/10)
==================

Feature
-------

* MC: stream Prescription fully functionnal
  [tiazma]

* Add `platau-cli`, a CLI tool to interact with Plat'AU
  [tiazma]

* CT: support `Envoi_CL` stream first version (WIP)
  [tiazma]



.. Template

.. Release and date
.. ================

.. Feature
.. -------

.. * Description 1
.. * Description 2

.. Fixes
.. -----

.. * Description 1
.. * Description 2
