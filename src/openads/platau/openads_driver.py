import logging
import mimetypes
import os
import re
import requests
import json

from base64 import b64encode
from dataclasses import dataclass

from openads.platau.base_objects import Actor, Attachment, ACTOR_ROLES
from openads.platau.mapper import _caster

log = logging.getLogger(__name__)


EXTRA_PROPERTIES = {
    "collectivite": ["sve_cgu_url"],
    "service_consulte": [],
}


class OpenADSError(RuntimeError):
    pass


@dataclass
class OpenADSDriver:
    """."""

    url: str
    auth: tuple
    mode: str = None
    actors: list = None
    check_ssl: bool = True

    # extra properties will be used to stored global config from openADS (ie. from om-parameter)
    extra_properties: dict = None

    def __init__(
        self, url: str, auth: tuple, mode: str = None, ignore_config: bool = False
    ):
        """."""
        self.url = url
        self.auth = auth
        log.info(f"openADS will be reach at {url} with user {self.auth[0]}")

        if mode in ("collectivite", "service_consulte"):
            self.mode = mode
            self.extra_properties = {key: None for key in EXTRA_PROPERTIES[self.mode]}

        try:
            requests.get(url)
        # do we have an auto-signed cert ? If True, we'll authorize it.
        except requests.exceptions.SSLError:
            self.check_ssl = False

        self.status()
        if not ignore_config:
            self.load_config()

    def status(self):
        """."""
        log.info(f"Asking for status from backend at {self.url}")
        url = f"{self.url}/status/{self.auth[0]}"
        try:
            response = requests.get(url, verify=self.check_ssl)
            log.info("openADS reached with success")
        except Exception as e:
            raise OpenADSError(f"openADS cannot be reached : {e}")

        if not response.ok:
            raise OpenADSError(
                f"openADS cannot be reached : {response.status_code} {response.reason}"
            )

        return True

    def load_config(self):
        """Load Plat'AU config from openADS."""
        log.info(f"Pulling config from openADS :: {self.url}")

        # Loading actors
        response = requests.get(
            f"{self.url}/config/platau_acteur_*", auth=self.auth, verify=self.check_ssl
        )
        if not response.ok:
            raise OpenADSError(
                f"Could not get actors UIDs from openADS : {response.status_code} {response.reason}"
            )
        self.actors = []
        actors_by_org_by_role = {}
        # emails = {}
        actor_extra_properties = []

        dummy_actor = Actor()

        # First iteration to handle actors creation
        for actor_prop in response.json():
            label = actor_prop["label"].replace("platau_acteur_", "")
            if label in ACTOR_ROLES:

                # At this point, extra properties are ignored
                if actor_prop["collectivite"] not in actors_by_org_by_role:
                    actors_by_org_by_role[actor_prop["collectivite"]] = {
                        role: None for role in ACTOR_ROLES
                    }

                actors_by_org_by_role[actor_prop["collectivite"]][label] = Actor(
                    role=label,
                    uid=actor_prop["value"],
                    organization=actor_prop["collectivite"],
                )

            else:
                actor_extra_properties.append(actor_prop)

        # Actors have been created, let's make a second iteration to handle
        # extra properties
        actors_roles_regex = re.compile("_|".join(ACTOR_ROLES) + "_")
        for extra_property in actor_extra_properties:

            # We extract property label to be mapped
            label = extra_property["label"].replace("platau_acteur_", "")
            # `label` shoud be like "guichet_unique_depot_adau"
            try:
                property_label = actors_roles_regex.split(label)[1]
                # `property_label` should be like "depot_adau"
            except IndexError:
                # Might be a bad param in openADS. Let's ignore that.
                log.warning("Ignoring malformed actor property: `%s`", label)
                continue

            # We need actor's role to properly store it

            role = label.replace("_" + property_label, "")
            # `role` shoud be like "guichet_unique"

            # XXX not a generic way to get a casted value, but at this point,
            # no other usecase than casting a boolean value
            if isinstance(getattr(dummy_actor, property_label), bool):
                extra_property["value"] = _caster(
                    {"type": "boolean"}, extra_property["value"]
                )

            try:
                setattr(
                    actors_by_org_by_role[extra_property["collectivite"]][role],
                    property_label,
                    extra_property["value"],
                )
            except (KeyError, AttributeError):
                log.warning(
                    "Ignoring extra property: `%s` on badly configured organization `%s`",
                    property_label,
                    extra_property["collectivite"],
                )

        # Let's store a flat actors batch
        for i in actors_by_org_by_role.values():
            for j in i.values():
                if j:
                    self.actors.append(j)

        log.info(f"Found actor(s) : {self.actors}")

        # Loading extra properties
        if not self.extra_properties:
            return

        for key in self.extra_properties:
            log.info(f"Loading extra property `{key}`")
            response = requests.get(
                f"{self.url}/config/{key}", auth=self.auth, verify=self.check_ssl
            )
            if not response.ok:
                log.warning(
                    "Could not get extra property `{key}`: {response.status_code} {response.reason}"
                )
                continue

            try:
                result = response.json()
            except Exception as e:
                log.warning(
                    f"Failed to extract extra property `{key}`. See traceback below."
                )
                log.warning(e, exc_info=True)

            if len(result) > 1:
                self.extra_properties[key] = [prop["value"] for prop in result]

            elif len(result) == 1:
                self.extra_properties[key] = result[0]

        log.info(f"Found extra_properties : {self.extra_properties}")

    def search_actors(
        self,
        role: str = None,
        organization: int = None,
        insee: str = None,
        uid: str = None,
    ):
        """WIP"""
        if self.actors is None:
            self.load_config()

        if role is None and organization is None and insee is None and uid is None:
            return self.actors

        target = Actor(role=role, organization=organization, uid=uid)
        actors = []
        for actor in self.actors:
            if actor.match(target):
                actors.append(actor)

        return actors

    def get_tasks(self, criterions={}):
        """Search tasks from openADS."""

        log.info(f"Retrieving tasks with criterions : {criterions}")

        url = "{}/platau/tasks".format(self.url)
        if criterions is not {}:
            url = "{}?{}".format(
                url,
                ("&").join(
                    [
                        "{}={}".format(criterion, value)
                        for criterion, value in criterions.items()
                    ]
                ),
            )

        response = requests.get(url, auth=self.auth, verify=self.check_ssl)

        if not response.ok:
            raise OpenADSError(
                f"Could not retrieve tasks from openADS : {response.status_code} {response.reason}"
            )

        json_payload = response.json()
        log.info(f"Retrieved {len(json_payload)} tasks with criterions : {criterions}")

        return json_payload

    def get_task(self, task_id):
        """Get task from openADS."""

        log.info(f"Retrieving task {task_id}")

        response = requests.get(
            "{}/platau/task/{}".format(self.url, task_id),
            auth=self.auth,
            verify=self.check_ssl,
        )

        if not response.ok:
            return

        return response.json()

    def update_task(self, task_id, state: str = None, external_uid=None):
        """Update task in openADS."""

        log.info(
            f"Updating task {task_id} with value : state -> {state}, external_uid -> {external_uid}"
        )

        if state is None and external_uid is None:
            raise ValueError("Both values cannot be None")

        payload = {}
        if state is not None:
            payload["state"] = state
        if external_uid is not None:
            payload["external_uid"] = external_uid

        response = requests.put(
            "{}/platau/task/{}".format(self.url, task_id),
            data=json.dumps(payload),
            auth=self.auth,
            verify=self.check_ssl,
        )

        if not response.ok:
            return

        return response.json()

    def get_file(self, task_id):
        """Get task from openADS."""

        log.info(f"Retrieving file from task {task_id}")

        response = requests.get(
            "{}/platau/task/{}/file".format(self.url, task_id),
            auth=self.auth,
            verify=self.check_ssl,
        )

        if not response.ok:
            return

        file_data = response.json()["files"][0]

        if not file_data["content_type"]:
            content_type = None
            guess_content_type = True
        else:
            content_type = file_data["content_type"]
            guess_content_type = False

        return Attachment(
            file_data["filename"],
            file_data["b64_content"],
            content_type=content_type,
            b64=True,
            guess_content_type=guess_content_type,
        )

    def create_task(self, typology, data, category="platau"):
        """Update task in openADS."""

        msg = f"Creating task {category}::{typology}"
        payload = {
            "category": category,
            "typology": typology,
            "json_payload": json.dumps(data),
        }

        log.info(msg)
        log.debug(f"Payload : {payload}")

        response = requests.post(
            f"{self.url}/platau/tasks",
            data=json.dumps(payload),
            auth=self.auth,
            verify=self.check_ssl,
        )
        if not response.ok:
            # We don't want huge binary content in our log
            if (
                "document_numerise" in data
                and "file_content" in data["document_numerise"]
            ):
                data["document_numerise"]["file_content"] = "BINARY CONTENT REMOVED"
                payload["json_payload"] = json.dumps(data)

            log.error(f"`create_task` failed with payload : {payload}")
            raise OpenADSError(
                f"Task creation failed. Payload response : {response.text}"
            )

        return response.json()

    def dossier_exists(self, dossier_id, response_format=None):
        """Update task in openADS."""

        log.info(f"Testing Dossier {dossier_id} existence")

        url = f"{self.url}/dossier/{dossier_id}/existe"
        if response_format is not None:
            url += f"?response_format={response_format}"

        response = requests.get(url, auth=self.auth, verify=self.check_ssl,)
        if not response.ok:
            log.error(
                f"`dossier_exists` call failed with dossier {dossier_id} : {response.text}"
            )
            raise OpenADSError(
                f"`dossier_exists` call failed with dossier {dossier_id} : {response.text}"
            )

        if response_format == "publik":
            return response.json()["data"]

        else:
            return response.json()

    def depot_possible(self, dossier_id, type_demande, response_format=None):
        """Update task in openADS."""

        log.info(f"Testing Depot {type_demande} on Dossier {dossier_id}")

        url = f"{self.url}/dossier/{dossier_id}/depot_possible"
        if response_format is not None:
            url += f"?response_format={response_format}"

        response = requests.post(
            url,
            data=json.dumps({"type_demande": type_demande}),
            auth=self.auth,
            verify=self.check_ssl,
        )
        if not response.ok:
            log.error(
                f"`depot_possible` call failed with dossier {dossier_id} and Depot {type_demande} : {response.text}"
            )
            raise OpenADSError(
                f"`depot_possible` call failed with dossier {dossier_id} and Depot {type_demande} : {response.text}"
            )

        if response_format == "publik":
            return response.json()["data"]

        else:
            return response.json()

    def create_dossier(self, data: dict) -> dict:
        """Create Dossier in openADS."""

        msg = f"Creating Dossier"

        log.info(msg)
        log.debug(f"Payload : {data}")

        response = requests.post(
            f"{self.url}/dossiers",
            data=json.dumps(data),
            auth=self.auth,
            verify=self.check_ssl,
        )

        return response.json()

    def add_pieces_from_path(self, dossier_id, pieces: list) -> dict:
        """Add Pieces in openADS."""

        log.info("Adding Pieces")

        payload = []
        # And the right payload
        for piece in pieces:
            if "content_type" not in piece:
                piece["content_type"] = mimetypes.guess_type(piece["path"])[0]

            if "filename" not in piece:
                piece["filename"] = os.path.basename(piece["path"])

            if "piece_type" not in piece:
                piece["piece_type"] = 111
                piece["custom_piece_type"] = "une description inutile"

            with open(piece["path"], "rb") as f:
                b64_content = str(b64encode(f.read()), "utf-8")

            payload.append(
                {
                    "filename": piece["filename"],
                    "content_type": piece["content_type"],
                    "b64_content": b64_content,
                    "piece_type": piece["piece_type"],
                    "custom_piece_type": piece["custom_piece_type"],
                }
            )

        response = requests.post(
            f"{self.url}/dossier/{dossier_id}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=self.check_ssl,
        )

        del payload

        return response.json()
