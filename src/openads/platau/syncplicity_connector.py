import hashlib
import json
import logging
import os
import requests
import subprocess
import uuid

from simplejson.errors import JSONDecodeError

from openads.platau.piste_connector import PISTEConnector
from openads.platau.utils import unique_filename

log = logging.getLogger(__name__)


# If filesize is greater than this value, pre-upload will be used,
# no matter user's choice. Size is in MB.
UPLOAD_METHOD_THRESHOLD = int(os.getenv("PLATAU_UPLOAD_METHOD_THRESHOLD", 5))


class SyncplicityError(Exception):
    pass


class SyncplicityUploadConnector(PISTEConnector):
    """."""

    spec_path = (
        os.path.dirname(os.path.abspath(__file__)) + "/specs/syncplicity_upload.json"
    )

    @property
    def method(self) -> None:
        if not getattr(self, "_method", None):
            return "direct"
        else:
            return self._method

    @method.setter
    def method(self, method: str) -> None:
        if method not in ("direct", "pre-upload"):
            raise ValueError("method is not one of ('direct', 'pre-upload')")
        self._method = method

    def add_file(self, path: str, file_name: str = None, uniquify: bool = True) -> dict:
        """."""

        if not file_name:
            file_name = os.path.basename(path)

        if uniquify:
            file_name = unique_filename(file_name)

        if self.method == "direct":

            file_size = os.path.getsize(path) / (1024 * 1024)  # size in MB

            if file_size < UPLOAD_METHOD_THRESHOLD:
                return self._add_small_file(path, file_name)
            else:
                return self._add_large_file(path, file_name)

        elif self.method == "pre-upload":
            return self._add_large_file(path, file_name)

        else:
            raise ValueError("method is not one of ('direct', 'pre-upload')")

    def _add_small_file(self, path: str, file_name: str) -> dict:
        """."""
        with open(path, "rb") as f:
            content = f.read()

        content_type = "application/pdf"

        url = f"https://{self.specs['host']}{self.specs['basePath']}/upload"

        log.info("REQUEST : POST {} : {}".format(url, file_name))
        response = requests.post(
            url,
            files={"fileData": (file_name, content, content_type)},
            headers={
                "Authorization": self.token["token_type"]
                + " "
                + self.token["access_token"]
            },
        )
        try:
            result = response.json()
        except JSONDecodeError:
            raise SyncplicityError(
                f"Could not upload file. {response.status_code} : {response.reason}"
            )
        log.info(f"RESPONSE : {result}")
        if result.get("ErrorCode"):
            raise SyncplicityError(
                f"Could not upload file with 'direct' method. {result}"
            )

        # Let's provide a hash to future integrity control
        result["sha512"] = hashlib.sha512(content).hexdigest()

        return result

    def _add_large_file(self, path: str, file_name: str) -> dict:
        """."""

        # TODO : move this call at Class in __init__
        # First, we get the metadata from the endpoint /pre-upload
        # Sometimes, pre-upload is broken.
        metadata = None
        for i in range(10):
            try:
                metadata = self.client.pre_upload.Pre_Upload().response().result
            except JSONDecodeError:
                continue
            break
        if metadata is None:
            raise SyncplicityError("Pre-Upload failed after multiple tries")

        # and prepare the request
        headers = {
            "Authorization": metadata.Authorization_for_upload,
            "AppKey": metadata.AppKey,
            "Content-Range": "0-*/*",
            "Accept": "application/json",
            "User-Agent": "openADS",
        }
        url = "{}/v2/mime/files?filepath={}/{}".format(
            metadata.Storage_URL, metadata.Folder_Name, file_name,
        )
        payload = {
            "filename": file_name,
            "type": "application/octet-stream",
            "transfer-encoding": "binary",
            "sessionKey": metadata.Authorization_for_upload,
            "virtualFolderId": metadata.VirtualFolderId,
        }

        # Then, we load the file
        with open(path, "rb") as f:
            data = f.read()

        payload["fileData"] = data
        # provide a sha256 hash
        data_hash = hashlib.sha256(data).hexdigest()
        payload["sha256"] = data_hash

        curl_command = (
            f"curl -k -L -X POST {url} "
            "-H 'User-Agent: openADS -H Content-Range: 0-*/*' "
            f"-H 'Authorization: {metadata.Authorization_for_upload}' "
            f"-H 'AppKey: {metadata.AppKey}' "
            "-F 'transfer-encoding=binary' -F 'type=application/octet-stream' "
            f"-F 'filename={file_name}' "
            f"-F 'fileData=@{path}' "
            f"-F 'sha256={data_hash}' "
            # f"-F 'SHA-512={data_hash}' "
            f"-F 'sessionKey={metadata.Authorization_for_upload}' "
            f"-F 'virtualFolderId={metadata.VirtualFolderId}'"
        )

        # Time for posting
        log.debug("Pre-Upload POST {} : {}".format(url, file_name))

        # TODO : at this time, no way  to succeed with requests ... 500 Internal server error
        # response = requests.post(url, files=payload, headers=headers)

        # fuck it, let's use a straight curl command
        status = subprocess.run(curl_command, shell=True, capture_output=True)
        if status.returncode != 0:
            raise SyncplicityError(f"Failed to transfer file {file_name}")

        try:
            response = json.loads(status.stdout)
        except json.decoder.JSONDecodeError:
            raise SyncplicityError(f"Failed to transfer file {file_name}")

        # Virtual_folder_id is no part of the large file upload response, let's add it
        response["VirtualFolderId"] = metadata.VirtualFolderId

        # Let's provide a hash to future integrity control
        response["sha512"] = hashlib.sha512(data).hexdigest()

        # Some ID from curl response are integer, and should be string
        for k, v in response.items():
            if isinstance(v, int):
                response[k] = str(v)

        log.debug("Response : {}".format(response))

        return response
