import os
import pkg_resources

PACKAGE_VERSION = str(pkg_resources.get_distribution(__package__)).split(" ")[-1]

# Timeout set while using straigth calls with requests lib (i.e. not wrapped by bravado)
# Default to 30 seconds
REQUESTS_TIMEOUT = os.getenv("requests_timeout", 30)

# Timeout set while using bravado (i.e. wrapped by bravado)
# Default to 30 seconds
PISTE_TIMEOUT = os.getenv("piste_timeout", 30)
