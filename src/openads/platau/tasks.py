import logging

from dataclasses import dataclass

from openads.platau.base_objects import Actor
from openads.platau.mapping import NOMENCLATURE, PLATAU_MAPPING
from openads.platau.openads_driver import OpenADSDriver, OpenADSError, Attachment
from openads.platau.openads_mapper import (
    AvisMapper,
    ConsultationMapper,
    ConsultationPieceMapper,
    DossierADAUMapper,
    DossierADAUPieceMapper,
    MessageMapper,
    PecMapper,
)
from openads.platau.piste_connector import PISTEConnector
from openads.platau.platau_connector import PlatAUEvent, PlatAUError

log = logging.getLogger(__name__)

# Which DI we support, by dossier_instruction_type_code
SUPPORTED_DI = [
    "P",
    "T",
    "M",
    "DOC",
    "DAACT",
    "ANNUL",
    "PRO",
]

# Given a DI identified by dossier_instruction_type_code, wich operations do we support
FILTERED_OPERATION_BY_DI = {
    "DOC": ["creation_DI", "depot_DI"],
    "DAACT": ["creation_DI", "depot_DI"],
    "ANNUL": ["decision_DI", "envoi_CL"],
    "PRO": ["decision_DI", "envoi_CL"],
}


# Determine calling actor role, needed for authentification
ROLE_BY_TASK = {
    # collectivite
    "creation_DA": "guichet_unique",
    "creation_DI": "guichet_unique",
    "modification_DA": "guichet_unique",
    "modification_DI": "guichet_unique",
    "ajout_piece": "guichet_unique",
    "suppression_piece": "guichet_unique",
    "modification_piece": "guichet_unique",
    "depot_DI": "guichet_unique",
    "qualification_DI": "guichet_unique",
    "creation_consultation": "guichet_unique",
    "lettre_majoration": "service_instructeur",
    "lettre_incompletude": "service_instructeur",
    "completude_DI": "service_instructeur",
    "incompletude_DI": "service_instructeur",
    "decision_DI": "autorite_competente",
    "envoi_CL": "autorite_competente",
    # service_consulte
    "pec_metier_consultation": "service_consulte",
    "avis_consultation": "service_consulte",
    "prescription": "service_consulte",
}

# Determine responsible actor role
INSTRUCTION_ROLE_BY_COMPETENCY = {
    "COM": "service_instructeur",
    "ETAT": "service_instructeur_ddt",
    "ETATMAIRE": "service_instructeur_ddt",
    "EPCI": "service_instructeur_epci",
    # Specific Marseille + GOU
    "MEP": "service_instructeur_epci",
}

# Determine responsible actor role
DECISION_ROLE_BY_COMPETENCY = {
    "COM": "autorite_competente",
    "ETATMAIRE": "autorite_competente",
    "EPCI": "autorite_competente_epci",
    # Specific Marseille + GOU
    "MEP": "autorite_competente_epci",
}

# Determine responsible actor role
LETTRE_PETITIONNAIRE_ROLE_BY_COMPETENCY = {
    "COM": "autorite_competente",
    "ETATMAIRE": "autorite_competente",
    "EPCI": "autorite_competente_epci",
    # Specific Marseille + GOU
    "MEP": "autorite_competente_epci",
}

TASKS_WITH_ATTACHMENT = [
    "ajout_piece",
    "suppression_piece",
    "modification_piece",
    "creation_consultation",
    "lettre_majoration",
    "lettre_incompletude",
    "decision_DI",
    "pec_metier_consultation",
    "avis_consultation",
    "prescription",
]

# Retrait in openADS
ABANDON_AVIS_DECISION = "30"

# Define specific avis_decision values that should be handled as Evolution Decision
EVOLUTION_AVIS_DECISION = [
    "24",  # Accord prorogation
    "39",  # Accord prorogation Tacite
    "2",  # Annulation
    "23",  # Annulation par tribunal
]


class Task(object):
    """."""

    openads: OpenADSDriver
    connector: PISTEConnector = None
    connector_config: dict = None

    def __init__(
        self,
        openads: OpenADSDriver,
        connector: PISTEConnector = None,
        connector_config: dict = None,
        **args,
    ):
        """."""

        self.openads = openads
        if (connector is None and connector_config is None) or (
            connector is not None and connector_config is not None
        ):
            raise ValueError("You should provide one value")
        if connector:
            self.connector = connector
        else:
            self.connector_config = connector_config

    def run(self):
        """."""
        if self.connector is None:
            log.debug(self.connector_config)
            Connector = self.connector_config["klass"]
            self.connector = Connector(**self.connector_config)


@dataclass()
class OutputTask(Task):
    """."""

    task_id: str
    typology: str = None
    di_type_code: str = None
    payload: dict = None
    state: str = "new"
    message: str = None
    attachment: Attachment = None
    platau_uid: str = None

    def __init__(self, openads: OpenADSDriver, connector: PISTEConnector, task_id: str):
        """Task builder.
        First we should retrieve task full payload, file if necessary.
        """
        super().__init__(openads, connector)

        self.task_id = task_id
        log.info(f"Wrapping task : {task_id}")

        self.state = "pending"
        if not self.openads.update_task(self.task_id, self.state):
            log.error(f"Unable to update task {self.task_id} with state {self.state}")
            return

        self.payload = self.openads.get_task(self.task_id)
        if self.payload is None:
            self.state = "error"
            log.error(f"Unable to retrieve payload for task {self.task_id}")
            if not self.openads.update_task(self.task_id, self.state):
                log.error(
                    f"Unable to update task {self.task_id} with state {self.state}"
                )
            return

        # `task` & `type` are a mandatory in a task
        try:
            self.typology = self.payload["task"]["type"]
        except (KeyError, TypeError):
            self.state = "error"
            log.error(
                f"Unable to retrieve mandatory properties in payload for task {self.task_id}"
            )
            if not self.openads.update_task(self.task_id, self.state):
                log.error(
                    f"Unable to update task {self.task_id} with state {self.state}"
                )
            return

        # DI type code will be unecessary for edge cases, let's get it if it exists
        try:
            self.di_type_code = self.payload["dossier"]["dossier_instruction_type_code"]
        except KeyError:
            pass

        # Do we support this kind of task ? If no, task should be canceled before going
        # further

        # CT : type DI currently not supported
        if self.di_type_code and self.di_type_code not in SUPPORTED_DI:
            log.warning(
                f"This type of Dossier is currently not implemented. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"

        # CT : only an operations subset is supported / necessary
        elif (
            self.di_type_code
            and self.di_type_code in FILTERED_OPERATION_BY_DI
            and self.typology not in FILTERED_OPERATION_BY_DI[self.di_type_code]
        ):
            log.info(
                f"Task typology `{self.typology}` is useless for current DI type. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"

        # CT : `decision_DI` and `envoi_CL` on Dossier with `ETAT` competency should be
        # properly canceled. Decison is handled by DDT.
        elif (
            self.typology in ("decision_DI", "envoi_CL")
            and self.payload["dossier"]["autorite_competente_code"] == "ETAT"
        ):
            log.info(
                f"Decision on DOssier with competency ETAT is not our duty. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"

        if self.state == "canceled":
            if not self.openads.update_task(self.task_id, self.state):
                log.error(
                    f"Unable to update task {self.task_id} with state {self.state}"
                )
            return

        # If the Decision is tacite, self.attachment will be None.
        # We'll handle the case later.
        # XXX should that be in run() ?
        if self.typology in TASKS_WITH_ATTACHMENT:
            try:
                self.attachment = self.openads.get_file(self.task_id)
            except Exception as e:
                self.state = "error"
                log.error(f"Unable to download file for task {task_id} : {e}")
                if not self.openads.update_task(self.task_id, self.state):
                    log.error(
                        f"Unable to update task {self.task_id} with state {self.state}"
                    )
                return

            if self.attachment is None:
                log.info(
                    f"Task {self.task_id} of type {self.typology} did not provide any attachment"
                )

    def run(self):
        """XXX few things might be prepared in the __init__ (setting actor, etc)"""
        # XXX - handle state == 'error' in __init__
        if self.state == "error":
            if not self.openads.update_task(self.task_id, self.state, self.platau_uid):
                raise OpenADSError("Could not update task state on openADS")
            return

        previous_state = self.state
        try:
            log.info(f"Running task `{self.typology}` : {self.task_id}")
            handler = getattr(self, f"handle_{self.typology}", None)
            if handler is None:
                raise NotImplementedError(f"handle_{self.typology} not implemented.")

            # We need to set actor in payload and in connector :
            #  - in connector, to authenticate the request
            #  - in payload, for business matter
            if self.typology not in ("creation_DA", "modification_DA"):
                organization = self.payload["dossier"]["om_collectivite"]
            else:
                # We do not have `dossier` key in payload at first
                organization = self.payload["dossier_autorisation"]["om_collectivite"]

            actor = self.openads.search_actors(
                role=ROLE_BY_TASK[self.typology], organization=organization,
            )
            if len(actor) > 1:
                raise OpenADSError(
                    f"Found multiple actors for role `{ROLE_BY_TASK[self.typology]}` within same organization"
                )
            elif len(actor) == 0:
                raise OpenADSError(
                    f"No actor found for role `{ROLE_BY_TASK[self.typology]}`"
                )
            self.connector.start(actor[0].uid)
            self.payload["task"]["acteur"] = actor[0].uid
            self.connector.calling_actor = actor[0].uid

            # and finally, handle the task
            handler()

        except Exception as e:
            log.error(e, exc_info=True)
            self.state = "error"

        if previous_state != self.state:
            if not self.openads.update_task(self.task_id, self.state, self.platau_uid):
                raise OpenADSError("Could not update task state on openADS")
        elif self.platau_uid is not None:
            if not self.openads.update_task(self.task_id, self.state, self.platau_uid):
                raise OpenADSError("Could not update task state on openADS")

    # ##################################################################################
    #
    # Collectivite OUTPUT tasks
    #
    # ##################################################################################
    def handle_creation_DA(self):
        """XXX - to be reviewed"""

        self.platau_uid = self.connector.create_projet(self.payload)[0]
        self.state = "done"

    def handle_creation_DI(self):
        """XXX - to be reviewed"""
        # If  we don't have DA external UID, we can't create a DI
        if self.payload["external_uids"]["dossier_autorisation"] == "":
            log.warn(
                "Failed to find `dossier_autorisation` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            # task should move to state `error`. This is not a nominal case and should
            # trigger a human action if the error persists
            self.state = "error"
            return

        if self.di_type_code in ("DOC", "DAACT"):

            self.payload["service_instructeur"] = self.openads.search_actors(
                role="service_instructeur",
                organization=self.payload["dossier"]["om_collectivite"],
            )[0].uid

            if self.di_type_code == "DOC":
                self.platau_uid = self.connector.add_DOC(self.payload, raw=False)[0]

            elif self.di_type_code == "DAACT":

                # AC must be provided in payload. We use the one from DECISION mapping
                autorite_competente = self._get_ac_actor_by_competency(
                    DECISION_ROLE_BY_COMPETENCY
                )
                self.payload["autorite_competente"] = autorite_competente.uid
                self.platau_uid = self.connector.add_DAACT(self.payload, raw=False)[0]

        else:
            self.platau_uid = self.connector.create_dossier(self.payload)[0]

        self.state = "done"

    def handle_modification_DA(self):
        """XXX - no endpoint at this time to update DA"""
        log.warning("Updating Projet is not available at this time.")
        # self.platau_uid = self.connector.update_projet(self.payload)[0]
        # XXX use state `canceled`
        self.state = "canceled"

    def handle_modification_DI(self):
        """."""
        # If  we don't have DI external UID, we can't create a DI
        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload" "Task %s will be canceled",
                self.task_id,
            )
            self.state = "canceled"

        self.platau_uid = self.connector.update_dossier(self.payload)[0]
        self.state = "done"

    def handle_ajout_piece(self):
        """XXX - to be reviewed"""

        # Some consistence check first
        # If  we don't have DI external UID, we can't add a Piece
        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return
        try:
            # Handle `document_numerise_nature` not mapped (ex :
            nature = self.payload["document_numerise"]["document_numerise_nature"]
            PLATAU_MAPPING["NATURE_PIECE"][nature]
        except KeyError:
            log.warning(
                f"Piece nature `{nature}` does not exist in Plat'AU. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return
        try:
            type_code = self.payload["document_numerise"]["document_numerise_type_code"]
            PLATAU_MAPPING["TYPE_PIECE"][type_code]
        except KeyError:
            log.warning(
                f"Piece type `{type_code}` does not exist in Plat'AU. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return

        self.payload["document_numerise"]["local_file_path"] = self.attachment.path
        self.payload["document_numerise"]["file_name"] = self.attachment.name

        self.platau_uid = self.connector.create_piece(self.payload)[0]
        # Only positive binary_ack will move task to 'done' state
        self.state = "pending"

    def handle_modification_piece(self):
        """XXX - to be reviewed"""

        # Some consistence check first
        # If  we don't have DI external UID, we can't add a Piece
        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return

        # Some consistence then check
        # If  we don't have DI external UID, we can't add a Piece
        if self.payload["external_uids"]["piece"] == "":
            log.warn(
                "Failed to find `piece` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return

        try:
            # On an Update we should always have  Modificative : 3 as document nature
            self.payload["document_numerise"]["document_numerise_nature"] = "3"
            # Next 2 lines are usless at time of writing but are done to maintaint data coherence in payload
            self.payload["document_numerise"]["document_numerise_nature_code"] = "MODIF"
            self.payload["document_numerise"][
                "document_numerise_nature_libelle"
            ] = "Modificative"

            type_code = self.payload["document_numerise"]["document_numerise_type_code"]
            PLATAU_MAPPING["TYPE_PIECE"][type_code]
        except KeyError:
            log.warning(
                f"Piece type `{type_code}` does not exist in Plat'AU. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return

        self.payload["document_numerise"]["local_file_path"] = self.attachment.path
        self.payload["document_numerise"]["file_name"] = self.attachment.name

        self.platau_uid = self.connector.update_piece(self.payload)[0]
        # Only positive binary_ack will move task to 'done' state
        self.state = "pending"

    def handle_suppression_piece(self):
        """XXX - to be reviewed"""

        # Some consistence check first
        # If  we don't have DI external UID, we can't delete a Piece

        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return

        try:
            # Handle `document_numerise_nature` not mapped (ex :
            nature = self.payload["document_numerise"]["document_numerise_nature"]
            PLATAU_MAPPING["NATURE_PIECE"][nature]
        except KeyError:
            log.warning(
                f"Piece nature `{nature}` does not exist in Plat'AU. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return
        try:

            type_code = self.payload["document_numerise"]["document_numerise_type_code"]
            PLATAU_MAPPING["TYPE_PIECE"][type_code]
        except KeyError:
            log.warning(
                f"Piece type `{type_code}` does not exist in Plat'AU. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return

        self.platau_uid = self.connector.delete_piece(self.payload)[0]

        # Only positive binary_ack will move task to 'done' state
        self.state = "pending"

    def handle_depot_DI(self):
        """XXX - to be implemented"""
        # if self.di_type_code in ("DOC", "DAACT"):

        # self.payload["service_instructeur"] = self.openads.search_actors(
        #     role="service_instructeur",
        #     organization=self.payload["dossier"]["om_collectivite"],
        # )[0].uid

        if self.di_type_code == "DOC":
            result = self.connector.depot_DOC(self.payload)[0]

        elif self.di_type_code == "DAACT":
            # AC must be provided in payload. We use the one from DECISION mapping
            autorite_competente = self._get_ac_actor_by_competency(
                DECISION_ROLE_BY_COMPETENCY
            )
            self.payload["autorite_competente"] = autorite_competente.uid
            result = self.connector.depot_DAACT(self.payload)[0]

        else:
            result = self.connector.depot_dossier(self.payload)[0]

        if result.erreurs:
            log.error(f"Failed to complete Depot with error: {result.erreurs}")
            self.state = "error"
        else:
            self.state = "done"

    def handle_qualification_DI(self):
        """Define and apply the competency.
        Given the autorite_competente, we need to override payload acteur set in run method.
        """
        try:
            role = INSTRUCTION_ROLE_BY_COMPETENCY[
                self.payload["dossier"]["autorite_competente_code"]
            ]
        except KeyError:
            log.error(
                "Code %s cannot be mapped to a specific role for Qualification",
                self.payload["dossier"]["autorite_competente_code"],
            )
            self.state = "error"
            return

        organization = self.payload["dossier"]["om_collectivite"]
        responsible_actor = self.openads.search_actors(
            role=role, organization=organization,
        )
        if len(responsible_actor) != 1:
            raise OpenADSError(
                f"Found zero or multiple actor(s) for role `{role}` within same organization"
            )
        self.payload["task"]["acteur"] = responsible_actor[0].uid

        result = self.connector.qualification_dossier(self.payload)
        self.state = "done"

    def handle_creation_consultation(self):
        """."""
        # If  we don't have  Dossier external UID, we can't create a DI
        if self.payload["external_uids"]["dossier"] == "":
            raise OpenADSError("Missing some external_uids")

        if self.attachment is not None:
            self.payload["consultation"]["local_file_path"] = self.attachment.path
            self.payload["consultation"]["file_name"] = self.attachment.name

        # Let's avoid some user misconfiguration and cancel the task if any
        if self.payload["service"]["service_type"] != "platau":
            log.warning(
                f"Service `{self.payload['service']['abrege']}` is not a `platau` service type"
            )
            self.state = "canceled"
            return

        try:
            result = self.connector.add_consultation(self.payload, raw=False)
        except Exception as e:
            log.warning(e, exc_info=True)
            dossier_uid = self.payload["external_uids"]["dossier"]
            consultation_id = self.payload["consultation"]["consultation"]
            message_type = "Consultation via Plat'AU impossible"
            content = (
                "Échec lors du transfert vers Plat'AU.\n"
                "Ce problème peut-être ponctuel, ou lié à une mauvaise configuration "
                f"du service associé à la consultation {consultation_id} "
                "(ex : identifiant acteur Plat'AU inconnu, etc)."
                "\nSi le problème persiste, contacter votre administrateur."
            )
            mapper = MessageMapper(dossier_uid, type=message_type, content=content)
            mapped_message = mapper.render()
            self.openads.create_task("create_message", mapped_message)
            self.state = "error"
            return

        self.platau_uid = result[0]
        # Only positive binary_ack will move task to 'done' state
        if self.attachment is not None:
            self.state = "pending"
        else:
            self.state = "done"

    def _handle_lettre_petitionnaire(self):
        """."""
        #  Now, we have to overridde current actor by the right `autorite_competente`actor
        actor = self._get_ac_actor_by_competency(
            LETTRE_PETITIONNAIRE_ROLE_BY_COMPETENCY
        )
        # Signataire is not mandatory on Decision, but it is for envoi_CL
        self.payload["task"]["signataire"] = actor.uid

        if self.attachment is None:
            log.error("")
            self.state = "error"
            return
        else:
            self.payload["instruction"]["local_file_path"] = self.attachment.path
            self.payload["instruction"]["file_name"] = self.attachment.name

        result = self.connector.add_lettre_petitionnaire(self.payload, raw=False)
        self.platau_uid = result[0]
        self.state = "pending"

    def handle_lettre_majoration(self):
        """."""
        # If  we don't have  Dossier external UID, we can't create a DI
        if self.payload["external_uids"]["dossier"] == "":
            raise OpenADSError("Missing some external_uids")

        # First, we update the date_limite_instruction
        # XXX  this sould be handled with a dedicated task like `udpate_date_limite_instruction`
        try:
            self.connector.set_date_limite_instruction(self.payload)
        except Exception as e:
            log.warning(
                "Failed to update date limite instruction. "
                "Process will continue further with `lettre_aux_petitionnaires` handling "
                f"Error details: {e}"
            )
            # For now, we just log and contine

        # Then we handle the letter
        return self._handle_lettre_petitionnaire()

    def handle_lettre_incompletude(self):
        """."""
        # If  we don't have  Dossier external UID, we can't create a DI
        if self.payload["external_uids"]["dossier"] == "":
            raise OpenADSError("Missing some external_uids")

        return self._handle_lettre_petitionnaire()

    def handle_completude_DI(self):
        """."""
        # Some consistence check first
        # If  we don't have DI external UID, we can't process with Completude
        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return

        result = self.connector.set_completude(self.payload)
        self.state = "done"

    def handle_incompletude_DI(self):
        """."""
        # Some consistence check first
        # If  we don't have DI external UID, we can't process with Incompletude
        if self.payload["external_uids"]["dossier"] == "":
            log.warn(
                "Failed to find `dossier` UID in payload. "
                "Task %s state will be set `error`",
                self.task_id,
            )
            self.state = "error"
            return

        result = self.connector.set_completude(self.payload)
        self.state = "done"

    def _get_ac_actor_by_competency(self, mapping: dict) -> Actor:
        """."""
        try:
            role = mapping[self.payload["dossier"]["autorite_competente_code"]]
        except KeyError:
            log.error(
                "Code %s cannot be mapped to a specific role",
                self.payload["dossier"]["autorite_competente_code"],
            )
            self.state = "error"
            return
        organization = self.payload["dossier"]["om_collectivite"]
        result = self.openads.search_actors(role=role, organization=organization,)
        if len(result) > 1:
            raise OpenADSError(
                f"Found multiple actors for role `{role}` within same organization"
            )
        elif len(result) == 0:
            raise OpenADSError(f"No actor found for role `{role}`")

        return result[0]

    # def _set_actor_by_competency(self):
    #     """."""
    #     try:
    #         role = DECISION_ROLE_BY_COMPETENCY[
    #             self.payload["dossier"]["autorite_competente_code"]
    #         ]
    #     except KeyError:
    #         log.error(
    #             "Code %s cannot be mapped to a specific role",
    #             self.payload["dossier"]["autorite_competente_code"],
    #         )
    #         self.state = "error"
    #         return
    #     organization = self.payload["dossier"]["om_collectivite"]
    #     result = self.openads.search_actors(role=role, organization=organization,)
    #     if len(result) > 1:
    #         raise OpenADSError(
    #             f"Found multiple actors for role `{role}` within same organization"
    #         )
    #     elif len(result) == 0:
    #         raise OpenADSError(f"No actor found for role `{role}`")
    #     actor = result[0]
    #     self.connector.start(credentials_key=actor.uid)
    #     self.connector.calling_actor = actor.uid
    #     self.payload["task"]["acteur"] = actor.uid

    #     return True

    def handle_decision_DI(self):
        """."""
        try:
            # Handle NATURE_DECISION not implemented (ex: "annulation, retrait")
            avis_decision = self.payload["dossier"]["avis_decision"]
            PLATAU_MAPPING["NATURE_DECISION_URBA"][avis_decision]
        except KeyError:

            # Retrait / Abandon from Petitionnaire
            if avis_decision == ABANDON_AVIS_DECISION:
                # Abandon should be called by guichet_unique
                guichet_unique = self.openads.search_actors(
                    role="guichet_unique",
                    organization=self.payload["dossier"]["om_collectivite"],
                )[0]
                self.payload["task"]["acteur"] = guichet_unique.uid
                # Actor has changed, we should restart the connector
                self.connector.start(credentials_key=guichet_unique.uid)
                self.connector.calling_actor = guichet_unique.uid

                result = self.connector.abandon(self.payload)[0]
                if result.erreurs:
                    log.error(
                        f"Failed to complete Abandon with error: {result.erreurs}"
                    )
                    self.state = "error"
                else:
                    self.state = "done"
                return

            else:
                log.warning(f"Decision {avis_decision} currently not implemented.")
                self.state = "canceled"
                return

        # self._set_actor_by_competency()
        # We may have to overridde current actor by the right `autorite_competente`
        actor = self._get_ac_actor_by_competency(DECISION_ROLE_BY_COMPETENCY)
        self.payload["task"]["acteur"] = actor.uid
        # Signataire is not mandatory on Decision, but it is for envoi_CL
        self.payload["task"]["signataire"] = self.payload["task"]["acteur"]

        # Let's start the connector with the overrided atcoir if needed
        if actor.uid != self.connector.calling_actor:
            self.connector.start(credentials_key=actor.uid)
            self.connector.calling_actor = actor.uid

        # Let's add attachment to the payload
        if self.attachment is not None:
            self.payload["instruction"]["local_file_path"] = self.attachment.path
            self.payload["instruction"]["file_name"] = self.attachment.name

        # Let's handle the Evolution Decision cases
        if avis_decision in EVOLUTION_AVIS_DECISION:
            self.platau_uid = self.connector.evolution_decision(
                self.payload, raw=False
            )[0]

        # General case
        else:
            self.platau_uid = self.connector.decision(self.payload, raw=False)[0]

        # in platau.decision or platau.evolution_decision, in case of Tacite Decision,
        # even if an attachment is present, we dont't upload it.
        if self.attachment is not None and self.payload["instruction"]["tacite"] == "f":
            self.state = "pending"
        else:
            self.state = "done"

    def handle_envoi_CL(self):
        """WIP"""
        # self._set_actor_by_competency()
        # We may have to overridde current actor by the right `autorite_competente`
        actor = self._get_ac_actor_by_competency(DECISION_ROLE_BY_COMPETENCY)
        self.payload["task"]["acteur"] = actor.uid
        # Signataire is not mandatory on Decision, but it is for envoi_CL
        self.payload["task"]["signataire"] = self.payload["task"]["acteur"]

        # Let's start the connector with the overided atcoir if needed
        if actor.uid != self.connector.calling_actor:
            self.connector.start(credentials_key=actor.uid)
            self.connector.calling_actor = actor.uid

        # # Actor is tthe Signataire
        # self.payload["task"]["signataire"] = self.payload["task"]["acteur"]

        result = self.connector.envoi_CL(self.payload)

        self.state = "done"

    # ##################################################################################
    #
    # Service Consulte OUTPUT tasks
    #
    # ##################################################################################
    def handle_pec_metier_consultation(self):
        """."""
        # If we don't have Dossier UID or Consultation UID, we can't do nothing
        if (
            self.payload["external_uids"]["dossier"] == ""
            or self.payload["external_uids"]["dossier_consultation"] == ""
        ):
            raise OpenADSError("Missing some external_uids")

        # Let's add attachment to the payload
        if self.attachment is not None:
            self.payload["instruction"]["local_file_path"] = self.attachment.path
            self.payload["instruction"]["file_name"] = self.attachment.name

        result = self.connector.add_pec_consultation(self.payload, raw=False)

        # Only positive binary_ack will move task to 'done' state
        if self.attachment is not None:
            self.state = "pending"
        else:
            self.state = "done"

        self.platau_uid = result[0]["pec_uid"]

    def handle_avis_consultation(self):
        """."""
        # If we don't have Dossier UID or Consultation UID, we can't do nothing
        if (
            self.payload["external_uids"]["dossier"] == ""
            or self.payload["external_uids"]["dossier_consultation"] == ""
        ):
            raise OpenADSError("Missing some external_uids")

        if (
            "avis_decision_type" not in self.payload["avis_decision"]
            or "avis_decision_nature" not in self.payload["avis_decision"]
        ):
            log.warning(
                f"This Avis does not provide `avis_decision_type` nor `avis_decision_nature`. "
                f"Task {self.task_id} will be canceled"
            )
            self.state = "canceled"
            return

        # There's a chance we didn't retrieve any attachment
        if self.attachment is not None:
            self.payload["instruction"]["local_file_path"] = self.attachment.path
            self.payload["instruction"]["file_name"] = self.attachment.name
        else:
            # By default, txAvis is set to "Voir document joint" by openADS
            # If there is no attachment, we dont' have to refer to a non existant doc.
            self.payload["avis_decision"]["txAvis"] = " "

        result = self.connector.add_avis_consultation(self.payload, raw=False)

        if self.attachment is not None:
            # Only positive binary_ack will move task to 'done' state
            self.state = "pending"
        else:
            # no attachment, no need to wait.
            self.state = "done"

        self.platau_uid = result[0]["avis_uid"]

    def handle_prescription(self):
        """."""
        # If we don't have Dossier UID or Consultation UID, we can't do nothing
        if (
            self.payload["external_uids"]["dossier"] == ""
            or self.payload["external_uids"]["dossier_consultation"] == ""
        ):
            raise OpenADSError("Missing some external_uids")

        # There's a chance we didn't retrieve any attachment
        if self.attachment is not None:
            self.payload["instruction"]["local_file_path"] = self.attachment.path
            self.payload["instruction"]["file_name"] = self.attachment.name

        result = self.connector.add_prescription(self.payload, raw=False)

        if self.attachment is not None:
            # Only positive binary_ack will move task to 'done' state
            self.state = "pending"
        else:
            # no attachment, no need to wait.
            self.state = "done"

        self.platau_uid = result[0]["prescription_uid"]


@dataclass
class InputTask(Task):
    """."""

    typology: str
    actor: Actor
    event: object
    state: str = None

    def __init__(
        self,
        event: PlatAUEvent,
        actor: Actor,
        openads: OpenADSDriver,
        connector: PISTEConnector = None,
        connector_config: dict = None,
    ):
        """Task builder.
        First we should retrieve task full payload, file if necessary.
        """
        super().__init__(openads, connector, connector_config)
        self.actor = actor
        self.event = event

        # 63,"Occurrence de versement d'un dossier AD'AU",03/03/2020
        # 68,"Occurrence d'une transcodification d'un dossier ADAU",17/08/2020
        if event.type_id == 68:
            self.typology = "dossier_adau"

        # print("Working on task : {}".format(task_id))
        # 57,"Notification de succès de versement de l'ensemble des pièces sur le dossier",26/02/2020
        # 58,"Notification d’échec de versement de l'ensemble des pièces sur le dossier",27/02/2020
        # 59,"Notification de succès de versement de l'ensemble des éléments sur le dossier",28/02/2020
        # 60,"Notification d’échec de versement de l'ensemble des éléments sur le dossier",29/02/2020
        elif event.type_id in (57, 58, 59, 60, 7):
            # we need to update the matching output task
            self.typology = "multi_binary_ack"

        # 82,"Notification de succès de versement d’une pièce",04/06/2021
        # 83,"Notification d’échec de versement d’une pièce",04/06/2021
        # 84,"Notification de succès de versement d’un document",04/06/2021
        # 85,"Notification d’échec de versement d’un document",04/06/2021
        elif event.type_id in (82, 83, 84, 85):
            # we need to update the matching output task
            self.typology = "binary_ack"

        # 19,"Occurrence d’une consultation",19/01/2020
        elif event.type_id == 19:
            self.typology = "consultation"

        # 20,"Occurrence d’une prise en compte métier",20/01/2020
        elif event.type_id == 20:
            self.typology = "pec_metier"

        # 21,"Occurrence d’un avis avec ou sans prescription",21/01/2020
        elif event.type_id == 21:
            self.typology = "avis"

        # 8,"Occurrence d’une déclaration de complétude d’un dossier",08/01/2020
        # 78,"Occurrence d’une déclaration d'incomplétude d’un dossier",19/04/2021
        elif event.type_id in (8, 78):
            self.typology = "completude"

            if actor.role != "service_consulte":
                log.info(
                    "Completude is not a subscribed event for actor "
                    "with role other than `service_consulte`"
                )
                raise NotImplementedError

        # Catch event type currently not handled
        else:
            msg = f"Unknown event type : {event.type_id}"
            try:
                msg += f"/ {NOMENCLATURE['TYPE_EVENEMENT'][event.type_id]}"
            except KeyError:
                # event.type_id does not exist in NOMENCLATURE['TYPE_EVENEMENT']
                msg += "/ unknow event id"
            log.warning(msg)
            raise NotImplementedError(msg)

    def run(self):
        """."""
        super().run()

        self.connector.calling_actor = self.actor.uid
        self.connector.start(self.actor.uid)

        handler = getattr(self, f"handle_{self.typology}", None)
        if handler is None:
            raise NotImplementedError

        log.info(f"handle_{self.typology} for actor {self.actor.uid} : {self.event}")
        try:
            handler()
        except Exception as e:
            log.error(e, exc_info=True)
            self.state = "error"

    def handle_binary_ack(self):
        """."""
        # XXX : here we only log stuff. What should we do ?
        # binary ack from a Decision
        criterions = dict(
            category="platau", state="pending", external_uid=self.event.target_object,
        )

        # 82,"Notification de succès de versement d’une pièce",04/06/2021
        if self.event.type_id == 82:
            # At this point, we cannot guess task's typology from the event.
            criterions["typology"] = ["ajout_piece", "modification_piece"]
            criterions["external_uid_type"] = "piece"
            log.info(
                f"Dossier {self.event.dossier_uid} : "
                f"Plat'AU successfully received Piece {self.event.target_object}"
            )

            # If Piece is 'Versée' and the Dossier is 'Déposé',
            # We are in one of two possible states :
            #  - The Piece is Complementaire
            #  - The Piece is à requested piece
            #  The Dossier already being 'Déposée' no action will change the state of the Piece to 'Déposée'
            #  So we need to force it.
            #  - if it is necessary, we call the Depot endpoint
            try:
                piece = self.connector.get_pieces(
                    self.event.dossier_uid, pieces_uids=[self.event.target_object]
                )[0]
            except Exception:
                log.warning(
                    f"Dossier {self.event.dossier_uid} : something is wrong with Piece {self.event.target_object}"
                )
                self.state = "error"

            else:
                # Nomenclature(idNom=1,libNom="Versée",dtMiseEnPlace=04/12/2019)
                if piece.nomEtatPiece.idNom == 1:
                    dossier = self.connector.get_dossier_by_uid(self.event.dossier_uid)
                    # If the DOssier is "Deposé", we need to do thje==e Piece dépôt
                    if dossier.dtDepot:
                        task = {
                            "piece_version": piece.noVersion,
                            "document_numerise": {
                                "date_creation": piece.dtProduction.strftime("%d/%m/%Y")
                            },
                            "external_uids": {
                                "document_numerise": self.event.target_object,
                                "dossier": self.event.dossier_uid,
                            },
                        }
                        self.connector.depot_pieces(task)

                self.state = "done"

        # 83,"Notification d’échec de versement d’une pièce",04/06/2021
        elif self.event.type_id == 83:
            # At this point, we cannot guess task's typology from the event.
            criterions["typology"] = ["ajout_piece", "modification_piece"]
            criterions["external_uid_type"] = "piece"
            log.error(
                f"Dossier {self.event.dossier_uid} : "
                f"Plat'AU failed to received Piece {self.event.target_object}"
            )
            self.state = "error"

        # 84,"Notification de succès de versement d’un document",04/06/2021
        elif self.event.type_id == 84:
            # Nothing to do here, ACK for Document are multi-handled"
            log.info(
                f"Dossier {self.event.dossier_uid} : "
                f"Plat'AU successfully received Document {self.event.target_object}"
            )
            self.state = "done"
            return

        # 85,"Notification d’échec de versement d’un document",04/06/2021
        elif self.event.type_id == 85:
            # Nothing to do here, ACK for Document are multi-handled"
            log.error(
                f"Dossier {self.event.dossier_uid} : "
                f"Plat'AU failed to received Document {self.event.target_object}"
            )
            self.state = "error"
            return

        # In some case, we cannot guess the right task typology based on event.
        # criterions["typology"] should the be a liste, we'll try every occurence until
        # a result is found.
        if isinstance(criterions["typology"], list):
            typologies = criterions.pop("typology")
            for typology in typologies:
                criterions["typology"] = typology
                tasks = self.openads.get_tasks(criterions)
                if tasks != []:
                    break

        else:
            tasks = self.openads.get_tasks(criterions)

        # XXX Temporary, identify false positive
        if (tasks is None or len(tasks) == 0) and criterions[
            "typology"
        ] == "ajout_piece":
            log.warning(
                f"Reception of an ACK for Piece {self.event.target_object}, "
                "but related task is not set to 'pending'"
            )
            if self.event.type_id == 83:
                criterions["state"] = "done"
                tasks = self.openads.get_tasks(criterions)

        if tasks is None or len(tasks) == 0:
            self.state == "error"
            raise OpenADSError("Could not find the task to update")
        elif len(tasks) > 1:
            self.state == "error"
            raise OpenADSError(
                "Found more than one task even trough only one is expected"
            )

        if not self.openads.update_task(tasks[0]["task"], state=self.state):
            self.state == "error"
            raise OpenADSError("Could not update task state on openADS")

    def handle_multi_binary_ack(self):
        """."""
        criterions = dict(category="platau", state="pending")

        # 57,"Notification de succès de versement de l'ensemble des pièces sur le dossier",26/02/2020
        if self.event.type_id == 57:
            log.info(
                f"Dossier {self.event.dossier_uid} : Pieces successfully received by Plat'AU. Nothing to do here, ACK for Piece are handled individually."
            )
            self.state = "done"
            return

        # 58,"Notification d’échec de versement de l'ensemble des pièces sur le dossier",27/02/2020
        elif self.event.type_id == 58:
            log.error(
                f"Dossier {self.event.dossier_uid} : Plat'AU failed to received some Pieces. See related Piece tasks for more info."
            )
            self.state = "error"
            return

        elif self.event.type_id in (59, 60):

            if self.event.object_type_id in (11, 15):
                # 11,"DECISION_URBA",11/01/2020
                # 15,"DECISION_EVOL",15/01/2020
                # binary ack from a Decision or Decision Evolution
                criterions["typology"] = "decision_DI"
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "instruction"

            elif self.event.object_type_id == 8:  # 8,"AVIS",08/01/2020
                # binary ack from an Avis
                criterions["typology"] = "avis_consultation"
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "avis_dossier_consultation"

            elif self.event.object_type_id == 9:  # 9,"ENSEMBLE_PRESCRIPTIONS"
                # binary ack from a Prescrition
                criterions["typology"] = "prescription"
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "prescription"

            elif self.event.object_type_id == 6:  # 6,"CONSULTATION",06/01/2020
                # binary ack from a Consultation
                criterions["typology"] = "creation_consultation"
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "consultation"

            elif self.event.object_type_id == 7:  # 7,"PEC_METIER",07/01/2020
                # binary ack from a PeC with attachment
                criterions["typology"] = "pec_metier_consultation"
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "pec_dossier_consultation"

            elif (
                self.event.object_type_id == 3
            ):  # 3,"LETTRE_AUX_PETITIONNAIRES",07/01/2020
                # binary ack from a Lettre au Petitionnaire with attachment
                # Target category may take differnet value
                criterions["typology"] = ["lettre_majoration", "lettre_incompletude"]
                criterions["external_uid"] = self.event.target_object
                criterions["external_uid_type"] = "instruction"

            else:
                msg = f"Unhandled object type in multi_binary_ack : {self.event.object_type_id} / {NOMENCLATURE['TYPE_OBJET_METIER'][self.event.object_type_id]}"
                log.warning(msg)
                return

            # 59,"Notification de succès de versement de l'ensemble des éléments sur le dossier",28/02/2020
            if self.event.type_id == 59:
                log.info(
                    f"Dossier {self.event.dossier_uid} : Documents successfully received by Plat'AU"
                )
                self.state = "done"

            # 60,"Notification d’échec de versement du contenu binaire d’un document",29/02/2020
            elif self.event.type_id == 60:
                log.error(
                    f"Dossier {self.event.dossier_uid} : Plat'AU failed to received some Documents"
                )
                self.state = "error"

        tasks = None
        # Some tasks with attachment (async) may return events with the same object_type_id
        # but different task category on openADS (lettre_petitionnaire).
        # In such case, we provide a list of typology, and iterate on task to find the one
        if isinstance(criterions["typology"], list):
            for typology in criterions["typology"]:
                curated_criterions = criterions.copy()
                curated_criterions["typology"] = typology
                tasks = self.openads.get_tasks(curated_criterions)
                if tasks:
                    break
        else:
            tasks = self.openads.get_tasks(criterions)

        if tasks is None or len(tasks) == 0:
            self.state == "error"
            raise OpenADSError("Could not find the task to update")
        elif len(tasks) > 1:
            self.state == "error"
            raise OpenADSError(
                "Found more than one task even trough only one is expected"
            )

        if not self.openads.update_task(tasks[0]["task"], state=self.state):
            self.state == "error"
            raise OpenADSError("Could not update task state on openADS")

    # ##################################################################################
    #
    # Collectivite INPUT tasks
    #
    # ##################################################################################
    def handle_dossier_adau(self):
        """."""
        dossier = self.connector.get_dossier_by_uid(self.event.dossier_uid, raw=True)
        infofiscales = self.connector.get_infofiscales(self.event.dossier_uid)
        mapped_dossier = DossierADAUMapper(
            dossier, actor_uid=self.actor.uid, infofiscales=infofiscales,
        ).render()

        self.openads.create_task("create_DI", mapped_dossier)
        pieces = self.connector.get_pieces_binaries(self.event.dossier_uid)
        for piece in pieces:

            # If we didn't success to download the file, `attachment` wil be None
            # No `add_piece` will be sent to openADS, but a message.
            if not piece.attachment:
                content = (
                    f"La pièce {piece.idPiece} n'a pas pu être récupérée "
                    "en raison d'un problème technique sur Plat'AU."
                )
                mapper = MessageMapper(
                    self.event.dossier_uid,
                    type="Réception de pièce impossible",
                    event=self.event,
                    content=content,
                    actor=self.actor,
                )
                mapped_message = mapper.render()
                self.openads.create_task("create_message", mapped_message)

                continue

            mapped_piece = DossierADAUPieceMapper(
                piece, self.actor.uid, opt_source=dossier
            ).render()
            self.openads.create_task("add_piece", mapped_piece)

        self.state = "done"

    def handle_pec_metier(self):
        """."""
        pecs = self.connector.get_pec_consultation(
            self.event.target_object, documents=True
        )
        if len(pecs.pecMetiers) > 1:
            # XXX : Multi pec case, get the last one ? (the more recent one)
            pecs.pecMetiers = pecs.pecMetiers[:-1]

        elif len(pecs.pecMetiers) == 0:
            raise PlatAUError("Inconsistent PEC response")

        consultation_r = self.connector.get_consultation_by_uid(
            self.event.target_object, self.event.dossier_uid
        )

        mapped_pec = PecMapper(pecs, consultation=consultation_r).render()
        self.openads.create_task("pec_metier_consultation", mapped_pec)

        # Let's add a message
        message_type = "Consultation prise en compte"
        message = None
        if pecs.pecMetiers[0].txObservations:
            message = (
                self.event.type_label
                + f". Observations : {pecs.pecMetiers[0].txObservations}"
            )
        if message:
            mapper = MessageMapper(
                consultation_r.dossier.idDossier,
                type=message_type,
                event=self.event,
                content=message,
                actor=self.actor,
            )
        else:
            mapper = MessageMapper(
                consultation_r.dossier.idDossier,
                type=message_type,
                event=self.event,
                actor=self.actor,
            )
        mapped_message = mapper.render()
        self.openads.create_task("create_message", mapped_message)

        self.state = "done"

    def handle_avis(self):
        """Handle avis notification.

            When a new Avis is added on Plat'AU, notifications are created for both
            Service instructeur and Service Consultant.

            In our usecase, (Consultation are sent with Guichet Unique), it means we'll
            receive 2 notifications. We don't want that.

            So, first, we exclude any treatment except for actor `guichet_unique`,
            then we treat the avis, along with a message.
        """
        if self.actor.role != "guichet_unique":
            log.info(
                "Avis received for actor with role different than `guichet_unique` role."
                "There wiln be no Avis task created."
            )
            self.state = "done"
            return

        avis = self.connector.get_avis_by_uid(
            self.event.target_object, dossier_uid=self.event.dossier_uid, documents=True
        )

        if avis is None:
            raise PlatAUError("Inconsistent Avis response")

        mapper = AvisMapper(avis)
        mapped_avis = mapper.render()
        self.openads.create_task("avis_consultation", mapped_avis)

        # Let's add a message
        message_type = "Réception d'un avis"
        mapper = MessageMapper(
            avis.dossier.idDossier,
            type=message_type,
            event=self.event,
            actor=self.actor,
        )
        mapped_message = mapper.render()
        self.openads.create_task("create_message", mapped_message)

        self.state = "done"

    # ##################################################################################
    #
    # Service Consulte INPUT tasks
    #
    # ##################################################################################
    def handle_consultation(self):
        """ XXX if async, move all openADS task creation at proces ned """
        consultation = self.connector.get_consultation_by_uid(
            self.event.target_object, self.event.dossier_uid
        )
        service_consultant = self.connector.get_acteur_by_uid(
            consultation.dossier.consultations[0].idServiceConsultant
        )
        infofiscales = self.connector.get_infofiscales(self.event.dossier_uid)
        mapped_consultation = ConsultationMapper(
            consultation,
            service_consultant=service_consultant,
            infofiscales=infofiscales,
        ).render()

        self.openads.create_task("create_DI_for_consultation", mapped_consultation)
        pieces = self.connector.get_pieces_binaries(self.event.dossier_uid)
        treated_pieces = []
        for piece in pieces:

            # If we didn't success to download the file, `attachment` wil be None
            # No `add_piece` will be sent to openADS, but a message.
            if not piece.attachment:
                content = (
                    f"La pièce {piece.idPiece} n'a pas pu être récupérée "
                    "en raison d'un problème technique sur Plat'AU."
                )
                mapper = MessageMapper(
                    self.event.dossier_uid,
                    type="Réception de pièce impossible",
                    event=self.event,
                    content=content,
                    actor=self.actor,
                )
                mapped_message = mapper.render()
                self.openads.create_task("create_message", mapped_message)

                continue

            # GTLB#27 we don't want any redundant Piece
            redundant = False
            for treated_piece in treated_pieces:
                if (
                    piece.attachment == treated_piece.attachment
                    and piece.hash == treated_piece.hash
                ):
                    redundant = True
                    break
            if redundant:
                log.warning(
                    f"Piece {piece.idPiece} identified by hash : `{piece.hash}` and "
                    f"filename : `{piece.attachment.name}` already exists in the Dossier."
                    "It will be ignored"
                )
                continue

            mapped_piece = ConsultationPieceMapper(
                piece, self.actor.uid, opt_source=consultation
            ).render()
            # Failing while creating `add_piece` task should not block other Piece creation
            try:
                self.openads.create_task("add_piece", mapped_piece)

                # multiple Piec : we will lookup later to check any redundancy
                treated_pieces.append(piece)

            except OpenADSError:
                # # XXX provide user message when add_piece is failing
                log.warning(f"Failed to create Piece")
                dossier = self.connector.get_dossier_by_uid(self.event.dossier_uid)
                message_type = "Réception de pièce impossible"

                # We absolutly don't want any fail here :
                try:
                    piece_uid = mapped_piece["external_uids"]["piece"]
                    file_name = piece.attachment.name
                    file_size = piece.attachment.size

                except Exception as e:
                    log.error(e, exc_info=True)
                    content = "Une des pièces ne peut être récupérée correctement."

                else:
                    content = (
                        f"La pièce {piece_uid} nommée {file_name} "
                        f"et de taille {file_size} Mo ne peut être récupérée."
                    )

                mapper = MessageMapper(
                    dossier.idDossier,
                    type=message_type,
                    event=self.event,
                    content=content,
                    actor=self.actor,
                )
                mapped_message = mapper.render()
                self.openads.create_task("create_message", mapped_message)

        self.state = "done"

    def handle_completude(self):
        """Handle Completude and Icompletude event."""
        dossier = self.connector.get_dossier_by_uid(self.event.dossier_uid)

        if self.event.type_id == 8:
            message_type = "Complétude"
            # We'll provide a message provided dtCompletude
            if dossier.dtCompletude is not None:
                content = (
                    self.event.type_label
                    + f". Date de complétude : {dossier.dtCompletude.strftime('%d/%m/%Y')}"
                )
                mapper = MessageMapper(
                    dossier.idDossier,
                    type=message_type,
                    event=self.event,
                    content=content,
                    actor=self.actor,
                )
            else:
                mapper = MessageMapper(
                    dossier.idDossier,
                    type=message_type,
                    event=self.event,
                    actor=self.actor,
                )

        elif self.event.type_id == 78:
            message_type = "Incomplétude"
            if dossier.dtLimiteFourniturePieces is not None:
                content = (
                    self.event.type_label
                    + f". Date limite de fourniture des pièces : {dossier.dtLimiteFourniturePieces.strftime('%d/%m/%Y')}"
                )
                mapper = MessageMapper(
                    dossier.idDossier,
                    type=message_type,
                    event=self.event,
                    content=content,
                    actor=self.actor,
                )
            else:
                mapper = MessageMapper(
                    dossier.idDossier,
                    type=message_type,
                    event=self.event,
                    actor=self.actor,
                )

        mapped_message = mapper.render()
        self.openads.create_task("create_message", mapped_message)

        self.state = "done"
