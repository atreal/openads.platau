import base64
import logging
import json
import os


# from cachetools import cached, TTLCache
from datetime import datetime, date
from bravado_core.model import Model as BravadoModel

from openads.platau.mapping import OPENADS_MAPPING, PLATAU_MAPPING

log = logging.getLogger(__name__)


class OADSMapper(object):

    mapped: dict = {}
    keys: tuple
    source: BravadoModel
    actor_uid: str = None

    def __init__(
        self, source: BravadoModel, actor_uid: str = None, **opt_sources,
    ) -> None:
        """."""
        self.source = source

        if actor_uid is not None:
            self.actor_uid = actor_uid

        for source_name, value in opt_sources.items():
            setattr(self, source_name, value)

    def render(self) -> dict:
        """."""
        for k in self.keys:
            mapper = getattr(self, f"_map_{k}", None)
            if mapper is None:
                raise NotImplementedError

            self.mapped[k] = mapper()

        log.debug(f"Mapped data : \n{self.mapped}")

        return self.mapped

    def to_json(self) -> str:
        """."""
        if self.mapped == {}:
            self.render()

        return json.dumps(self.mapped)

    def to_file(self, file_path):
        """."""
        if self.mapped == {}:
            self.render()

        with open(file_path, "w") as f:
            return json.dump(self.mapped, f, indent=2, sort_keys=True, default=str)


def map_adresse(adresse: BravadoModel, future: bool = False) -> dict:
    """."""
    voie = ""
    if adresse.nomVoie is not None:
        voie = adresse.nomVoie
    if adresse.libTypeVoie is not None:
        # XXX - length control should be in openADS
        voie = f"{adresse.libTypeVoie} {voie}"[:55]
    if not future:
        mapped = {
            "bp": cast_str(adresse.noBoitePostale),
            "cedex": cast_str(adresse.noCedex),
            "code_postal": cast_str(adresse.codePostal),
            "lieu_dit": cast_str(adresse.nomLieuDit),
            "localite": cast_str(adresse.nomLocalite),
            "numero": cast_str(adresse.noVoie),
            "pays": cast_str(adresse.nomPays),
            "voie": voie,
        }
    else:
        mapped = {
            "tax_adresse_future_cp": cast_str(adresse.codePostal),
            "tax_adresse_future_localite": cast_str(adresse.nomLocalite),
            "tax_adresse_future_numero": cast_str(adresse.noVoie),
            "tax_adresse_future_pays": cast_str(adresse.nomPays),
            "tax_adresse_future_voie": voie,
        }

    return mapped


def map_one_to_one(source: BravadoModel, properties: dict) -> dict:
    """ One to one mapping-based properties."""
    mapped = {}
    for prop, mapping in properties.items():
        values = getattr(source, prop)
        if values is None:
            continue
        elif not isinstance(values, list):
            values = [values]
        for item in values:
            if not item:
                continue
            try:
                field_name = OPENADS_MAPPING[mapping][item.idNom]
            except KeyError:
                log.warn("Property %s : failed to map value %s", prop, item.idNom)
                continue

            mapped[field_name.replace("donnees_techniques.", "")] = "t"
    return mapped


class BaseDossierMapper(OADSMapper):
    """Main source is a BravadoR, with a projet and dossier attributes.
    Two others sources are available :
        - self.infofiscales, a ReponseInfoFiscalesRecherche
    """

    mapped: dict = {}
    keys: tuple = [
        "donnees_techniques",
        "demandeur",
        "dossier",
    ]

    def _map_demandeur(self) -> dict:
        """."""
        demandeurs = []
        genre_mapping = OPENADS_MAPPING["GENRE"]
        role_mapping = OPENADS_MAPPING["ROLE"]
        personnes_morales = {}
        personnes_physiques = {}
        petitionnaire_principal_found = False

        def map_coordonnees(coordonnees: list) -> dict:
            """."""
            mapped = {}
            for c in coordonnees:
                if c.nomTypeCoordonnee.idNom == 1:
                    mapped["indicatif"] = cast_str(c.indicatifPays)
                    # This is not optimal
                    if (
                        c.coordonnee is not None
                        and c.coordonnee.startswith("06")
                        or c.coordonnee.startswith("07")
                    ):
                        mapped["telephone_mobile"] = c.coordonnee
                    else:
                        mapped["telephone_fixe"] = c.coordonnee

                elif c.nomTypeCoordonnee.idNom == 2:
                    mapped["fax"] = cast_str(c.coordonnee)

                elif c.nomTypeCoordonnee.idNom == 3:
                    mapped["courriel"] = cast_str(c.coordonnee)

            return mapped

        def map_roles(roles: list) -> dict:
            mapped = {}
            if len(roles) == 0:
                return
            if len(roles) > 1:
                log.warning(
                    "Multiple roles found. Not implemented. First one will be used"
                )
            r = roles[0]
            mapped["type_demandeur"] = role_mapping[r.nomRole.idNom]
            if (
                mapped["type_demandeur"] == "petitionnaire"
                and r.noOrdrePersonneDansRole == 1
            ):
                mapped["petitionnaire_principal"] = "t"
            # XXX in openADS, there are 2 kind of Architecte :
            #   - the main one is stored in the Donnees Techniques
            #   - the Legislation Connexe (architecte_lc) one is stored as a Demandeur
            # In Plat'AU, it's just a PersonneP1 with role `architecte`
            # For now, we store every Architecte from Plat'AU in a Demandeur.
            if mapped["type_demandeur"] == "architecte_lc":
                mapped["conseil_regional"] = cast_str(r.libCROA)
                mapped["num_inscription"] = cast_str(r.noInscriptionCROA)
            return mapped

        # Pre-sort Personnes. This will :
        # - clean-up Personnes with bad properties (ie. no role, etc)
        # - help us to merge a Personne Morale with its Representant
        for p in self.source.dossier.personnes:
            if not p.roles:
                log.warning(
                    f"Personne {p.idPersonne} does not provide Roles."
                    "This Personne will be ignored"
                )
                continue

            if p.boEstPersonneMorale:
                personnes_morales[p.idPersonne] = p
            else:
                personnes_physiques[p.idPersonne] = p

        personnes_to_delete = []
        # We want to merge Representant to its Represente
        for uid, p in personnes_physiques.items():
            uid_personne_representee = None
            if p.roles:

                # Do we have Personne Physique that represents a Personne Morale
                for role in p.roles:  # 3,"Représentant",04/12/2019
                    if role.nomRole.idNom == 3 and role.idsPersonnesRepresentees:
                        uid_personne_representee = role.idsPersonnesRepresentees[0]

                # We link Representant and Respresente and mark the Reprensant to be deleted
                if uid_personne_representee in personnes_morales:
                    personnes_morales[uid_personne_representee].representant = p
                    personnes_to_delete.append(p.idPersonne)

            # openADS can't accept Personne Physique without Noms
            # We'll add a specific value
            if uid_personne_representee is None:

                # Drop empty list items if any
                if p.noms is not None:
                    p.noms = [nom for nom in p.noms if nom]

                if not p.noms:
                    p.noms = ["N/C"]
                    log.warning(
                        f"Personne {p.idPersonne} does not provide Noms."
                        "Arbitrary 'N/C' value will be used."
                    )

        # cleanup the merged or bad personne
        for uid in personnes_to_delete:
            del personnes_physiques[uid]

        # Let's map Personne Morale
        for uid, p in personnes_morales.items():
            demandeur = {}
            demandeur.update(
                {
                    "qualite": "personne_morale",
                    "personne_morale_categorie_juridique": cast_str(p.libTypeSociete),
                    "personne_morale_denomination": cast_str(
                        p.libDenomination, mandatory_value=True
                    ),
                    "personne_morale_raison_sociale": cast_str(
                        p.libRaisonSociale, mandatory_value=True
                    ),
                    "personne_morale_siret": cast_str(p.siret),
                    "notification": cast_boolean(p.boOkTransmissionDemat),
                }
            )

            has_representant = getattr(p, "representant", None)
            if has_representant:
                # We have representant, let's use it
                r = p.representant
                demandeur.update(
                    {
                        "personne_morale_civilite": r.nomGenre
                        and genre_mapping[r.nomGenre.idNom]
                        or "",
                        "personne_morale_nom": secure_join(r.noms, " "),
                        "personne_morale_prenom": secure_join(r.prenoms, " "),
                    }
                )

            # Adresse Courante
            # No Representant, we use the Personne Morale one
            if p.adresseCourante is not None and has_representant is None:
                demandeur.update(map_adresse(p.adresseCourante))

            # There is a Representant and Represente
            elif has_representant is not None and p.adresseCourante is not None:

                # We have Adresses Courante for both Representant and Represente
                # we log the case (losing some data in the process)
                if p.representant.adresseCourante is not None:
                    log.warning(
                        "Found AdresseCourante for both Personne Representante and Representee."
                        " We keep the Representee one."
                    )

                # We use the Adresse Courante from Represente
                demandeur.update(map_adresse(p.adresseCourante))

            elif (
                has_representant is not None
                and p.representant.adresseCourante is not None
            ):
                demandeur.update(map_adresse(p.representant.adresseCourante))

            else:
                log.warning(
                    "No AdresseCourante found for both Personne Representante and Representee."
                )

            # Coordonnees
            # No Representant, we use the Personne Morale one
            if p.coordonnees is not None and has_representant is None:
                demandeur.update(map_coordonnees(p.coordonnees))

            # There is a Representant and Represente
            elif has_representant is not None and p.coordonnees is not None:

                # We have Coordonnees for both Representant and Represente
                # we log the case (losing some data in the process)
                if p.representant.coordonnees is not None:
                    log.warning(
                        "Found AdresseCourante for both Personne Representante and Representee."
                        " We keep the Representee one."
                    )

                # We use the Coordonnees from Represente
                demandeur.update(map_coordonnees(p.coordonnees))

            elif (
                has_representant is not None and p.representant.coordonnees is not None
            ):
                demandeur.update(map_coordonnees(p.representant.coordonnees))

            else:
                log.warning(
                    "No Coordonnees found for both Personne Representante and Representee."
                )

            # Roles
            demandeur.update(map_roles(p.roles))
            # Did we found a petitionnaire_principal ?
            if (
                not petitionnaire_principal_found
                and "petitionnaire_principal" in demandeur
            ):
                petitionnaire_principal_found = True
            elif (
                petitionnaire_principal_found and "petitionnaire_principal" in demandeur
            ):
                # we already found a Petitionnaire Principal. We cannot have both
                del demandeur["petitionnaire_principal"]

            demandeurs.append(demandeur)

        # Let's map Personne Physique
        for uid, p in personnes_physiques.items():
            demandeur = {}
            demandeur.update(
                {
                    "qualite": "particulier",
                    "particulier_civilite": p.nomGenre
                    and genre_mapping[p.nomGenre.idNom]
                    or "",
                    "particulier_commune_naissance": cast_str(p.libCommuneNaissance),
                    "particulier_date_naissance": cast_date(p.dtNaissance),
                    "particulier_departement_naissance": cast_str(
                        p.noDepartementNaissance
                    ),
                    "particulier_nom": secure_join(p.noms, " "),
                    "particulier_pays_naissance": cast_str(p.libPaysNaissance),
                    "particulier_prenom": secure_join(p.prenoms, " "),
                    "notification": cast_boolean(p.boOkTransmissionDemat),
                }
            )

            if p.adresseCourante is not None:
                demandeur.update(map_adresse(p.adresseCourante))

            if p.coordonnees is not None:
                demandeur.update(map_coordonnees(p.coordonnees))

            demandeur.update(map_roles(p.roles))
            if (
                not petitionnaire_principal_found
                and "petitionnaire_principal" in demandeur
            ):
                petitionnaire_principal_found = True
            elif (
                petitionnaire_principal_found and "petitionnaire_principal" in demandeur
            ):
                # we already found a Petitionnaire Principal. We cannot have both
                del demandeur["petitionnaire_principal"]

            demandeurs.append(demandeur)

        # XXX handle petitionnaire_principal_found == False
        if not petitionnaire_principal_found:
            # We did not find any petitionnaire pricnipal. Let's fix an arbitrary one,
            # on the first  petitionnaire
            for d in demandeurs:
                if d["type_demandeur"] == "petitionnaire":
                    d["petitionnaire_principal"] = "t"
                    break

        return demandeurs

    def _map_dossier(self) -> dict:
        """."""
        d = self.source.dossier
        # cs = self.source.dossier.consultations[0]
        dossier_mapping = OPENADS_MAPPING["TYPE_DOSSIER"]
        dossier = {
            # "acteur": d.consultations[0].idServiceConsulte,
            "date_depot_mairie": cast_date(d.dtDepot),
            "depot_electronique": "t",  # Not sure, but it comes from Plat'AU, right ?
            "description_projet": cast_str(d.txDescriptifGlobal),
            "dossier": d.suffixeNoLocal and d.noLocal + d.suffixeNoLocal or d.noLocal,
            "dossier_autorisation_type_detaille_code": d.nomTypeDossier
            and dossier_mapping[d.nomTypeDossier.idNom]
            or "",
            "insee": cast_str(d.idCommuneDepot),
        }
        dossier["date_demande"] = cast_date(datetime.now())

        # Specific case for DP, DPM, DPL, PC, PCMI. Those Dossier typologies don't exist
        # in Platau, we should look for others properties.
        if (
            d.boMaisonIndividuelle
            and dossier["dossier_autorisation_type_detaille_code"] == "DP"
        ):
            dossier["dossier_autorisation_type_detaille_code"] = "DPMI"
        # XXX this is not discriminant
        #  We should use nomNatureTravaux, values 4 & 5
        elif (
            d.terrains is not None
            and len(d.terrains) > 0
            and d.terrains[0].boEstSitueDansUnLotissement
            and dossier["dossier_autorisation_type_detaille_code"] == "DPL"
        ):
            dossier["dossier_autorisation_type_detaille_code"] = "DPMI"
        elif (
            d.boMaisonIndividuelle
            and dossier["dossier_autorisation_type_detaille_code"] == "PC"
        ):
            dossier["dossier_autorisation_type_detaille_code"] = "PI"

        # YES, we may get some empty Terrain
        if not d.terrains:
            return dossier

        references_cadastrales = []
        for t in d.terrains:
            # As we do no support multiple Terrain Adresses in openADS, we extract
            # Adresse from the first one.
            if (
                t.noTerrain is not None
                and t.adresses is not None
                and t.noTerrain == "1"
                and len(t.adresses) >= 1
            ):
                dossier["terrain_adresse_code_postal"] = cast_str(
                    t.adresses[0].codePostal
                )
                dossier["terrain_adresse_localite"] = cast_str(
                    t.adresses[0].nomLocalite
                )
                voie = cast_str(t.adresses[0].nomVoie)
                dossier["terrain_adresse_voie"] = (
                    t.adresses[0].libTypeVoie
                    and f"{t.adresses[0].libTypeVoie} {voie}"
                    or voie
                )
                dossier["terrain_adresse_voie_numero"] = cast_str(t.adresses[0].noVoie)
                dossier["terrain_adresse_bp"] = cast_str(t.adresses[0].noBoitePostale)
                dossier["terrain_adresse_lieu_dit"] = cast_str(t.adresses[0].nomLieuDit)
                dossier["terrain_superficie"] = cast_str(t.surfTerrainCERFA)

            if t.refCadastrales is None:
                continue

            for r in t.refCadastrales:

                # Those are required in CDS, but we assume it can be null
                if (
                    r.prefixeReference is None
                    or r.noReference is None
                    or r.sectionReference is None
                ):
                    continue

                # Fix badly composed Parcelle
                prefixeReference = r.prefixeReference.rjust(3, "0")
                noReference = r.noReference.rjust(4, "0")
                references_cadastrales.append(
                    prefixeReference + r.sectionReference + noReference
                )

        dossier["terrain_references_cadastrales"] = ";".join(references_cadastrales)
        # terrain_references_cadastrales should always end by a semi-column
        dossier["terrain_references_cadastrales"] += ";"

        return dossier

    def _map_donnees_techniques(self):
        """."""
        d = self.source.dossier
        p = self.source.projet
        d_t = {}

        def clean_dt_key(key):
            return key.replace("donnees_techniques.", "")

        ########################
        # 1 - Data from Projet #
        ########################

        # Boolean Mapping
        d_t["terr_juri_afu"] = cast_boolean(p.boConcerneUneAFU, textual=True)
        d_t["terr_juri_pup"] = cast_boolean(p.boReleveDUnPUP, textual=True)

        # Generic
        for prop, field_name in {
            "boDdeFinitionDifferee": "am_lot_fin_diff",
            "boDdeVenteOuLocLotsParAnticipation": "am_lot_vente_ant",
            # XXX - Not mapped in CT / Not found in XLS
            # 'boReleveDUnPAE': "",
            "boResulteDUnPPR": "tax_trx_presc_ppr",
        }.items():
            prop_value = getattr(p, prop)
            if prop_value is None:
                continue
            elif prop_value is True:
                d_t[field_name] = "t"
            elif prop_value is False:
                d_t[field_name] = "f"

        d_t["vsd_rescr_fisc"] = cast_date(p.dtRescritFiscal)
        d_t["ope_proj_desc"] = cast_str(p.nomProjet)

        # nomTypeGarantieFinition
        if p.nomTypeGarantieFinition:
            d_t.update(map_one_to_one(p, {"nomTypeGarantieFinition": "TYPE_GARANTIE"}))

        # XXX  we shoulmd use the one from Terrains
        d_t["terr_juri_desc"] = cast_str(p.txComplementsJuridiques)

        # Not mapped in CT
        # "txDescriptifGlobal"
        # "txNomConventionPUP"

        #########################
        # 2 - Data from Dossier #
        #########################
        # AdressesFuture handler. We'll walk on d.personnes.adresseFuture
        # First AdresseFuture is used.
        # TODO : be more clever ?
        for personne in d.personnes:
            if personne.adresseFuture is not None:
                d_t.update(map_adresse(personne.adresseFuture, future=True))
                break

        # AdressesStationnementsExternes handler.
        if d.adressesStationnementsExternes:

            # Only 2 adresses are allowed for Stationnement
            for index, adresse in enumerate(d.adressesStationnementsExternes[:2]):

                # Specific case for Voie
                voie = ""
                if adresse.nomVoie is not None:
                    voie = cast_str(adresse.nomVoie)
                if adresse.libTypeVoie is not None:
                    # XXX - length control should be in openADS
                    voie = f"{cast_str(adresse.libTypeVoie)} + {voie}"[:55]

                d_t[f"s1na{index + 1}_numero"] = cast_str(adresse.noVoie)
                d_t[f"s1va{index + 1}_voie"] = voie
                d_t[f"s1wa{index + 1}_lieudit"] = cast_str(adresse.nomLieuDit)
                d_t[f"s1la{index + 1}_localite"] = cast_str(adresse.nomLocalite)
                d_t[f"s1pa{index + 1}_codepostal"] = cast_str(adresse.codePostal)

        # Amenagements handler. See  mapping/many_to_many/amenagements.json
        if d.amenagements:
            amenagements_properties = [
                "hautExhaussements",
                "nbMaxLots",
                "nbMoisInstallationSiSaisonniere",
                "profAffouillement",
                "surfMaxPlancher",
                "surfTravauxAffouillement",
                "txAgeBoisement",
                "txAutresBoisements",
                "txDensiteBoisement",
                "txDossierEtOuAutorisationPrecedent",
                "txEssencesBoisement",
                "txPeriodeDemontageInstallationSiSaisonniere",
                "txPeriodeExploitSiSaisonniere",
                "txQualiteBoisement",
                "txTraitementBoisement",
            ]

            for a_p in d.amenagements:
                for a_o in PLATAU_MAPPING["amenagements"]:
                    if (
                        a_p.nomTypeAmenagement  # property could be None
                        and a_p.nomTypeAmenagement.idNom == a_o["nomTypeAmenagement"]
                    ):

                        if (
                            a_p.nomModeRepartitionConstructibilite  # property could be None
                            and "nomModeRepartitionConstructibilite" in a_o
                            and a_p.nomModeRepartitionConstructibilite.idNom
                            != a_o["nomModeRepartitionConstructibilite"]
                        ):
                            continue

                        # We now have a match
                        if "x-openads-condition" in a_o:
                            field_name = clean_dt_key(a_o["x-openads-condition"])
                            d_t[field_name] = "t"

                        for prop in amenagements_properties:
                            if prop in a_o:
                                am_value = getattr(a_p, prop, "")
                                if am_value is not None:
                                    field_name = clean_dt_key(a_o[prop])
                                    d_t[field_name] = str(am_value)

                        if a_p.attributsAmenagement is None:
                            continue
                        for attr_p in a_p.attributsAmenagement:
                            for attr_o in a_o["attributsAmenagement"]:
                                if (
                                    attr_p.nomStatutInformation  # property could be None
                                    and attr_p.nomStatutVuDuDossier  # property could be None
                                    and attr_o["nomStatutInformation"]
                                    == attr_p.nomStatutInformation.idNom
                                    and attr_o["nomStatutVuDuDossier"]
                                    == attr_p.nomStatutVuDuDossier.idNom
                                ):
                                    for k, v in attr_o.items():
                                        # We'll ignore nomStatutInformation and
                                        # nomStatutVuDuDossier which are numeric values.
                                        if isinstance(v, str) and v.startswith(
                                            "donnees_techniques"
                                        ):
                                            attr_am_value = getattr(attr_p, k)
                                            if attr_am_value is not None:
                                                field_name = clean_dt_key(v)
                                                d_t[field_name] = str(attr_am_value)

        # Annexes handler. See  mapping/many_to_many/annexes.json
        if d.annexes:
            for anx_platau in d.annexes:
                # special case 1
                if anx_platau.nomTypeAnnexe.idNom == 1 and anx_platau.surfAnnexe != 0:
                    d_t["tax_sup_bass_pisc_cr"] = str(anx_platau.surfAnnexe)
                    continue

                # special case 2
                if (
                    anx_platau.nomTypeAnnexe.idNom == 6
                    and anx_platau.libAutreTypeAnnexe
                ):
                    # XXX freshly corrected
                    d_t["co_anx_autr_desc"] = cast_str(anx_platau.libAutreTypeAnnexe)

                for anx_openads in PLATAU_MAPPING["annexes"]:
                    if (
                        "x-openads-condition" in anx_openads
                        and anx_platau.nomTypeAnnexe.idNom
                        == anx_openads["nomTypeAnnexe"]
                    ):
                        field_name = anx_openads["x-openads-condition"]
                        break

                d_t[clean_dt_key(field_name)] = "t"

        # Batiments handler
        if d.batiments:
            if len(d.batiments) > 1:
                log.warning(
                    "Multiple batiments found. Not implemented. First one will be used"
                )
            d_t["co_mais_piece_nb"] = cast_str(d.batiments[0].nbPiecesMaison)
            d_t["co_mais_niv_nb"] = cast_str(d.batiments[0].niveauMax)

        # Various boolean
        d_t["vsd_surf_planch_smd"] = cast_boolean(d.boAtteinteDuSMD)
        d_t["tax_monu_hist"] = cast_boolean(d.boConcerneBatimentHistorique)
        d_t["co_div_terr"] = cast_boolean(d.boDivisionAvantAchevement)
        d_t["co_archi_recours"] = cast_boolean(d.boRecoursAUnProfessionnel)
        d_t["co_archi_attest_honneur"] = cast_boolean(
            d.boRecoursAUnProfessionnelNonOblig
        )
        d_t["tax_terrassement_arch"] = cast_boolean(d.boTerrassement)
        d_t["e3c_certification"] = cast_boolean(d.boOkCompetenceElaborationPape)
        d_t["e3a_competence"] = cast_boolean(d.boOkParticipationArchiPaysPape)

        if d.dtDecisionTransfert:
            d_t["date_decision_transfert"] = cast_date(d.dtDecisionTransfert)

        # Engagement, Date and Lieu
        # if d.dtEngagementAAvoirQualite:
        d_t["enga_decla_date"] = cast_date(d.dtEngagementAAvoirQualite)
        # if d.lieuEngagementAAvoirQualite:
        d_t["enga_decla_lieu"] = cast_str(d.lieuEngagementAAvoirQualite)

        # Equipements
        # "equipementsPrives":
        if d.equipementsPrives:
            for e in d.equipementsPrives:
                if e.nomTypeEquipPrive.idNom == 1 and e.surfEquipPrive is not None:
                    d_t["tax_pann_volt_sup_cr"] = str(e.surfEquipPrive)
                elif e.nomTypeEquipPrive.idNom == 2 and e.nbEquipPrive is not None:
                    d_t["tax_eol_haut_nb_cr"] = str(e.nbEquipPrive)

        # "equipementsPublics" (CU)
        if d.equipementsPublics:
            # Let's build a mapping with a composed key
            e_p_mapping = {
                f"{e['nomTypeEquipPublic']}_{e['nomStatutEquipPublic']}": e
                for e in PLATAU_MAPPING["equipementsPublics"]
            }
            for equipement in d.equipementsPublics:

                e_p_key = (
                    f"{equipement.nomTypeEquipPublic}_{equipement.nomStatutEquipPublic}"
                )

                if e_p_key in e_p_mapping:

                    # We map the parent boolean
                    if "x-openads-condition" in e_p_mapping[e_p_key]:
                        d_t[e_p_mapping[e_p_key]["x-openads-condition"]] = cast_boolean(
                            True
                        )

                    # We map the Conessionaire name and the date
                    if "libConcessionnaire" in e_p_mapping[e_p_key]:
                        d_t[e_p_mapping[e_p_key]["libConcessionnaire"]] = cast_str(
                            equipement.libConcessionnaire
                        )
                    if "dtPrevuAvantLe" in e_p_mapping[e_p_key]:
                        d_t[e_p_mapping[e_p_key]["dtPrevuAvantLe"]] = cast_date(
                            equipement.dtPrevuAvantLe
                        )

        # infoFiscalesOuStatistiquesAgregees handler
        if self.infofiscales:
            for iffc_platau in self.infofiscales:

                # Let's get rid off wrongly constitued data
                if (
                    not iffc_platau.nomStatutInformation
                    or not iffc_platau.nomStatutVuDuDossier
                ):
                    continue

                for iffc_openads in PLATAU_MAPPING[
                    "infoFiscalesOuStatistiquesAgregees"
                ]:

                    if (
                        iffc_platau.nomStatutInformation.idNom
                        != iffc_openads["nomStatutInformation"]
                    ) or (
                        iffc_platau.nomStatutVuDuDossier.idNom
                        != iffc_openads["nomStatutVuDuDossier"]
                    ):
                        continue

                    # nomDestinationFiscale
                    #  XXX attr on iffc always exists !!
                    if (
                        not hasattr(iffc_platau, "nomDestinationFiscale")
                        and "nomDestinationFiscale" in iffc_openads
                    ) or (
                        hasattr(iffc_platau, "nomDestinationFiscale")
                        and "nomDestinationFiscale" not in iffc_openads
                    ):
                        continue
                    if (
                        hasattr(iffc_platau, "nomDestinationFiscale")
                        and "nomDestinationFiscale" in iffc_openads
                    ):
                        if (
                            iffc_platau.nomDestinationFiscale is not None
                            and iffc_platau.nomDestinationFiscale.idNom
                            != iffc_openads["nomDestinationFiscale"]
                        ) or (
                            iffc_platau.nomDestinationFiscale is None
                            and iffc_openads["nomDestinationFiscale"] is not None
                        ):
                            continue

                    # nomTypeFinancement
                    if (
                        not hasattr(iffc_platau, "nomTypeFinancement")
                        and "nomTypeFinancement" in iffc_openads
                    ) or (
                        hasattr(iffc_platau, "nomTypeFinancement")
                        and "nomTypeFinancement" not in iffc_openads
                    ):
                        continue
                    if (
                        hasattr(iffc_platau, "nomTypeFinancement")
                        and "nomTypeFinancement" in iffc_openads
                    ):
                        if (
                            iffc_platau.nomTypeFinancement is not None
                            and iffc_platau.nomTypeFinancement.idNom
                            != iffc_openads["nomTypeFinancement"]
                        ) or (
                            iffc_platau.nomTypeFinancement is None
                            and iffc_openads["nomTypeFinancement"] is not None
                        ):
                            continue

                    # We now have a match. We can map the value.
                    for prop in (
                        "libAutreTypeFinancement",
                        "nbLocaux",
                        "surfFiscaleHorsSCC",
                        "surfFiscaleSCC",
                        "surfaceFiscaleSSCIncluses",
                        "surfFiscaleSCCNSVB",
                        "surfFiscaleSCCNVB",
                    ):
                        value = getattr(iffc_platau, prop, None)
                        if value is not None:
                            # openADS accepts only integer for sufacce values
                            # TODO : change to float in openADS ?
                            if prop.startswith("surf"):
                                value = int(value)

                            if prop in iffc_openads:
                                d_t[clean_dt_key(iffc_openads[prop])] = cast_str(value)
                            if "x-openads-condition" in iffc_openads:
                                d_t[
                                    clean_dt_key(iffc_openads["x-openads-condition"])
                                ] = "t"

        # libAutreUsagePrevuLocaux handler
        d_t["co_resid_autr_desc"] = cast_str(d.libAutreUsagePrevuLocaux)

        # logementsParType
        if d.logementsParType:
            for logmnt in d.logementsParType:
                if (
                    logmnt.nbLogements is not None
                    and logmnt.nomTypeLogement is not None
                ):
                    d_t[
                        f"co_log_{logmnt.nomTypeLogement.idNom}p_nb"
                    ] = logmnt.nbLogements

        d_t["co_foyer_chamb_nb"] = cast_str(d.nbChambresHotelOuResidence)
        d_t["co_tot_log_nb"] = cast_str(d.nbLogementsCrees)
        d_t["co_tot_coll_nb"] = cast_str(d.nbLogementsCreesDansBatimentCollectif)
        d_t["co_tot_ind_nb"] = cast_str(d.nbLogementsCreesDansBatimentIndiv)
        d_t["dm_tot_log_nb"] = cast_str(d.nbLogementsDemolis)

        d_t["co_trav_supp_dessous"] = cast_str(d.nbNiveauxSupAuDessousDuSol)
        d_t["co_trav_supp_dessus"] = cast_str(d.nbNiveauxSupAuDessusDuSol)

        d_t["co_statio_place_nb"] = cast_str(d.nbPlacesStationnementExternes)
        d_t["co_bat_niv_nb"] = cast_str(d.niveauMaxBatiments)
        d_t["co_bat_niv_dessous_nb"] = cast_str(d.niveauMinBatiments)

        # One to one mapping-based properties. See DT_ONE_TO_ONE for details.
        nomenclature_based = {
            # NomFinalitesPrevues. See mapping/one_to_one/FINALITE_PREVUE.json
            "nomFinalitesPrevues": "FINALITE_PREVUE",
            # InfosLegiConnexes. See mapping/one_to_one/INFO_LEGI_CONNEXE.json
            "nomInfosLegiConnexes": "INFO_LEGI_CONNEXE",
            # nomNaturesTravaux. See mapping/one_to_one/NATURE_TRAVAUX.json
            "nomNaturesTravaux": "NATURE_TRAVAUX",
            # nomNaturesTravaux. See mapping/one_to_one/PRECISION_NAT_TRAV.json
            "nomPrecisionsNatTrav": "PRECISION_NAT_TRAV",
            # nomTypeDemolition. See mapping/one_to_one/TYPE_DEMOLITION.json
            "nomTypeDemolition": "TYPE_DEMOLITION",
            # nomUsagesPrevusLocaux. See mapping/one_to_one/USAGE_PREVU.json
            "nomUsagesPrevusLocaux": "USAGE_PREVU",
        }
        d_t.update(map_one_to_one(d, nomenclature_based))

        d_t["c2zp1_crete"] = cast_str(d.puissanceCreteProduite)
        d_t["co_elec_tension"] = cast_str(d.puissanceElectriqueRequise)
        d_t["vsd_unit_fonc_constr_sup"] = cast_str(d.surfConstructibleTotale)
        d_t["vsd_const_sxist_non_dem_surf"] = cast_str(d.surfConstruiteConservee)
        d_t["tax_surf_abr_jard_pig_colom"] = cast_str(d.surfFiscaleAnnexes)

        # surfInstructionAgregees handler
        if d.surfInstructionAgregees:
            for surface_platau in d.surfInstructionAgregees:
                for surface_openads in PLATAU_MAPPING["surfInstructionAgregees"]:
                    if (
                        surface_platau.nomDestinationInstruction is not None
                        and surface_platau.nomDestinationInstruction.idNom
                        != surface_openads["nomDestinationInstruction"]
                    ) or (
                        surface_platau.nomDestinationInstruction is None
                        and surface_openads["nomDestinationInstruction"] is not None
                    ):
                        continue
                    if (
                        surface_platau.nomSousDestinationInstruction is not None
                        and surface_platau.nomSousDestinationInstruction.idNom
                        != surface_openads["nomSousDestinationInstruction"]
                    ) or (
                        surface_platau.nomSousDestinationInstruction is None
                        and surface_openads["nomSousDestinationInstruction"] is not None
                    ):
                        continue
                    if (
                        surface_platau.nomStatutInformation.idNom
                        != surface_openads["nomStatutInformation"]
                    ):
                        continue
                    if (
                        surface_platau.nomStatutVuDuDossier.idNom
                        != surface_openads["nomStatutVuDuDossier"]
                    ):
                        continue

                    # We now have a match
                    field_name = clean_dt_key(surface_openads["surfInstruction"])
                    d_t[field_name] = cast_str(surface_platau.surfInstruction)

        d_t["am_terr_surf"] = cast_str(d.surfTotaleDesAmenagements)
        d_t["dm_constr_dates"] = cast_str(d.txDateApproxConstructionBatimentsDemolis)
        d_t["co_bat_existant"] = cast_str(d.txDescriptifBatimentAConserverOuADemolir)
        d_t["co_bat_projete"] = cast_str(d.txDescriptifBatimentsProjetes)
        d_t["am_projet_desc"] = cast_str(d.txDescriptifDesAmenagements)
        d_t["co_projet_desc"] = cast_str(d.txDescriptifGlobal)
        d_t["co_bat_nature"] = cast_str(d.txDescriptifPiecesFournies)
        d_t["am_tranche_desc"] = cast_str(d.txDescriptifTranchesPrevues)
        d_t["dm_projet_desc"] = cast_str(d.txDescriptifTravauxSurConstructionRestante)
        d_t["tax_desc"] = cast_str(d.txRenseignementsAFinaliteFiscale)

        d_t["x1p_precisions"] = cast_str(d.txInfosLegiConnexesPrecisions)
        d_t["u2a_observations"] = cast_str(d.txObservations)
        d_t["u3t_observations"] = cast_str(d.txObservationsMairie)
        d_t["c2zr1_destination"] = cast_str(d.destinationElectriciteProduite)
        d_t["c2zp1_crete"] = cast_str(d.puissanceCreteProduite)

        # txsAutresPrecisionsNatTrav
        if d.txsAutresPrecisionsNatTrav:
            d_t["co_trx_autre"] = " ".join(
                [str(item) for item in d.txsAutresPrecisionsNatTrav if item is not None]
            )

        d_t["vsd_val_terr"] = cast_str(d.valeurM2NuEtLibre)

        # We may have not d.terrains :
        if not d.terrains:
            return d_t

        # We have data that comes from d.terrains :
        #  boEstSitueDansUnLotissement, boEstSitueDansUneOIN, boEstSitueDansUneZAC
        lotissement = None
        oin = None
        zac = None
        for t in d.terrains:
            if t.boEstSitueDansUnLotissement is not None:
                lotissement = lotissement or t.boEstSitueDansUnLotissement
            if t.boEstSitueDansUneOIN is not None:
                oin = oin or t.boEstSitueDansUneOIN
            if t.boEstSitueDansUneZAC is not None:
                zac = zac or t.boEstSitueDansUneZAC

        d_t["terr_juri_lot"] = cast_boolean(lotissement, textual=True)
        d_t["terr_juri_oin"] = cast_boolean(oin, textual=True)
        d_t["terr_juri_zac"] = cast_boolean(zac, textual=True)

        return d_t


class ConsultationMapper(BaseDossierMapper):
    """Main source is a ConsultationR. Two others sources are available :
        - self.service_consultant, a ReponseActeurRecherche
        - self.infofiscales, a ReponseInfoFiscalesRecherche
    """

    mapped: dict = {}
    keys: tuple = [
        "donnees_techniques",
        "demandeur",
        "dossier",
        "external_uids",
        "consultation",
    ]

    def _map_dossier(self) -> dict:
        """."""
        d = self.source.dossier
        cs = self.source.dossier.consultations[0]
        _dossier_mapping = OPENADS_MAPPING["TYPE_DOSSIER"]

        dossier = super()._map_dossier()

        dossier["acteur"] = cs.idServiceConsulte

        # XP 1.4 - old consultation may not provides valued hdPremiereConsultation
        if cs.hdPremiereConsultation is not None:
            dossier["date_demande"] = cast_date(cs.hdPremiereConsultation)

        # XXX - noLocal is null XP 1.3
        # We should check is this is a valid business usecase
        if d.noLocal is None:
            dossier["dossier"] = "%s%s_%s" % (
                _dossier_mapping[d.nomTypeDossier.idNom],
                d.idCommuneDepot,
                d.idDossier,
            )
            log.warning(
                f"Dossier {d.idDossier} : noLocal not provided. "
                f"We generated an arbitrary one : {dossier['dossier']}"
            )

        return dossier

    def _map_consultation(self) -> dict:
        """."""
        cs = self.source.dossier.consultations[0]
        s_c = self.service_consultant  # service_consultant

        consultation = {
            "delai_reponse": cast_str(cs.delaiDeReponse),
            "type_delai": cs.nomTypeDelai and cs.nomTypeDelai.libNom or "",
            "date_consultation": cast_date(cs.dtConsultation),
            "date_emission": cast_date(cs.dtEmission),
            "service_consultant_id": cs.idServiceConsultant,
            "service_consultant_libelle": cast_str(s_c.designationActeur),
            "service_consultant_insee": cast_str(s_c.idCommuneRattachement),
            "service_consultant_mail": cast_str(s_c.mail),
            "service_consultant_type": s_c.nomTypeActeur
            and s_c.nomTypeActeur.libNom
            or "",
            "service_consultant__siren": cast_str(s_c.siren),
            # both following items will only be displayed in openADS, no mapping needed
            "etat_consultation": cs.nomEtatConsultation
            and cs.nomEtatConsultation.libNom
            or "",
            "type_consultation": cs.nomEtatConsultation
            and cs.nomTypeConsultation.libNom
            or "",
            "objet_consultation": cs.nomObjetConsultation
            and cs.nomObjetConsultation.libNom
            or "",
            "texte_fondement_reglementaire": cast_str(cs.txFondementReglementaire),
            "texte_objet_consultation": cast_str(cs.txObjetDeLaConsultation),
            "date_production_notification": cast_date(cs.hdProductionNotification),
            "date_premiere_consultation": cast_date(cs.hdPremiereConsultation),
        }
        return consultation

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier_autorisation": self.source.projet.idProjet,
            "dossier": self.source.dossier.idDossier,
            "dossier_consultation": self.source.dossier.consultations[0].idConsultation,
            "acteur": self.source.dossier.consultations[0].idServiceConsulte,
        }
        return external_uids


class DossierADAUMapper(BaseDossierMapper):
    """Main source is a DossierR. :
    """

    mapped: dict = {}
    keys: tuple = [
        "donnees_techniques",
        "demandeur",
        "dossier",
        "external_uids",
    ]

    # def _map_dossier(self) -> dict:
    #     """."""
    #     d = self.source.dossier
    #     _dossier_mapping = OPENADS_MAPPING["TYPE_DOSSIER"]

    #     dossier = super()._map_dossier()

    # dossier["acteur"] = cs.idServiceConsulte

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier_autorisation": self.source.projet.idProjet,
            "dossier": self.source.dossier.idDossier,
            "acteur": self.actor_uid,
            "adau": self.source.dossier.noTeledemarcheADAU,
        }
        return external_uids


class BasePieceMapper(OADSMapper):
    """Main source is a PieceR. One other source is available :
        - self.opt_source, a BravadoR with a projet and dossier attributes
    """

    mapped: dict = {}
    # keys: tuple = ["document_numerise", "dossier", "external_uids"]
    keys: tuple = ["document_numerise", "external_uids"]
    opt_source = None

    def _map_document_numerise(self):
        """."""
        _nature = OPENADS_MAPPING["NATURE_PIECE"]
        _type = OPENADS_MAPPING["TYPE_PIECE"]

        if self.source.dtDepot:
            date_depot = self.source.dtDepot

        # following FWE feedbacks, dtProduction should not be used here.
        # elif self.source.dtProduction:
        #     date_depot = self.source.dtProduction

        # Neither dtDepot nor dtProduction is provided. We have to consider if the Piece
        # is Initiale or Complementaire
        else:
            if (
                self.source.nomNaturePiece.idNom == 1
                and self.opt_source.dossier.dtDepot
            ):
                # Initiale
                date_depot = self.opt_source.dossier.dtDepot
            else:  # Complementaire
                # XXX - as dtDepot nor dtProduction are not mandatory, we'll use today's
                # date
                date_depot = datetime.now()

        if self.source.noPiece is not None and self.source.noPiece != "0":
            base_file_name, file_extension = os.path.splitext(
                self.source.attachment.name
            )
            file_name = f"{base_file_name}-{self.source.noPiece}{file_extension}"
        else:
            file_name = self.source.attachment.name

        result = {
            "date_creation": cast_date(date_depot),
            "document_numerise_nature": self.source.nomNaturePiece
            and _nature[self.source.nomNaturePiece.idNom],
            "document_numerise_type_code": self.source.nomTypePiece
            and _type[self.source.nomTypePiece.idNom],
            "nom_fichier": file_name,
            "file_content_type": self.source.attachment.content_type,
        }
        # XXX - Piece Modicative will be arbitrary mapped to Piece Complementaire
        # Has to be changed in the future.
        # From Nomenclature_TYPE_PIECE :
        # 2,"Complémentaire",04/12/2019
        # 3,"Modificative",04/12/2019
        if self.source.nomNaturePiece and self.source.nomNaturePiece.idNom == 3:
            result["document_numerise_nature"] = 2
        # Piece "Remplacement d'une pièce initiale" should be mapped to Piece Initiale
        # From Nomenclature_TYPE_PIECE :
        # 1,"Initiale",04/12/2019
        # 4,"Remplacement d'une pièce initiale",04/05/2023
        if self.source.nomNaturePiece and self.source.nomNaturePiece.idNom == 4:
            result["document_numerise_nature"] = 1

        with open(self.source.attachment.path, "rb") as f:
            result["file_content"] = base64.encodebytes(f.read()).decode("utf-8")

        return result


class ConsultationPieceMapper(BasePieceMapper):
    """Main source is a PieceR. One other source is available :
        - self.opt_source, a ConsultationR
    """

    mapped: dict = {}
    # keys: tuple = ["document_numerise", "dossier", "external_uids"]
    keys: tuple = ["document_numerise", "external_uids"]

    def _map_external_uids(self):
        """."""
        consultation = self.opt_source.dossier.consultations[0]
        external_uids = {
            "dossier": self.source.idDossier,
            "piece": self.source.idPiece,
            "dossier_consultation": consultation.idConsultation,
            "acteur": self.actor_uid,
        }
        return external_uids


class DossierADAUPieceMapper(BasePieceMapper):
    """Main source is a PieceR. One other source is available :
        - self.opt_source, a DossierR
    """

    mapped: dict = {}
    # keys: tuple = ["document_numerise", "dossier", "external_uids"]
    keys: tuple = ["document_numerise", "external_uids"]

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier": self.source.idDossier,
            "piece": self.source.idPiece,
            "acteur": self.actor_uid,
        }
        return external_uids


class PecMapper(OADSMapper):
    """Main source is a PecMetierR. One other source is available :
        - self.consultation, a ConsultationR
    """

    mapped: dict = {}
    keys: tuple = ["pec_metier", "external_uids", "document_numerise"]

    def _map_pec_metier(self):
        """."""
        p = self.source.pecMetiers[0]
        c = self.consultation.dossier.consultations[0]
        _statut = OPENADS_MAPPING["STATUT_PEC_METIER"]
        _type_piece = OPENADS_MAPPING["TYPE_PIECE"]
        return {
            "date_pec_metier": cast_date(p.dtPecMetier),
            # "date_reception": c.hdPremiereConsultation
            # and cast_date(c.hdPremiereConsultation)
            # or cast_date(c.hdProductionNotification),
            "date_reception": cast_date(c.hdProductionNotification),
            "date_limite_prescrire": cast_date(p.dtLimitePourPrescrire),
            "date_limite_reponse": cast_date(p.dtLimiteReponse),
            "service_consulte": p.idActeurGenerateur,
            "statut_pec_metier": _statut[p.nomStatutPecMetier.idNom],
            "texte_observations": cast_str(p.txObservations),
            "types_pieces_manquantes": p.nomTypesPiecesManquantes
            and [_type_piece[tp.idNom] for tp in p.nomTypesPiecesManquantes]
            or None,
        }

    def _map_document_numerise(self):
        """."""
        documents = self.source.pecMetiers[0].documents
        result = {}
        # openADS supports only one attachment to a PeC
        if documents is not None and len(documents) >= 1:
            result = {
                "nom_fichier": documents[0].file_name,
                "file_content_type": documents[0].content_type,
            }
            with open(documents[0].file_path, "rb") as f:
                result["file_content"] = base64.encodebytes(f.read()).decode("utf-8")

        return result

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier": self.consultation.dossier.idDossier,
            "consultation": self.consultation.dossier.consultations[0].idConsultation,
            "pec_metier": self.source.pecMetiers[0].idPecMetier,
        }
        return external_uids


class AvisMapper(OADSMapper):
    """Main source is a AvisR.
    """

    mapped: dict = {}
    keys: tuple = ["avis", "external_uids", "document_numerise"]

    def _map_avis(self):
        """."""
        avis = self.source.dossier.avis[0]
        # c = self.consultation.dossier.consultations[0]
        _nature_avis = OPENADS_MAPPING["NATURE_AVIS"]

        if avis.boEstTacite is not None and avis.boEstTacite:
            avis_consultation = "4"  # `Tacite` in openADS
        elif avis.idAvis is not None:
            # cf. #GTLB19, avis from AVIS'AU might provide a nomNatureAvisRendu at None
            if avis.nomNatureAvisRendu is None:
                avis_consultation = "5"
            else:
                avis_consultation = _nature_avis[avis.nomNatureAvisRendu.idNom]
        else:
            avis_consultation = "5"  # `Autre` in openADS

        return {
            # "date_avis": cast_date(avis.dtAvis),
            "date_avis": cast_date(datetime.now()),
            "date_emission": cast_date(avis.dtEmission),
            "texte_fondement_avis": cast_str(avis.txFondementAvis),
            "texte_avis": cast_str(avis.txAvis),
            "texte_hypotheses": cast_str(avis.txHypotheses),
            "nom_auteur": cast_str(avis.nomAuteur, max_length=255),
            "prenom_auteur": cast_str(avis.prenomAuteur, max_length=255),
            "qualite_auteur": cast_str(avis.txQualiteAuteur, max_length=255),
            "avis_consultation": avis_consultation,
        }

    def _map_document_numerise(self):
        """."""
        documents = self.source.dossier.avis[0].documents
        result = {}
        # openADS supports only one attachment to an Avis
        if documents is not None and len(documents) >= 1:
            result = {
                "nom_fichier": documents[0].file_name,
                "file_content_type": documents[0].content_type,
            }
            with open(documents[0].file_path, "rb") as f:
                result["file_content"] = base64.encodebytes(f.read()).decode("utf-8")

        return result

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier": self.source.dossier.idDossier,
            "consultation": self.source.dossier.avis[0].idConsultation,
            "avis": self.source.dossier.avis[0].idAvis,
        }
        return external_uids


class MessageMapper(OADSMapper):
    """Main source is a Dossier UID. Others sources available :
        - self.event, a PlatAUEvent object
        - self.type, the message type, string, mandatory
        - self.content, will override message content if provided
        - self.actor, target actor of the message
    """

    mapped: dict = {}
    keys: tuple = ["message", "external_uids"]
    source: str

    def __init__(self, dossier_uid: str, **opt_sources,) -> None:
        """."""
        self.source = dossier_uid

        for source_name, value in opt_sources.items():
            setattr(self, source_name, value)

    def _map_message(self):
        """."""
        message = {
            "type": cast_str(self.type, max_length=60),
            "destinataire": "instructeur",
            "categorie": "platau",
        }
        # Priority to content if provided
        if getattr(self, "content", None):
            message["contenu"] = self.content
        # No content -> we use the event label provided by Plat'AU
        elif getattr(self, "event", None):
            message["contenu"] = self.event.type_label
        # Anyway, we should provide a content
        else:
            message["contenu"] = "N/A"

        return message

    def _map_external_uids(self):
        """."""
        external_uids = {
            "dossier": self.source,
        }
        if getattr(self, "actor"):
            external_uids.update({"acteur": self.actor.uid})

        return external_uids


def secure_join(iterable, separator):
    """Secure string join. Join a currated `iterable` list with `separator`"""
    if iterable is None:
        return ""
    curred_iterable = [item for item in iterable if isinstance(item, str)]
    return separator.join(curred_iterable)


def cast_boolean(value: bool, textual: bool = False):
    """Boolean fields in opennADS can be :
        `t`, `f`, `oui`, `non`
        Setting `textual` to True wil use `oui|non|nesaispas` values
    """
    if value is None and not textual:
        return ""
    elif value is None and textual:
        return "nesaispas"
    elif value and not textual:
        return "t"
    elif value and textual:
        return "oui"
    elif not value and not textual:
        return "f"
    elif not value and textual:
        return "non"


def cast_date(value):
    """."""
    if value is None or not (isinstance(value, date) or isinstance(value, datetime)):
        return ""
    else:
        return value.strftime("%Y-%m-%d")


def cast_str(value, max_length: int = None, mandatory_value: bool = False) -> str:
    """Carefully cast a value to a str one.

        :param value: the value to be casted

        :param max_length: if set, outp string will be shortened to `max_length` characters.

        :param mandatory_value: sometime openADS does not support "" value. If set, a
            "N/C" value will be returned (meaning "Non communiqué")

        :return str: a casted string.
    """
    casted = ""
    if value is None:
        if mandatory_value:
            return "N/C"
        else:
            return ""
    elif not isinstance(value, str):
        casted = str(value).replace("<", "&#60;").replace(">", "&#62;")
    else:
        casted = value.replace("<", "&#60;").replace(">", "&#62;")

    if max_length is not None:
        return casted[:max_length]
    else:
        return casted


def _log(msg, level="debug"):
    """."""
    getattr(log, level)(msg)
    print("{}: {}".format(level, msg))
