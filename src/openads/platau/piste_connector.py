import logging
import os
import requests
import six
import time

# import re
# from cachetools import cached, TTLCache
from oauthlib.oauth2 import BackendApplicationClient
from datetime import datetime, timedelta
from requests_oauthlib import OAuth2Session
from bravado.client import SwaggerClient
from bravado.swagger_model import load_file
from bravado.requests_client import RequestsClient, RequestsFutureAdapter
from six import iteritems

from openads.platau import PACKAGE_VERSION
from openads.platau import PISTE_TIMEOUT

log = logging.getLogger(__name__)

DRY_SAFE = os.getenv("piste_dry_safe", None)

# Maximum time before token renewal
MAX_TIME_BEFORE_TOKEN_RENEWAL = 15  # in minutes

# Time to wait before retry when receiving a 429 HTTP code
CODE_429_SLEEPING_INTERVAL = 2  # in seconds


class PISTEConnector(object):
    """Base class for PISTE-based API connector

        `credentials` should match the following structure :
        {
            "default": {"client_id": "XXX", "client_secret": "YYY"},
            "CREDENTIAL_KEY": {"client_id": "XXX", "client_secret": "YYY"},
            # [...]
        }
        where `default` key is mandatory.

    """

    credentials: dict
    auth_scope = "openid"

    spec_path: str
    spec_overrides: str = None
    specs: dict

    token: str = None
    dry_safe: bool = False
    token_url: str
    app_host: str

    software: str = "openADS"
    software_version: str = PACKAGE_VERSION

    def __init__(
        self,
        credentials: dict,
        spec_path: str = None,
        spec_overrides: dict = None,
        dry_safe: bool = False,
        bravado_config: dict = None,
        **kwargs,
    ):
        """."""
        self.credentials = credentials

        if spec_path:
            self.spec_path = spec_path

        if self.spec_path is None:
            raise NotImplementedError(
                "PISTE-based connector cannot work without specs."
            )
        # Loading specs as a dict
        self.specs = load_file(self.spec_path)

        # We allow overriding few spec properties
        if spec_overrides:
            self.spec_overrides = spec_overrides
        if self.spec_overrides:

            # api_path can be overrided
            if "api_path" in spec_overrides:
                self.specs["basePath"] = spec_overrides["api_path"]

            # if target api is a sandbox, we have to overrides host urls
            if "sandbox" in spec_overrides and spec_overrides["sandbox"]:
                self.specs["host"] = "sandbox-" + self.specs["host"]
                self.specs["securityDefinitions"]["OAuthAccessCode"][
                    "authorizationUrl"
                ] = self.specs["securityDefinitions"]["OAuthAccessCode"][
                    "authorizationUrl"
                ].replace(
                    "https://", "https://sandbox-"
                )
                self.specs["securityDefinitions"]["OAuthAccessCode"][
                    "tokenUrl"
                ] = self.specs["securityDefinitions"]["OAuthAccessCode"][
                    "tokenUrl"
                ].replace(
                    "https://", "https://sandbox-"
                )
                self.specs["securityDefinitions"]["OAuthImplicit"][
                    "authorizationUrl"
                ] = self.specs["securityDefinitions"]["OAuthImplicit"][
                    "authorizationUrl"
                ].replace(
                    "https://", "https://sandbox-"
                )

        self.app_host = self.specs["host"]
        self.token_url = self.specs["securityDefinitions"]["OAuthAccessCode"][
            "tokenUrl"
        ]

        # General config
        self.bravado_config = {
            "validate_requests": True,
            "validate_responses": False,
            "validate_swagger_spec": False,
            "use_models": True,
        }
        if bravado_config:
            self.bravado_config.update(bravado_config)

        if dry_safe or DRY_SAFE is not None:
            self.dry_safe = True

    def start(self, credentials_key: str = None):
        """."""

        if credentials_key is None or credentials_key not in self.credentials:
            credentials = self.credentials["default"]
        else:
            credentials = self.credentials[credentials_key]

        if self.token is not None:

            expires_at = datetime.fromtimestamp(self.token["expires_at"])
            token_still_valid = expires_at > (
                datetime.now() + timedelta(minutes=MAX_TIME_BEFORE_TOKEN_RENEWAL)
            )

            if not token_still_valid:
                log.debug("Token too close to expiration")
                self._authenticate(credentials)

            elif self.token["client_id"] != credentials["client_id"]:
                log.debug("`client_id` is different")
                self._authenticate(credentials)

            else:
                log.debug(
                    "`client_id` : OK / expiration date : OK. No need to authenticate"
                )

        else:
            self._authenticate(credentials)

        if self.dry_safe:
            http_client = RequestsClient(
                future_adapter_class=DrySafeLoggerFutureAdapter
            )
        else:
            # Provides token authentication via a specific HTTPClient
            http_client = RequestsClient(future_adapter_class=LoggerFutureAdapter)
            http_client.set_api_key(
                self.app_host,
                self.token["token_type"] + " " + self.token["access_token"],
                param_name="Authorization",
                param_in="header",
            )

        # Let's get the client API from the localy Swagger 2.0 file,
        log.debug("{0} will be used as specifications".format(self.spec_path))
        self.client = SwaggerClient.from_spec(
            self.specs, config=self.bravado_config, http_client=http_client
        )

    def _authenticate(self, credentials: dict):
        """."""
        log.debug("Authentication is needed")
        log.debug(
            "Authentication starting using id : {} and secret : {}".format(
                credentials["client_id"], credentials["client_secret"]
            )
        )

        client = BackendApplicationClient(client_id=credentials["client_id"])
        oauth = OAuth2Session(client=client)
        self.token = oauth.fetch_token(
            token_url=self.token_url,
            client_id=credentials["client_id"],
            client_secret=credentials["client_secret"],
            include_client_id=True,
            scope=self.auth_scope,
        )
        self.auth_scope = self.token["scope"]
        self.token["client_id"] = credentials["client_id"]

        oauth.close()
        log.debug("Current token : %s" % self.token)


class LoggerFutureAdapter(RequestsFutureAdapter):
    """."""

    def result(self, timeout=None):
        """."""
        request = self.request

        # Ensure that all the headers are converted to strings.
        # This is need to workaround https://github.com/requests/requests/issues/3491
        request.headers = {
            k: str(v) if not isinstance(v, six.binary_type) else v
            for k, v in iteritems(request.headers)
        }

        prepared_request = self.session.prepare_request(request)

        self._request_dumpper(prepared_request)

        settings = self.session.merge_environment_settings(
            prepared_request.url,
            proxies={},
            stream=None,
            verify=self.misc_options["ssl_verify"],
            cert=self.misc_options["ssl_cert"],
        )

        for i in range(30):
            # To avoid `429 Too Many Requests`, we will rest until 60+ seconds
            # We should take care of PISTE call quotas :
            #  - by seconds : 20/s
            #  - by minutes : ??

            response = self.session.send(
                prepared_request,
                timeout=PISTE_TIMEOUT,
                allow_redirects=self.misc_options["follow_redirects"],
                **settings,
            )

            if response.status_code == 429:
                # `429 Too Many Requests` case
                # let's get some rest before retrying.
                log.warning(
                    "`429 Too Many Requests` received. "
                    f"Let's sleep {CODE_429_SLEEPING_INTERVAL} seconds"
                )
                time.sleep(CODE_429_SLEEPING_INTERVAL)
                continue

            else:
                # Nominal case
                break

        self._response_dumpper(response)

        return response

    def _request_dumpper(self, request):
        """."""
        msg = f"REQUEST : {request.method} {request.url}"
        if "Id-Acteur-Appelant" in request.headers:
            msg += f" Id-Acteur-Appelant={request.headers['Id-Acteur-Appelant']}"
        log.info(msg)
        if request.body is not None:

            payload = request.body
            # # XXX get verbsosity
            # if len(request.body) > 256: and log.getEffectiveLevel() >= 20:
            #     # 20 == INFO mode
            #     payload = payload[:256] + "..."

            log.info(f"Payload : {payload}")
            # print(f"Payload : {payload}")

    def _response_dumpper(self, response):
        """."""
        log.info(f"RESPONSE : {response.status_code} {response.reason}")
        if response.content is not None:

            payload = response.content.decode("utf-8")
            # # XXX get verbsosity
            # if len(payload) > 256 and log.getEffectiveLevel() >= 20:
            #     # 20 == INFO mode
            #     payload = payload[:256] + "..."

            log.info(f"Payload : {payload}")
            # print(f"Payload : {payload}")


class DrySafeLoggerFutureAdapter(LoggerFutureAdapter):
    """."""

    def result(self, timeout=None):
        """."""
        request = self.request

        # Ensure that all the headers are converted to strings.
        # This is need to workaround https://github.com/requests/requests/issues/3491
        request.headers = {
            k: str(v) if not isinstance(v, six.binary_type) else v
            for k, v in iteritems(request.headers)
        }

        prepared_request = self.session.prepare_request(request)

        self._request_dumpper(prepared_request)

        # Fake empty response
        response = requests.models.Response()
        response.status_code = 200

        return response
