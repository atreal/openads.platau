import logging

from dataclasses import dataclass
from datetime import datetime

from openads.platau.openads_driver import OpenADSDriver, Attachment, OpenADSError
from openads.platau.publik2openads_mapper import (
    DemandeInitialeMapper,
    VanillaDemandeDossierExistantMapper,
    DemandePieceCompSuppMapper,
    DemandeTransfertMapper,
    DemandeModificatifMapper,
    DemandeDOCMapper,
    DemandeDAACTMapper,
    CerfaMapper,
    PiecesMapper,
)
from openads.platau.publik_connector import PublikConnector

log = logging.getLogger(__name__)

DEMANDE_TYPE_SUIVI = "suivi-de-demande"

TASKS_WITH_ATTACHMENT = [
    "notification_recepisse",
    "notification_instruction",
    "notification_decision",
]

WORKFLOW = {
    "handle_demande_initiale": "transferer_demande",
    "handle_demande_dossier_existant": "transferer_demande",
    "handle_notification_instruction": "ajout_document",
    "close": "cloturer_demande",
    "failure": "echec_demande",
}

CODE_TYPE_DEMANDE_COMPLEMENTAIRE = "dpc"
CODE_TYPE_DEMANDE_SUPPLEMENTAIRE = "dps"


class Task(object):
    """."""

    openads: OpenADSDriver
    connector: PublikConnector = None

    def __init__(
        self, openads: OpenADSDriver, connector: PublikConnector, **args,
    ):
        """."""
        self.openads = openads
        self.connector = connector


@dataclass()
class OutputTask(Task):
    """."""

    task_id: str
    typology: str
    payload: str
    state: str
    message: str
    demande_uid: str
    source_depot: str
    base_payload: dict
    attachment: Attachment = None

    def __init__(
        self, openads: OpenADSDriver, connector: PublikConnector, task_id: str
    ):
        """Task builder.
        First we should retrieve task full payload, file if necessary.
        """
        super().__init__(openads, connector)

        self.task_id = task_id
        log.info(f"Wrapping task : {task_id}")

        self.state = "pending"
        if not self.openads.update_task(self.task_id, self.state):
            log.error(f"Unable to update task {self.task_id} with state {self.state}")
            return

        self.payload = self.openads.get_task(self.task_id)
        if self.payload is None:
            self.state = "error"
            log.error(f"Unable to retrieve payload for task {self.task_id}")
            if not self.openads.update_task(self.task_id, self.state):
                log.error(
                    f"Unable to update task {self.task_id} with state {self.state}"
                )
            return

        self.typology = self.payload["task"]["type"]
        self.demande_uid = self.payload["external_uids"]["demande"]
        self.source_depot = self.payload["dossier"]["source_depot"]

        # Set common data that will be used for each Publik request
        self.base_payload = {}
        self.base_payload["numero_dossier"] = self.payload["dossier"]["dossier"]
        # (list -> set -> list) allow us to avoid duplicates
        self.base_payload["emails_destinataires"] = ",".join(
            list(
                set(
                    [
                        personne["courriel"]
                        for personne in self.payload["demandeur"].values()
                        if personne["courriel"]
                    ]
                )
            )
        )

        if self.typology in TASKS_WITH_ATTACHMENT:
            try:
                self.attachment = self.openads.get_file(self.task_id)
            except Exception as e:
                self.state = "error"
                log.error(f"Unable to download file for task {task_id} : {e}")
                if not self.openads.update_task(self.task_id, self.state):
                    log.error(
                        f"Unable to update task {self.task_id} with state {self.state}"
                    )
            if self.attachment is None:
                log.info(
                    f"Task {self.task_id} of type {self.typology} did not provide any attachment"
                )

    def run(self):
        """."""
        previous_state = self.state
        try:
            log.info(f"Running task `{self.typology}` : {self.task_id}")

            if not self.demande_uid:
                # This means Demande does not exist on Publik.
                # Few thing must be done in order to follow with regular handlers:
                #  + create a SuiviDeDemande form, obviously
                #  + retreive and provide tracking-code to openADS

                # Dossier comes from openADS or Plat'AU
                if self.source_depot == "portal":
                    raise OpenADSError(
                        "Can't proceed task with source depot `portal` and empty Demande UID"
                    )

                # Demande does not exist in portal, we have to create a Demande first
                demande = {}
                # We need a Prenom, Nom and email
                for personne in self.payload["demandeur"].values():
                    if (
                        personne.get("type_demandeur") == "petitionnaire"
                        and personne.get("petitionnaire_principal") == "t"
                    ):

                        demande["coordonnees_courriel"] = personne["courriel"]
                        if not demande["coordonnees_courriel"]:
                            continue

                        if personne.get("qualite") == "particulier":
                            demande["identite_nom"] = personne["particulier_nom"]
                            demande["identite_prenom"] = personne["particulier_prenom"]

                        elif personne.get("qualite") == "personne_morale":
                            demande["identite_nom"] = personne["personne_morale_nom"]
                            demande["identite_prenom"] = personne[
                                "personne_morale_prenom"
                            ]

                        # We have a contact, no need to go further
                        break

                if not demande:
                    # With no contact, Demande can't be created on Publik
                    raise OpenADSError("Can't create Demande without contact info")

                demande["source_depot"] = self.source_depot

                # Data is fully populated. Let's create a Deamde
                response = self.connector.create_demande(DEMANDE_TYPE_SUIVI, demande)
                self.demande_uid = f"{DEMANDE_TYPE_SUIVI}/{response['id']}"
                log.info(f"Demande {self.demande_uid} created on Publik")

                # Demande is OK. Let's retrieve tracking code
                publik_demande = self.connector.get_demande(
                    DEMANDE_TYPE_SUIVI, response["id"]
                )
                try:
                    # tracking code is set in workflow treatment properte
                    code_suivi = publik_demande["workflow"]["fields"]["code_suivi"]
                except KeyError:
                    code_suivi = None

                if code_suivi:
                    # tracking coed is pushed to openADS with the standard UID update
                    # method, but using a protocol as a prefix
                    if not self.openads.update_task(
                        self.task_id, self.state, f"code-suivi://{code_suivi}"
                    ):
                        raise OpenADSError(
                            f"Demande {self.demande_uid}: "
                            "could not update `code_suivi` on openADS"
                        )
                    log.info(
                        f"Demande {self.demande_uid}: `code_suivi` {code_suivi} "
                        "successfully pushed to openADS"
                    )

            if self.typology in TASKS_WITH_ATTACHMENT:
                handler = getattr(self, "handle_notification_document", None)
            else:
                handler = getattr(self, f"handle_{self.typology}", None)

            if handler is None:
                raise NotImplementedError(f"handle_{self.typology} not implemented.")

            handler()

        except Exception as e:
            log.error(e, exc_info=True)
            self.state = "error"

        if previous_state != self.state or self.demande_uid is not None:

            if not self.openads.update_task(self.task_id, self.state, self.demande_uid):
                raise OpenADSError("Could not update task state on openADS")

    def handle_notification_document(self):
        """."""
        data = self.base_payload.copy()
        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        document_type = self.typology.replace("handle_notification_", "")

        if self.attachment.content_type == "application/zip":
            self.attachment.name = f"{data['numero_dossier']}-{document_type}_{now}.zip"
        else:
            self.attachment.name = f"{data['numero_dossier']}-{document_type}_{now}.pdf"
        data["document"] = self.attachment

        if self.source_depot not in ("portal", "app", "platau"):
            raise NotImplementedError(f"Unknown source_depot {self.source_depot}")

        next_transition = WORKFLOW["handle_notification_instruction"]

        self.connector.trigger_transition(self.demande_uid, next_transition, data)

        # Legacy code. A Demande should only be archived on legal terms
        #
        # if self.payload["instruction"].get("final", "") == "t":
        #     # This is a final instruction, Demande can be closed
        #     next_transition = WORKFLOW["close"]
        #     self.connector.trigger_transition(self.demande_uid, next_transition)

        self.state = "done"


@dataclass
class InputTask(Task):
    """."""

    typology: str
    transition: str = None

    def __init__(
        self,
        form_slug: str,
        form_type: str,
        demande: str,
        openads: OpenADSDriver,
        connector: PublikConnector = None,
    ):
        """Task builder.
        First we should retrieve task full payload, file if necessary.
        """
        super().__init__(openads, connector)
        self.demande_summary = demande
        self.typology = form_type

        self.demande = self.connector.get_demande(
            form_slug, self.demande_summary["id"]
        )
        self.demande["demande_id"] = f"{form_slug}/{self.demande_summary['id']}"

        # Prepare piece if needed
        # Dropped empty file if any:
        clean_piece_raw = []
        # First, we should check if the demande provides piece_raw property.
        # Depending of the form on Publik, value can be None or a list
        if "pieces_raw" in self.demande["fields"] and isinstance(
            self.demande["fields"]["pieces_raw"], list
        ):
            for piece in self.demande["fields"]["pieces_raw"]:
                # Then we clean useless entries
                if piece["fichier"] is not None:
                    clean_piece_raw.append(piece)
            self.demande["fields"]["pieces_raw"] = clean_piece_raw

    def run(self):
        """."""
        handler = getattr(self, f"handle_{self.typology}", None)
        if handler is None:
            raise NotImplementedError

        log.info(f"handle_{self.typology}")
        try:
            handler()
        except Exception as e:
            log.error(e, exc_info=True)
            self.transition = WORKFLOW["failure"]

        self.connector.trigger_transition(self.demande["demande_id"], self.transition)

    def handle_demande_initiale(self):
        """."""
        mapped_demande = DemandeInitialeMapper(self.demande).render()
        self.openads.create_task("create_DI", mapped_demande, category="portal")

        mapped_cerfa = CerfaMapper(self.demande).render()
        self.openads.create_task("add_piece", mapped_cerfa, category="portal")

        mapped_pieces = PiecesMapper(
            self.demande, multi_field_key="pieces_raw"
        ).render()
        for piece in mapped_pieces:
            self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_initiale"]

    def handle_dossier_existant(self):
        """WIP"""
        mapped_demande = VanillaDemandeDossierExistantMapper(self.demande).render()

        self.openads.create_task("create_DI", mapped_demande, category="portal")

        if "pieces_raw" in self.demande["fields"]:
            mapped_pieces = PiecesMapper(
                self.demande, multi_field_key="pieces_raw"
            ).render()
            for piece in mapped_pieces:
                self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_dossier_existant"]

    def handle_pieces_complementaires(self):
        """WIP"""
        mapped_demande = DemandePieceCompSuppMapper(self.demande).render()

        # Should we create a Piece Complementaire or Supplemnatire
        result = self.openads.depot_possible(
            mapped_demande["demande"]["dossier_initial"],
            CODE_TYPE_DEMANDE_COMPLEMENTAIRE,
        )

        if result is None or "depot_possible" not in result:
            msg = (
                f"Failed to guess Pieces operation type on Dossier `{mapped_demande['demande']['dossier_initial']}`. "
                f"Demande `{self.demande['demande_id']}` will not be transmitted"
            )
            raise RuntimeError(msg)

        elif result["depot_possible"]:
            mapped_demande["demande"]["type_demande"] = CODE_TYPE_DEMANDE_COMPLEMENTAIRE

        else:
            mapped_demande["demande"]["type_demande"] = CODE_TYPE_DEMANDE_SUPPLEMENTAIRE

        self.openads.create_task("create_DI", mapped_demande, category="portal")

        if "pieces_raw" in self.demande["fields"]:

            # Mapper need to now if it's a DEMANDE_COMPLEMENTAIRE or a DEMANDE_SUPPLEMENTAIRE
            self.demande["type_demande"] = mapped_demande["demande"]["type_demande"]

            mapped_pieces = PiecesMapper(
                self.demande, multi_field_key="pieces_raw"
            ).render()
            for piece in mapped_pieces:
                self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_dossier_existant"]

    def handle_transfert(self):
        """."""
        mapped_demande = DemandeTransfertMapper(self.demande).render()
        self.openads.create_task("create_DI", mapped_demande, category="portal")

        mapped_cerfa = CerfaMapper(self.demande).render()
        self.openads.create_task("add_piece", mapped_cerfa, category="portal")

        mapped_pieces = PiecesMapper(
            self.demande, multi_field_key="pieces_raw"
        ).render()
        for piece in mapped_pieces:
            self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_initiale"]

    def handle_modificatif(self):
        """."""
        mapped_demande = DemandeModificatifMapper(self.demande).render()
        self.openads.create_task("create_DI", mapped_demande, category="portal")

        mapped_cerfa = CerfaMapper(self.demande).render()
        self.openads.create_task("add_piece", mapped_cerfa, category="portal")

        mapped_pieces = PiecesMapper(
            self.demande, multi_field_key="pieces_raw"
        ).render()
        for piece in mapped_pieces:
            self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_initiale"]

    def handle_doc(self):
        """WIP"""
        mapped_demande = DemandeDOCMapper(self.demande).render()
        self.openads.create_task("create_DI", mapped_demande, category="portal")

        self.transition = WORKFLOW["handle_demande_dossier_existant"]

    def handle_daact(self):
        """WIP"""
        mapped_demande = DemandeDAACTMapper(self.demande).render()
        self.openads.create_task("create_DI", mapped_demande, category="portal")

        mapped_pieces = PiecesMapper(
            self.demande, multi_field_key="pieces_raw"
        ).render()
        for piece in mapped_pieces:
            self.openads.create_task("add_piece", piece, category="portal")

        self.transition = WORKFLOW["handle_demande_dossier_existant"]
