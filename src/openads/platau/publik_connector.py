import base64
import logging
import requests
import json

from dataclasses import dataclass

from openads.platau.openads_driver import Attachment


log = logging.getLogger(__name__)


class PublikError(RuntimeError):
    pass


@dataclass
class PublikConnector:
    """."""

    url: str
    auth: tuple
    s: requests.Session

    def __init__(self, url: str, auth: tuple):
        """."""
        self.url = url
        self.auth = auth
        log.info(f"Publik will be reach at {url} with user {self.auth[0]}")

        self.s = requests.Session()
        self.s.auth = self.auth
        self.s.headers.update(
            {"Accept": "application/json", "Content-type": "application/json"}
        )

        try:
            self.s.get(url)
        # do we have an auto-signed cert ? If True, we'll authorize it.
        except requests.exceptions.SSLError:
            self.s.verify = False

    def status(self):
        """."""
        log.info(f"Asking for status from Publik at {self.url}")
        url = f"{self.url}/api/status/{self.auth[0]}"
        try:
            # response = self.s.get(url, verify=self.check_ssl)
            response = self.s.get(url)
            log.info("Publik reached with success")
        except Exception as e:
            raise PublikError(f"Publik cannot be reached : {e}")

        if not response.ok:
            raise PublikError(f"Publik cannot be reached : {response.reason}")

        return True

    def list_forms(self):
        """."""
        log.info(f"Listing forms from Publik at {self.url}")
        url = f"{self.url}/api/formdefs/"

        try:
            response = self.s.get(url)
            # response = self.s.get(url, verify=self.check_ssl)
            log.info("Publik reached with success")
        except Exception as e:
            raise PublikError(f"Publik cannot be reached : {e}")

        result = response.json()

        if not response.ok:
            raise PublikError(f"{result['err_class']} : {result['err_desc']}")

        if result["err"] != 0:
            raise PublikError(f"{result['err_class']} : {result['err_desc']}")

        return result["data"]

    def list_demandes(
        self,
        form_slug: str,
        criterions: dict = None,
        fields_criterions: dict = {},
        workflow_criterions: dict = {},
    ):
        """."""
        # XXX if fields_criterions is not None, full:on must be provided in criterions
        msg = f"Retrieving Demandes `{form_slug}` from Publik at {self.url}"
        if criterions:
            msg += f" with criterions {criterions}"
        if fields_criterions:
            msg += f", fields criterions {fields_criterions}"
        if workflow_criterions:
            msg += f", workflow criterions {workflow_criterions}"
        log.info(msg)

        url = f"{self.url}/api/forms/{form_slug}/list"
        if criterions:
            url = url + "?" + "&".join([f"{k}={v}" for k, v in criterions.items()])

        try:
            response = self.s.get(url)
        except Exception as e:
            raise PublikError(f"Publik cannot be reached : {e}")

        if not response.ok:
            raise PublikError(f"{response.text}")

        demandes = response.json()

        if fields_criterions == {} and workflow_criterions == {}:
            return demandes

        curated_demandes = []
        for demande in demandes:

            matched = True

            for criterion, value in fields_criterions.items():
                if demande["fields"][criterion] != value:
                    matched = False

            for criterion, value in workflow_criterions.items():
                if isinstance(value, dict):

                    for subcriterion, subvalue in value.items():
                        if demande["workflow"][criterion][subcriterion] != subvalue:
                            matched = False

                elif demande["workflow"][criterion] != value:
                    matched = False

            if matched:
                curated_demandes.append(demande)

        return curated_demandes

    def get_demande(self, form_slug: str, demande_id: str):
        """."""
        url = f"{self.url}/api/forms/{form_slug}/{demande_id}"
        log.info(f"Retrieving Demande from Publik at `{url}`")

        try:
            response = self.s.get(url)
            # response = self.s.get(url, verify=self.check_ssl)
        except Exception as e:
            raise PublikError(f"Publik cannot be reached : {e}")

        result = response.json()

        if not response.ok:
            raise PublikError(f"{response.text}")

        if "err" in result and result["err"] != 0:
            raise PublikError(f"{result['err_class']} : {result['err_desc']}")

        return result

    def create_demande(self, form_slug: str, data: dict):
        """."""
        log.info(f"Creating Demande in Publik at {self.url} with values {data}")

        url = f"{self.url}/api/formdefs/{form_slug}/submit"
        payload = {"data": data}

        try:
            response = self.s.post(url, data=json.dumps(payload))
            log.info("Publik reached with success")
        except Exception as e:
            raise PublikError(f"Publik cannot be reached : {e}")

        result = response.json()

        if not response.ok:
            raise PublikError(f"{result['err_class']} : {result['err_desc']}")

        if result["err"] != 0:
            raise PublikError(f"{result['err_class']} : {result['err_desc']}")

        return result["data"]

    def trigger_transition(self, slug_demande: str, transition: str, data: dict = None):
        """."""

        url = f"{self.url}/{slug_demande}/jump/trigger/{transition}/"

        msg = f"Triggering transition {transition} in Publik at {url}"
        if data is not None:
            msg += f" with values {data.keys()}"
        log.info(msg)

        payload = {}
        if data is not None:
            payload = data.copy()
            for k, v in data.items():
                if isinstance(v, Attachment):
                    payload[k] = _prepare_attachment(v)
        try:
            response = self.s.post(url, data=json.dumps(payload))
        except Exception as e:
            raise PublikError(
                f"Failing trigerring transition {transition} on {slug_demande} : {e}"
            )

        result = response.json()

        if not response.ok:
            raise PublikError(
                f"Failing trigerring transition {transition} on {slug_demande}"
            )

        if result["err"] != 0:
            raise PublikError(
                f"Failing trigerring transition {transition} on {slug_demande}"
            )

        log.info(f"Transition {transition} triggered with success")

        return result


def _prepare_attachment(attachment: Attachment):
    """."""
    with open(attachment.path, "rb") as f:
        file_content = f.read()

    return {
        "filename": attachment.name,
        "b64_content": base64.encodebytes(file_content).decode("utf-8"),
        "content_type": attachment.content_type,
    }
