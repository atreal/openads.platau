import json
import logging
import magic
import os
import re
import requests
import tempfile
import time
import uuid

from flatten_json import flatten
from bravado.exception import HTTPInternalServerError, HTTPBadRequest
from bravado_core.model import Model as BravadoModel
from datetime import date, datetime
from dataclasses import dataclass
from typing import List, Union
from zipfile import ZipFile

from openads.platau import REQUESTS_TIMEOUT
from openads.platau.base_objects import Attachment, Actor
from openads.platau.mapper import PlatAUMapper
from openads.platau.mapping import NOMENCLATURE, PLATAU_MAPPING
from openads.platau.piste_connector import PISTEConnector
from openads.platau.syncplicity_connector import SyncplicityUploadConnector
from openads.platau.utils import generate_temporary_pdf

log = logging.getLogger(__name__)

MAX_DOWNLOAD_TRY_NUMBER = (
    10  # number maximum we'll try to download a Piece from Plat'AU
)


class PlatAUError(Exception):
    pass


@dataclass
class PlatAUEvent(object):

    dossier_uid: str
    event_uid: str
    type_id: str
    type_label: str
    target_object: str
    object_type_id: str
    object_type_label: str
    bravado_object: BravadoModel

    def __init__(self, notification: object):
        """."""
        self.bravado_object = notification
        self.dossier_uid = notification.idDossier
        self.event_uid = notification.idEvenement

        if hasattr(notification, "idTypeEvenement"):
            self.type_id = notification.idTypeEvenement
        elif hasattr(notification, "nomTypeEvenement"):
            self.type_id = notification.nomTypeEvenement.idNom
        try:
            self.type_label = NOMENCLATURE["TYPE_EVENEMENT"][self.type_id]
        except KeyError:
            self.type_label = None

        self.target_object = notification.idElementConcerne
        if hasattr(notification, "idTypeObjetMetier"):
            self.object_type_id = notification.idTypeObjetMetier
        elif hasattr(notification, "nomTypeObjetMetier"):
            self.object_type_id = notification.nomTypeObjetMetier.idNom
        try:
            self.object_type_label = NOMENCLATURE["TYPE_OBJET_METIER"][
                self.object_type_id
            ]
        except KeyError:
            self.object_type_label = None

    def as_json(self, bravado: bool = False):
        """."""
        if bravado:
            event = self.bravado_object._as_dict()
            event["hdEvenement"] = event["hdEvenement"].strftime("%d/%m/%Y %H:%M:%S.%f")
        else:
            event = self.__dict__
            del event["bravado_object"]

        return json.dumps(event, indent=4)

    def as_dict(self, bravado: bool = False, date_as_str: bool = False):
        """."""
        if bravado:
            if date_as_str:
                event = self.bravado_object._as_dict()
                event["hdEvenement"] = event["hdEvenement"].strftime(
                    "%d/%m/%Y %H:%M:%S.%f"
                )
                return event
            else:
                return self.bravado_object._as_dict()
        else:
            event = self.__dict__
            del event["bravado_object"]
            return event


class PlatAUConnector(PISTEConnector):
    """."""

    platau_version: str

    def __init__(
        self,
        credentials: dict,
        spec_path: str,
        spec_overrides: dict = None,
        calling_actor: str = None,
        dry_safe: bool = False,
        bravado_config: dict = None,
        **kwargs,
    ):
        """Overload PISTE init.
        We need to provide a calling actor."""
        super().__init__(
            credentials,
            spec_path,
            spec_overrides=spec_overrides,
            dry_safe=dry_safe,
            bravado_config=bravado_config,
            **kwargs,
        )
        self.calling_actor = calling_actor
        try:
            self.platau_version = self.specs["info"]["version"]
        except Exception:
            self.platau_version = "N/A"

    def start(self, credentials_key: str = None):
        """Overload PISTE start.
        We need to provide a calling actor."""
        super().start(credentials_key)
        if credentials_key is not None and credentials_key != "default":
            self.calling_actor = credentials_key

    def _init_SyncplicityUploadConnector(self) -> SyncplicityUploadConnector:
        """Handle `sandbox` case while calling Syncplicity.

            :return SyncplicityUploadConnector: a connector instance
        """
        spec_overrides = getattr(self, "spec_overrides", {})
        if spec_overrides != {} and self.spec_overrides.get("sandbox") is True:
            spec_overrides = {"sandbox": True}
            connector = SyncplicityUploadConnector(
                self.credentials, spec_overrides=spec_overrides
            )
        else:
            connector = SyncplicityUploadConnector(self.credentials)

        return connector

    # ##################################################################################
    #
    # Common methods
    #
    # ##################################################################################

    def get_config(self) -> dict:
        """."""
        return dict(
            calling_actor=self.calling_actor,
            spec_path=self.spec_path,
            token_url=self.token_url,
            app_host=self.app_host,
            credentials=self.credentials,
            dry_safe=self.dry_safe,
            bravado_config=getattr(self, "bravado_config", None),
            klass=self.__class__,
        )

    def status(self):
        """."""
        try:
            return self.client.healthcheck.healthcheckGET().response().result
        except HTTPInternalServerError:
            print("API si down")
            return False
        return True

    def get_notifications(
        self, acteur_id: str = None, offset: int = None, raw: bool = True
    ) -> list:
        """Get notifications for self.calling_actor or the acteur_id provided.
            Each time a notification is created by PLATAU, an offset is incremented and
            attached to the new notification.

            :param acteur_id: an actor's id to authenticate the request

            :param offset: providing an offset value means : get me the notifications
            for the specified actor, starting from the offset value.

            :param raw: if True, will return bravado-style objects.
                        if False, specific PlatAUEvent objects

            :return list: notifications for acteur_id
        """
        actor = self.calling_actor
        if acteur_id:
            actor = acteur_id

        log.info(f"Getting notifications for actor {actor}")

        if offset is None:

            response = (
                self.client.notifications.notificationsGet(
                    _request_options={"headers": {"Id-Acteur-Appelant": actor}},
                )
                .response()
                .result
            )
        else:
            try:
                log.info(f"Notifications will be retrieved starting offset {offset}")
                response = (
                    self.client.notifications.notificationsGet(
                        offset=offset,
                        _request_options={"headers": {"Id-Acteur-Appelant": actor}},
                    )
                    .response()
                    .result
                )
            except HTTPBadRequest:
                return None

        if not raw:

            events = []
            for notification in response.notifications:
                # When retrieving notifications idAtor is None. We want keep this info.
                notification.idActeur = actor

                event = PlatAUEvent(notification)
                if event.type_label is None or event.object_type_label is None:
                    log.error(
                        f"Event {event} is unkown in Nomenclature "
                        "and will be oingored"
                    )
                else:
                    events.append(event)

            return events

        else:
            return response

    def get_pieces(
        self, dossier_uid: str, pieces_uids: list = None, nature: str = None
    ) -> list:
        """."""
        result = (
            self.client.dossiers.piecesDossierGET(
                idDossier=dossier_uid,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor},
                },
            )
            .response()
            .result
        )
        # If both pieces_uid and nature are provide, we filter only by piece_uid.
        if pieces_uids:
            result = [piece for piece in result if piece.idPiece in pieces_uids]
            return result

        if nature:
            result = [
                piece for piece in result if piece.nomNaturePiece.libNom == nature
            ]
        return result

    def get_pieces_binaries(self, dossier_uid: str) -> list:
        """."""
        binaries = []

        for piece in self.get_pieces(dossier_uid):
            headers = {"Authorization": f"Bearer {piece.token}"}

            # XXX :  we should catch any exception and append an error info in the list
            download_failed = False
            response = None
            for i in range(MAX_DOWNLOAD_TRY_NUMBER):
                try:
                    response = requests.get(
                        piece.url, headers=headers, timeout=REQUESTS_TIMEOUT
                    )
                except requests.exceptions.ReadTimeout as e:
                    if i == MAX_DOWNLOAD_TRY_NUMBER - 1:
                        log.error(
                            f"Failed downloading Piece {piece.idPiece} from Plat'AU: "
                            "ReadTimeout encountered."
                        )
                        log.error(e)
                        download_failed = True
                        break
                    else:
                        log.warning(
                            f"Failed downloading Piece {piece.idPiece} from Plat'AU. "
                            "Next try's coming."
                        )

            if response is not None and response.status_code != 200:
                log.error(
                    f"Failed downloading Piece {piece.idPiece} from Plat'AU."
                    f" Status code: {response.status_code}"
                )
                piece.attachment = None

            elif download_failed:
                piece.attachment = None

            else:
                file_name = self._extract_filename_from_headers(response.headers)

                piece.attachment = Attachment(
                    file_name, response.content, guess_content_type=True
                )
                log.info(
                    f"Retrieved Piece {file_name} with content-type {piece.attachment.content_type} on Dossier {dossier_uid}"
                )

            binaries.append(piece)

        return binaries

    def get_dossiers(self, criterions):
        """Get dossier from Plat'AU based on provided criterions.

        :param criterions: a dict of AppelDossierRecherche criterions

        :return list: a list of bravado-style Dossier objects
        """
        return (
            self.client.dossiers.dossiersGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_dossier_by_uid(self, dossier_uid, raw=False):
        """."""
        criterions = {"criteresSurDossiers": {"idDossier": dossier_uid}}
        result = self.get_dossiers(criterions)

        if len(result) != 0:
            if not raw:
                return result[0].dossier
            else:
                return result[0]
        else:
            return
        # return len(result) != 0 and result[0].dossier or None

    def get_dossier_by_numero(self, dossier_numero):
        """."""
        criterions = {"criteresSurDossiers": {"noLocal": dossier_numero}}
        result = self.get_dossiers(criterions)

        return len(result) != 0 and result[0].dossier or None

    def get_dossier_version(self, dossier_uid):
        """Retrieve dossier version"""
        dossier = self.get_dossier_by_uid(dossier_uid)
        if not dossier:
            raise PlatAUError(
                f"Could not retrieve version from Dossier {dossier_uid} : Dossier does not exist."
            )
        return dossier.noVersion

    def get_decisions(self, criterions):
        """."""
        return (
            self.client.decisionsUrba.decisionUrbanismeGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_decision_by_uid(self, decision_uid: str):
        """."""
        criterions = {"criteresSurDecisionsUrba": {"idDecisionUrba": decision_uid}}
        result = self.get_decisions(criterions)

        if len(result) != 0:
            if len(result[0].dossier.decisionsUrbanisme) != 0:
                return result[0].dossier.decisionsUrbanisme[0]
            else:
                return
        else:
            return

    def get_decision_version(self, decision_uid):
        """Retrieve decision version"""
        decision = self.get_decision_by_uid(decision_uid)
        if decision is not None:
            return decision.noVersion
        else:
            log.error(
                f"Decision {decision_uid} not found while trying to retreive version number"
            )
            return

    def get_evolution_decisions(self, criterions):
        """."""
        return (
            self.client.decisionsEvolution.decisionEvolutionGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_evolution_decision_by_uid(self, decision_uid: str):
        """."""
        criterions = {"criteresSurDecisionsEvol": {"idDecisionEvol": decision_uid}}
        result = self.get_evolution_decisions(criterions)

        if len(result) != 0:
            if len(result[0].dossier.decisionsEvolution) != 0:
                return result[0].dossier.decisionsEvolution[0]
            else:
                return
        else:
            return

    def get_evolution_decisison_version(self, decision_uid):
        """Retrieve decision version"""
        decision = self.get_evolution_decision_by_uid(decision_uid)
        if decision is not None:
            return decision.noVersion
        else:
            log.error(
                f"Evolution Decision {decision_uid} not found while trying to retreive version number"
            )
            return

    def get_doc_by_uid(self, doc_uid):
        """."""
        criterion = {
            "criteresSurDeclarationsOuvertureChantier": {
                "idDeclarationOuvertureChantier": doc_uid
            }
        }
        result = (
            self.client.declarationsOuvertureChantier.declarationsOuvertureChantierGET(
                body=criterion,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if len(result.resultats) != 0:
            return result.resultats[0]
        else:
            return

    def get_doc_version(self, doc_uid):
        """Retrieve decision version"""
        doc = self.get_doc_by_uid(doc_uid)
        if doc is not None:
            return doc.noVersion
        else:
            log.error(
                f"DOC {doc_uid} not found while trying to retreive version number"
            )
            return

    def get_daact_by_uid(self, daact_uid):
        """."""
        criterion = {
            "criteresSurDeclarationsAchevementTravaux": {
                "idDeclarationAchevementTravaux": daact_uid
            }
        }
        result = (
            self.client.declarationsAchevementTravaux.declarationsAchevementTravauxGET(
                body=criterion,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if len(result.resultats) != 0:
            return result.resultats[0]
        else:
            return

    def get_daact_version(self, daact_uid):
        """Retrieve decision version"""
        daact = self.get_daact_by_uid(daact_uid)
        if daact is not None:
            return daact.noVersion
        else:
            log.error(
                f"DAACT {daact_uid} not found while trying to retreive version number"
            )
            return

    def get_evenements(self, criterions):
        """WIP"""
        return (
            self.client.evenements.evenementsGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_evenement_by_uid(self, evenement_uid, raw=False):
        """WIP"""
        criterions = {"criteresSurEvenements": {"idEvenement": evenement_uid}}
        result = self.get_evenements(criterions)

        if len(result) != 0:
            if not raw:
                for event in result[0].dossier.evenements:
                    if event.idEvenement == evenement_uid:
                        # idDossier is not provides in EvenementR
                        event.idDossier = result[0].dossier.idDossier
                        return event
                log.error(f"Event {evenement_uid} not found")
                return
            else:
                return result
        else:
            log.info(f"Event {evenement_uid} not found")
            return

    def get_evenements_by_dossier(self, dossier_uid, raw=False):
        """WIP"""
        criterions = {"criteresSurDossiers": {"idDossier": dossier_uid}}
        result = self.get_evenements(criterions)

        events = []
        if len(result) != 0:
            if not raw:
                for event in result[0].dossier.evenements:
                    # idDossier is not provides in EvenementR
                    event.idDossier = result[0].dossier.idDossier
                    events.append(event)

                return events
            else:
                return result
        else:
            log.info(f"Events for {dossier_uid} not found")
            return

    def get_nomenclature(self, nomenclature_type):
        """."""
        nomenclature = self.client.nomenclatures.nomenclaturesGET(
            codeNomenclature=nomenclature_type,
            _request_options={"headers": {"Id-Acteur-Appelant": self.calling_actor}},
        )

        return nomenclature.response().result

    def get_infofiscales(self, dossier_uid: str):
        """."""
        result = (
            self.client.dossiers.infoFiscalesDossierGET(
                idDossier=dossier_uid,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        return result

    def get_dossier_ADAU(self, criterions: dict, raw=False):
        """WIP"""
        return (
            self.client.dossiersADAU.dossiersAdauGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_dossier_ADAU_by_uid(
        self, dossier_adau_uid: str, download_archive: bool = False
    ):
        """Retrieved Dossier ADAU based on UID."""

        criterions = {"criteresSurDossiersADAU": {"idDossierADAU": dossier_adau_uid}}

        result = self.get_dossier_ADAU(criterions)

        if len(result) > 1:
            raise PlatAUError("An UID-based search cannot have more than one result")

        if len(result) != 0:
            dossier_adau = result[0].dossierADAU
        else:
            return

        if not download_archive:
            return dossier_adau

        log.info("Downloading ADAU archive file")
        headers = {"Authorization": f"Bearer {dossier_adau.token}"}
        download_failed = False
        response = None
        for i in range(MAX_DOWNLOAD_TRY_NUMBER):
            try:
                response = requests.get(
                    dossier_adau.url, headers=headers, timeout=REQUESTS_TIMEOUT
                )
            except requests.exceptions.ReadTimeout as e:
                if i == MAX_DOWNLOAD_TRY_NUMBER - 1:
                    log.error(f"Failed downloading ADAU archive file from Plat'AU.")
                    log.error(e)
                    download_failed = True
                    break
                else:
                    log.warning(
                        f"Failed downloading ADAU archive file from Plat'AU. "
                        "Next try's coming."
                    )

        if (response and response.status_code != 200) or download_failed:
            dossier_adau.attachment = None

        else:
            file_name = self._extract_filename_from_headers(response.headers)

            dossier_adau.attachment = Attachment(
                file_name, response.content, guess_content_type=True
            )
            log.info(
                "Successfully downloaded ADAU archive from Dossier ADAU %s",
                dossier_adau_uid,
            )

        return dossier_adau

    # Acteurs ##########################################################################

    def get_acteurs(self, criterions):
        """."""
        # cached ?
        return (
            self.client.acteurs.acteursGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_acteur_by_uid(self, uid: str):
        """."""
        criterions = {"idActeur": uid}
        result = self.get_acteurs(criterions)
        if len(result) != 1:
            raise PlatAUError(
                "An UID-based search cannot have more or less than one result"
            )

        return result[0]

    def get_DDTs(self):
        """."""
        # cached ?
        result = (
            self.client.acteurs.acteursGET(
                body={"nomTypeActeur": 13},
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        return {item.designationActeur: item.idActeur for item in result}

    def get_DDT_by_departement(self, dept):
        """."""
        # cached ?
        result = (
            self.client.acteurs.acteursGET(
                body={"nomTypeActeur": 13, "codeInseeDepartementRattachement": dept},
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        return {item.designationActeur: item.idActeur for item in result}

    def create_acteurs(self, actors: List[Actor], raw: bool = True):
        """Create Acteurs."""
        model_map = {
            "guichet_unique": "GuichetUniqueCreation",
            "service_instructeur": "ServiceInstructeurCreation",
            "autorite_competente": "AutoriteCompetenteCreation",
            "service_consulte": "ServiceConsultableCreation",
        }
        principal = self.client.get_model("AppelActeurCreationPartenaire")(
            autoritesCompetentes=[],
            guichetsUniques=[],
            servicesConsultables=[],
            servicesInstructeurs=[],
        )

        for actor in actors:
            actor_p = self.client.get_model(model_map[actor.role])()
            actor_p.txLibelleEntitePublique = actor.organization_label
            actor_p.siren = actor.siren

            actor_p.txLogicielActeur = self.software
            actor_p.txVersionLogiciel = self.software_version
            actor_p.txVersionPLATAU = self.platau_version

            if actor.role == "guichet_unique":
                # from TYPE_COMPATIBILITE_ADAU
                # 2,"TRANSCAU",29/06/2020;
                # 3,"Non compatible",29/06/2020
                if actor.depot_adau:
                    actor_p.nomTypeCompatibiliteADAU = 2
                    # SVE CGU URL must be provided
                    actor_p.txURLCGUGuichetCompatAdAU = actor.extra_properties.get(
                        "sve_cgu_url", "N/A"
                    )

                else:
                    actor_p.nomTypeCompatibiliteADAU = 3

                actor_p.idCommuneRattachement = actor.insee

                if actor.email is not None:
                    actor_p.mail = actor.email

                principal.guichetsUniques.append(actor_p)

            elif actor.role == "service_instructeur":
                actor_p.mail = actor.email

                if actor.insee is not None:
                    actor_p.idCommuneRattachement = actor.insee
                if actor.insee_competence is not None:
                    actor_p.codesInseeCommunesCompetences = actor.insee_competence

                principal.servicesInstructeurs.append(actor_p)

            elif actor.role == "autorite_competente":
                # 1,"Le maire",03/01/2020
                # 2,"Le président de l’ECPI",04/01/2020
                # XXX - organization_type is never set
                if actor.organization_type == "epci":
                    actor_p.nomTypeSignataire = 2
                else:
                    actor_p.nomTypeSignataire = 1

                if actor.insee is not None:
                    actor_p.idCommuneRattachement = actor.insee
                if actor.insee_competence is not None:
                    actor_p.codesInseeCommunesCompetences = actor.insee_competence
                if actor.email is not None:
                    actor_p.mail = actor.email

                principal.autoritesCompetentes.append(actor_p)

            elif actor.role == "service_consulte":
                # XXX some properties are not bounded yet. No usecase.
                actor_p.mail = actor.email
                actor_p.boEstCompatibleAVISAU = False
                principal.servicesConsultables.append(actor_p)

        response = self.client.enrolement.acteursPOST(body=principal).response().result

        if raw:
            return response
        else:
            # XXX usecase ?
            return response

    def update_acteurs(self, actors: list, raw: bool = True):
        """Update Acteurs."""
        model_map = {
            "guichet_unique": "GuichetUniqueModification",
            "service_instructeur": "ServiceInstructeurModification",
            "autorite_competente": "AutoriteCompetenteModification",
            "service_consulte": "ServiceConsultableModification",
        }
        principal = self.client.get_model("AppelActeurModificationPartenaire")(
            autoritesCompetentes=[],
            guichetsUniques=[],
            servicesConsultables=[],
            servicesInstructeurs=[],
        )

        update_required = False
        for actor in actors:
            # We do not support every role
            if actor.role not in model_map:
                log.warning("The role %s is currently not supported", actor.role)
                continue

            new_actor = self.client.get_model(model_map[actor.role])()
            existing_actor = self.get_acteur_by_uid(actor.uid)

            new_actor.idActeur = actor.uid
            new_actor.txLogicielActeur = self.software
            new_actor.txVersionLogiciel = self.software_version
            new_actor.txVersionPLATAU = self.platau_version

            # Best scenario: the organization label is provided
            if actor.organization_label:
                new_actor.txLibelleEntitePublique = actor.organization_label

            # Data value is already defined in Plat'AU, let's use it
            elif existing_actor.txLibelleEntitePublique:
                new_actor.txLibelleEntitePublique = (
                    existing_actor.txLibelleEntitePublique
                )

            # We previously defined the value, but in Plat'Au v9, based on template :
            # "ORGANIZATION_LABEL (INSEE) - ACTOR ROLE"
            # Let's get rid of role and keep org name +  INSEE code
            elif existing_actor.designationActeur:
                new_actor.txLibelleEntitePublique = existing_actor.designationActeur.split(
                    " - "
                )[
                    0
                ].strip()

            # We don't have proper value, use a fallback
            else:
                new_actor.txLibelleEntitePublique = "N/A"

            # new_actor.mail = actor.email
            if actor.email:
                new_actor.mail = actor.email
            else:
                new_actor.mail = existing_actor.mail

            # if actor.label:
            #     new_actor.designationActeur = actor.label
            # else:
            #     new_actor.designationActeur = existing_actor.designationActeur

            if actor.siren:
                new_actor.siren = actor.siren
            else:
                new_actor.siren = existing_actor.siren

            if actor.role == "guichet_unique":
                # from TYPE_COMPATIBILITE_ADAU
                # 2,"TRANSCAU",29/06/2020;
                # 3,"Non compatible",29/06/2020
                if actor.depot_adau is True:
                    new_actor.nomTypeCompatibiliteADAU = 2

                    # New value is provided
                    if "sve_cgu_url" in actor.extra_properties:
                        new_actor.txURLCGUGuichetCompatAdAU = actor.extra_properties[
                            "sve_cgu_url"
                        ]

                    # We kept the existing one
                    elif existing_actor.txURLCGUGuichetCompatAdAU:
                        new_actor.txURLCGUGuichetCompatAdAU = (
                            existing_actor.txURLCGUGuichetCompatAdAU
                        )

                    # Fallback to undefined case
                    else:
                        new_actor.txURLCGUGuichetCompatAdAU = "N/A"

                elif actor.depot_adau is False:
                    new_actor.nomTypeCompatibiliteADAU = 3

                else:
                    new_actor.nomTypeCompatibiliteADAU = (
                        existing_actor.nomTypeCompatibiliteADAU
                    )

                # new_actor.idCommuneRattachement = existing_actor.idCommuneRattachement

                principal.guichetsUniques.append(new_actor)
                update_required = True

            elif actor.role == "service_instructeur":

                if actor.insee_competence:
                    new_actor.codesInseeCommunesCompetences = actor.insee_competence

                principal.servicesInstructeurs.append(new_actor)
                update_required = True

            elif actor.role == "autorite_competente":
                # 1,"Le maire",03/01/2020
                # 2,"Le président de l’ECPI",04/01/2020
                # XXX
                if actor.insee_competence:
                    new_actor.codesInseeCommunesCompetences = actor.insee_competence

                principal.autoritesCompetentes.append(new_actor)
                update_required = True

            elif actor.role == "service_consulte":
                # XXX some properties are not bounded yet. No usecase.
                principal.servicesConsultables.append(new_actor)
                update_required = True

        if update_required:
            response = (
                self.client.enrolement.acteursPUT(body=principal).response().result
            )
        else:
            log.warning("Nothing to update here.")
            return []

        if raw:
            return response
        else:
            result = []
            for p_role in response._properties.keys():
                edited_actors = getattr(response, p_role)
                if not edited_actors:
                    continue

                result.extend(
                    [
                        {
                            "uid": e_a.idActeur,
                            "role": p_role,
                            "status": e_a.erreurs is None and "OK" or "KO",
                        }
                        for e_a in edited_actors
                    ]
                )
            return result

    def delete_acteurs(self, actor_uids: Union[str, List[str]], raw: bool = True):
        """Delete Acteurs."""
        if not isinstance(actor_uids, list):
            actor_uids = [actor_uids]

        body = self.client.get_model("AppelActeurSuppression")()
        for uid in actor_uids:
            self.client.acteurs.acteursDELETE(
                idActeur=uid,
                body=body,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            ).response().result

    # ##################################################################################
    #
    # Collectivite relative methods
    #
    # ##################################################################################

    def create_projet(self, tasks):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        projects = []
        for task in tasks:

            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            projects.append(mapper.map("AppelProjetCreation"))

        result = (
            self.client.projets.projetsPOST(
                body=projects,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        return [p.idProjet for p in result]

    def get_projet_by_uid(self, projet_uid: str):
        """."""
        criterions = {"criteresSurProjets": {"idProjet": projet_uid}}
        result = self.get_dossiers(criterions)

        if len(result) != 0:
            return result[0].projet
        else:
            return

    def get_projet_version(self, projet_uid: str):
        """Retrieve projet version"""
        projet = self.get_projet_by_uid(projet_uid)
        if not projet:
            raise PlatAUError(
                f"Could not retrieve version from Projet {projet_uid} : Projet does not exist."
            )
        return projet.noVersion

    def _prepare_dossier(self, task: dict):
        """Format data before creating or updating a Dossier"""
        demandeurs = []
        source_demandeurs = task.pop("demandeur")
        if source_demandeurs is not None and len(source_demandeurs) != 0:
            for demandeur in source_demandeurs.values():

                # Avoid bad formatted SIRET number (legacy openADS issue or Modificatif
                # on pre-Plat'AU Dossier Initial)
                siret = demandeur.get("personne_morale_siret", "")
                if siret != "":
                    siret_validator = re.compile(r"^[0-9]{14}$")
                    if not siret_validator.match(siret):
                        demandeur["personne_morale_siret"] = ""
                        log.warning(
                            f"{task['dossier']['dossier']} : cleaned-up bad SIRET"
                        )

                demandeurs.append({f"demandeur.{k}": v for k, v in demandeur.items()})

        parcelles = []
        source_parcelles = task.pop("dossier_parcelle")
        if source_parcelles is not None and len(source_parcelles) != 0:
            for parcelle in source_parcelles.values():
                parcelles.append(
                    {f"dossier_parcelle.{k}": v for k, v in parcelle.items()}
                )

        architecte = task.pop("architecte")

        task = flatten(task, ".")
        task["demandeurs"] = demandeurs
        task["parcelles"] = parcelles
        if architecte:
            task["architecte"] = {f"architecte.{k}": v for k, v in architecte.items()}
            task["architecte"]["personne.type"] = "architecte"

        # XXX : Mandatory properties fix
        if task["donnees_techniques.enga_decla_date"] == "":
            task["donnees_techniques.enga_decla_date"] = date.today().strftime(
                "%Y-%m-%d"
            )
        if task["donnees_techniques.enga_decla_lieu"] == "":
            task["donnees_techniques.enga_decla_lieu"] = "Non fourni"
        if task["dossier.terrain_adresse_localite"] == "":
            task["dossier.terrain_adresse_localite"] = "Non fourni"

        # If we are adding a Transfert or a Modificatif, we should provide some data
        # from source Dossier  (UID, suffixe, etc)
        if task["dossier.dossier_instruction_type_code"] in ("M", "T", "DOC", "DAACT"):

            results = self.get_dossiers(
                {
                    "criteresSurDossiers": {
                        "noLocal": task["dossier.dossier_autorisation"]
                    }
                }
            )
            for result in results:
                # We have to check :
                # - that's not the current Dossier
                # - this is a Dossier Initial
                # - that's not a transfert
                if (
                    task["external_uids.dossier"] != result.dossier.idDossier
                    and result.dossier.nomNatureDossier.idNom
                    == 1  # 1,"Initial",04/12/2019
                    and result.dossier.nomTypeDossier.idNom
                    != 7  # 7,"Demande de transfert (DT)",04/12/2019
                ):
                    task["external_uids.dossier_initial"] = result.dossier.idDossier
                    task[
                        "dossier.dossier_initial_suffixe"
                    ] = result.dossier.suffixeNoLocal
                    break

        if task["dossier.dossier_instruction_type_code"] == "T":
            # In Plat'AU, a Transfert is a Dossier Initial. Will be properly mapped
            # by the generic mapper.
            task["dossier.dossier_instruction_type_code"] = "P"
            task["dossier.dossier_autorisation_type_detaille_code"] = "TF"

        return task

    def create_dossier(self, tasks, raw: bool = False):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        dossiers = []
        for task in tasks:

            task = self._prepare_dossier(task)

            mapper = PlatAUMapper(task, self.client.get_model)
            dossiers.append(mapper.map("AppelDossierCreation"))

        result = (
            self.client.dossiers.dossiersPOST(
                body=dossiers,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return result
        else:
            return [p.idDossier for p in result]

    def update_dossier(self, tasks, raw: bool = False):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        dossiers = []
        dossiers_without_nolocal = {}

        for task in tasks:

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            # then set dossier version number in task
            dossier_uid = task["external_uids"]["dossier"]

            # We do not use get_dossier_version, as we need all Dossier data to check is noLocal is set
            dossier = self.get_dossier_by_uid(dossier_uid)

            if not dossier:
                raise PlatAUError(
                    f"Could not update Dossier {dossier_uid} : Dossier does not exist."
                )
            task["dossier_version"] = dossier.noVersion

            task = self._prepare_dossier(task)
            mapper = PlatAUMapper(task, self.client.get_model)
            dossiers.append(mapper.map("AppelDossierMAJ"))

            # Do we have a noLocal set ? If not, a specific update should be run.
            # We need to keep tracks of the inital task.
            # (AD'AU Dossier first update case)
            if not dossier.noLocal:
                dossiers_without_nolocal[dossier_uid] = task

        # Global update
        result = (
            self.client.dossiers.dossiersPUT(
                body=dossiers,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        # (AD'AU Dossier first update case)
        if dossiers_without_nolocal:

            nolocal_to_update = []

            for updated_dossier in result:

                # We should use the last version number. It has been updated on Dossier modification
                if updated_dossier.idDossier in dossiers_without_nolocal:
                    dossiers_without_nolocal[updated_dossier.idDossier][
                        "dossier_version"
                    ] = updated_dossier.noVersion

                    # Now we use the right mapper to update noLocal with updated task
                    mapper = PlatAUMapper(
                        dossiers_without_nolocal[updated_dossier.idDossier],
                        self.client.get_model,
                    )
                    nolocal_to_update.append(mapper.map("AppelNumerotationDossierMAJ"))

            # Dossier Numero / noLocal update
            dummy = (
                self.client.dossiers.dossiersNumerotationPUT(
                    body=nolocal_to_update,
                    _request_options={
                        "headers": {"Id-Acteur-Appelant": self.calling_actor}
                    },
                )
                .response()
                .result
            )

        if raw:
            return result
        else:
            return [p.idDossier for p in result]

    def create_piece(self, tasks, uniquify: bool = True):
        """Add binary files and link those to the Dossier.

            :param tasks: a task or a list of tasks, straight from openADS

            :param uniquify: if True, file(s) will be prefixed with a unique id

            :return list: a list of Piece UID created against Plat'AU
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.start(self.calling_actor)

        result = []

        piece_uids = []
        for task in tasks:

            # Let's push the binary file to syncplicity an retrieve file info
            task["file"] = upload_app.add_file(
                task["document_numerise"]["local_file_path"],
                "file_name" in task["document_numerise"]
                and task["document_numerise"]["file_name"]
                or None,
                uniquify=uniquify,
            )
            dossier_uid = task["external_uids"]["dossier"]

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            # then set dossier version number in task
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            # Data mapping
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            piece = mapper.map("AppelPieceCreation")

            # preload is over, we can push files metadata to plat'au
            for i in range(10):
                try:
                    result = (
                        self.client.pieces.piecesPOST(
                            body=[piece],
                            _request_options={
                                "headers": {"Id-Acteur-Appelant": self.calling_actor}
                            },
                        )
                        .response()
                        .result
                    )
                except HTTPBadRequest as e:
                    #  OK, this is too much, let's raise the execption again
                    if i == 9:
                        raise e

                    log.warning(e)

                    wrong_version = False
                    for error in e.response.json()[0]["erreurs"]:
                        #  from https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/platau/wiki/Liste_des_codes_d'erreur_et_leur_libell%C3%A9_associ%C3%A9
                        if (
                            error["codeErreur"] == 2
                        ):  # [{'attribut': 'noVersion', 'codeErreur': 2, 'libErreur': "La version X n'est pas la version actuelle"}]
                            wrong_version = True
                    # It's not a version number issue, let's re-throw the catchedexception
                    if wrong_version is False:
                        raise e
                    # lets' give Plat'AU some time to handle his queue
                    time.sleep(5)
                    # and update version number
                    piece.noVersion = self.get_dossier_version(dossier_uid)

                else:
                    break

            piece_uid = result[0].pieces[0].idPiece
            piece_uids.append(piece_uid)
        return piece_uids

    def update_piece(self, tasks, uniquify: bool = True):
        """Update binary files linked to the Dossier.

            :param tasks: a task or a list of tasks, straight from openADS

            :param uniquify: if True, file(s) will be prefixed with a unique id

            :return list: a list of Piece UID created against Plat'AU
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.start(self.calling_actor)

        result = []

        piece_uids = []
        for task in tasks:

            # Let's push the binary file to syncplicity an retrieve file info
            task["file"] = upload_app.add_file(
                task["document_numerise"]["local_file_path"],
                "file_name" in task["document_numerise"]
                and task["document_numerise"]["file_name"]
                or None,
                uniquify=uniquify,
            )
            dossier_uid = task["external_uids"]["dossier"]

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            # then set dossier version number in task
            task["dossier_version"] = self.get_dossier_version(dossier_uid)
            # We should have the last noVersion from the Piece
            # Let's get this straight from plat'au
            # then set piece version number in task
            task["piece_version"] = self.get_pieces(
                dossier_uid, pieces_uids=[task["external_uids"]["piece"]]
            )[0].noVersion

            # Data mapping
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            piece = mapper.map("AppelPieceMAJ")

            # preload is over, we can push files metadata to plat'au
            for i in range(10):
                try:
                    result = (
                        self.client.pieces.piecesPUT(
                            body=[piece],
                            _request_options={
                                "headers": {"Id-Acteur-Appelant": self.calling_actor}
                            },
                        )
                        .response()
                        .result
                    )
                except HTTPBadRequest as e:
                    #  OK, this is too much, let's raise the execption again
                    if i == 9:
                        raise e

                    log.warning(e)

                    wrong_version = False
                    for error in e.response.json()[0]["erreurs"]:
                        #  from https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/platau/wiki/Liste_des_codes_d'erreur_et_leur_libell%C3%A9_associ%C3%A9
                        if (
                            error["codeErreur"] == 2
                        ):  # [{'attribut': 'noVersion', 'codeErreur': 2, 'libErreur': "La version X n'est pas la version actuelle"}]
                            wrong_version = True
                    # It's not a version number issue, let's re-throw the catchedexception
                    if wrong_version is False:
                        raise e
                    # lets' give Plat'AU some time to handle his queue
                    time.sleep(5)
                    # and update version number
                    piece.noVersion = self.get_dossier_version(dossier_uid)

                else:
                    break

            piece_uid = result[0].pieces[0].idPiece
            piece_uids.append(piece_uid)
        return piece_uids

    def depot_pieces(self, tasks):
        """Add binary file and link this to the Dossier.
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        pieces = []
        for task in tasks:

            dossier_uid = task["external_uids"]["dossier"]

            # Data mapping
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            pieces.append(mapper.map("PiecesDepots"))

        piece_depot = self.client.get_model("AppelPieceDepotMAJ")(
            idDossier=dossier_uid,
            noVersion=self.get_dossier_version(dossier_uid),
            pieces=pieces,
        )

        result = (
            self.client.pieces.piecesDepotsPUT(
                body=[piece_depot],
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        return result

    def delete_pieces(self, tasks):
        """Delete binary files linked to the Dossier.

            :param tasks: a task or a list of tasks, straight from openADS

            :return list: a list of Piece UID deleted against Plat'AU
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        # TODO : START Replace this code when file deletion is possible:
        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.start(self.calling_actor)
        # END TODO!

        piece_uids = []
        for task in tasks:

            # TODO : START Replace this code when file deletion is possible
            # Generate a tmeporary unique PDF file
            now = datetime.now().strftime("%m/%d/%Y à %H:%M:%S")
            filename = task["document_numerise"]["nom_fichier"]
            tmp_file_path = generate_temporary_pdf(
                filename, f"Pièce {filename} supprimée le {now}"
            )
            task["document_numerise"]["local_file_path"] = tmp_file_path
            task["document_numerise"]["filename"] = filename

            # Let's push the binary file to syncplicity an retrieve file info
            task["file"] = upload_app.add_file(
                task["document_numerise"]["local_file_path"],
                "filename" in task["document_numerise"]
                and task["document_numerise"]["filename"]
                or None,
                uniquify=True,
            )

            # On an Update we should always have  Modificative : 3 as document nature
            try:
                task["document_numerise"]["document_numerise_nature"] = "3"
                task["document_numerise"]["document_numerise_nature_code"] = "MODIF"
                task["document_numerise"][
                    "document_numerise_nature_libelle"
                ] = "Modificative"
                type_code = task["document_numerise"]["document_numerise_type_code"]
                PLATAU_MAPPING["TYPE_PIECE"][type_code]

            except KeyError:
                log.warning(
                    f"Piece type `{type_code}` does not exist in Plat'AU. "
                    f"Task {self.task_id} will be canceled"
                )
                self.state = "canceled"
                return

            platau_uid = self.update_piece(task)[0]
            piece_uids.append(platau_uid)

            # temporary file can safely be removed
            try:
                os.remove(tmp_file_path)
            except FileNotFoundError:
                log.warning(f"File at {tmp_file_path} already removed")

            # END TODO!

        return piece_uids

    def depot_dossier(self, tasks):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        depots = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            depots.append(mapper.map("AppelDepotDossierMAJ"))

        result = (
            self.client.dossiers.dossiersDepotPUT(
                body=depots,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        return result

    def depot_DOC(self, tasks, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        depots = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_doc_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            depots.append(mapper.map("AppelDeclarationOuvertureChantierMAJ"))

        response = (
            self.client.declarationsOuvertureChantier.declarationsOuvertureChantierDepotPUT(
                body=depots,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response
        else:
            result = []
            for item in response:
                result.append(item.idDeclarationOuvertureChantier)

        return result

    def depot_DAACT(self, tasks, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        depots = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_daact_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            depots.append(mapper.map("AppelDeclarationAchevementTravauxMAJ"))

        response = (
            self.client.declarationsAchevementTravaux.declarationsAchevementTravauxDepotPUT(
                body=depots,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response
        else:
            result = []
            for item in response:
                result.append(item.idDeclarationAchevementTravaux)

        return result

    def qualification_dossier(self, tasks):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        a_qualifier = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            a_qualifier.append(mapper.map("AppelQualificationDossierMAJ"))

        result = (
            self.client.dossiers.dossiersQualificationPUT(
                body=a_qualifier,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        return result

    def set_date_limite_instruction(self, tasks: list) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        date_limite_updates = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            date_limite_updates.append(mapper.map("AppelDtLimitesInstructionMAJ"))

        result = (
            self.client.dossiers.dtLimiteInstructionPUT(
                body=date_limite_updates,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        return result

    def set_completude(self, tasks):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        completudes = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            completudes.append(mapper.map("AppelCompletudesDossierMAJ"))

        result = (
            self.client.dossiers.dossiersCompletudesPUT(
                body=completudes,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        return result

    def add_consultation(self, tasks: dict, raw: bool = True) -> list:
        """Add a Consultation, with binaries attached.
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        result = []
        consultations = []
        for task in tasks:

            if "local_file_path" in task["consultation"]:
                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["consultation"]["local_file_path"],
                    task["consultation"]["file_name"],
                )

                # acteur_uid = task["task"]["acteur"]
            #  END WIP

            dossier_uid = task["external_uids"]["dossier"]

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            if "file" in task:
                # We fix the Document type
                # TYPE_DOCUMENT : 7,"Consultation",07/01/2020
                task["file"]["document_type"] = 7

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            consultation = mapper.map("AppelConsultationCreation")

            # if there is no document attached, we can drop the sub-array
            # WIP
            for c in consultation.consultations:
                if len(c.documents) == 1 and c.documents[0].fileId is None:
                    c.documents = None
            # END WIP
            consultations.append(consultation)

        response = (
            self.client.consultations.consultationsPOST(
                body=consultations,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if raw:
            return response

        result = []
        for item in response:
            for consultation in item.consultations:
                result.append(consultation.idConsultation)

        return result

    def get_avis(self, criterions):
        """."""
        return (
            self.client.avis.avisGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_avis_by_uid(
        self, uid: str, dossier_uid: str = None, documents: bool = False
    ):
        """."""
        criterions = {"criteresSurAvis": {"idAvis": uid}}
        # Providing the dossier uid decrease request time by 10
        if dossier_uid:
            criterions["criteresSurDossiers"] = {"idDossier": dossier_uid}
        result = self.get_avis(criterions)
        if len(result) > 1:
            raise PlatAUError("An UID-based search cannot have more than one result")

        if len(result[0].dossier.avis) > 1:
            raise PlatAUError("An UID-based search cannot have more than one result")

        if documents:
            result[0].dossier.avis[0].documents = self._get_documents(
                result[0].dossier.avis[0], zipped_archive=True
            )

        return len(result) != 0 and result[0] or None

    def get_prescriptions(self, criterions):
        """."""
        return (
            self.client.prescriptions.prescriptionsGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result.resultats
        )

    def get_prescription_by_uid(self, uid: str, dossier_uid: str = None):
        """."""
        criterions = {"criteresSurPrescriptions": {"idEnsemblePrescriptions": uid}}
        # Providing the dossier uid decrease request time by 10
        if dossier_uid:
            criterions["criteresSurDossiers"] = {"idDossier": dossier_uid}
        result = self.get_prescriptions(criterions)
        if len(result) > 1:
            raise PlatAUError("An UID-based search cannot have more than one result")

        if len(result[0].dossier.prescriptions) > 1:
            raise PlatAUError("An UID-based search cannot have more than one result")

        return len(result) != 0 and result[0] or None

    def add_lettre_petitionnaire(self, tasks: list, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        lettres = []
        for task in tasks:

            # Let's push the binary file to syncplicity an retrieve file info
            task["file"] = upload_app.add_file(
                task["instruction"]["local_file_path"],
                "file_name" in task["instruction"]
                and task["instruction"]["file_name"]
                or None,
            )
            # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
            # 14,"Arrêté",14/01/2020
            task["file"]["document_type"] = task["lettre_petitionnaire"][
                "nomTypeDocument"
            ]

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            lettre = mapper.map("AppelLettrePetitionnaireCreation")
            lettre.lettresAuxPetitionnaires = [mapper.map("LettresPetitionnairesAppel")]

            lettres.append(lettre)

        response = (
            self.client.lettresAuxPetitionnaires.lettresPetitionnairesPOST(
                body=lettres,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if raw:
            return response

        else:
            result = []
            # Response is always multi valued
            for item in response:
                for lettre in item.lettresAuxPetitionnaires:
                    result.append(lettre.idLettreAuxPetitionnaires)

            return result

    def abandon(self, tasks: list, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        abandons = []
        for task in tasks:

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            task["dossier_version"] = self.get_dossier_version(
                task["external_uids"]["dossier"]
            )

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            abandon = mapper.map("AppelAbandonDossierMAJ")

            abandons.append(abandon)

        response = (
            self.client.dossiers.dossiersAbandonPUT(
                body=abandons,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if raw:
            return response

        else:
            result = []
            for item in response:
                result.append(item.idDossier)

        return result

    def decision(self, tasks: dict, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        decisions = []
        for task in tasks:

            # We should provide binary files only if the Decision is not *Tacite*
            if task["instruction"]["tacite"] == "f":
                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["instruction"]["local_file_path"],
                    "file_name" in task["instruction"]
                    and task["instruction"]["file_name"]
                    or None,
                )
                # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
                # 14,"Arrêté",14/01/2020
                task["file"]["document_type"] = 14

            # acteur_uid = task["task"]["acteur"]
            dossier_uid = task["external_uids"]["dossier"]

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            # TODO - if decision == Refus, dossier.date_validite is empty.
            # at this time, matching platau (1.6) property dtFinValiditeADefautDeTravaux
            # is required. We used the Decison date, until this is fixed
            # XXX - to be corrected.
            # if "" != task["dossier"]["date_validite"]:
            #     task["dossier"]["date_validite"] = task["dossier"]["date_decision"]

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            decision = mapper.map("AppelDecisionUrbaCreation")

            # if there is no document attached, we can drop the sub-array
            if (
                len(decision.decisionUrba.documents) == 1
                and decision.decisionUrba.documents[0].fileId is None
            ):
                decision.decisionUrba.documents = None

            decisions.append(decision)

        response = (
            self.client.decisionsUrba.decisionUrbanismePOST(
                body=decisions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if raw:
            return response

        else:
            result = []
            for item in response:
                result.append(item.decisionUrba.idDecisionUrba)

        return result

    def evolution_decision(self, tasks: list, raw: bool = True) -> list:
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        evolution_decisions = []
        for task in tasks:

            results = self.get_decisions(
                {
                    "criteresSurDossiers": {
                        "noLocal": task["dossier"]["dossier_autorisation"],
                        # "suffixeNoLocal": task["dossier.dossier_suffixe"]
                    }
                }
            )

            # Let's find Decision on which Evolution should apply
            # First, we have to look for the Dossier Initial
            dossier = None
            initial_decision = None
            try:
                for result in results:
                    if result.dossier.suffixeNoLocal in (None, "", "P0"):
                        # We are looking for the the last decision
                        dossier = result.dossier
                        initial_decision = result.dossier.decisionsUrbanisme[-1]
                        break
            except KeyError:
                raise PlatAUError(
                    f"Could not find any Decision {task['dossier']['dossier_autorisation']}"
                )

            if not initial_decision or not dossier:
                raise PlatAUError(
                    f"Could not find Dossier {task['dossier']['dossier_autorisation']}"
                )

            task["external_uids"]["dossier"] = dossier.idDossier
            task["external_uids"]["decision"] = initial_decision.idDecisionUrba

            # We should have the last noVersion from the Dossier
            # Let's get this straight from plat'au
            task["dossier_version"] = self.get_dossier_version(dossier.idDossier)

            # We should provide binary files only if the evolution_decision is not *Tacite*
            if task["instruction"]["tacite"] == "f":
                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["instruction"]["local_file_path"],
                    "file_name" in task["instruction"]
                    and task["instruction"]["file_name"]
                    or None,
                )
                # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
                # 14,"Arrêté",14/01/2020
                task["file"]["document_type"] = 14

            # Now we can  map the data
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            evolution_decision = mapper.map("AppelDecisionEvolCreation")

            # if there is no document attached, we can drop the sub-array
            if (
                len(evolution_decision.decisionEvol.documents) == 1
                and evolution_decision.decisionEvol.documents[0].fileId is None
            ):
                evolution_decision.decisionEvol.documents = None

            evolution_decisions.append(evolution_decision)

        response = (
            self.client.decisionsEvolution.decisionEvolutionPOST(
                body=evolution_decisions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        if raw:
            return response

        else:
            result = []
            for item in response:
                result.append(item.decisionEvol.idDecisionEvol)

        return result

    def envoi_CL(self, tasks):
        """."""
        if not isinstance(tasks, list):
            tasks = [tasks]

        envois = []
        for task in tasks:

            dossier_uid = task["external_uids"]["dossier"]
            decision_uid = task["external_uids"]["decision"]

            if task["dossier"]["dossier_instruction_type_code"] in ("ANNUL", "PRO"):

                # We'll handle a decisionEvolution object
                # mandatory values are missing (dossier_uid, etc), let's get these
                criterions = {
                    "criteresSurDecisionsEvol": {"idDecisionEvol": decision_uid}
                }
                result = self.get_evolution_decisions(criterions)
                try:
                    task["external_uids"]["dossier"] = result[0].dossier.idDossier
                    task["dossier_version"] = result[0].dossier.noVersion
                    task["decision_version"] = (
                        result[0].dossier.decisionsEvolution[0].noVersion
                    )

                except (IndexError, AttributeError):
                    raise PlatAUError(
                        "Could not retrieve Evolution Decision or Dossier"
                    )

                # Now we can  map the data
                mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
                cl_payload = mapper.map("AppelEvolutionsEnvoiAuCLMAJ")

                result = (
                    self.client.decisionsEvolution.evolutionEnvoisAuCLPUT(
                        body=[cl_payload],
                        _request_options={
                            "headers": {"Id-Acteur-Appelant": self.calling_actor}
                        },
                    )
                    .response()
                    .result
                )

            else:
                # We'll handle an initial decision object
                task["dossier_version"] = self.get_dossier_version(dossier_uid)
                task["decision_version"] = self.get_decision_version(decision_uid)

                mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
                cl_payload = mapper.map("AppelEnvoiAuCLMAJ")

                result = (
                    self.client.decisionsUrba.envoisAuCLPUT(
                        body=[cl_payload],
                        _request_options={
                            "headers": {"Id-Acteur-Appelant": self.calling_actor}
                        },
                    )
                    .response()
                    .result
                )

            envois.append(result)

        return envois

    def add_DOC(self, tasks: list, raw: bool = True) -> list:
        """Add a DOC, without binaries attached.
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        docs = []
        for task in tasks:

            # output is a flatten JSON
            task = self._prepare_dossier(task)

            task["projet_version"] = self.get_projet_version(
                task["external_uids.dossier_autorisation"]
            )

            mapper = PlatAUMapper(task, self.client.get_model)
            doc = mapper.map("AppelDeclarationOuvertureChantierCreation")
            doc_subobject = mapper.map("DeclarationOuvertureChantierAppelCreation")

            # XXX: for now we does not support Pieces, but the mapping provided an empty one
            # We should get rid of it
            doc_subobject.pieces = None

            doc.declarationsOuvertureChantier = [doc_subobject]
            docs.append(doc)

        response = (
            self.client.declarationsOuvertureChantier.declarationsOuvertureChantierPOST(
                body=docs,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response

        else:
            result = []
            for item in response:
                result.append(item.idDeclarationOuvertureChantier)

        return result

    def add_DAACT(self, tasks: list, raw: bool = True) -> list:
        """Add a DAACT, without binaries attached.
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        daacts = []
        for task in tasks:

            # output is a flatten JSON
            task = self._prepare_dossier(task)

            task["projet_version"] = self.get_projet_version(
                task["external_uids.dossier_autorisation"]
            )

            mapper = PlatAUMapper(task, self.client.get_model)
            daact = mapper.map("AppelDeclarationAchevementTravauxCreation")
            daact_subobject = mapper.map("DeclarationAchevementTravauxP1")

            # XXX: for now we does not support Pieces, but the mapping provided an empty one
            # We should get rid of it
            daact_subobject.pieces = None

            daact.declarationsAchevementTravaux = [daact_subobject]
            daacts.append(daact)

        response = (
            self.client.declarationsAchevementTravaux.declarationsAchevementTravauxPOST(
                body=daacts,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response

        else:
            result = []
            for item in response:
                result.append(item.idDeclarationAchevementTravaux)

        return result

    # ##################################################################################
    #
    # Service Consulte relative methods
    #
    # ##################################################################################

    def get_consultations(self, criterions):
        """."""
        result = (
            self.client.consultations.consultationsGET(
                body=criterions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )
        return result.resultats

    def get_consultation_by_uid(self, uid: str, dossier_uid: str = None):
        """."""
        criterions = {"criteresSurConsultations": {"idConsultation": uid}}
        # Providing the dossier UID decrease request time by 10
        if dossier_uid:
            criterions["criteresSurDossiers"] = {"idDossier": dossier_uid}
            log.info(f"Retrieving Consultation {uid} on Dossier {dossier_uid}")
        else:
            log.info(f"Retrieving Consultation {uid}")

        consultation_response = self.get_consultations(criterions)
        if (
            len(consultation_response) != 1
            or len(consultation_response[0].dossier.consultations) != 1
        ):
            raise PlatAUError(
                "An UID-based search cannot have more or less than one result"
            )

        return len(consultation_response) != 0 and consultation_response[0] or None

    def get_consultation_version(self, uid: str, dossier_uid: str = None):
        """."""
        consultation_response = self.get_consultation_by_uid(uid, dossier_uid)
        if consultation_response is None:
            raise ValueError(f"There is no consultation with the UID : {uid}")

        consultation = consultation_response.dossier.consultations[0]
        return consultation.noVersion

    def add_pec_consultation(self, tasks: list, raw: bool = True) -> list:
        """Add pec_consultation.

            :param tasks: a list of openADS `pec_metier_consultation` output tasks

            :param raw: if True, will return bravado-style objects
                        if False, objects uids

            :return list: a list of bravado-style objects OR a list of PeC uids
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        # Do we have an attachment ? This is less time-consuming to iterate on tasks
        # than instanciate SyncplicityUploadConnector various time
        has_attachment = False
        for t in tasks:
            if "local_file_path" in t["instruction"]:
                has_attachment = True
                break

        if has_attachment:
            upload_app = self._init_SyncplicityUploadConnector()
            upload_app.method = "direct"
            upload_app.start(self.calling_actor)

        pecs = []
        for task in tasks:

            if has_attachment:
                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["instruction"]["local_file_path"],
                    "file_name" in task["instruction"]
                    and task["instruction"]["file_name"]
                    or None,
                )
                # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
                # 6,"Demande de pièces complémentaires",06/01/2020
                task["file"]["document_type"] = 6

            consultation_uid = task["external_uids"]["dossier_consultation"]
            dossier_uid = task["external_uids"]["dossier"]

            log.info(
                f"Sending PeC for Consultation {consultation_uid} on Dossier {dossier_uid}"
            )

            # XXX -  a Pec from an Incompletude or Hors-champ will not provide a
            # date_limite, altrough dtLimiteReponse is mandatory. We'll provide the
            # current day. Should be removed after CDS > 4.7.
            # if task["dossier"]["date_limite"] == "":
            #     task["dossier"]["date_limite"] = date.today().strftime("%Y-%m-%d")

            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            # We should have the last noVersion from the Consultation
            task["consultation_version"] = self.get_consultation_version(
                consultation_uid, dossier_uid,
            )
            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            mapped_pec = mapper.map("AppelPecMetierConsultationCreation")

            # if there is no document attached, we can drop the sub-array
            # XXX : will be modifed when we'll support PeC with attachment(s)
            for c in mapped_pec.consultations:
                if (
                    len(c.pecMetier.documents) == 1
                    and c.pecMetier.documents[0].fileId is None
                ):
                    c.pecMetier.documents = None
            pecs.append(mapped_pec)

        response = (
            self.client.pecMetier.pecMetierConsultationsPOST(
                body=pecs,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response

        result = []
        for item in response:
            for consultation in item.consultations:
                result.append(
                    {
                        "consultation_uid": consultation.idConsultation,
                        "pec_uid": consultation.pecMetier.idPecMetier,
                    }
                )

        return result

    def get_pec_consultation(self, consultation_uid, documents: bool = False):
        """."""
        result = (
            self.client.consultations.pecMetierConsultationGET(
                idConsultation=consultation_uid,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if documents:
            result.pecMetiers[0].documents = self._get_documents(
                result.pecMetiers[0], zipped_archive=True
            )

        return result

    def add_avis_consultation(self, tasks: list, raw: bool = True) -> list:
        """Add avis on Transmission.

            :param tasks: a list of openADS `avis_consultation` output tasks

            :param raw: if True, will return bravado-style objects
                        if False, objects uids

            :return list: a list of bravado-style objects OR a list of avis uids
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        avis = []
        for task in tasks:

            consultation_uid = task["external_uids"]["dossier_consultation"]
            dossier_uid = task["external_uids"]["dossier"]

            log.info(
                f"Sending Avis for Consultation {consultation_uid} on Dossier {dossier_uid}"
            )
            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            # We should provide binary files only if the Avis is not *Tacite*
            if (
                task["avis_decision"]["tacite"] == "f"
                and "local_file_path" in task["instruction"]
            ):

                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["instruction"]["local_file_path"],
                    "file_name" in task["instruction"]
                    and task["instruction"]["file_name"]
                    or None,
                )
                # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
                # 9,"Avis avec ou sans prescription",09/01/2020
                task["file"]["document_type"] = 9

            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            mapped_avis = mapper.map("AppelAvisCreation")

            # Even if no file is provided in task, the mapper will generate a list
            # with a dummy document. Let's get rid of that.
            if (
                task["avis_decision"]["tacite"] == "t"
                or "local_file_path" not in task["instruction"]
            ):
                mapped_avis.avis[0].documents = []

            avis.append(mapped_avis)

        response = (
            self.client.avis.avisPOST(
                body=avis,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response

        result = []
        for item in response:
            for avis in item.avis:
                result.append({"dossier_uid": item.idDossier, "avis_uid": avis.idAvis})

        return result

    def add_prescription(self, tasks: list, raw: bool = True) -> list:
        """Add prescritption on Plat'AU.

            :param tasks: a list of openADS `avis_prescription` output tasks

            :param raw: if True, will return bravado-style objects
                        if False, objects uids

            :return list: a list of bravado-style objects OR a list of prescription uids
        """
        if not isinstance(tasks, list):
            tasks = [tasks]

        upload_app = self._init_SyncplicityUploadConnector()
        upload_app.method = "direct"
        upload_app.start(self.calling_actor)

        prescriptions = []
        for task in tasks:

            consultation_uid = task["external_uids"]["dossier_consultation"]
            dossier_uid = task["external_uids"]["dossier"]

            log.info(
                f"Sending Prescription for Consultation {consultation_uid} on Dossier {dossier_uid}"
            )
            # We should have the last noVersion from the Dossier
            task["dossier_version"] = self.get_dossier_version(dossier_uid)

            has_attachment = "local_file_path" in task["instruction"]
            if has_attachment:

                # Let's push the binary file to syncplicity an retrieve file info
                task["file"] = upload_app.add_file(
                    task["instruction"]["local_file_path"],
                    "file_name" in task["instruction"]
                    and task["instruction"]["file_name"]
                    or None,
                )
                # Nomenclature TYPE_DOCUMENT. Implement something dynamically is useless
                # 11,"Prescriptions indépendantes d’un avis",11/01/2020
                task["file"]["document_type"] = 11

            if "complement_om_html" not in task["instruction"]:
                if "commentaire" in task["instruction"]:
                    task["instruction"]["complement_om_html"] = task["instruction"][
                        "commentaire"
                    ]
                else:
                    task["instruction"]["complement_om_html"] = "N/A"

            mapper = PlatAUMapper(flatten(task, "."), self.client.get_model)
            mapped_prescription = mapper.map("AppelPrescriptionCreation")

            # Even if no file is provided in task, the mapper will generate a list
            # with a dummy document. Let's get rid of that.
            if not has_attachment:
                mapped_prescription.prescriptions[0].documents = []

            prescriptions.append(mapped_prescription)

        response = (
            self.client.prescriptions.prescriptionsPOST(
                body=prescriptions,
                _request_options={
                    "headers": {"Id-Acteur-Appelant": self.calling_actor}
                },
            )
            .response()
            .result
        )

        if raw:
            return response

        result = []
        for item in response:
            for prescription in item.prescriptions:
                result.append(
                    {
                        "dossier_uid": item.idDossier,
                        "prescription_uid": prescription.idEnsemblePrescriptions,
                    }
                )

        return result

    # ##################################################################################
    #
    # Various helpers Utils methods
    #
    # ##################################################################################

    def _extract_filename_from_headers(self, headers: dict):
        """Extract file name from headers.

            :param headers: a headers dict (could be a requests response.headers)

            :return str: the file name

        """
        filename_exp = re.compile("attachment; filename\*\=.*'.*'")

        # Extract and clean-up filename from headers
        try:
            file_name = filename_exp.split(headers["Content-Disposition"])[1]
        except Exception:
            # We don't know why filename extraction failed, but we must have a file name
            log.warning(
                f"Failed to extract file name from {headers}. "
                "We will use a generated file name."
            )
            file_name = str(uuid.uuid4())

        return file_name

    def _get_documents(
        self, bravado_object: BravadoModel, zipped_archive: bool = False
    ):
        """Download and store document(s) against a Bravado object.

            :param bravado_object: a bravado object, with first-level key `documents`

            :param zipped_archive: if True, returns document(s) as a zip archive

            :return BravadoModel: a bravado object, completed with binary file info
        """
        if not hasattr(bravado_object, "documents"):
            raise PlatAUError(f"No `documents` attributes on object {bravado_object}")
        elif bravado_object.documents is None:
            return []

        result = []

        zip_file = None
        if len(bravado_object.documents) > 1 and zipped_archive:
            with tempfile.NamedTemporaryFile(delete=False) as f:
                zip_file_path = f.name
            zip_file = ZipFile(zip_file_path, "w")

        for document in bravado_object.documents:
            headers = {"Authorization": f"Bearer {document.token}"}

            response = requests.get(
                document.url, headers=headers, timeout=REQUESTS_TIMEOUT
            )
            if response.status_code != 200:
                log.error(f"Could not download document from {document.url}")
                continue

            file_name = self._extract_filename_from_headers(response.headers)

            if zip_file is None:
                with tempfile.NamedTemporaryFile(delete=False) as f:
                    f.write(response.content)
                    temp_path = f.name

                document.file_name = file_name
                document.file_path = temp_path
                with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
                    document.content_type = m.id_buffer(response.content)

                result.append(document)
                log.info(
                    f"Retrieved Document {file_name} with content-type {document.content_type}"
                )

            else:
                zip_file.writestr(file_name, response.content)
                log.info(
                    f"Retrieved Document {file_name} and write it to archive {zip_file_path}"
                )

        if zip_file is not None:
            zip_file.close()
            doc = self.client.get_model("DocumentR")()
            doc.file_name = "avis_consultation.zip"
            doc.file_path = zip_file_path
            doc.content_type = "application/zip"

            result.append(doc)

        return result
