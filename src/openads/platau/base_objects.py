import logging
import magic
import os
import tempfile

from base64 import b64decode
from dataclasses import dataclass

log = logging.getLogger(__name__)

ACTOR_ROLES = [
    "guichet_unique",
    "service_instructeur",
    "service_instructeur_epci",
    "service_instructeur_ddt",
    "autorite_competente",
    "autorite_competente_epci",
    "service_consulte",
]


@dataclass
class Attachment(object):
    name: str
    path: str
    size: int
    content_type: str = ""

    def __init__(
        self,
        name: str,
        data: str,
        content_type: str = None,
        b64: bool = False,
        guess_content_type: bool = False,
    ):
        """."""
        self.name = name

        if b64:
            data = b64decode(data)

        with tempfile.NamedTemporaryFile(delete=False) as fd:
            fd.write(data)
            self.path = fd.name

        self.size = os.path.getsize(self.path) / (1024 * 1024)  # size in MB

        if guess_content_type:
            with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
                self.content_type = m.id_buffer(data)
        else:
            self.content_type = content_type

    def __del__(self):
        """"."""
        try:
            os.remove(self.path)
        except FileNotFoundError:
            log.warning(f"File {self.name} at {self.path} already removed")

    def __eq__(self, other):
        """."""
        return (
            self.name == other.name
            and self.size == other.size
            and self.content_type == other.content_type
        )


@dataclass
class Actor(object):
    role: str = None
    uid: str = None
    # XXX - we should use an Organization object
    organization: str = None
    organization_type: str = None
    organization_label: str = None

    label: str = None
    insee: str = None
    insee_competence: list = None
    email: str = None
    siren: str = None
    depot_adau: bool = False

    extra_properties: dict = None

    def match(self, mask):
        """."""
        if mask.role is not None and mask.role != self.role:
            return False

        if mask.uid is not None and mask.uid != self.uid:
            return False

        if mask.organization is not None and mask.organization != self.organization:
            return False

        return True
