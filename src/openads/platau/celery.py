from openads.platau.tasks import Task
from celery import Celery

app = Celery("openads.platau", broker="pyamqp://guest@localhost//")
app.conf.update(task_serializer="pickle", accept_content=["pickle"])


@app.task
def task_runner(task: Task):
    """."""
    task.run()
