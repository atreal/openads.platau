# -*- coding: utf-8 -*-

import os
import base64

from openads.platau.conductor import Conductor, OAPConfig
from openads.platau.tests.base_test import BaseTestCase, CT_CONFIG_PATH, CURRENT_PATH
from openads.platau.syncplicity_connector import SyncplicityUploadConnector


class SyncplicityUploadConnectorTest(BaseTestCase):
    """This test case validate PlatAUConnetcor against current API.
        No interaction with openADS are done.
    """

    def setUp(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        # if self.dummy_backend is None:
        #     self.dummy_backend = DOSSIERS.copy()
        # self.actor = ACTEURS["culture"]["DEV-UDAP44"]
        # self.app = PlatAUConnector(
        #     self.conf.platau_credentials,
        #     self.conf.spec_path,
        #     spec_overrides=self.conf.spec_overrides,
        #     calling_actor=self.actor,
        # )
        # self.app.start(self.actor)
        # self.openads_url = self.conf.openadsapi_url
        # self.openads_auth = self.conf.openadsapi_config["auth"]
        # self.kwargs = FakeArgs()
        # spec_overrides = getattr(self, "spec_overrides", {})
        if (
            self.conf.spec_overrides != {}
            and self.conf.spec_overrides.get("sandbox") is True
        ):
            spec_overrides = {"sandbox": True}
            self.connector = SyncplicityUploadConnector(
                self.conf.platau_credentials, spec_overrides=spec_overrides
            )
        else:
            self.connector = SyncplicityUploadConnector(self.conf.platau_credentials)
        self.connector.start()

    def test_direct_upload(self):
        """ """
        self.connector.method = "direct"
        result = self.connector.add_file(f"{CURRENT_PATH}/binary_files/DP_116.pdf")
        expected_keys = set(
            ["data_file_id", "data_file_version_id", "VirtualFolderId", "sha512",]
        )
        # Result should look like:
        # {'data_file_id': '276241063', 'data_file_version_id': '348869510', 'VirtualFolderId': '200369043', 'sha512': 'e51531fd90c3ad7013354265492062f4b7c9e5a7b7aec0d263bba3184a5493d71d10fa06da686bd146549c344cf9804095e94aa988395c6b363c41e2dc2d0907'}}
        self.assertEqual(expected_keys, set([k for k in result.keys()]))

    def test_pre_upload(self):
        """ """
        self.connector.method = "pre-upload"
        result = self.connector.add_file(f"{CURRENT_PATH}/binary_files/DP_116.pdf")
        expected_keys = set(
            [
                "Status",
                "data_file_id",
                "data_file_version_id",
                "client_file_version_id",
                "date_added_utc",
                "VirtualFolderId",
                "sha512",
            ]
        )
        # Result should look like:
        # {'Status': 'Already Stored', 'data_file_id': '276242330', 'data_file_version_id': '348870822', 'client_file_version_id': '348870822', 'date_added_utc': '2024-01-19T08:15:54.2630000Z', 'VirtualFolderId': '200369043', 'sha512': 'e51531fd90c3ad7013354265492062f4b7c9e5a7b7aec0d263bba3184a5493d71d10fa06da686bd146549c344cf9804095e94aa988395c6b363c41e2dc2d0907'}
        self.assertEqual(expected_keys, set([k for k in result.keys()]))

    def test_upload_autoselect_method(self):
        """ XXX """
        pass

    def test_non_ASCII_filename(self):
        """ """
        self.connector.method = "direct"
        result = self.connector.add_file(
            f"{CURRENT_PATH}/binary_files/DP_116_façade_étages.pdf"
        )
        expected_keys = set(
            ["data_file_id", "data_file_version_id", "VirtualFolderId", "sha512",]
        )
        # Result should look like:
        # {'data_file_id': '276241063', 'data_file_version_id': '348869510', 'VirtualFolderId': '200369043', 'sha512': 'e51531fd90c3ad7013354265492062f4b7c9e5a7b7aec0d263bba3184a5493d71d10fa06da686bd146549c344cf9804095e94aa988395c6b363c41e2dc2d0907'}}
        self.assertEqual(expected_keys, set([k for k in result.keys()]))

        self.connector.method = "pre-upload"
        result = self.connector.add_file(
            f"{CURRENT_PATH}/binary_files/DP_116_façade_étages.pdf"
        )
        expected_keys = set(
            [
                "Status",
                "data_file_id",
                "data_file_version_id",
                "client_file_version_id",
                "date_added_utc",
                "VirtualFolderId",
                "sha512",
            ]
        )
        # Result should look like:
        # {'Status': 'Already Stored', 'data_file_id': '276242330', 'data_file_version_id': '348870822', 'client_file_version_id': '348870822', 'date_added_utc': '2024-01-19T08:15:54.2630000Z', 'VirtualFolderId': '200369043', 'sha512': 'e51531fd90c3ad7013354265492062f4b7c9e5a7b7aec0d263bba3184a5493d71d10fa06da686bd146549c344cf9804095e94aa988395c6b363c41e2dc2d0907'}
        self.assertEqual(expected_keys, set([k for k in result.keys()]))
