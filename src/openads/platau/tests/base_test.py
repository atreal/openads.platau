# -*- coding: utf-8 -*-

import hashlib
import json
import os
import logging
import unittest
import requests
import tempfile
import time
import uuid

from base64 import b64decode, b64encode
from dataclasses import dataclass
from datetime import date, timedelta

from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import OAPConfig


REMOTE_URL = os.getenv("openadsapi_url", "http://api.openads.local:6543")
SC_OPENADS_LEGACY_WS = (
    os.getenv("sc_openads_legacy_ws_url", ""),
    tuple(os.getenv("sc_openads_legacy_ws_auth", "").split(":")),
)
CT_OPENADS_LEGACY_WS = (
    os.getenv("ct_openads_legacy_ws_url", ""),
    tuple(os.getenv("ct_openads_legacy_ws_auth", "").split(":")),
)
# OPENADS_LEGACY_WS_AUTH = tuple(os.getenv("openads_legacy_ws_auth", "").split(":"))
CT_CONFIG_PATH = os.getenv("ct_platau_config_path", "")
SC_CONFIG_PATH = os.getenv("sc_platau_config_path", "")

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

CONSULTATION_TEMPLATE = {
    "consultation": {
        "date_envoi": "27/11/2000",
        "texte_reglementaire": "Ceci est un texte indiquant le(s) article(s) réglémentaire(s) sur le(s)quel(s) se fonde la consultation",
        "texte_objet_consultation": "Ce texte descriptif de l'objet de la consultation",
    },
    "service": {
        "type_consultation": "avec_avis_attendu",
        "delai_type": "mois",
        "delai": "2",
        "uid_platau_acteur": "",
    },
    "task": {"acteur": None},
}


@dataclass
class FakeArgs:
    """Fake command line params."""

    mode: str = None
    stream: str = None
    asynchronous: bool = False
    log_level: str = logging.DEBUG
    verbosity: int = 0
    offset: int = None
    actor: str = None
    task: str = None


def time_me(func):
    """."""

    def wrapper(*arg):
        """."""
        # func_name = func.__name__
        # print(func_name)
        # print("-" * len(func_name))
        start = time.time()
        result = func(*arg)
        end = time.time()
        print(f"Total test time : {end - start}")
        return result

    return wrapper


class BaseTestCase(unittest.TestCase):
    """."""

    dummy_backend = None

    # def setUp(self):
    #     """."""
    #     self.openads_url = self.conf.openadsapi_url
    #     self.openads_auth = self.conf.openadsapi_config["auth"]

    # def set_auth(self):
    #     """."""
    #     self.auth = tuple(
    #         [self.conf.openadsapi_username, self.conf.openadsapi_password]
    #     )

    def start_platau_connector(self, mode: str):
        """."""
        config_path = mode == "CT" and CT_CONFIG_PATH or SC_CONFIG_PATH
        self.conf = OAPConfig(config_path)
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
        )
        self.app.start()

    def _get_remote_dossier(self, dossier_id):
        """Get openads Dossier via opeands.api."""

        # Let's try to get some info, the old way
        response = requests.get(
            "{}/dossier/{}".format(self.openads_url, dossier_id), auth=self.openads_auth
        )

        if not response.ok:
            return

        return response.json()

    def _get_tasks(self, criterions={}):
        """Get tasks from openADS."""

        url = "{}/platau/tasks".format(self.openads_url)
        if criterions is not {}:
            url = "{}?{}".format(
                url,
                ("&").join(
                    [
                        "{}={}".format(criterion, value)
                        for criterion, value in criterions.items()
                    ]
                ),
            )

        response = requests.get(url, auth=self.openads_auth)

        if not response.ok:
            return

        return response.json()

    def _get_task(self, task_id=None, json_path=None):
        """Get task from openADS."""

        if task_id is None and json_path is None:
            raise ValueError("Both values cannot be None")

        # XXX : fail-soft mode
        # Get task from dumpped FS file
        if json_path:
            file_path = "{}/{}".format(
                os.path.dirname(os.path.abspath(__file__)), json_path
            )
            return json.loads(get_file_data(file_path, b64=False))

        response = requests.get(
            "{}/platau/task/{}".format(self.openads_url, task_id),
            auth=self.openads_auth,
        )

        if not response.ok:
            return

        return response.json()

    def _update_task(self, task_id, state=None, external_uid=None, update_backend=None):
        """Update task in openADS."""
        if state is None and external_uid is None:
            raise ValueError("Both values cannot be None")

        payload = {}
        if state is not None:
            payload["state"] = state
        if external_uid is not None:
            payload["external_uid"] = external_uid

        # XXX : fail-soft mode
        if update_backend:
            self.dummy_backend[update_backend["dossier"]]["external_uids"][
                update_backend["typology"]
            ] = external_uid
            return

        response = requests.put(
            "{}/platau/task/{}".format(self.openads_url, task_id),
            data=json.dumps(payload),
            auth=self.openads_auth,
        )

        if not response.ok:
            return

        return response.json()

    def _get_file(self, task_id=None, file_path=None):
        """Get task from openADS."""

        if task_id is None and file_path is None:
            raise ValueError("Both values cannot be None")

        # XXX : fail-soft mode
        # Get task from dumpped FS file
        if file_path:
            file_path = "{}/{}".format(
                os.path.dirname(os.path.abspath(__file__)), file_path
            )
            with open(file_path) as f:
                return f.read()
            return json.loads(get_file_data(file_path, b64=False))

        # Let's try to get some info, the old way
        response = requests.get(
            "{}/platau/task/{}/file".format(self.openads_url, task_id),
            auth=self.openads_auth,
        )

        if not response.ok:
            return

        file_data = response.json()["files"][0]
        with tempfile.NamedTemporaryFile(delete=False) as f:
            f.write(b64decode(file_data["b64_content"]))
            temp_path = f.name

        return file_data["filename"], temp_path

    def run_openads_taskmanager(self, mode: str):
        """Call the task manager WS in openADS."""
        url, auth = mode == "CT" and CT_OPENADS_LEGACY_WS or SC_OPENADS_LEGACY_WS
        print(
            f"Running tasks manager with params:\n"
            f"URL: {url}/taches\n"
            f"AUTH: {auth}"
        )
        response = requests.post(
            f"{url}/taches",
            auth=auth,
            json={"module": "taches"},
            headers={"Content-Type": "application/json"},
        )
        print(response)
        return response

    def add_instruction(
        self,
        dossier: str,
        evenement: str,
        lettre_type: bool = False,
        event_date: date = None,
        signataire_arrete: str = None,
    ) -> dict:
        """Update task in openADS."""

        # log.info(f"Adding Instruction {instruction} on Dossier {dossier}")
        payload = {"dossier_id": dossier, "evenement": evenement}
        if lettre_type:
            payload["lettre_type"] = lettre_type
        if event_date is not None:
            payload["date_evenement"] = event_date.strftime("%d/%m/%Y")
        if signataire_arrete is not None:
            payload["signataire_arrete"] = signataire_arrete

        response = requests.post(
            f"{self.openads_url}/instructions",
            data=json.dumps(payload),
            auth=self.openads_auth,
        )
        if not response.ok:
            return

        # Finaliser
        # POST
        # http://glenfiddich.atreal.net/~v13055/openads/app/index.php?module=sousform&obj=instruction&idx=243&action=100&advs_id=16384865009472&retourformulaire=dossier_instruction&idxformulaire=DP0130552100009&retour=form&validation=1&contentonly=true

        # Suiv des dates
        # date_retour_signature
        # POST
        # =Valider&instruction=243&destinataire=DP0130552100009&=undefined&evenement=452&commentaire=&date_evenement=03%2F12%2F2021&lettretype=decision_non_opposition_DP_sr&signataire_arrete=1&flag_edition_integrale=f&om_final_instruction_utilisateur=admin%20(Administrateur)&=undefined&date_finalisation_courrier=03%2F12%2F2021&date_envoi_signature=&date_envoi_rar=&date_envoi_controle_legalite=&date_retour_signature=03%2F12%2F2021&date_retour_rar=&date_retour_controle_legalite=&numero_arrete=&complement_om_html=&bible_auto=bible_auto()&bible=bible(1)&complement2_om_html=&bible2=bible(2)&complement3_om_html=&bible3=bible(3)&complement4_om_html=&bible4=bible(4)&titre_om_htmletat=&corps_om_htmletatex=&btn_preview=&btn_redaction=&btn_refresh=&live_preview=&dossier=DP0130552100009&action=modif_etat_tacite&delai=0&etat=Accord_en_cours&autorite_competente=&accord_tacite=Non&delai_notification=0&avis_decision=7&archive_delai=1&archive_accord_tacite=Oui&archive_etat=notifier&archive_avis=&archive_date_complet=2021-12-03&archive_date_rejet=&archive_date_limite=2022-01-03&archive_date_notification_delai=2022-01-03&archive_date_decision=&archive_date_validite=&archive_date_achevement=&archive_date_conformite=&archive_date_chantier=&archive_date_dernier_depot=2021-12-03&date_depot=2021-12-03&date_depot_mairie=&complement5_om_html=&bible5=&complement6_om_html=&bible6=&complement7_om_html=&bible7=&complement8_om_html=&bible8=&complement9_om_html=&bible9=&complement10_om_html=&bible10=&complement11_om_html=&bible11=&complement12_om_html=&complement13_om_html=&complement14_om_html=&complement15_om_html=&archive_incompletude=f&archive_incomplet_notifie=f&archive_evenement_suivant_tacite=218&archive_evenement_suivant_tacite_incompletude=&archive_etat_pendant_incompletude=&archive_date_limite_incompletude=&archive_delai_incompletude=&archive_autorite_competente=1&code_barres=110000000243&om_fichier_instruction=e1190cddad81541e42fe4b6f7cb3c351&om_final_instruction=t&om_fichier_instruction_dossier_final=f&document_numerise=&duree_validite_parametrage=36&duree_validite=36&created_by_commune=f&archive_date_cloture_instruction=&archive_date_premiere_visite=&archive_date_derniere_visite=&archive_date_contradictoire=&archive_date_retour_contradictoire=&archive_date_ait=&archive_date_transmission_parquet=&archive_dossier_instruction_type=600&archive_date_affichage=&pec_metier=&archive_pec_metier=&archive_a_qualifier=t&id_parapheur_signature=&statut_signature=&commentaire_signature=&historique_signature=&suivi_notification=&preview_edition=&form_resubmit_identifier=e73df1f61053198be63531f46d32aabf&=Valider&

        return response.json()

    def cleanup_tasks(
        self, typology: str = None, state: str = None, category: str = None
    ):
        """Clean-up specific tasks before running any testcase."""
        criterions = {}
        if typology:
            criterions = {"typology": typology}
        if state:
            criterions["state"] = state
        if category:
            criterions["category"] = category

        print(f"Cleaning tasks with criterions : {criterions}")
        existing_tasks = self._get_tasks(criterions)
        if not existing_tasks:
            return
        for t in existing_tasks:
            self._update_task(t["task"], state="debug")

    def _waiting_prompt(self, timeout: int = 10):
        """."""
        print(f"Let's wait {timeout} seconds")
        for i in range(timeout):
            print(".", end="", flush=True)
            time.sleep(1)
        print(".", flush=True)

    def control_binaries_ack(
        self,
        platau_connector: PlatAUConnector,
        actor_uid: str,
        targeted_uids: list,
        timeout: int = 30,
        waiting_interval: int = 2,
    ):
        """. """
        # We should wait Pieces ACK
        print("Plat'AU files handling is async. Let's check if files are OK.")
        uids = targeted_uids.copy()
        for i in range(timeout):
            notifications = platau_connector.get_notifications(actor_uid).notifications
            targeted_objects = [n.idElementConcerne for n in notifications]
            for uid in uids:
                if uid in targeted_objects:
                    uids.remove(uid)

            if len(uids) == 0:
                break

            if i == 0:
                print(f"Wait {timeout * waiting_interval} seconds more")
            print(".", end="", flush=True)
            time.sleep(waiting_interval)

        if i != timeout - 1:
            print("\nBinarie(s) successfully received")
            print(f"Notifications : {notifications}")
            return i * waiting_interval
        else:
            print("\nSome binarie(s) were not properly received.")
            return

    def get_hash_by_path(self, file_path: str):
        """."""
        with open(file_path, "rb") as f_local:
            return hashlib.sha512(f_local.read()).hexdigest()

    def _build_dataset(self, dataset=None, local_number=None):
        """Build a basic dataset on Plat'AU"".
            - create a Projet
            - create a Dossier
            - add some pieces
            - apply the Depot
            - set the Competence
            - send consultation

            1 / First option
            You should have have a class attribute named `dummy_backend`. It will be
            used as a openADS replacement backend, providing dataset and in charge of
            results storage. `dummy_backend` will updated in place, no values are returned.

            Structure must be like :

            {
                "DOSSIER_NAME": {
                    "insee": "27004",
                    "acteurs": {
                        "guichet_unique": ID_ACTEUR_01,
                        "service_instructeur": ID_ACTEUR_01,
                    },
                    "tasks": {
                        "creation_DA": 6,
                        "creation_DI": 7,
                        "ajout_piece": [116, 117,],
                        "depot_dossier": 7,
                        "qualification_DI": 8,
                        "consultation": ["8XP-YEN-0XP",],
                    },
                    "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
                    "local_number": "",
                    "type": "DP",
                    "parcelles": ["000ab0123", "098A01245"],
                    "competence": "etat_maire",
                }
            }

            2 / Second option
            You can provide your own dataset, with the `dataset` param. Updated `dataset`
            will returned.

        """
        # Lading CT config
        conf = OAPConfig(CT_CONFIG_PATH)

        platau = PlatAUConnector(
            conf.platau_credentials, conf.spec_path, spec_overrides=conf.spec_overrides
        )
        platau.start()

        result = {}
        store_results_in_place = False
        if dataset is None:
            dataset = self.dummy_backend
            store_results_in_place = True

        today = date.today().strftime("%Y-%m-%d")
        yesterday = (date.today() - timedelta(days=1)).strftime("%Y-%m-%d")
        date_limite = (date.today() + timedelta(days=30)).strftime("%Y-%m-%d")

        for dossier, values in dataset.items():

            result[dossier] = {}

            insee = values["insee"]
            acteur_GU = values["acteurs"]["guichet_unique"]
            acteur_SI = values["acteurs"]["service_instructeur"]
            platau.calling_actor = acteur_GU

            # DA
            if "creation_DA" in values["tasks"]:
                print("Creating Projet")
                task_id = values["tasks"]["creation_DA"]
                task = self._get_task(
                    task_id=task_id,
                    json_path="json_tasks/{}_creation_DA.json".format(values["type"]),
                )

                task["dossier_autorisation"]["insee"] = insee
                task["task"]["acteur"] = acteur_GU

                projet_uid = platau.create_projet(task)[0]

                print("Projet created : {}".format(projet_uid))

                update_backend = {
                    "dossier": dossier,
                    "typology": "dossier_autorisation",
                }
                self._update_task(task_id, "done", projet_uid, update_backend)

            # DI
            if "creation_DI" in values["tasks"]:
                task_id = values["tasks"]["creation_DI"]
                task = self._get_task(
                    task_id=task_id,
                    json_path="json_tasks/{}_creation_DI.json".format(values["type"]),
                )
                task["dossier"]["insee"] = insee
                task["task"]["acteur"] = acteur_GU
                task["external_uids"].update(
                    {
                        "dossier_autorisation": values["external_uids"][
                            "dossier_autorisation"
                        ]
                    }
                )

                # Override few data from the json
                if "parcelles" in values:
                    task["dossier_parcelle"] = {
                        r_f: {"libelle": r_f, "no_ordre": "1"}
                        for r_f in values["parcelles"]
                    }

                if local_number is not None:
                    # Priority to local_number passed as method parameter
                    pass

                elif values.get("local_number", None):
                    # then the dataset one
                    local_number = values.get("local_number")

                else:
                    # Fallback : we provide a unique Dossier local number, without suffix
                    if task["dossier"]["dossier"].endswith("P0"):
                        task["dossier"]["dossier"] = task["dossier"]["dossier"][:-2]
                    local_number = (
                        task["dossier"]["dossier_autorisation"][:-5]
                        + str(uuid.uuid1()).split("-")[0][:-3]
                    )

                task["dossier"]["dossier_autorisation"] = local_number

                task["dossier"]["date_complet"] = yesterday
                task["dossier"]["date_demande"] = yesterday
                task["dossier"]["date_depot"] = yesterday
                task["dossier"]["date_dernier_depot"] = yesterday
                task["dossier"]["date_limite"] = date_limite

                # we store it for further usage in next tests case
                result[dossier]["local_number"] = local_number

                print(f"Creating Dossier: {local_number}")
                dossier_uid = platau.create_dossier(task)[0]

                print(f"Dossier created : {dossier_uid}")

                update_backend = {"dossier": dossier, "typology": "dossier"}
                self._update_task(task_id, "done", dossier_uid, update_backend)

            # Pieces
            if "ajout_piece" in values["tasks"]:
                print("Adding Piece(s)")
                tasks = []
                for task_id in values["tasks"]["ajout_piece"]:

                    task = self._get_task(
                        task_id=task_id,
                        json_path=f"json_tasks/ajout_piece_{task_id}.json",
                        # json_path="json_tasks/{}_ajout_piece_{}.json".format(
                        #     values["type"], task_id
                        # ),
                    )

                    task["task"]["acteur"] = acteur_GU

                    file_path = "{}/binary_files/{}".format(
                        os.path.dirname(os.path.abspath(__file__)),
                        task["document_numerise"]["nom_fichier"],
                    )

                    task["document_numerise"]["local_file_path"] = file_path

                    # XXX : fail-safe mode
                    task["external_uids"].update(
                        {
                            "dossier_autorisation": values["external_uids"][
                                "dossier_autorisation"
                            ],
                            "dossier": values["external_uids"]["dossier"],
                        }
                    )
                    tasks.append(task)
                pieces_uids = platau.create_piece(tasks)
                print(f"Piece(s) added : {pieces_uids}")

                update_backend = {"dossier": dossier, "typology": "pieces"}
                self._update_task(None, "done", pieces_uids.copy(), update_backend)

                if len(values["tasks"]["ajout_piece"]) >= 1:
                    # We should wait Pieces ACK
                    self.control_binaries_ack(platau, acteur_GU, pieces_uids)

            # Depot
            if "depot_dossier" in values["tasks"]:
                print("Depot Dossier")
                task_id = values["tasks"]["depot_dossier"]
                task = self._get_task(
                    task_id=task_id,
                    # json_path="json_tasks/{}_creation_DI.json".format(values["type"]),
                    json_path="json_tasks/depot_DI.json",
                )

                task["dossier"]["date_depot"] = today
                task["external_uids"].update(
                    {
                        "dossier_autorisation": values["external_uids"][
                            "dossier_autorisation"
                        ],
                        "dossier": values["external_uids"]["dossier"],
                    }
                )

                platau.depot_dossier(task)

            # Qualification
            if "qualification_DI" in values["tasks"]:
                print("Qualification Dossier")

                task_id = values["tasks"]["qualification_DI"]
                task = self._get_task(
                    task_id=task_id, json_path="json_tasks/qualification_DI.json",
                )

                task["external_uids"].update(
                    {"dossier": values["external_uids"]["dossier"]}
                )

                if values["competence"] == "etat":
                    task["dossier"]["autorite_competente_code"] = "ETAT"
                elif values["competence"] == "etat_maire":
                    task["dossier"]["autorite_competente_code"] = "ETATMAIRE"
                elif values["competence"] == "commune":
                    task["dossier"]["autorite_competente_code"] = "COM"
                else:
                    raise ValueError("Found an unknown Competence")

                task["task"]["acteur"] = acteur_SI

                platau.qualification_dossier(task)

            # Modification DI
            if "modification_DI" in values["tasks"]:
                print("Editing Dossier")
                task = self._get_task(
                    json_path="json_tasks/{}_modification_DI.json".format(
                        values["type"]
                    ),
                )
                task["dossier"]["insee"] = insee
                task["task"]["acteur"] = acteur_GU
                task["external_uids"].update(
                    {
                        "dossier_autorisation": values["external_uids"][
                            "dossier_autorisation"
                        ],
                        "dossier": values["external_uids"]["dossier"],
                    }
                )

                # Override few data from the json
                overrides = values["tasks"]["modification_DI"]
                if overrides is not None:
                    for k, v in overrides.items():
                        prop, subprop = k.split(".")
                        task[prop][subprop] = v

                dossier_uid = platau.update_dossier(task)[0]

                print("Dossier updated : {}".format(dossier_uid))

                update_backend = {"dossier": dossier, "typology": "dossier"}
                self._update_task(task_id, "done", dossier_uid, update_backend)

            if "consultation" in values["tasks"]:

                print("Sending Consultation")

                if not isinstance(values["tasks"]["consultation"], list):
                    target_services = [values["tasks"]["consultation"]]
                else:
                    target_services = values["tasks"]["consultation"]

                consultation_uids = []
                for service_consulte in target_services:
                    task = CONSULTATION_TEMPLATE.copy()
                    task["external_uids"] = values["external_uids"].copy()
                    task["task"]["acteur"] = acteur_SI
                    platau.calling_actor = acteur_SI
                    task["service"]["uid_platau_acteur"] = service_consulte
                    task["consultation"]["date_consultation"] = yesterday
                    print(task)
                    consultation_uids.extend(platau.add_consultation(task, raw=False))

                update_backend = {"dossier": dossier, "typology": "consultation"}
                self._update_task(None, "done", consultation_uids, update_backend)

            if (
                "incompletude_DI" in values["tasks"]
                and values["tasks"]["incompletude_DI"]
            ):

                print("Sending Incompletude")

                # XXX make the task dynnamic
                platau.calling_actor = acteur_SI

                task["dossier"] = {"incomplet_notifie": "t"}
                task["dossier"]["date_limite_incompletude"] = "2021-09-01"
                task["external_uids"].update(
                    {"dossier": values["external_uids"]["dossier"]}
                )

                platau.set_completude(task)

            if "completude_DI" in values["tasks"] and values["tasks"]["completude_DI"]:

                print("Sending Completude")

                # XXX make the task dynnamic
                platau.calling_actor = acteur_SI

                task["dossier"] = {"incomplet_notifie": "f"}
                task["dossier"]["date_complet"] = "2022-09-01"
                task["external_uids"].update(
                    {"dossier": values["external_uids"]["dossier"]}
                )

                platau.set_completude(task)

            if "decision_DI" in values["tasks"]:

                print("Adding Decision on Dossier")

                task_id = values["tasks"]["decision_DI"]
                task = self._get_task(
                    task_id=task_id,
                    json_path="json_tasks/{}_decision_DI.json".format(values["type"]),
                )

                task["task"]["acteur"] = values["acteurs"]["autorite_competente"]
                platau.calling_actor = values["acteurs"]["autorite_competente"]
                task["task"]["signataire"] = values["acteurs"]["autorite_competente"]

                file_path = "{}/binary_files/{}".format(
                    os.path.dirname(os.path.abspath(__file__)),
                    task["instruction"]["nom_fichier"],
                )
                task["instruction"]["local_file_path"] = file_path

                task["external_uids"].update(
                    {"dossier": values["external_uids"]["dossier"]}
                )

                platau.decision(task)

        for dossier in dataset:
            if "local_number" in result[dossier]:
                dataset[dossier]["local_number"] = result[dossier]["local_number"]

        print(dataset)

        if store_results_in_place:
            self.dummy_backend = dataset
        else:
            return dataset


class UtilsTestCase(BaseTestCase):
    """."""

    def test_debug(self):
        """."""
        import ipdb

        ipdb.set_trace()

    def test_get_nomenclature(self):
        """."""
        for code in (
            "DEST_INST",
            "ETAT_GENE_DOSSIER",
            "ETAT_PIECE",
            "GENRE",
            "INFO_LEGI_CONNEXE",
            "MODALITE_DEPOT",
            "NATURE_DOSSIER",
            "NATURE_PIECE",
            "NATURE_TRAVAUX",
            "NIVEAU_AGREGATION",
            "PRECISION_NAT_TRAV",
            "ROLE",
            "SOUS_DEST_INST",
            "STATUT_INFO",
            "STATUT_VU_DU_DOSSIER",
            "TYPE_ACTEUR",
            "TYPE_ADRESSE",
            "TYPE_COORDONNEE",
            "TYPE_DOSSIER",
            "TYPE_PIECE",
        ):
            result = self.app.get_nomenclature(code)
            print("%s résulats pour la nomenclature %s" % (len(result), code))
            print(result)
            if code == "TYPE_PIECE":
                import ipdb

                ipdb.set_trace()


def drop_linebreak(value):
    """."""
    if value.endswith("\n"):
        return value[:-1]


def get_file_data(path, b64=True):
    """."""
    with open(path, "r") as f:
        if b64:
            return b64encode(f.read())
        return f.read()
