# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import datetime, timedelta
from random import choice

from openads.platau.tests.base_test import (
    BaseTestCase,
    CT_CONFIG_PATH,
    FakeArgs,
    OAPConfig,
    REMOTE_URL,
    time_me,
)
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import Conductor


ACTEURS = {
    "32013": {"guichet_unique": "YX3-G82-PD1", "service_instructeur": "EWV-20J-VRW",},
    "culture": {
        # # XP
        "UDAP14": "71Z-ZKN-D1Q",
    },
}

DOSSIERS = {
    # "CU": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "CU",
    #     "competence": "commune",
    # },
    # "DP": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "DP",
    #     "competence": "commune",
    # },
    # "DPL": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "DPL",
    #     "competence": "commune",
    # },
    # "PD": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "PD",
    #     "competence": "commune",
    # },
    # "PA": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "PA",
    #     "competence": "commune",
    # },
    # "PC": {
    #     "insee": "32013",
    #     "acteurs": {
    #         "guichet_unique": ACTEURS["32013"]["guichet_unique"],
    #         "service_instructeur": ACTEURS["32013"]["service_instructeur"],
    #     },
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [116, 118,],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "incompletude_DI": True,
    #         "completude_DI": True,
    #         "consultation": [ACTEURS["culture"]["UDAP14"],],
    #     },
    #     "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
    #     "local_number": "",
    #     "type": "PC",
    #     "competence": "commune",
    # },
    "PCMI": {
        "insee": "32013",
        "acteurs": {
            "guichet_unique": ACTEURS["32013"]["guichet_unique"],
            "service_instructeur": ACTEURS["32013"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [116, 118,],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": True,
            "completude_DI": True,
            "consultation": [ACTEURS["culture"]["UDAP14"],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "PCMI",
        "competence": "commune",
    },
}


class XPAutresDossiers(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()
        self.actor = ACTEURS["32013"]["guichet_unique"]
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)
        self.openads_url = self.conf.openadsapi_url
        self.openads_auth = self.conf.openadsapi_config["auth"]
        self.kwargs = FakeArgs()

    def test_00_XPAD_build_dataset(self):
        """."""
        self._build_dataset()
        for dossier, values in self.dummy_backend.items():
            response = self.app.get_dossier_by_uid(
                self.dummy_backend[dossier]["external_uids"]["dossier"], raw=True
            )
            print(
                json.dumps(
                    response[0]._as_dict(), indent=2, sort_keys=True, default=str
                )
            )
            with open(f"/tmp/{dossier}.json", "w") as f:
                json.dump(
                    response[0]._as_dict(), f, indent=2, sort_keys=True, default=str
                )

            if dossier not in ("PA", "PC", "PCMI"):
                continue

            response = self.app.get_infofiscales(
                self.dummy_backend[dossier]["external_uids"]["dossier"]
            )
            print(
                json.dumps(
                    [item._as_dict() for item in response],
                    indent=2,
                    sort_keys=True,
                    default=str,
                )
            )
            with open(f"/tmp/{dossier}_fiscalite.json", "w") as f:
                json.dump(
                    [item._as_dict() for item in response],
                    f,
                    indent=2,
                    sort_keys=True,
                    default=str,
                )

    def test_00_XPAD_fiscalite(self):
        """."""
        response = self.app.get_infofiscales("71O-4EL-NV1")
        print(
            json.dumps(
                [item._as_dict() for item in response],
                indent=2,
                sort_keys=True,
                default=str,
            )
        )
        with open(f"/tmp/PA_fiscalite.json", "w") as f:
            json.dump(
                [item._as_dict() for item in response],
                f,
                indent=2,
                sort_keys=True,
                default=str,
            )
