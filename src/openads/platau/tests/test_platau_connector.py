# -*- coding: utf-8 -*-

import os
import time
import uuid

from datetime import date, datetime
from pypdf import PdfReader

from openads.platau.tests.base_test import (
    BaseTestCase,
    CT_CONFIG_PATH,
)
from openads.platau.base_objects import Actor
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import OAPConfig
from openads.platau.mapping import PLATAU_MAPPING

ACTEURS = {
    "32013": {
        "guichet_unique": "YX3-G82-PD1",
        "service_instructeur": "EWV-20J-VRW",
        "autorite_competente": "LX6-L20-KEW",
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
    },
}


class PlatAUConnectorTest(BaseTestCase):
    """This test case validate PlatAUConnetcor against current API.
        No interaction with openADS are done.
    """

    def setUp(self):
        """."""
        # if self.dummy_backend is None:
        #     self.dummy_backend = DOSSIERS.copy()
        self.actor = ACTEURS["32013"]["service_instructeur"]
        self.conf = OAPConfig(CT_CONFIG_PATH)
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)

    def _test_00_PAC_debug(self):
        """."""
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on
        self.assertTrue(self.app.status())

    def test_01_CT_instruction_complete(self):
        """Usecase:
            - create Projet
            - create Dossier
            - add 2 Pieces
            - notify Depot
            - change Qualification
            - add consultation
            - add decision
        """
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116],
                    # "ajout_piece": [116, 117, 118, 119],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "consultation": [ACTEURS["culture"]["DEV-UDAP44"]],
                    "decision_DI": 15,
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }
        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # We build a simple dataset : one dossier trough
        self._build_dataset()

        projet_uid = self.dummy_backend["A.1"]["external_uids"]["dossier_autorisation"]
        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        result = self.app.get_dossier_by_uid(dossier_uid, raw=True)
        # We should only have ONE result
        self.assertNotEqual(result, None)

        projet = result.projet
        # Same Projet UID ?
        self.assertEqual(projet_uid, projet.idProjet)

        dossier = result.dossier
        # Let's check the local number
        self.assertEqual(self.dummy_backend["A.1"]["local_number"], dossier.noLocal)

        # Let's allow PlatAU to retrieve Piece and send the notifications
        print(
            "test_01_CT_instruction_complete: Let's allow PlatAU to retrieve Piece and send the notifications"
        )
        pieces_number = len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        time.sleep(10 * pieces_number)

        pieces = self.app.get_pieces(dossier_uid)
        # Do we have the same number of Pieces ?
        self.assertEqual(
            len(pieces), len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        )
        binaries = self.app.get_pieces_binaries(dossier_uid)
        # Do we have the same number of Pieces binaries ?
        self.assertEqual(
            len(binaries), len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        )

        # Here we suppose Piece are retrieved in the same order when sent.
        i = 0
        for task_id in self.dummy_backend["A.1"]["tasks"]["ajout_piece"]:
            task = self._get_task(
                task_id=task_id,
                json_path="json_tasks/ajout_piece_{}.json".format(task_id),
            )
            file_path = "{}/binary_files/{}".format(
                os.path.dirname(os.path.abspath(__file__)),
                task["document_numerise"]["nom_fichier"],
            )
            binary = binaries[i]

            # Content should not be corrupted
            with open(binary.attachment.path, "rb") as f_remote:
                with open(file_path, "rb") as f_local:
                    self.assertEqual(f_remote.read(), f_local.read())

            # Is the nature OK ?
            self.assertEqual(
                binary.nomNaturePiece.idNom,
                int(task["document_numerise"]["document_numerise_nature"]),
            )
            # Is the Type OK ?
            self.assertEqual(
                binary.nomTypePiece.idNom,
                int(task["document_numerise"]["document_numerise_type_code"]),
            )
            i += 1

        # Depot is done with this date
        # "date_depot": "2020-01-01"
        self.assertEqual(date.today(), dossier.dtDepot)

        # Qualification
        self.assertEqual(
            self.dummy_backend["A.1"]["acteurs"]["service_instructeur"],
            dossier.idServiceInstructeur,
        )
        # Nomenclature(idNom=1, libNom='Le maire')
        self.assertEqual(1, dossier.nomTypeSignataire.idNom)
        # Nomenclature(idNom=1, libNom='Au nom de la commune')
        self.assertEqual(1, dossier.nomTypeSignature.idNom)

        # Consultation
        # We should have Consultation `En cours` : Nomenclature(idNom=2, libNom='Consultations en cours')
        self.assertEqual(dossier.nomEtatOuSousEtatConsultations.idNom, 2)
        # We should have one and only pending Consultation on the Dossier
        consultation_result = self.app.get_consultation_by_uid(
            self.dummy_backend["A.1"]["external_uids"]["consultation"][0]
        )
        self.assertEqual(len(consultation_result.dossier.consultations), 1)
        # Is this the right Service Consulte ?
        consultation = consultation_result.dossier.consultations[0]
        self.assertEqual(
            consultation.idServiceConsulte,
            self.dummy_backend["A.1"]["tasks"]["consultation"][0],
        )

        # # NOTIFICATIONS
        # # Let's allow PlatAU to retrieve Piece and send the notifications
        # wait_seconds = 60
        # print(
        #     "test_01_CT_instruction_complete: Let's allow PlatAU to retrieve Piece and send the notifications"
        # )
        # time.sleep(wait_seconds)
        # # Do we have the Piece notifications ?
        # notif_gu = self.app.get_notifications(
        #     self.dummy_backend["A.1"]["acteurs"]["guichet_unique"]
        # )
        # self.assertTrue(notif_gu.erreurs is None)
        # notifications = notif_gu.notifications

        # # Events type should be 2, 57 and 82
        # # 2,"Occurrence d’un dépôt de dossier",02/01/2020
        # # 57,"Notification de succès de versement de l'ensemble des pièces sur le dossier",26/02/2020
        # # 82,"Notification de succès de versement d’une pièce",04/06/2021
        # wished_event = {2: 1, 57: 1, 82: 1}
        # received_events = {}
        # for event in notifications:
        #     if event.idEvenement not in received_events:
        #         received_events[event.idTypeEvenement] = 1
        #     else:
        #         received_events[event.idTypeEvenement] += 1

        # self.assertEqual(wished_event, received_events)

        # # Do we have the Qualifications notification ?
        # notif_si = self.app.get_notifications(
        #     self.dummy_backend["A.1"]["acteurs"]["service_instructeur"]
        # )
        # notifications = notif_si.notifications
        # # 3,"Occurrence d’une qualification de dossier",03/01/2020
        # # 5,"Occurrence d’une décision expresse ou tacite (hors transfert)",05/01/2020
        # self.assertEqual(len(notifications), 2)
        # self.assertTrue(notifications[0].idTypeEvenement in (3, 5))
        # self.assertTrue(notifications[1].idTypeEvenement in (3, 5))

        # XXX Decision
        # Assert something

    def test_02a_CT_modification_DI(self):
        """Usecase:
            - create Projet
            - create Dossier
            - qualify Dossier
            - modify Dossier
        """
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "depot_dossier": 8,
                    "qualification_DI": 9,
                    "modification_DI": None,
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }

        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # Let's override some Dossier data
        self.dummy_backend["A.1"]["tasks"]["modification_DI"] = {
            # "puissanceElectriqueRequise": {
            #   "x-openads-mapping": "donnees_techniques.co_elec_tension"
            # },
            "donnees_techniques.co_elec_tension": "90KW",
            # "surfTotaleDesAmenagements": {
            #   "x-openads-mapping": "donnees_techniques.am_terr_surf"
            # },
            "donnees_techniques.am_terr_surf": "1500",
            # "txDescriptifGlobal": {
            # "x-openads-mapping": "donnees_techniques.co_projet_desc"
            # },
            "donnees_techniques.co_projet_desc": "XXXYYYZZZ",
        }

        # We build a simple dataset : one dossier trough
        self._build_dataset()

        projet_uid = self.dummy_backend["A.1"]["external_uids"]["dossier_autorisation"]
        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        self.app.calling_actor = self.dummy_backend["A.1"]["acteurs"]["guichet_unique"]
        result = self.app.get_dossier_by_uid(dossier_uid, raw=True)
        # We should only have ONE result
        self.assertNotEqual(result, None)

        projet = result.projet
        # Same Projet UID ?
        self.assertEqual(projet_uid, projet.idProjet)

        dossier = result.dossier

        # Let's check overrided data
        self.assertEqual(dossier.puissanceElectriqueRequise, "90KW")
        self.assertEqual(dossier.surfTotaleDesAmenagements, 1500)
        self.assertEqual(dossier.txDescriptifGlobal, "XXXYYYZZZ")

        self.app.calling_actor = self.dummy_backend["A.1"]["acteurs"][
            "service_instructeur"
        ]
        # Do we have the right notifications ?
        notif_si = self.app.get_notifications(
            self.dummy_backend["A.1"]["acteurs"]["service_instructeur"]
        )
        notifications = notif_si.notifications
        # Events type should be 2, 57 and 82
        # 3,"Occurrence d’une qualification de dossier",03/01/2020
        # 18,"Occurrence de nouvelles informations ou d’une mise à jour d’informations dans un dossier",18/01/2020
        wished_event = {3: 1, 18: 1}
        received_events = {}
        for event in notifications:
            if event.idEvenement not in received_events:
                received_events[event.idTypeEvenement] = 1
            else:
                received_events[event.idTypeEvenement] += 1

        self.assertEqual(wished_event, received_events)

    def test_02b_CT_modification_DI_without_local_number(self):
        """Usecase:
            - create Projet
            - create Dossier without local number
            - qualify Dossier
            - modify Dossier, providing a local number
        """
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {"creation_DA": 6, "creation_DI": 7,},
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }

        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # We build a simple dataset : one dossier trough
        self._build_dataset(local_number="")

        projet_uid = self.dummy_backend["A.1"]["external_uids"]["dossier_autorisation"]
        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        self.app.calling_actor = self.dummy_backend["A.1"]["acteurs"]["guichet_unique"]
        result = self.app.get_dossier_by_uid(dossier_uid, raw=True)
        dossier = result.dossier

        # We should not have a noLocal
        self.assertEqual(dossier.noLocal, None)

        # Modification DI
        task = self._get_task(
            json_path="json_tasks/{}_modification_DI.json".format(
                self.dummy_backend["A.1"]["type"]
            ),
        )
        # task["dossier"]["insee"] = insee
        task["task"]["acteur"] = self.app.calling_actor
        task["external_uids"].update(
            {"dossier_autorisation": projet_uid, "dossier": dossier_uid,}
        )

        local_number = (
            task["dossier"]["dossier_autorisation"][:-5]
            + str(uuid.uuid1()).split("-")[0][:-3]
        )
        # Let's set the local number
        task["dossier.dossier_autorisation"] = local_number
        dummy = self.app.update_dossier(task)[0]

        result = self.app.get_dossier_by_uid(dossier_uid, raw=True)
        # We should have a noLocal
        self.assertEqual(result.dossier.noLocal, local_number)

    def test_03a_create_acteurs(self):
        """."""
        _type_actor = PLATAU_MAPPING["TYPE_ACTEUR"]
        actors = [
            Actor(
                role="guichet_unique",
                label="Auterive (31033) - Guichet Unique",
                insee="31033",
                depot_adau=True,
                email="support+openads_v31033_GU@atreal.fr",
                siren="111222333",
                organization_label="Auterive",
                extra_properties={"sve_cgu_url": "https://ideau-preprod.atreal.fr"},
            ),
            Actor(
                role="service_instructeur",
                label="Auterive (31033) - Service Instrcuteur",
                email="support+openads_v31033_SI@atreal.fr",
                insee="31033",
                insee_competence=["31033",],
                siren="111222333",
                organization_label="Auterive",
            ),
            Actor(
                role="autorite_competente",
                label="Auterive (31033) - Autorite Compétente",
                email="support+openads_v31033_AC@atreal.fr",
                insee="31033",
                insee_competence=["31033",],
                siren="111222333",
                organization_label="Auterive",
            ),
        ]
        nb_actors_by_role = {
            "guichet_unique": 0,
            "service_instructeur": 0,
            "autorite_competente": 0,
            "service_consulte": 0,
        }
        model_map = {
            "guichet_unique": "guichetsUniques",
            "service_instructeur": "servicesInstructeurs",
            "autorite_competente": "autoritesCompetentes",
            "service_consulte": "servicesConsultables",
        }
        curated_actors = []
        for actor in actors:
            criterions = {
                "nomTypeActeur": _type_actor[actor.role]["idNom"],
                "siren": actor.siren,
                "idCommuneRattachement": actor.insee,
            }
            result = self.app.get_acteurs(criterions)
            if len(result) == 0:
                curated_actors.append(actor)
                nb_actors_by_role[actor.role] += 1

        if len(curated_actors) != 0:
            print(
                f"These actors will be created: {', '.join([a.label for a in curated_actors])}"
            )
        else:
            print("There is no actor to be created.")
            return

        result = self.app.create_acteurs(curated_actors)
        for role, number in nb_actors_by_role.items():
            nb_acteur_created = (
                getattr(result, model_map[role])
                and len(getattr(result, model_map[role]))
                or 0
            )
            self.assertEqual(nb_acteur_created, number)

            # Clean-up
            if getattr(result, model_map[role]):
                self.app.delete_acteurs(
                    [a.idActeur for a in getattr(result, model_map[role])]
                )

    def test_03b_update_acteurs(self):
        """."""
        actors = [
            Actor(
                role="guichet_unique",
                label="Auterive (31033) - Guichet Unique",
                insee="31033",
                depot_adau=True,
                email="support+openads_v31033_GU@atreal.fr",
                siren="111222333",
                organization_label="Auterive",
                extra_properties={"sve_cgu_url": "https://ideau-preprod.atreal.fr"},
            ),
        ]
        result = self.app.create_acteurs(actors)
        self.assertEqual(len(result.guichetsUniques), 1)

        actor_uid = result.guichetsUniques[0].idActeur

        timestamp = datetime.now().timestamp()
        new_email = f"support+openads_v31033_GU{timestamp}@atreal.fr"
        actors = [
            Actor(
                role="guichet_unique",
                label="Auterive (31033) - Guichet Unique",
                insee="31033",
                depot_adau=True,
                email=new_email,
                siren="111222333",
                organization_label="Auterive",
                extra_properties={"sve_cgu_url": "https://ideau-preprod.atreal.fr"},
                uid=actor_uid,
            ),
        ]

        result = self.app.update_acteurs(actors)
        self.assertEqual(result.guichetsUniques[0].idActeur, actor_uid)

        modified_actor = self.app.get_acteur_by_uid(actor_uid)

        # Clean-up
        self.app.delete_acteurs(actor_uid)

        self.assertEqual(modified_actor.mail, new_email)

    def test_04_depot_piece_versee(self):
        """ Usecase:
            - Create Projet
            - Create Dossier
            - Add 2 Pièces
            - Notify Dépot
            - Add new Piece
            - test that Pièce is Versée
            - try to Depose Pièce
        """
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116, 117],
                    "depot_dossier": 7,
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }
        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # We build a simple dataset : one dossier trough
        self._build_dataset()

        # Become guichet_unique to send Piece as in the workplan
        self.app.start(ACTEURS["32013"]["guichet_unique"])

        # Get the Dossier id
        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        # result = self.app.get_dossier_by_uid(dossier_uid, raw=True)
        # dossier = result.dossier

        pieces = self.app.get_pieces(dossier_uid)
        # Do we have all Piece in state Deposee ?
        for piece in pieces:
            # Nommeclature (2,"Déposée",04/12/2019)
            self.assertEqual(piece.nomEtatPiece.idNom, 2)

        # We test
        #   - add new Piece Initial
        #   - verify new Piece is status Deposee
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {"ajout_piece": [118],},
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": dossier_uid,
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }
        self.dummy_backend = dataset

        # We rebuild the simple dataset : one dossier trough
        self._build_dataset()

        # Get the Dossier id
        dossier_uid_after = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        # Same Folder UID ?
        self.assertEqual(dossier_uid, dossier_uid_after)

        # Get the created Piece ID
        piece_uid = self.dummy_backend["A.1"]["external_uids"]["pieces"][0]

        # Become guichet_unique to send Piece as in the workplan
        self.app.start(ACTEURS["32013"]["guichet_unique"])

        pieces = self.app.get_pieces(dossier_uid_after)

        # Do we have all Piece in state Déposée excepte new Piece ?
        for piece in pieces:
            if piece.idPiece == piece_uid:
                # Nommeclature (1,"Versée")
                self.assertEqual(piece.nomEtatPiece.idNom, 1)
            else:
                # Nommeclature (2,"Déposée")
                self.assertEqual(piece.nomEtatPiece.idNom, 2)

        # Try and Depose It
        task = {
            "document_numerise": {
                "date_creation": piece.hdDebutVersion.strftime("%d/%m/%Y")
            },
            "external_uids": {
                "document_numerise": piece.idPiece,
                "dossier": piece.idDossier,
            },
        }
        result = self.app.depot_pieces(task)

        self.assertEqual(result[0].erreurs, None)

        # Check if all piece are now Déposée
        pieces = self.app.get_pieces(dossier_uid_after)
        for piece in pieces:
            # Nommeclature (2,"Déposée")
            self.assertEqual(piece.nomEtatPiece.idNom, 2)

    def test_05_update_piece(self):
        """Usecase:
            - create Projet
            - create Dossier
            - add 2 Pieces
            - notify Depot
            - update 1 Piece
        """
        actor_gu = ACTEURS["32013"]["guichet_unique"]
        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": actor_gu,
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116],
                    "depot_dossier": 7,
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }
        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # We build a simple dataset : one dossier trough
        self._build_dataset()
        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        # We need to keep the original file hash
        task = self._get_task(
            task_id=116, json_path=f"json_tasks/ajout_piece_116.json",
        )
        original_hash = self.get_hash_by_path(
            "{}/binary_files/{}".format(
                os.path.dirname(os.path.abspath(__file__)),
                task["document_numerise"]["nom_fichier"],
            )
        )

        # Become guichet_unique to send Piece as in the workplan
        self.app.calling_actor = actor_gu

        pieces = self.app.get_pieces(dossier_uid)
        # Do we have the same number of Pieces ?
        self.assertEqual(
            len(pieces), len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        )

        # We want to update Piece with a new file
        print("Updating Piece(s) 116 to 117")
        task = self._get_task(
            task_id=117, json_path=f"json_tasks/modification_piece_117.json",
        )
        task["task"]["acteur"] = actor_gu
        file_path = "{}/binary_files/{}".format(
            os.path.dirname(os.path.abspath(__file__)),
            task["document_numerise"]["nom_fichier"],
        )
        task["document_numerise"]["local_file_path"] = file_path
        task["piece_version"] = pieces[0].noVersion
        task["external_uids"].update(
            {"dossier": dossier_uid, "piece": pieces[0].idPiece}
        )
        # Hash of the new file should be kept for further assertion
        new_hash = self.get_hash_by_path(file_path)

        print(f"idPiece : {pieces[0].idPiece}")
        print(f"file_path : {file_path}")

        # Everything is ready, let's update the Piece !!!
        pieces_uids = self.app.update_piece([task])
        print(f"Piece(s) updated : {pieces_uids}")

        # Let's give Plat'AU some time to handle the Piece update
        self.control_binaries_ack(
            self.app, actor_uid=actor_gu, targeted_uids=pieces_uids,
        )

        binary = self.app.get_pieces_binaries(dossier_uid)

        # We should have one and only file
        self.assertEqual(len(binary), 1)

        # I can assure you we updated the Piece with a different file
        self.assertNotEqual(original_hash, new_hash)

        # Local file hash and hash from remote file should be identical
        self.assertEqual(new_hash, self.get_hash_by_path(binary[0].attachment.path))

    def test_06_delete_piece(self):
        """Usecase:
            - create Projet
            - create Dossier
            - add 1 Piece
            - notify Depot
            - delete 1 Piece
        """
        actor_gu = ACTEURS["32013"]["guichet_unique"]

        dataset = {
            "A.1": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": actor_gu,
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                    "autorite_competente": ACTEURS["32013"]["autorite_competente"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116],
                    "depot_dossier": 7,
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            },
        }
        self.dummy_backend = dataset

        # Reset all notifications for our current actors
        for actor_role, actor_uid in self.dummy_backend["A.1"]["acteurs"].items():
            self.app.get_notifications(actor_uid, raw=True)

        # We build a simple dataset : one dossier trough
        self._build_dataset()

        dossier_uid = self.dummy_backend["A.1"]["external_uids"]["dossier"]

        # Become guichet_unique to send Piece as in the workplan
        self.app.calling_actor = actor_gu

        pieces = self.app.get_pieces_binaries(dossier_uid)
        # Do we have the same number of Pieces ?
        self.assertEqual(
            len(pieces), len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        )
        # Let's compute the original piece hash
        original_hash = self.get_hash_by_path(pieces[0].attachment.path)

        task_id = self.dummy_backend["A.1"]["tasks"]["ajout_piece"][0]
        task = self._get_task(
            task_id=task_id, json_path=f"json_tasks/delete_piece_{task_id}.json",
        )
        task["task"]["acteur"] = actor_gu
        task["piece_version"] = pieces[0].noVersion
        task["external_uids"].update(
            {"dossier": dossier_uid, "piece": pieces[0].idPiece}
        )

        # Everything is ready, let's delete the Piece !!!
        pieces_uids = self.app.delete_pieces([task])
        print(f"Piece(s) deleted : {pieces_uids}")

        # Because for now we handle a Piece deletion as an update with an empty PDF,
        # let's give Plat'AU some time to handle the Piece update
        self.control_binaries_ack(
            self.app, actor_uid=actor_gu, targeted_uids=pieces_uids,
        )

        new_pieces = self.app.get_pieces_binaries(dossier_uid)

        # Piece(s) number should be the same
        # TODO : Replace this to 0 when file deletion is possible
        self.assertEqual(
            len(new_pieces), len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        )

        # Original file hash and hash from remote file should be different
        self.assertNotEqual(
            original_hash, self.get_hash_by_path(new_pieces[0].attachment.path)
        )

        # Let's dive into this wonderful PDF file and find some proof everything went perfectly
        reader = PdfReader(new_pieces[0].attachment.path)
        number_of_pages = len(reader.pages)
        # We sholud have one an only page
        self.assertEqual(number_of_pages, 1)

        page = reader.pages[0]
        text = page.extract_text()
        # Filename should be present in the text
        self.assertTrue(task["document_numerise"]["filename"] in text)
