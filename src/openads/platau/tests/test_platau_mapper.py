# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from datetime import datetime, timedelta
from flatten_json import flatten
from pprint import PrettyPrinter


from openads.platau.tests.base_test import BaseTestCase, CT_CONFIG_PATH
from openads.platau.conductor import Conductor, OAPConfig
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.mapper import PlatAUMapper
from openads.platau.tasks import InputTask
from bravado.exception import HTTPInternalServerError, HTTPBadRequest, HTTPUnauthorized


# TODO : use object config
ACTEURS = {
    "13055": {
        "commune": {
            "guichet_unique": "8XP-G2Z-2XP",
            "service_instructeur": "2XN-Z0D-VXJ",
            "autorite_competente": "YW8-DV2-3W3",
        },
        "etat": {
            "service_instructeur": "571-M6E-NXZ",
            "autorite_competente": "3Y1-LPM-4XP",
        },
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
    },
}


class PlatAUMapperTest(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
        )
        self.app.start()

    def _test_DossierMapper(self, type_dossier: str, task_overrides: dict = None):
        """XXX : write some assertion to validate mapping"""

        # task_id = DATASET["type_dossier"]["tasks"]["creation_DI"]
        task = self._get_task(json_path=f"json_tasks/{type_dossier}_creation_DI.json",)

        demandeurs = []
        source_demandeurs = task.pop("demandeur")
        if source_demandeurs is not None and len(source_demandeurs) != 0:
            for demandeur in source_demandeurs.values():
                demandeurs.append({f"demandeur.{k}": v for k, v in demandeur.items()})

        parcelles = []
        source_parcelles = task.pop("dossier_parcelle")
        if source_parcelles is not None and len(source_parcelles) != 0:
            for parcelle in source_parcelles.values():
                parcelles.append(
                    {f"dossier_parcelle.{k}": v for k, v in parcelle.items()}
                )

        architecte = task.pop("architecte")

        task = flatten(task, ".")
        task["demandeurs"] = demandeurs
        task["parcelles"] = parcelles
        if architecte:
            task["architecte"] = {f"architecte.{k}": v for k, v in architecte.items()}
            task["architecte"]["personne.type"] = "architecte"

        # XXX : Mandatory properties fix
        if task["donnees_techniques.enga_decla_date"] == "":
            task["donnees_techniques.enga_decla_date"] = date.today().strftime(
                "%Y-%m-%d"
            )
        if task["donnees_techniques.enga_decla_lieu"] == "":
            task["donnees_techniques.enga_decla_lieu"] = "Non fourni"
        if task["dossier.terrain_adresse_localite"] == "":
            task["dossier.terrain_adresse_localite"] = "Non fourni"

        if task_overrides:
            task.update(task_overrides)

        mapper = PlatAUMapper(task, self.app.client.get_model)
        mapped = mapper.map("AppelDossierCreation")

        pp = PrettyPrinter()
        pp.pprint(mapped._as_dict())

        with open(f"/tmp/{type_dossier}_mapped.json", "w") as f:
            json.dump(mapped._as_dict(), f, indent=4, sort_keys=True, default=str)

        return mapped

    def test_CUMapper(self):
        """XXX : write some assertion to validate mapping"""
        self._test_DossierMapper("CU")

    def test_DPLMapper(self):
        """XXX : write some assertion to validate mapping"""
        self._test_DossierMapper("DPL")

    def test_PAMapper(self):
        """XXX : write some assertion to validate mapping"""
        self._test_DossierMapper("PA")

    def test_PCMapper(self):
        """XXX : write some assertion to validate mapping"""
        self._test_DossierMapper("PC")

    def test_PDMapper(self):
        """XXX : write some assertion to validate mapping"""
        self._test_DossierMapper("PD")


def _dict_equals(d1, d2):
    """."""
    s1 = set([f"{k}::{v}" for k, v in d1])
    s2 = set([f"{k}::{v}" for k, v in d2])

    return s1 == s2
