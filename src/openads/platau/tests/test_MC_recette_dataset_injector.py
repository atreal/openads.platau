# -*- coding: utf-8 -*-

from openads.platau.tests.base_test import (
    BaseTestCase,
    FakeArgs,
    SC_CONFIG_PATH,
)
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import OAPConfig

FROM_CT = "69243"
TO_MC = "UDAP69"
PIECES = False


ACTEURS = {
    "13005": {"guichet_unique": "8XP-G2Z-2XP", "service_instructeur": "2XN-Z0D-VXJ",},
    "32013": {"guichet_unique": "YX3-G82-PD1", "service_instructeur": "EWV-20J-VRW",},
    "44109": {"guichet_unique": "71M-R43-ZWZ", "service_instructeur": "7XY-2EP-0XZ",},
    "27004": {
        "guichet_unique": "8XP-YMY-2XP",
        "service_instructeur": "2XN-K2K-VXJ",
        "service_instructeur_ddt": "91D-GY5-E14",
    },
    "69243": {
        "guichet_unique": "91K-8O8-7WJ",
        "service_instructeur": "VX2-L2L-E1E",
        "service_instructeur_ddt": "71Z-V34-DWQ",
    },
    "07019": {
        "guichet_unique": "71M-O2O-6WZ",
        "service_instructeur": "E10-VQV-OWZ",
        "service_instructeur_ddt": "MWJ-879-GWP",
    },
    "culture": {
        # # XP
        "UDAP01": "71Z-ZKM-D1Q",
        "UDAP14": "71Z-ZKN-D1Q",
        "UDAP27": "71Z-ZKY-N1Q",
        "UDAP69": "91K-6LV-N1J",
        "UDAP76": "71M-M87-R1Z",
        "UDAP91": "91D-Z27-PX4",
        "SRA-ARA": "71M-M84-61Z",
        "SRA-NORMANDIE": "GXG-V9J-LX3",
    },
}

CONSULTATION = {
    "date_consultation": "27/11/2000",
    "service_consulte": None,
    "type_consultation": 1,
    "delai_reponse": 1,
}


DOSSIERS = {
    "A.0": {
        "insee": FROM_CT,
        "acteurs": {
            "guichet_unique": ACTEURS[FROM_CT]["guichet_unique"],
            "service_instructeur": ACTEURS[FROM_CT]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [116,],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": True,
            "completude_DI": True,
            "consultation": [ACTEURS["culture"][TO_MC],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "parcelles": ["00000000"],
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    },
    "A.1": {
        "insee": FROM_CT,
        "acteurs": {
            "guichet_unique": ACTEURS[FROM_CT]["guichet_unique"],
            "service_instructeur": ACTEURS[FROM_CT]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [116,],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": True,
            "completude_DI": True,
            "consultation": [ACTEURS["culture"][TO_MC],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "parcelles": ["000C0315"],
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    },
    "A.2": {
        "insee": FROM_CT,
        "acteurs": {
            "guichet_unique": ACTEURS[FROM_CT]["guichet_unique"],
            "service_instructeur": ACTEURS[FROM_CT]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": False,
            "completude_DI": False,
            "consultation": [ACTEURS["culture"][TO_MC],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "parcelles": ["000 A0968"],
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    },
    "A.3": {
        "insee": FROM_CT,
        "acteurs": {
            "guichet_unique": ACTEURS[FROM_CT]["guichet_unique"],
            "service_instructeur": ACTEURS[FROM_CT]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": False,
            "completude_DI": False,
            "consultation": [ACTEURS["culture"][TO_MC],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "parcelles": ["000AB0658"],
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    },
}
INSTRUCTIONS = {
    "pec_metier": {"id": "5006", "name": "Accepté - Dossier Complet"},
    "pec_metier_with_attachment": {
        "id": "5002",
        "name": "Refusé : Pièces manquantes ou inexploitables",
    },
    "avis_consultation": {"id": "5042", "name": "Abords Accord"},
    "prescription": {"id": "", "name": "TEST PRESCRIPTION"},
}


class MCRecetteDatasetInjector(BaseTestCase):
    """Culture recette dataset injection.
    Use this testcase to populate MC recette with Consultation from Plat'AU.
    """

    def setUp(self):
        """."""
        self.conf = OAPConfig(SC_CONFIG_PATH)
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()

        if not PIECES:
            for _, values in self.dummy_backend.items():
                values["tasks"]["ajout_piece"] = []
            self.dummy_backend

        self.actor = ACTEURS["culture"][TO_MC]
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)
        self.openads_url = self.conf.openadsapi_url
        self.openads_auth = self.conf.openadsapi_config["auth"]
        self.kwargs = FakeArgs()

    def test_00_SC_build_dataset(self):
        """."""
        self._build_dataset()
