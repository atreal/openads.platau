*** Settings ***
Documentation     Actions spécifiques au jeu de test de l'experimentation Plat'AU
Library  ./OpenADSCerfaCSV.py

*** Keywords ***
Plat'AU Isolation d'un contexte
    [Tags]  utils
    [Arguments]  ${values}
    [Documentation]  Permet d'isoler un contexte avec la création :
    ...  - d'une collectivité mono et de son param minimum (dep, com et insee)
    ...  - d'une direction
    ...  - d'une division
    ...  - d'un utilisateur avec le profil "GUICHET UNIQUE"
    ...  - d'un utilisateur avec le profil "INSTRUCTEUR" et de son instructeur
    ...  - d'une affectation automatique de l'instructeur sur les PCI
    ...  ###
    ...  Liste des valeurs à passer dans le dictionnaire en argument :
    ...  om_collectivite_libelle
    ...  departement
    ...  commune
    ...  insee
    ...  direction_code
    ...  direction_libelle
    ...  direction_chef
    ...  division_code
    ...  division_libelle
    ...  division_chef
    ...  guichet_om_utilisateur_nom
    ...  guichet_om_utilisateur_email
    ...  guichet_om_utilisateur_login
    ...  guichet_om_utilisateur_pwd
    ...  instr_om_utilisateur_nom
    ...  instr_om_utilisateur_email
    ...  instr_om_utilisateur_login
    ...  instr_om_utilisateur_pwd

    Ajouter la collectivité depuis le menu  ${values.om_collectivite_libelle}  mono
    Ajouter le paramètre depuis le menu  departement  ${values.departement}  ${values.om_collectivite_libelle}
    Ajouter le paramètre depuis le menu  commune  ${values.commune}  ${values.om_collectivite_libelle}
    Ajouter le paramètre depuis le menu  insee  ${values.insee}  ${values.om_collectivite_libelle}
    Ajouter la direction depuis le menu  ${values.direction_code}  ${values.direction_libelle}  null  ${values.direction_chef}  null  null  ${values.om_collectivite_libelle}
    Ajouter la division depuis le menu  ${values.division_code}  ${values.division_libelle}  null  ${values.division_chef}  null  null  ${values.direction_libelle}
    Ajouter l'utilisateur depuis le menu  ${values.guichet_om_utilisateur_nom}  ${values.guichet_om_utilisateur_email}  ${values.guichet_om_utilisateur_login}  ${values.guichet_om_utilisateur_pwd}  GUICHET UNIQUE  ${values.om_collectivite_libelle}
    Ajouter l'utilisateur depuis le menu  ${values.instr_om_utilisateur_nom}  ${values.instr_om_utilisateur_email}  ${values.instr_om_utilisateur_login}  ${values.instr_om_utilisateur_pwd}  INSTRUCTEUR  ${values.om_collectivite_libelle}
    Ajouter l'instructeur depuis le menu  ${values.instr_om_utilisateur_nom}  ${values.division_libelle}  instructeur  ${values.instr_om_utilisateur_nom}
    &{args_affectation} =  Create Dictionary
    ...  instructeur=${values.instr_om_utilisateur_nom} (${values.division_code})
    ...  om_collectivite=${values.om_collectivite_libelle}
    ...  dossier_autorisation_type_detaille=${values.dossier_autorisation_type_detaille}
    Ajouter l'affectation depuis le menu  ${args_affectation}

Plat'AU Saisir les données techniques du DI

    [Documentation]  Permet de saisir les données techniques dans n'importe quel fieldset
    ...  Prend en paramètre le n° de dossier d'inscription, et la liste de données
    ...  techniques à insérer sous la forme de dictionary ex : ope_proj_desc=testset

    [Arguments]  ${dossier_instruction}  ${donnees_techniques_values}  ${donnees_techniques_terrain}=${EMPTY}  ${donnees_techniques_architecte}=${EMPTY}  ${menu}=null  ${class_suffix}=${EMPTY}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction}  ${menu}

    # Défini le type de dossier
    ${dossier} =  Set Variable If  '${menu}' == 'infraction'  dossier_contentieux_toutes_infractions
    ...                            '${menu}' == 'recours'     dossier_contentieux_tous_recours
    ...                                                       dossier_instruction

    # On clique sur l'action données techniques du portlet confirmée par une modale
    Click On Form Portlet Action  ${dossier}  donnees_techniques  modale

    # Défini le contexte de l'action
    ${contexte} =  Set Variable If  '${menu}' == 'infraction'  donnees_techniques_contexte_ctx
    ...                             '${menu}' == 'recours'     donnees_techniques_contexte_ctx
    ...                                                        donnees_techniques

    # Attend l'apparition de l'action dans la fenêtre modale
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#action-sousform-${contexte}-modifier

    # On clique sur l'action modifier
    Click On SubForm Portlet Action  ${contexte}  modifier

    Plat'AU Saisir les données techniques  ${donnees_techniques_values}  ${class_suffix}  ${donnees_techniques_terrain}  ${donnees_techniques_architecte}

Plat'AU Saisir les données techniques

    [Arguments]  ${donnees_techniques_values}  ${class_suffix}=${EMPTY}  ${donnees_techniques_terrain}=${EMPTY}  ${donnees_techniques_architecte}=${EMPTY}

    # Contient l'id de tous les fieldsets qui seront dépliés automatiquement
    @{liste_fieldsets} =  Create List
    ...  terrain
    ...  description-de-la-demande---du-projet
    ...  amenager
    ...  projet-d_amenagement
    ...  description-amenagement
    ...  complement-d_amenagement
    ...  construire
    ...  projet-construction
    ...  complement-construction
    ...  demolir
    ...  destinations-et-surfaces-des-constructions
    ...  divers-construction
    ...  informations-pour-lapplication-dune-legislation-connexe
    ...  engagement-du-declarant
    ...  ouverture-de-chantier
    ...  achevement-des-travaux
    ...  cnil-opposition-a-lutilisation-des-informations-du-formulaire-a-des-fins-commerciales
    ...  declaration-des-elements-necessaires-au-calcul-des-impositions

    # Déplie tous les fieldsets présents dans la liste {liste_fieldsets}
    :FOR  ${values}  IN  @{liste_fieldsets}
    \  ${fieldset_exists}=  Run Keyword And Return Status  Element Should Be Visible  css=#fieldset-sousform-donnees_techniques${class_suffix}-${values}
    \  Run Keyword If  ${fieldset_exists}==True  Open Fieldset In Subform  donnees_techniques${class_suffix}  ${values}

    Si "terr_juri_titul" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire
    Si "terr_juri_lot" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire
    Si "terr_juri_zac" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire
    Si "terr_juri_afu" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire
    Si "terr_juri_pup" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire
    Si "terr_juri_oin" existe dans "${donnees_techniques_terrain}" on execute "Select From List By Label" dans le formulaire


    # On convertit le dictionnaire en liste
    ${items}=    Get Dictionary Items    ${donnees_techniques_values}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items}
    # On saisit la valeur dans le champ
    \   Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=div#sousform-donnees_techniques${class_suffix} #${field}
    \  Run Keyword If  "${value}" == "t"  Select Checkbox  css=div#sousform-donnees_techniques${class_suffix} #${field}  ELSE IF  "${value}" == "f"  Unselect Checkbox  css=div#sousform-donnees_techniques${class_suffix} #${field}  ELSE  Input Text  css=div#sousform-donnees_techniques${class_suffix} #${field}  ${value}

    Log Dictionary  ${donnees_techniques_architecte}

    # On verifie que le dictionnaire de l'architecte n'est pas vide et qu'il contient au moins un nom
    ${length_donnee_techniques_architecte}=  Get Length  ${donnees_techniques_architecte}
    Run Keyword If  ${length_donnee_techniques_architecte}!=0  Run Keyword If  '${donnees_techniques_architecte.nom}'!='${EMPTY}'  Ajouter l'architecte  ${donnees_techniques_architecte}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be  Vos modifications ont bien été enregistrées.

Convertir une date du format ddmmyyyy au format dd/mm/yyyy
    [Tags]
    [Arguments]  ${string}
    ${yyyy} =  Get Substring  ${string}  -4
    ${mm} =  Get Substring  ${string}  2  4
    ${dd} =  Get Substring  ${string}  0  2
    [return]  ${dd}/${mm}/${yyyy}


Charger le json et créer le dictionnaire du petitionnaire
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire du petionnaire
    [Arguments]  ${nom_du_dossier}

    # Correspondance entre les données pdf et le formulaire pétitionnaire

    # Identité déclarant
    #  Si particulier
    #  D1F_femme: Oui ou Non
    #  D1H_homme: Oui ou Non
    #  D1A_naissance: particulier_date_naissance
    #  D1C_commune: particulier_commune_naissance
    #  D1D_dept: particulier_departement_naissance
    #  D1E_pays: particulier_pays_naissance
    #  D1N_nom: particulier_nom
    #  D1P_prenom: particullier_prenom
    #
    #  Si personne morale
    #  D2D_denomination: personne_morale_denomination
    #  D2R_raison: personne_morale_raison_sociale
    #  D2S_SIRET: siret
    #  D2J_type: type_societe
    #  D2F_madame: Oui ou Non
    #  D2H_monsieur: Oui ou Non
    #  D2N_nom: personne_morale_nom
    #  D2P_prenom: personne_morale_prenom

    # Coordonnées du déclarant
    #  D3B_boite: bp
    #  D3C_code: code_postal
    #  D3D_division: division_territoriale
    #  D3K_indicatif: indicatif
    #  D3L_localite: localite
    #  D3N_numero: numero
    #  D3P_pays: pays
    #  D3T_telephone: telephone
    #  D3V_voie: voie
    #  D3W_lieudit: lieu_dit
    #  D3X_cedex: cedex

    # Notification (Oui ou Non) et Courriel
    #  D5A_acceptation: Oui ou Non
    #  D5GE1_email: courriel
    #  D5GE2_email: suffixe_courriel

    # ${field_value_cerfa['']}

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    # Vérification de la qualite du déclarant (particulier ou personne morale)
    ${qualite}=  Set Variable If  "${field_value_cerfa['D1N_nom']}" != "${EMPTY}"  particulier
    ...                                                                                      personne morale

    ${particulier_civilite}=  Set Variable If  '${field_value_cerfa['D1H_homme']}' == 'Oui'  Monsieur
    ...                                        '${field_value_cerfa['D1F_femme']}' == 'Oui'  Madame
    ...                                                                                                  choisir civilité

    ${personne_morale_civilite}=  Set Variable If  '${field_value_cerfa['D2H_monsieur']}' == 'Oui'  Monsieur
    ...                                            '${field_value_cerfa['D2F_madame']}' == 'Oui'    Madame
    ...                                                                                                         choisir civilité

    ${date_naissance_length}=  Get Length  ${field_value_cerfa['D1A_naissance']}
    ${date_naissance}=  Run Keyword If  ${date_naissance_length} != 0  Convertir une date du format ddmmyyyy au format dd/mm/yyyy  ${field_value_cerfa['D1A_naissance']}

    ${notification}=  Set Variable If  '${field_value_cerfa['D5A_acceptation']}' == 'Oui'  true

    ${courriel}=  Set Variable If  '${field_value_cerfa['D5GE1_email']}' != '${EMPTY}'  ${field_value_cerfa['D5GE1_email']}@${field_value_cerfa['D5GE2_email']}
    ...                                                                                             ${EMPTY}

    ${pays}=  Set Variable If  '${field_value_cerfa['D3P_pays']}'=='${EMPTY}'  France
    ...                                                                                    ${field_value_cerfa['D3P_pays']}

    &{personne_morale_dict}=  Create Dictionary
    ...  personne_morale_denomination=${field_value_cerfa['D2D_denomination']}
    ...  personne_morale_raison_sociale=${field_value_cerfa['D2R_raison']}
    ...  personne_morale_siret=${field_value_cerfa['D2S_SIRET']}
    ...  personne_morale_categorie_juridique=${field_value_cerfa['D2J_type']}
    ...  personne_morale_civilite=${personne_morale_civilite}
    ...  personne_morale_nom=${field_value_cerfa['D2N_nom']}
    ...  personne_morale_prenom=${field_value_cerfa['D2P_prenom']}

    &{particulier_dict}=  Create Dictionary
    ...  particulier_civilite=${particulier_civilite}
    ...  particulier_nom=${field_value_cerfa['D1N_nom']}
    ...  particulier_prenom=${field_value_cerfa['D1P_prenom']}
    ...  particulier_commune_naissance=${field_value_cerfa['D1C_commune']}
    ...  particulier_departement_naissance=${field_value_cerfa['D1D_dept']}
    ...  particulier_pays_naissance=${field_value_cerfa['D1E_pays']}


    Run Keyword If  ${date_naissance_length}!=0  Set To Dictionary  ${particulier_dict}  particulier_date_naissance=${date_naissance}

    # Création du dictionnaire pour le pétitionnaire
    &{dict_demandeur}=  Create Dictionary
    ...  om_collectivite=${OM_COLLECTIVITE}
    ...  qualite=${qualite}
    ...  numero=${field_value_cerfa['D3N_numero']}
    ...  voie=${field_value_cerfa['D3V_voie']}
    ...  lieu_dit=${field_value_cerfa['D3W_lieudit']}
    ...  localite=${field_value_cerfa['D3L_localite']}
    ...  code_postal=${field_value_cerfa['D3C_code']}
    ...  bp=${field_value_cerfa['D3B_boite']}
    ...  cedex=${field_value_cerfa['D3X_cedex']}
    ...  pays=${pays}
    ...  division_territoriale=${field_value_cerfa['D3D_division']}
    ...  indicatif=${field_value_cerfa['D3K_indicatif']}
    ...  notification=${notification}
    ...  courriel=${courriel}

    ${telephone_first_two_char} =  Get Substring  ${field_value_cerfa['D3T_telephone']}  0  2

    Run Keyword If  '${telephone_first_two_char}' == '06' or '${telephone_first_two_char}' == '07'   Set To Dictionary  ${dict_demandeur}  telephone_mobile=${field_value_cerfa['D3T_telephone']}
    ...  ELSE  Set To Dictionary  ${dict_demandeur}  telephone_fixe=${field_value_cerfa['D3T_telephone']}
    # Ajout du particulier ou de la personne morale dans le dictionnaire à retourner
    ${items}=  Run Keyword If  '${qualite}' == 'personne morale'  Get Dictionary Items    ${personne_morale_dict}
    ...  ELSE  Get Dictionary Items    ${particulier_dict}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items}
    \  Set To Dictionary  ${dict_demandeur}  ${field}=${value}


    [Return]    ${dict_demandeur}


Charger le json et créer le dictionnaire de la demande
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire de la demande
    [Arguments]  ${nom_du_dossier}

    # Correspondance entre les données pdf et le formulaire de la demande

    # Données de la demande (terrain)
    #  T2B_boite: terrain_adresse_bp
    #  T2C_code: terrain_adresse_code_postal
    #  T2F_prefixe: reference cadastrale part 1
    #  T2S_section: reference cadastrale part 2
    #  T2N_numero: reference cadastrale part 3
    #  T2L_localite: terrain_adresse_localite
    #  T2Q_numero: terrain_adresse_voie_numero
    #  T2V_voie: terrain_adresse_voie
    #  T2T_superficie: terrain_superficie
    #  T2W_lieudit: terrain_adresse_lieu_dit
    #  T2X_cedex: terrain_adresse_cedex
    #  R2M_date: date_demande

    #  Références cadastrales complémentaires
    #  T5ZA1: 111
    #  T5ZA2: AA
    #  T5ZA3: 1111

    #  T5ZB1: 222
    #  T5ZB2: ZZ
    #  T5ZB3: 2222

    #  T5ZC1: 333
    #  T5ZC2: EE
    #  T5ZC3: 3333

    #  T5ZD1: 444
    #  T5ZD2: RR
    #  T5ZD3: 4444

    #  T5ZE1: 555
    #  T5ZE2: TT
    #  T5ZE3: 5555

    #  T5ZF1: 666
    #  T5ZF2: YY
    #  T5ZF3: 6666

    #  T5ZG1: 777
    #  T5ZG2: UU
    #  T5ZG3: 7777

    #  T5ZH1: 888
    #  T5ZH2: II
    #  T5ZH3: 8888

    #  T5ZI1: 999
    #  T5ZI2: OO
    #  T5ZI3: 9999

    #  T5ZJ1: 000
    #  T5ZJ2: QQ
    #  T5ZJ3: 0000

    #  T5ZK1: 111
    #  T5ZK2: SS
    #  T5ZK3: 1111

    #  T5ZL1: 222
    #  T5ZL2: DD
    #  T5ZL3: 2222

    #  T5ZM1: 333
    #  T5ZM2: FF
    #  T5ZM3: 3333

    #  T5ZN1: 444
    #  T5ZN2: GG
    #  T5ZN3: 4444

    #  T5ZP1: 555
    #  T5ZP2: HH
    #  T5ZP3: 5555

    #  T5ZQ1: 666
    #  T5ZQ2: JJ
    #  T5ZQ3: 6666

    #  T5ZR1: 777
    #  T5ZR2: KK
    #  T5ZR3: 7777

    #  T5ZS1: 888
    #  T5ZS2: LL
    #  T5ZS3: 8888

    #  T5ZT1: 999
    #  T5ZT2: MM
    #  T5ZT3: 9999

    #  T5ZU1: 000
    #  T5ZU2: WW
    #  T5ZU3: 0000


    # ${field_value_cerfa['']}

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    ${date_demande}=  Convertir une date du format ddmmyyyy au format dd/mm/yyyy  ${field_value_cerfa['R2M_date']}

    ${dpmi_or_pcmi} =  Get Substring  ${nom_du_dossier}  0  4
    ${dp_or_pc} =  Get Substring  ${nom_du_dossier}  0  2

    ${dossier_autorisation_type_detaille}=  Set Variable If  '${dpmi_or_pcmi}'=='DPMI'  DÉCLARATION PRÉALABLE MAISON INDIVIDUELLE
    ...                                                      '${dpmi_or_pcmi}'=='PCMI'  PERMIS DE CONSTRUIRE MAISON INDIVIDUELLE
    ...                                                      '${dp_or_pc}'=='DP'        DECLARATION PREALABLE
    ...                                                      '${dp_or_pc}'=='PC'        PERMIS DE CONSTRUIRE

    # Références cadastrales de base
    @{ref_cad} =  Create List  ${field_value_cerfa['T2F_prefixe']}  ${field_value_cerfa['T2S_section']}  ${field_value_cerfa['T2N_numero']}
    # Mapping des références cadastrales complémentaires
    # @{ref_cad_comp}=  Create List  T5ZA1  T5ZA2  T5ZA3
    # ...  T5ZB1  T5ZB2  T5ZB3
    # ...  T5ZC1  T5ZC2  T5ZC3
    # ...  T5ZD1  T5ZD2  T5ZD3
    # ...  T5ZE1  T5ZE2  T5ZE3
    # ...  T5ZF1  T5ZF2  T5ZF3
    # ...  T5ZG1  T5ZG2  T5ZG3
    # ...  T5ZH1  T5ZH2  T5ZH3
    # ...  T5ZI1  T5ZI2  T5ZI3
    # ...  T5ZJ1  T5ZJ2  T5ZJ3
    # ...  T5ZK1  T5ZK2  T5ZK3
    # ...  T5ZL1  T5ZL2  T5ZL3
    # ...  T5ZM1  T5ZM2  T5ZM3
    # ...  T5ZN1  T5ZN2  T5ZN3
    # ...  T5ZP1  T5ZP2  T5ZP3
    # ...  T5ZQ1  T5ZQ2  T5ZQ3
    # ...  T5ZR1  T5ZR2  T5ZR3
    # ...  T5ZS1  T5ZS2  T5ZS3
    # ...  T5ZT1  T5ZT2  T5ZT3
    # ...  T5ZU1  T5ZU2  T5ZU3

    # # Utilisation du module pyton OpenADSCerfaCSV.py pour récupérer le mapping des parcelles
    @{ref_cad_comp}=  get_parcelles_list  mapping_cerfa_v7_to_openads.csv  True
    Log  ${ref_cad_comp}

    # On vérifie si le champ existe et si il n'est pas vide on l'ajoute à la liste des références cadastrales de base
    :FOR    ${value}    IN    @{ref_cad_comp}
    \  ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \  Run Keyword If  ${exist} == True  Run Keyword If  '${field_value_cerfa['${value}']}' != '${EMPTY}'  Append To List  ${ref_cad}  ${field_value_cerfa['${value}']}

    # Composition du dictionnaire de la demande
    &{dict_demande} =  Create Dictionary
    ...  om_collectivite=${OM_COLLECTIVITE}
    ...  dossier_autorisation_type_detaille=${dossier_autorisation_type_detaille}
    ...  demande_type=Dépôt Initial DP
    ...  date_demande=${date_demande}
    ...  terrain_adresse_voie_numero=${field_value_cerfa['T2Q_numero']}
    ...  terrain_adresse_voie=${field_value_cerfa['T2V_voie']}
    ...  terrain_adresse_code_postal=${field_value_cerfa['T2C_code']}
    ...  terrain_adresse_localite=${field_value_cerfa['T2L_localite']}
    ...  terrain_superficie=${field_value_cerfa['T2T_superficie']}
    ...  terrain_adresse_bp=${field_value_cerfa['T2B_boite']}
    ...  terrain_adresse_cedex=${field_value_cerfa['T2X_cedex']}
    ...  terrain_references_cadastrales_ligne=${ref_cad}

    [Return]    ${dict_demande}


Charger le json et créer le dictionnaire des données techniques de terrain
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire des données techniques de terrain
    ...  (cf surcharge du keyword 'Saisir les données techniques')
    [Arguments]  ${nom_du_dossier}

    # Correspondance entre les données pdf et le formulaire des données techniques

    # Données qui représentent les données techniques
    #  T3A_CUoui: ]
    #  T3H_CUnon: |= terr_juri_titul
    #  T3B_CUnc:  ]

    #  T3P_PUPoui: ]
    #  T3C_PUPnon: |= terr_juri_pup
    #  T3F_PUPnc:  ]

    #  T3M_OINoui: ]
    #  T3D_OINnon: |= terr_juri_oin
    #  T3K_OINnc:  ]

    #  T3J_ZACoui: ]
    #  T3Q_ZACnon: |= terr_juri_zac
    #  T3T_ZACnc:  ]

    #  T3G_AFUoui: ]
    #  T3R_AFUnon: |= terr_juri_afu
    #  T3E_AFUnc:  ]

    #  T3I_lotoui: ]
    #  T3L_lotnon: |= terr_juri_lot
    #  T3S_lotnc:  ]

    #  T2J_lotissement: terr_juri_lot

    # ${field_value_cerfa['']}

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    &{dict_donnees_techniques_terrain_mapping} =  Create Dictionary
    ...  terr_juri_titul=T3A_CU
    ...  terr_juri_pup=T3P_PUP
    ...  terr_juri_oin=T3M_OIN
    ...  terr_juri_zac=T3J_ZAC
    ...  terr_juri_afu=T3G_AFU
    ...  terr_juri_lot=T3I_lot

    &{dict_donnees_techniques_not_in_same_format_mapping}=  Create Dictionary
    ...  terr_juri_lot=T2J_lotissement

    &{dict_donnees_techniques_terrain} =  Create Dictionary

    ${items_donnees_technique_nisf_mapping}=  Get Dictionary Items  ${dict_donnees_techniques_not_in_same_format_mapping}
    :FOR    ${field}    ${value}    IN    @{items_donnees_technique_nisf_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['${value}']}' == 'Oui'  Oui
    \   ...                                                                 '${field_value_cerfa['${value}']}' == 'Off'  Non
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_donnees_techniques_terrain}  ${field}=${new_value}


    # titul
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3A_CUoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3A_CUoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3H_CUnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3B_CUnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'   Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_titul=${new_value}

    # pup
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3P_PUPoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3P_PUPoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3C_PUPnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3F_PUPnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'   Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_pup=${new_value}

    # oin
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3M_OINoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3M_OINoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3D_OINnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3K_OINnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'   Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_oin=${new_value}

    # zac
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3J_ZACoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3J_ZACoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3Q_ZACnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3T_ZACnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'    Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_zac=${new_value}

    # afu
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3G_AFUoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3G_AFUoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3R_AFUnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3E_AFUnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'   Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_afu=${new_value}

    # lot
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  T3I_lotoui
    ${new_value}=  Run Keyword If    ${exist} == True  Set Variable If  '${field_value_cerfa['T3I_lotoui']}' == 'Oui'  Oui
    ...                                                                 '${field_value_cerfa['T3L_lotnon']}' == 'Oui'  Non
    ...                                                                 '${field_value_cerfa['T3S_lotnc']}'=='Oui'     Je ne sais pas
    Run Keyword If    ${exist} == True and '${new_value}' != 'None'    Set To Dictionary  ${dict_donnees_techniques_terrain}  terr_juri_lot=${new_value}


    [Return]    ${dict_donnees_techniques_terrain}


Charger le json et créer le dictionnaire des données techniques
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire des données techniques
    [Arguments]  ${nom_du_dossier}

    # Correspondance entre les données pdf et le formulaire des données techniques
    # * (ncac) veut dire que ce n'est pas une case à cocher
    # Données qui représentent les données techniques
    #  A2A_lotissement: am_lotiss
    #  A2L_division: am_div_mun
    #  A2T_camping:  am_camping
    #  A2D_caravanes: am_caravane
    #  A2S_caravaneshors: am_carav_duree (ncac)
    #  A2M_RML: am_statio
    #  A2H_RMLnombre: am_statio_cont (ncac)
    #  A2Q_affouillements: am_affou_exhau
    #  A2Y_superficie: am_affou_exhau_sup (ncac)
    #  A2B_profondeur: am_affou_prof (ncac)
    #  A2E_hauteur: am_exhau_haut (ncac)
    #  A2K_coupe: am_coupe_abat
    #  A2F_protege: am_prot_plu
    #  A2Z_deliberation: am_prot_muni
    #  A2I_residences: am_air_terr_res_mob
    #  A2R_mobile: am_aire_voyage
    #  A2P_sauvegarde: am_rememb_afu
    #  A2W_demontables: am_terr_res_demon
    #  A2C_accueil: am_aire_voyage

    #  A3C_culture: am_mob_art
    #  A3M_public: am_modif_voie_esp
    #  A3B_plantation: am_plant_voie_esp

    #  A3D_description: am_projet_desc (ncac)
    #  A3A_superficie: am_terr_surf (ncac)
    #  A3G_tranches: am_tranche_desc (ncac)

    #  G1A_agrandissementoui:]
    #                                    |= am_exist_agrand
    #  G1H_agrandissementnon:]

    #  G1N_numero: am_exist_date (ncac)
    #  G1G_emplacements: am_exist_num (ncac)
    #  G1B_emplacementsavant: am_exist_nb_avant (ncac)
    #  G1R_emplacementsapres: am_exist_nb_apres (ncac)
    #  G1F_tentesnombre: am_tente_nb (ncac)
    #  G1M_caravanesnombre: am_carav_nb (ncac)
    #  G1T_RMLnombre: am_mobil_nb (ncac)
    #  G1K_personnesmax: am_pers_nb (ncac)
    #  G1P_HLLnombre: am_empl_hll_nb (ncac)
    #  G1L_HLLsurface: am_hll_shon (ncac)

    #  A4A_bois: am_coupe_bois
    #  A4Q_parc: am_coupe_parc
    #  A4C_alignement: am_coupe_align
    #  A4B_essences: am_coupe_ess (ncac)
    #  A4I_age: am_coupe_age (ncac)
    #  A4N_densite: am_coupe_dens (ncac)
    #  A4H_qualite: am_coupe_qual (ncac)
    #  A4J_traitement: am_coupe_trait (ncac)
    #  A4P_autres: am_coupe_autr (ncac)

    #  C2ZA1_nouvelle: co_cstr_nouv
    #  C2ZB1_existante: co_cstr_exist
    #  C2ZC2_piscine: co_piscine
    #  C2ZC3_cloture: co_cloture
    #  C2ZD1_description: co_projet_desc (ncac)
    #  C2ZE1_puissance: co_elec_tension (ncac)

    #  C5ZA1_logements: co_tot_log_nb (ncac)
    #  C5ZA2_individuels: co_tot_ind_nb (ncac)
    #  C5ZA3_collectifs: co_tot_coll_nb (ncac)

    #  C5ZC1_nombreLLS: co_fin_lls_nb (ncac)
    #  C5ZC2_nombreAS: co_fin_aa_nb (ncac)
    #  C5ZC3_nombrePTZ: co_fin_ptz_nb (ncac)
    #  C5ZC5_nombreautres: co_fin_autr_nb (ncac)

    #  C5ZD1_personnel: co_uti_pers
    #  C5ZD2_vente: co_uti_vente
    #  C5ZD3_location: co_uti_loc

    #  C2ZF1_principale: co_uti_princ
    #  C2ZF2_secondaire: co_uti_secon

    #  C5ZE1_piscine: co_anx_pisc
    #  C5ZE2_garage: co_anx_gara
    #  C5ZE3_veranda: co_anx_veran
    #  C5ZE4_abri: co_anx_abri
    #  C5ZE5_annexes: co_anx_autr

    #  C5ZH1_agees: co_resid_agees
    #  C5ZH2_etudiants: co_resid_etud
    #  C5ZH3_tourisme: co_resid_tourism
    #  C5ZH4_residence: co_resid_hot_soc
    #  C5ZH5_sociale: co_resid_soc
    #  C5ZH6_handicapes: co_resid_hand
    #  C5ZH7_autres: co_resid_autr
    #  C5ZH8_autresprecision: co_resid_autr_desc (ncac)
    #  C5ZI1_chambres: co_foyer_chamb_nb (ncac)

    #  C5ZI2_1piece: co_log_1p_nb (ncac)
    #  C5ZI3_2pieces: co_log_2p_nb (ncac)
    #  C5ZI4_3pieces: co_log_3p_nb (ncac)
    #  C5ZI5_4pieces: co_log_4p_nb (ncac)
    #  C5ZI6_5pieces: co_log_5p_nb (ncac)
    #  C5ZI7_6pieces: co_log_6p_nb (ncac)
    #  C5ZJ1_niveaux: co_bat_niv_nb (ncac)
    #  C5ZJ2_dessous: co_bat_niv_dessous_nb (ncac)

    #  C5ZK1_extension: co_trx_exten
    #  C5ZK2_surelevation: co_trx_surelev
    #  C5ZK3_supplementaires: co_trx_nivsup

    #  B1G_transport: co_sp_transport
    #  B1L_enseignement: co_sp_enseign
    #  B1K_social: co_sp_act_soc
    #  B1B_special: co_sp_ouvr_spe
    #  B1S_sante: co_sp_sante
    #  B1F_culture: co_sp_culture

    # Tableaux

    #  W1AA1: su_avt_shon5
    #  W1AB1: su_cstr_shon5
    #  W1AC1: su_chge_shon5
    #  W1AD1: su_demo_shon5
    #  W1AE1: su_sup_shon5
    #  W1AF1: su_tot_shon5
    #  W1BA1: su_avt_shon3
    #  W1BB1: su_cstr_shon3
    #  W1BC1: su_chge_shon3
    #  W1BD1: su_demo_shon3
    #  W1BE1: su_sup_shon3
    #  W1BF1: su_tot_shon3
    #  W1CA1: su_avt_shon4
    #  W1CB1: su_cstr_shon4
    #  W1CC1: su_chge_shon4
    #  W1CD1: su_demo_shon4
    #  W1CE1: su_sup_shon4
    #  W1CF1: su_tot_shon4
    #  W1EA1: su_avt_shon8
    #  W1EB1: su_cstr_shon8
    #  W1EC1: su_chge_shon8
    #  W1ED1: su_demo_shon8
    #  W1EE1: su_sup_shon8
    #  W1EF1: su_tot_shon8
    #  W1FA1: su_avt_shon7
    #  W1FB1: su_cstr_shon7
    #  W1FC1: su_chge_shon7
    #  W1FD1: su_demo_shon7
    #  W1FE1: su_sup_shon7
    #  W1FF1: su_tot_shon7
    #  W1HA1: su_avt_shon2
    #  W1HB1: su_cstr_shon2
    #  W1HC1: su_chge_shon2
    #  W1HD1: su_demo_shon2
    #  W1HE1: su_sup_shon2
    #  W1HF1: su_tot_shon2
    #  W1IA1: su_avt_shon6
    #  W1IB1: su_cstr_shon6
    #  W1IC1: su_chge_shon6
    #  W1ID1: su_demo_shon6
    #  W1IE1: su_sup_shon6
    #  W1IF1: su_tot_shon6
    #  W1MA1: su_avt_shon1
    #  W1MB1: su_cstr_shon1
    #  W1MC1: su_chge_shon1
    #  W1MD1: su_demo_shon1
    #  W1ME1: su_sup_shon1
    #  W1MF1: su_tot_shon1
    #  W1PA1: su_avt_shon9
    #  W1PB1: su_cstr_shon9
    #  W1PC1: su_chge_shon9
    #  W1PD1: su_demo_shon9
    #  W1PE1: su_sup_shon9
    #  W1PF1: su_tot_shon9
    #  W1SA1: su_avt_shon_tot
    #  W1SB1: su_cstr_shon_tot
    #  W1SC1: su_chge_shon_tot
    #  W1SD1: su_demo_shon_tot
    #  W1SE1: su_sup_shon_tot
    #  W1SF1: su_tot_shon_tot
    #  W2AA1: su2_avt_shon1
    #  W2AB1: su2_cstr_shon1
    #  W2AC1: su2_chge_shon1
    #  W2AD1: su2_demo_shon1
    #  W2AE1: su2_sup_shon1
    #  W2AF1: su2_tot_shon1
    #  W2BA1: su2_avt_shon19
    #  W2BB1: su2_cstr_shon19
    #  W2BC1: su2_chge_shon19
    #  W2BD1: su2_demo_shon19
    #  W2BE1: su2_sup_shon19
    #  W2BF1: su2_tot_shon19
    #  W2CA1: su2_avt_shon5
    #  W2CB1: su2_cstr_shon5
    #  W2CC1: su2_chge_shon5
    #  W2CD1: su2_demo_shon5
    #  W2CE1: su2_sup_shon5
    #  W2CF1: su2_tot_shon5
    #  W2DA1: su2_avt_shon8
    #  W2DB1: su2_cstr_shon8
    #  W2DC1: su2_chge_shon8
    #  W2DD1: su2_demo_shon8
    #  W2DE1: su2_sup_shon8
    #  W2DF1: su2_tot_shon8
    #  W2EA1: su2_avt_shon18
    #  W2EB1: su2_cstr_shon18
    #  W2EC1: su2_chge_shon18
    #  W2ED1: su2_demo_shon18
    #  W2EE1: su2_sup_shon18
    #  W2EF1: su2_tot_shon18
    #  W2FA1: su2_avt_shon2
    #  W2FB1: su2_cstr_shon2
    #  W2FC1: su2_chge_shon2
    #  W2FD1: su2_demo_shon2
    #  W2FE1: su2_sup_shon2
    #  W2FF1: su2_tot_shon2
    #  W2GA1: su2_avt_shon7
    #  W2GB1: su2_cstr_shon7
    #  W2GC1: su2_chge_shon7
    #  W2GD1: su2_demo_shon7
    #  W2GE1: su2_sup_shon7
    #  W2GF1: su2_tot_shon7
    #  W2HA1: su2_avt_shon9
    #  W2HB1: su2_cstr_shon9
    #  W2HC1: su2_chge_shon9
    #  W2HD1: su2_demo_shon9
    #  W2HE1: su2_sup_shon9
    #  W2HF1: su2_tot_shon9
    #  W2IA1: su2_avt_shon17
    #  W2IB1: su2_cstr_shon17
    #  W2IC1: su2_chge_shon17
    #  W2ID1: su2_demo_shon17
    #  W2IE1: su2_sup_shon17
    #  W2IF1: su2_tot_shon17
    #  W2JA1: su2_avt_shon14
    #  W2JB1: su2_cstr_shon14
    #  W2JC1: su2_chge_shon14
    #  W2JD1: su2_demo_shon14
    #  W2JE1: su2_sup_shon14
    #  W2JF1: su2_tot_shon14
    #  W2KA1: su2_avt_shon10
    #  W2KB1: su2_cstr_shon10
    #  W2KC1: su2_chge_shon10
    #  W2KD1: su2_demo_shon10
    #  W2KE1: su2_sup_shon10
    #  W2KF1: su2_tot_shon10
    #  W2LA1: su2_avt_shon3
    #  W2LB1: su2_cstr_shon3
    #  W2LC1: su2_chge_shon3
    #  W2LD1: su2_demo_shon3
    #  W2LE1: su2_sup_shon3
    #  W2LF1: su2_tot_shon3
    #  W2MA1: su2_avt_shon4
    #  W2MB1: su2_cstr_shon4
    #  W2MC1: su2_chge_shon4
    #  W2MD1: su2_demo_shon4
    #  W2ME1: su2_sup_shon4
    #  W2MF1: su2_tot_shon4
    #  W2NA1: su2_avt_shon16
    #  W2NB1: su2_cstr_shon16
    #  W2NC1: su2_chge_shon16
    #  W2ND1: su2_demo_shon16
    #  W2NE1: su2_sup_shon16
    #  W2NF1: su2_tot_shon16
    #  W2PA1: su2_avt_shon11
    #  W2PB1: su2_cstr_shon11
    #  W2PC1: su2_chge_shon11
    #  W2PD1: su2_demo_shon11
    #  W2PE1: su2_sup_shon11
    #  W2PF1: su2_tot_shon11
    #  W2QA1: su2_avt_shon20
    #  W2QB1: su2_cstr_shon20
    #  W2QC1: su2_chge_shon20
    #  W2QD1: su2_demo_shon20
    #  W2QE1: su2_sup_shon20
    #  W2QF1: su2_tot_shon20
    #  W2RA1: su2_avt_shon6
    #  W2RB1: su2_cstr_shon6
    #  W2RC1: su2_chge_shon6
    #  W2RD1: su2_demo_shon6
    #  W2RE1: su2_sup_shon6
    #  W2RF1: su2_tot_shon6
    #  W2SA1: su2_avt_shon_tot
    #  W2SB1: su2_cstr_shon_tot
    #  W2SC1: su2_chge_shon_tot
    #  W2SD1: su2_demo_shon_tot
    #  W2SE1: su2_sup_shon_tot
    #  W2SF1: su2_tot_shon_tot
    #  W2TA1: su2_avt_shon12
    #  W2TB1: su2_cstr_shon12
    #  W2TC1: su2_chge_shon12
    #  W2TD1: su2_demo_shon12
    #  W2TE1: su2_sup_shon12
    #  W2TF1: su2_tot_shon12
    #  W2UA1: su2_avt_shon13
    #  W2UB1: su2_cstr_shon13
    #  W2UC1: su2_chge_shon13
    #  W2UD1: su2_demo_shon13
    #  W2UE1: su2_sup_shon13
    #  W2UF1: su2_tot_shon13
    #  W2VA1: su2_avt_shon15
    #  W2VB1: su2_cstr_shon15
    #  W2VC1: su2_chge_shon15
    #  W2VD1: su2_demo_shon15
    #  W2VE1: su2_sup_shon15
    #  W2VF1: su2_tot_shon15

    # C7A_surface: su_avt_shon_tot
    # C7K_supprimee: su_demo_shon_tot
    # C7U_creee: su_cstr_shon_tot

    #  C5ZF1_LLS: co_fin_lls
    #  C5ZF2_AS: co_fin_aa
    #  C5ZF3_PTZ: co_fin_ptz
    #  C5ZF5_autres: co_fin_autr

    # Stationnement

    #  S1A_stationnementavant: co_statio_avt_nb
    #  S1B_nombre: co_statio_place_nb
    #  S1C_adresses: co_statio_adr
    #  S1G_bati: co_statio_tot_shob
    #  S1H_surface: co_statio_tot_surf
    #  S1I_emprise: co_statio_comm_cin_surf
    #  S1M_stationnementapres: co_statio_apr_nb

    # Calcul imposition

    #  F1EP0_aidenon: ]
    #                              |= tax_ext_pret
    #  F1EP1_aideoui: ]

    #  F1EP2_lequel: tax_ext_desc

    #  F1GL1: tax_su_princ_log_nb1
    #  F1GS1: tax_su_princ_surf1
    #  F1GT1: tax_su_princ_surf_stat1
    #  F1HS1: tax_su_heber_surf1
    #  F1HT1: tax_su_heber_surf_stat1
    #  F1JS1: tax_surf_abr_jard_pig_colom
    #  F1LL1: tax_su_princ_log_nb2
    #  F1LS1: tax_su_princ_surf2
    #  F1LT1: tax_su_princ_surf_stat2
    #  F1MS1: tax_su_heber_surf2
    #  F1MT1: tax_su_heber_surf_stat2
    #  F1PL1: tax_su_princ_log_nb4
    #  F1PS1: tax_su_princ_surf4
    #  F1PT1: tax_su_princ_surf_stat4
    #  F1QS1: tax_su_heber_surf3
    #  F1QT1: tax_su_heber_surf_stat3
    #  F1TL0_existants: tax_log_exist_nb
    #  F1TL1: tax_su_tot_log_nb
    #  F1TS0_surfaceexistante: tax_surf_tax_exist_cons
    #  F1TS1_totale: tax_surf_tot_cstr
    #  F1TT1_creee: tax_surf_loc_stat
    #  F1XL1: tax_su_secon_log_nb
    #  F1XS1: tax_su_secon_surf
    #  F1XT1: tax_su_secon_surf_stat
    #  F1ZL1: tax_su_princ_log_nb3
    #  F1ZS1: tax_su_princ_surf3
    #  F1ZT1: tax_su_princ_surf_stat3
    #  F2AS1: tax_su_non_habit_surf3
    #  F2AT1: tax_su_non_habit_surf_stat3
    #  F2CN1: tax_comm_nb
    #  F2CS1: tax_su_non_habit_surf1
    #  F2CT1: tax_su_non_habit_surf_stat1
    #  F2ES1: tax_su_non_habit_surf6
    #  F2ET1: tax_su_non_habit_surf_stat6
    #  F2HS1: tax_su_non_habit_surf_stat4
    #  F2HT1: tax_su_non_habit_surf_stat4
    #  F2PS1: tax_su_non_habit_abr_jard_pig_colom
    #  F2PT1: tax_su_parc_statio_expl_comm_surf
    #  F2QS1: tax_su_non_habit_surf7
    #  F2QT1: tax_su_non_habit_surf_stat7
    #  F2US1: tax_su_non_habit_surf2
    #  F2UT1: tax_su_non_habit_surf_stat2
    #  F3NC1_caravanes: tax_empl_ten_carav_mobil_nb_cr
    #  F3NE1_eoliennes: tax_eol_haut_nb_cr
    #  F3NG1_HLL: tax_empl_hll_nb_cr
    #  F3NT1_station: tax_am_statio_ext_cr
    #  F3NV1_photovoltaique: tax_pann_volt_sup_cr
    #  F3SP1_bassin: tax_sup_bass_pisc_cr
    #  F4PC1_tentes: tax_empl_ten_carav_mobil_nb_arch
    #  F4PG1_HLL: tax_empl_hll_nb_arch
    #  F4PL1_locaux: tax_surf_loc_arch
    #  F4PP1_piscine: tax_surf_pisc_arch
    #  F4PT1_stationnement: tax_am_statio_ext_arch

    #  F5IC0_classenon:]
    #                               |=tax_monu_hist
    #  F5IC1_classeoui:]

    #  F5PR0_risquesnon: ]
    #                                 |=tax_trx_presc_ppr
    #  F5PR1_risquesoui: ]

    #  F6A_densiteoui:]
    #                              |=vsd_surf_planch_smd
    #  F6B_densitenon:]

    #  F6C_superficie: vsd_unit_fonc_constr_sup
    #  F6D_valeur: vsd_val_terr
    #  F6E_plancher: vsd_const_sxist_non_dem_surf
    #  F6F_rescrit: vsd_rescr_fisc
    #  F6N_Fonciere: vsd_unit_fonc_sup

    #  F1JS1_abris: tax_surf_abr_jard_pig_colom
    #  C5ZE6_autresannexes: co_anx_autr_desc
    #  H1H_honneur: co_archi_recours

    #  K1D_date : dm_constr_dates
    #  K1S_totale : dm_total
    #  K1E_partielle : dm_partiel
    #  K1L_logements : dm_tot_log_nb
    #  K1J_travaux : dm_projet_desc

    #  F1TL1_apres : tax_log_ap_trvx_nb

    #  C5ZB1_pieces : co_mais_piece_nb
    #  C5ZB2_niveaux : co_mais_niv_nb

    # ${field_value_cerfa['']}

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json

    # Création du dictionnaire contenant le mapping des cases à cocher.
    # ${dict_cac_mapping}=  Create Dictionary
    # ...  am_lotiss=A2A_lotissement
    # ...  am_lotiss=T2J_lotissement
    # ...  am_div_mun=A2L_division
    # ...  am_camping=A2T_camping
    # ...  am_caravane=A2D_caravanes
    # ...  am_statio=A2M_RML
    # ...  am_affou_exhau=A2Q_affouillements
    # ...  am_coupe_abat=A2K_coupe
    # ...  am_prot_plu=A2F_protege
    # ...  am_prot_muni=A2Z_deliberation
    # ...  am_air_terr_res_mob=A2I_residences
    # ...  am_aire_voyage=A2R_mobile
    # ...  am_rememb_afu=A2P_sauvegarde
    # ...  am_terr_res_demon=A2W_demontables
    # ...  am_aire_voyage=A2C_accueil
    # ...  am_mob_art=A3C_culture
    # ...  am_modif_voie_esp=A3M_public
    # ...  am_plant_voie_esp=A3B_plantation
    # ...  am_coupe_bois=A4A_bois
    # ...  am_coupe_parc=A4Q_parc
    # ...  am_coupe_align=A4C_alignement
    # ...  co_cstr_nouv=C2ZA1_nouvelle
    # ...  co_cstr_exist=C2ZB1_existante
    # ...  co_cloture=C2ZC3_cloture
    # ...  co_uti_pers=C5ZD1_personnel
    # ...  co_uti_vente=C5ZD2_vente
    # ...  co_uti_loc=C5ZD3_location
    # ...  co_uti_princ=C2ZF1_principale
    # ...  co_uti_secon=C2ZF2_secondaire
    # ...  co_anx_pisc=C5ZE1_piscine
    # ...  co_anx_gara=C5ZE2_garage
    # ...  co_anx_veran=C5ZE3_veranda
    # ...  co_anx_abri=C5ZE4_abri
    # ...  co_anx_autr=C5ZE5_annexes
    # ...  co_resid_agees=C5ZH1_agees
    # ...  co_resid_etud=C5ZH2_etudiants
    # ...  co_resid_tourism=C5ZH3_tourisme
    # ...  co_resid_hot_soc=C5ZH4_residence
    # ...  co_resid_soc=C5ZH5_sociale
    # ...  co_resid_hand=C5ZH6_handicapes
    # ...  co_resid_autr=C5ZH7_autres
    # ...  co_trx_exten=C5ZK1_extension
    # ...  co_trx_surelev=C5ZK2_surelevation
    # ...  co_trx_nivsup=C5ZK3_supplementaires
    # ...  co_sp_transport=B1G_transport
    # ...  co_sp_enseign=B1L_enseignement
    # ...  co_sp_act_soc=B1K_social
    # ...  co_sp_ouvr_spe=B1B_special
    # ...  co_sp_sante=B1S_sante
    # ...  co_sp_culture=B1F_culture
    # ...  co_piscine=C2ZC2_piscine
    # ...  am_exist_agrand=G1A_agrandissementoui
    # ...  tax_ext_pret=F1EP1_aideoui
    # ...  tax_monu_hist=F5IC1_classeoui
    # ...  tax_trx_presc_ppr=F5PR1_risquesoui
    # ...  vsd_surf_planch_smd=F6A_densiteoui
    # ...  co_fin_lls=C5ZF1_LLS
    # ...  co_fin_aa=C5ZF2_AS
    # ...  co_fin_ptz=C5ZF3_PTZ
    # ...  co_archi_recours=H1H_honneur
    # ...  dm_total=K1S_totale
    # ...  dm_partiel=K1E_partielle
    # ...  co_mais_contrat_ind=C5ZG1_contratoui

    # Utilisation du module pyton OpenADSCerfaCSV.py pour récupérer le mapping des données techniques (champs Button)
    ${dict_cac_mapping}=  get_donnees_techniques_assoc  mapping_cerfa_v7_to_openads.csv  Button

    # Création du dictionnaire contenant le mapping des données techniques
    # ${dict_donnees_techniques_mapping}=  Create Dictionary
    # ...  am_carav_duree=A2S_caravaneshors
    # ...  am_statio_cont=A2H_RMLnombre
    # ...  am_affou_exhau_sup=A2Y_superficie
    # ...  am_affou_prof=A2B_profondeur
    # ...  am_exhau_haut=A2E_hauteur
    # ...  am_projet_desc=A3D_description
    # ...  am_terr_surf=A3A_superficie
    # ...  am_tranche_desc=A3G_tranches
    # ...  am_exist_num=G1G_emplacements
    # ...  am_exist_nb_avant=G1B_emplacementsavant
    # ...  am_exist_nb_apres=G1R_emplacementsapres
    # ...  am_tente_nb=G1F_tentesnombre
    # ...  am_carav_nb=G1M_caravanesnombre
    # ...  am_mobil_nb=G1T_RMLnombre
    # ...  am_pers_nb=G1K_personnesmax
    # ...  am_empl_hll_nb=G1P_HLLnombre
    # ...  am_hll_shon=G1L_HLLsurface
    # ...  am_coupe_ess=A4B_essences
    # ...  am_coupe_age=A4I_age
    # ...  am_coupe_dens=A4N_densite
    # ...  am_coupe_qual=A4H_qualite
    # ...  am_coupe_trait=A4J_traitement
    # ...  am_coupe_autr=A4P_autres
    # ...  co_projet_desc=C2ZD1_description
    # ...  co_elec_tension=C2ZE1_puissance
    # ...  co_tot_log_nb=C5ZA1_logements
    # ...  co_tot_ind_nb=C5ZA2_individuels
    # ...  co_tot_coll_nb=C5ZA3_collectifs
    # ...  co_fin_lls_nb=C5ZC1_nombreLLS
    # ...  co_fin_aa_nb=C5ZC2_nombreAS
    # ...  co_fin_ptz_nb=C5ZC3_nombrePTZ
    # ...  co_fin_autr_nb=C5ZC5_nombreautres
    # ...  co_resid_autr_desc=C5ZH8_autresprecision
    # ...  co_foyer_chamb_nb=C5ZI1_chambres
    # ...  co_log_1p_nb=C5ZI2_1piece
    # ...  co_log_2p_nb=C5ZI3_2pieces
    # ...  co_log_3p_nb=C5ZI4_3pieces
    # ...  co_log_4p_nb=C5ZI5_4pieces
    # ...  co_log_5p_nb=C5ZI6_5pieces
    # ...  co_log_6p_nb=C5ZI7_6pieces
    # ...  co_bat_niv_nb=C5ZJ1_niveaux
    # ...  co_bat_niv_dessous_nb=C5ZJ2_dessous
    # ...  am_exist_date=G1N_numero
    # ...  su_avt_shon5=W1AA1
    # ...  su_cstr_shon5=W1AB1
    # ...  su_chge_shon5=W1AC1
    # ...  su_demo_shon5=W1AD1
    # ...  su_sup_shon5=W1AE1
    # ...  su_tot_shon5=W1AF1
    # ...  su_avt_shon3=W1BA1
    # ...  su_cstr_shon3=W1BB1
    # ...  su_chge_shon3=W1BC1
    # ...  su_demo_shon3=W1BD1
    # ...  su_sup_shon3=W1BE1
    # ...  su_tot_shon3=W1BF1
    # ...  su_avt_shon4=W1CA1
    # ...  su_cstr_shon4=W1CB1
    # ...  su_chge_shon4=W1CC1
    # ...  su_demo_shon4=W1CD1
    # ...  su_sup_shon4=W1CE1
    # ...  su_tot_shon4=W1CF1
    # ...  su_avt_shon8=W1EA1
    # ...  su_cstr_shon8=W1EB1
    # ...  su_chge_shon8=W1EC1
    # ...  su_demo_shon8=W1ED1
    # ...  su_sup_shon8=W1EE1
    # ...  su_tot_shon8=W1EF1
    # ...  su_avt_shon7=W1FA1
    # ...  su_cstr_shon7=W1FB1
    # ...  su_chge_shon7=W1FC1
    # ...  su_demo_shon7=W1FD1
    # ...  su_sup_shon7=W1FE1
    # ...  su_tot_shon7=W1FF1
    # ...  su_avt_shon2=W1HA1
    # ...  su_cstr_shon2=W1HB1
    # ...  su_chge_shon2=W1HC1
    # ...  su_demo_shon2=W1HD1
    # ...  su_sup_shon2=W1HE1
    # ...  su_tot_shon2=W1HF1
    # ...  su_avt_shon6=W1IA1
    # ...  su_cstr_shon6=W1IB1
    # ...  su_chge_shon6=W1IC1
    # ...  su_demo_shon6=W1ID1
    # ...  su_sup_shon6=W1IE1
    # ...  su_tot_shon6=W1IF1
    # ...  su_avt_shon1=W1MA1
    # ...  su_cstr_shon1=W1MB1
    # ...  su_chge_shon1=W1MC1
    # ...  su_demo_shon1=W1MD1
    # ...  su_sup_shon1=W1ME1
    # ...  su_tot_shon1=W1MF1
    # ...  su_avt_shon9=W1PA1
    # ...  su_cstr_shon9=W1PB1
    # ...  su_chge_shon9=W1PC1
    # ...  su_demo_shon9=W1PD1
    # ...  su_sup_shon9=W1PE1
    # ...  su_tot_shon9=W1PF1
    # ...  su_avt_shon_tot=W1SA1
    # ...  su_cstr_shon_tot=W1SB1
    # ...  su_chge_shon_tot=W1SC1
    # ...  su_demo_shon_tot=W1SD1
    # ...  su_sup_shon_tot=W1SE1
    # ...  su_tot_shon_tot=W1SF1
    # ...  su2_avt_shon1=W2AA1
    # ...  su2_cstr_shon1=W2AB1
    # ...  su2_chge_shon1=W2AC1
    # ...  su2_demo_shon1=W2AD1
    # ...  su2_sup_shon1=W2AE1
    # ...  su2_tot_shon1=W2AF1
    # ...  su2_avt_shon19=W2BA1
    # ...  su2_cstr_shon19=W2BB1
    # ...  su2_chge_shon19=W2BC1
    # ...  su2_demo_shon19=W2BD1
    # ...  su2_sup_shon19=W2BE1
    # ...  su2_tot_shon19=W2BF1
    # ...  su2_avt_shon5=W2CA1
    # ...  su2_cstr_shon5=W2CB1
    # ...  su2_chge_shon5=W2CC1
    # ...  su2_demo_shon5=W2CD1
    # ...  su2_sup_shon5=W2CE1
    # ...  su2_tot_shon5=W2CF1
    # ...  su2_avt_shon8=W2DA1
    # ...  su2_cstr_shon8=W2DB1
    # ...  su2_chge_shon8=W2DC1
    # ...  su2_demo_shon8=W2DD1
    # ...  su2_sup_shon8=W2DE1
    # ...  su2_tot_shon8=W2DF1
    # ...  su2_avt_shon18=W2EA1
    # ...  su2_cstr_shon18=W2EB1
    # ...  su2_chge_shon18=W2EC1
    # ...  su2_demo_shon18=W2ED1
    # ...  su2_sup_shon18=W2EE1
    # ...  su2_tot_shon18=W2EF1
    # ...  su2_avt_shon2=W2FA1
    # ...  su2_cstr_shon2=W2FB1
    # ...  su2_chge_shon2=W2FC1
    # ...  su2_demo_shon2=W2FD1
    # ...  su2_sup_shon2=W2FE1
    # ...  su2_tot_shon2=W2FF1
    # ...  su2_avt_shon7=W2GA1
    # ...  su2_cstr_shon7=W2GB1
    # ...  su2_chge_shon7=W2GC1
    # ...  su2_demo_shon7=W2GD1
    # ...  su2_sup_shon7=W2GE1
    # ...  su2_tot_shon7=W2GF1
    # ...  su2_avt_shon9=W2HA1
    # ...  su2_cstr_shon9=W2HB1
    # ...  su2_chge_shon9=W2HC1
    # ...  su2_demo_shon9=W2HD1
    # ...  su2_sup_shon9=W2HE1
    # ...  su2_tot_shon9=W2HF1
    # ...  su2_avt_shon17=W2IA1
    # ...  su2_cstr_shon17=W2IB1
    # ...  su2_chge_shon17=W2IC1
    # ...  su2_demo_shon17=W2ID1
    # ...  su2_sup_shon17=W2IE1
    # ...  su2_tot_shon17=W2IF1
    # ...  su2_avt_shon14=W2JA1
    # ...  su2_cstr_shon14=W2JB1
    # ...  su2_chge_shon14=W2JC1
    # ...  su2_demo_shon14=W2JD1
    # ...  su2_sup_shon14=W2JE1
    # ...  su2_tot_shon14=W2JF1
    # ...  su2_avt_shon10=W2KA1
    # ...  su2_cstr_shon10=W2KB1
    # ...  su2_chge_shon10=W2KC1
    # ...  su2_demo_shon10=W2KD1
    # ...  su2_sup_shon10=W2KE1
    # ...  su2_tot_shon10=W2KF1
    # ...  su2_avt_shon3=W2LA1
    # ...  su2_cstr_shon3=W2LB1
    # ...  su2_chge_shon3=W2LC1
    # ...  su2_demo_shon3=W2LD1
    # ...  su2_sup_shon3=W2LE1
    # ...  su2_tot_shon3=W2LF1
    # ...  su2_avt_shon4=W2MA1
    # ...  su2_cstr_shon4=W2MB1
    # ...  su2_chge_shon4=W2MC1
    # ...  su2_demo_shon4=W2MD1
    # ...  su2_sup_shon4=W2ME1
    # ...  su2_tot_shon4=W2MF1
    # ...  su2_avt_shon16=W2NA1
    # ...  su2_cstr_shon16=W2NB1
    # ...  su2_chge_shon16=W2NC1
    # ...  su2_demo_shon16=W2ND1
    # ...  su2_sup_shon16=W2NE1
    # ...  su2_tot_shon16=W2NF1
    # ...  su2_avt_shon11=W2PA1
    # ...  su2_cstr_shon11=W2PB1
    # ...  su2_chge_shon11=W2PC1
    # ...  su2_demo_shon11=W2PD1
    # ...  su2_sup_shon11=W2PE1
    # ...  su2_tot_shon11=W2PF1
    # ...  su2_avt_shon20=W2QA1
    # ...  su2_cstr_shon20=W2QB1
    # ...  su2_chge_shon20=W2QC1
    # ...  su2_demo_shon20=W2QD1
    # ...  su2_sup_shon20=W2QE1
    # ...  su2_tot_shon20=W2QF1
    # ...  su2_avt_shon6=W2RA1
    # ...  su2_cstr_shon6=W2RB1
    # ...  su2_chge_shon6=W2RC1
    # ...  su2_demo_shon6=W2RD1
    # ...  su2_sup_shon6=W2RE1
    # ...  su2_tot_shon6=W2RF1
    # ...  su2_avt_shon_tot=W2SA1
    # ...  su2_cstr_shon_tot=W2SB1
    # ...  su2_chge_shon_tot=W2SC1
    # ...  su2_demo_shon_tot=W2SD1
    # ...  su2_sup_shon_tot=W2SE1
    # ...  su2_tot_shon_tot=W2SF1
    # ...  su2_avt_shon12=W2TA1
    # ...  su2_cstr_shon12=W2TB1
    # ...  su2_chge_shon12=W2TC1
    # ...  su2_demo_shon12=W2TD1
    # ...  su2_sup_shon12=W2TE1
    # ...  su2_tot_shon12=W2TF1
    # ...  su2_avt_shon13=W2UA1
    # ...  su2_cstr_shon13=W2UB1
    # ...  su2_chge_shon13=W2UC1
    # ...  su2_demo_shon13=W2UD1
    # ...  su2_sup_shon13=W2UE1
    # ...  su2_tot_shon13=W2UF1
    # ...  su2_avt_shon15=W2VA1
    # ...  su2_cstr_shon15=W2VB1
    # ...  su2_chge_shon15=W2VC1
    # ...  su2_demo_shon15=W2VD1
    # ...  su2_sup_shon15=W2VE1
    # ...  su2_tot_shon15=W2VF1
    # ...  su_avt_shon_tot=C7A_surface
    # ...  su_demo_shon_tot=C7K_supprimee
    # ...  su_cstr_shon_tot=C7U_creee
    # ...  co_statio_avt_nb=S1A_stationnementavant
    # ...  co_statio_place_nb=S1B_nombre
    # ...  co_statio_adr=S1C_adresses
    # ...  co_statio_tot_shob=S1G_bati
    # ...  co_statio_tot_surf=S1H_surface
    # ...  co_statio_comm_cin_surf=S1I_emprise
    # ...  co_statio_apr_nb=S1M_stationnementapres
    # ...  tax_ext_desc=F1EP2_lequel
    # ...  tax_su_princ_log_nb1=F1GL1
    # ...  tax_su_princ_surf1=F1GS1
    # ...  tax_su_princ_surf_stat1=F1GT1
    # ...  tax_su_heber_surf1=F1HS1
    # ...  tax_su_heber_surf_stat1=F1HT1
    # ...  tax_surf_abr_jard_pig_colom=F1JS1
    # ...  tax_su_princ_log_nb2=F1LL1
    # ...  tax_su_princ_surf2=F1LS1
    # ...  tax_su_princ_surf_stat2=F1LT1
    # ...  tax_su_heber_surf2=F1MS1
    # ...  tax_su_heber_surf_stat2=F1MT1
    # ...  tax_su_princ_log_nb4=F1PL1
    # ...  tax_su_princ_surf4=F1PS1
    # ...  tax_su_princ_surf_stat4=F1PT1
    # ...  tax_su_heber_surf3=F1QS1
    # ...  tax_su_heber_surf_stat3=F1QT1
    # ...  tax_log_exist_nb=F1TL0_existants
    # ...  tax_su_tot_log_nb=F1TL1
    # ...  tax_surf_tax_exist_cons=F1TS0_surfaceexistante
    # ...  tax_surf_tot_cstr=F1TS1_totale
    # ...  tax_surf_loc_stat=F1TT1_creee
    # ...  tax_su_secon_log_nb=F1XL1
    # ...  tax_su_secon_surf=F1XS1
    # ...  tax_su_secon_surf_stat=F1XT1
    # ...  tax_su_princ_log_nb3=F1ZL1
    # ...  tax_su_princ_surf3=F1ZS1
    # ...  tax_su_princ_surf_stat3=F1ZT1
    # ...  tax_su_non_habit_surf3=F2AS1
    # ...  tax_su_non_habit_surf_stat3=F2AT1
    # ...  tax_comm_nb=F2CN1
    # ...  tax_su_non_habit_surf1=F2CS1
    # ...  tax_su_non_habit_surf_stat1=F2CT1
    # ...  tax_su_non_habit_surf6=F2ES1
    # ...  tax_su_non_habit_surf_stat6=F2ET1
    # ...  tax_su_non_habit_surf_stat4=F2HS1
    # ...  tax_su_non_habit_surf_stat4=F2HT1
    # ...  tax_su_non_habit_abr_jard_pig_colom=F2PS1
    # ...  tax_su_parc_statio_expl_comm_surf=F2PT1
    # ...  tax_su_non_habit_surf7=F2QS1
    # ...  tax_su_non_habit_surf_stat7=F2QT1
    # ...  tax_su_non_habit_surf2=F2US1
    # ...  tax_su_non_habit_surf_stat2=F2UT1
    # ...  tax_empl_ten_carav_mobil_nb_cr=F3NC1_caravanes
    # ...  tax_eol_haut_nb_cr=F3NE1_eoliennes
    # ...  tax_empl_hll_nb_cr=F3NG1_HLL
    # ...  tax_am_statio_ext_cr=F3NT1_station
    # ...  tax_pann_volt_sup_cr=F3NV1_photovoltaique
    # ...  tax_sup_bass_pisc_cr=F3SP1_bassin
    # ...  tax_empl_ten_carav_mobil_nb_arch=F4PC1_tentes
    # ...  tax_empl_hll_nb_arch=F4PG1_HLL
    # ...  tax_surf_loc_arch=F4PL1_locaux
    # ...  tax_surf_pisc_arch=F4PP1_piscine
    # ...  tax_am_statio_ext_arch=F4PT1_stationnement
    # ...  vsd_unit_fonc_constr_sup=F6C_superficie
    # ...  vsd_val_terr=F6D_valeur
    # ...  vsd_const_sxist_non_dem_surf=F6E_plancher
    # ...  vsd_rescr_fisc=F6F_rescrit
    # ...  vsd_unit_fonc_sup=F6N_Fonciere
    # ...  tax_surf_abr_jard_pig_colom=F1JS1_abris
    # ...  co_anx_autr_desc=C5ZE6_autresannexes
    # ...  co_fin_autr=C5ZF5_autres
    # ...  dm_tot_log_nb=K1L_logements
    # ...  dm_constr_dates=K1D_date
    # ...  dm_projet_desc=K1J_travaux
    # ...  tax_log_ap_trvx_nb=F1TL1_apres
    # ...  co_mais_piece_nb=C5ZB1_pieces
    # ...  co_mais_niv_nb=C5ZB2_niveaux
    # ...  tax_desc=F9R_informations

    # Utilisation du module pyton OpenADSCerfaCSV.py pour récupérer le mapping des données techniques (champs Text)
    ${dict_donnees_techniques_mapping}=  get_donnees_techniques_assoc  mapping_cerfa_v7_to_openads.csv  Text

    # Affichage du contenu des deux dictionnaires dans les logs
    Log  ${dict_cac_mapping}
    Log  ${dict_donnees_techniques_mapping}

    # Création du dictionnaire qui sera retourné
    &{dict_donnees_techniques}=  Create Dictionary

    # Création du dictionnaire qui contiendra les casesà cocher à traiter
    &{dict_cac}=  Create Dictionary

    # On récupère le mapping des données techniques
    ${items_donnees_technique_mapping}=  Get Dictionary Items  ${dict_donnees_techniques_mapping}
    # Pour chaque couple champ-valeur du mapping on vérifie que le champ existe dans le json. Si le champ existe il est ajouté dans le dictionnaire qui sera retourné
    :FOR    ${field}    ${value}    IN    @{items_donnees_technique_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_donnees_techniques}  ${field}=${field_value_cerfa['${value}']}

    # On récupère le mapping des cases à cocher
    ${items_dict_cac_mapping}=  Get Dictionary Items  ${dict_cac_mapping}
    # Pour chaque couple champ-valeur du mapping on vérifie que le champ existe dans le json. Si le champ existe il est ajouté dans le dictionnaire des cases à cocher qui vont être traitées.
    :FOR    ${field}    ${value}    IN    @{items_dict_cac_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_cac}  ${field}=${field_value_cerfa['${value}']}

    # Traitement des cases à cocher pour que les valeurs soient dans le bon format
    ${items_dict_cac}=  Get Dictionary Items  ${dict_cac}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items_dict_cac}
    \  ${new_value}=  Set Variable If  '${value}'=='Oui'  t
    \  ...                             '${value}'=='1'  t
    \  ...                                                f
    \  Set To Dictionary  ${dict_donnees_techniques}  ${field}=${new_value}

    [Return]    ${dict_donnees_techniques}

Charger le json et créer le dictionnaire de l'architecte
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire des données techniques
    [Arguments]  ${nom_du_dossier}

    # Mapping de l'architecte

    #  H1AE1_email: archi prefixe email
    #  H1AE2_email: archi suffixe email
    #  H1C_code: cp
    #  H1F_fax: fax
    #  H1K_ordre: inscription
    #  H1L_localite: ville
    #  H1N_nom: nom
    #  H1P_prenom: prenom
    #  H1R_conseil: conseil_regional
    #  H1T_telephone: telephone
    #  H1Q_numero: numero adresse ]
    #                                | = Concaténation des deux champ => adresse1
    #  H1V_voie: voie adresse     ]
    #  H1W_lieudit: lieu_dit
    #  H1D_cedex: cedex
    #  H1B_boite: boite_postale

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    ${nom_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  H1N_nom
    Return From Keyword If  ${nom_exist}==False
    Return From Keyword If  '${field_value_cerfa['H1N_nom']}'=='${EMPTY}'

    # Création du dictionnaire de l'architecte
    &{dict_architecte_mapping}=  Create Dictionary
    ...  cp=H1C_code
    ...  fax=H1F_fax
    ...  inscription=H1K_ordre
    ...  ville=H1L_localite
    ...  nom=H1N_nom
    ...  prenom=H1P_prenom
    ...  conseil_regional=H1R_conseil
    ...  telephone=H1T_telephone
    ...  lieu_dit=H1W_lieudit
    ...  cedex=H1D_cedex
    ...  boite_postale=H1B_boite
    ...  prefixe_email=H1AE1_email
    ...  suffixe_email=H1AE2_email
    ...  numero_adresse=H1Q_numero
    ...  voie_adresse=H1V_voie

    &{dict_architecte}=  Create Dictionary

    # On récupère le mapping des données techniques
    ${items_architecte_mapping}=  Get Dictionary Items  ${dict_architecte_mapping}
    # Pour chaque couple champ-valeur du mapping on vérifie que le champ existe dans le json. Si le champ existe il est ajouté dans le dictionnaire qui sera retourné
    :FOR    ${field}    ${value}    IN    @{items_architecte_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_architecte}  ${field}=${field_value_cerfa['${value}']}

    Run Keyword If  '${dict_architecte.prefixe_email}'!='${EMPTY}' and '${dict_architecte.suffixe_email}'!='${EMPTY}'  Set To Dictionary  ${dict_architecte}  email=${dict_architecte.prefixe_email}@${dict_architecte.suffixe_email}

    Set To Dictionary  ${dict_architecte}  adresse1=${dict_architecte.numero_adresse} ${dict_architecte.voie_adresse}

    [Return]    ${dict_architecte}


Charger le json et créer le dictionnaire du délégataire
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire du délégataire
    [Arguments]  ${nom_du_dossier}

    # Autre correspondance
    #  D4F_femme:]
    #                        |= particulier_civilite
    #  D4H_homme:]
    #  D4K_morale:
    #  D4B_boite: bp
    #  D4C_code: code_postal
    #  D4D_division: division_territoriale
    #  D4E_pays: pays
    #  D4I_indicatif: indicatif
    #  D4L_localite: localite
    #  D4N_nom: particulier_nom
    #  D4P_prenom: particulier_prenom
    #  D4Q_numero: numero
    #  D4R_raison: personne_morale_raison_sociale
    #  D4T_telephone: telephone_fixe
    #  D4V_voie: voie
    #  D4W_lieudit: lieu_dit
    #  D4X_cedex: cedex


    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    ${nom_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  D4N_nom
    ${raison_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  D4R_raison

    Return From Keyword If  ${nom_exist}==False and ${raison_exist}==False

    Return From Keyword If  '${field_value_cerfa['D4N_nom']}'=='${EMPTY}' and '${field_value_cerfa['D4R_raison']}'=='${EMPTY}'

    # Vérification de la qualite du déclarant (particulier ou personne morale)
    ${autre_corres_qualite}=  Set Variable If  '${field_value_cerfa['D4K_morale']}' != 'Oui'  particulier
    ...                                                                                          personne morale

    ${autre_corres_particulier_civilite}=  Set Variable If  '${field_value_cerfa['D4H_homme']}' == 'Oui'  Monsieur
    ...                                                     '${field_value_cerfa['D4F_femme']}' == 'Oui'  Madame
    ...                                                                                                               choisir civilité

    ${autre_corres_pays}=  Set Variable If  '${field_value_cerfa['D4E_pays']}'=='${EMPTY}'  France
    ...                                                                                                 ${field_value_cerfa['D4E_pays']}

    &{autre_corres_personne_morale_dict}=  Create Dictionary
    ...  personne_morale_raison_sociale=${field_value_cerfa['D4R_raison']}

    &{autre_corres_particulier_dict}=  Create Dictionary
    ...  particulier_civilite=${autre_corres_particulier_civilite}
    ...  particulier_nom=${field_value_cerfa['D4N_nom']}
    ...  particulier_prenom=${field_value_cerfa['D4P_prenom']}

    # Création du dictionnaire pour le pétitionnaire
    &{dict_autre_correspondant}=  Create Dictionary
    ...  om_collectivite=${OM_COLLECTIVITE}
    ...  qualite=${autre_corres_qualite}
    ...  numero=${field_value_cerfa['D4Q_numero']}
    ...  voie=${field_value_cerfa['D4V_voie']}
    ...  localite=${field_value_cerfa['D4L_localite']}
    ...  code_postal=${field_value_cerfa['D4C_code']}
    ...  bp=${field_value_cerfa['D4B_boite']}
    ...  cedex=${field_value_cerfa['D4X_cedex']}
    ...  pays=${autre_corres_pays}
    ...  division_territoriale=${field_value_cerfa['D4D_division']}
    ...  indicatif=${field_value_cerfa['D4I_indicatif']}
    ...  lieu_dit=${field_value_cerfa['D4W_lieudit']}

    ${telephone_first_two_char} =  Get Substring  ${field_value_cerfa['D4T_telephone']}  0  2

    Run Keyword If  '${telephone_first_two_char}' == '06' or '${telephone_first_two_char}' == '07'   Set To Dictionary  ${dict_autre_correspondant}  telephone_mobile=${field_value_cerfa['D4T_telephone']}
    ...  ELSE  Set To Dictionary  ${dict_autre_correspondant}  telephone_fixe=${field_value_cerfa['D4T_telephone']}

    # Ajout du particulier ou de la personne morale dans le dictionnaire à retourner
    ${items}=  Run Keyword If  '${dict_autre_correspondant.qualite}' == 'personne morale'  Get Dictionary Items    ${autre_corres_personne_morale_dict}
    ...  ELSE  Get Dictionary Items    ${autre_corres_particulier_dict}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items}
    \  Set To Dictionary  ${dict_autre_correspondant}  ${field}=${value}

    [Return]    ${dict_autre_correspondant}


Charger le json et créer le dictionnaire pour un autre pétitionnaire
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire pour un autre péttitionnaire
    [Arguments]  ${nom_du_dossier}

    # Demandeur PVR
    #  V1F_femme:]
    #                        |= particulier_civilite
    #  V1H_homme:]
    #  V1Q_morale:
    #  V1B_boite: bp
    #  V1C_code: code_postal
    #  V1D_division: division_territoriale
    #  V1E_pays: pays
    #  V1L_localite: localite
    #  V1N_nom: particulier_nom
    #  V1P_prenom: particulier_prenom
    #  V1R_raison:
    #  V1V_voie: voie
    #  V1W_lieudit: lieu_dit
    #  V1X_cedex: cedex
    #  V1Z_numero: numero

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}

    ${nom_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  V1N_nom
    ${raison_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  V1R_raison

    Return From Keyword If  ${nom_exist}==False and ${raison_exist}==False
    Return From Keyword If  '${field_value_cerfa['V1N_nom']}'=='${EMPTY}' and '${field_value_cerfa['V1R_raison']}'=='${EMPTY}'

    # Vérification de la qualite du déclarant (particulier ou personne morale)
    ${autre_petitionnaire_qualite}=  Set Variable If  '${field_value_cerfa['V1Q_morale']}' != 'Oui'  particulier
    ...                                                                                                 personne morale

    ${autre_petitionnaire_particulier_civilite}=  Set Variable If  '${field_value_cerfa['V1H_homme']}' == 'Oui'  Monsieur
    ...                                                     '${field_value_cerfa['V1F_femme']}' == 'Oui'         Madame
    ...                                                                                                             choisir civilité

    ${autre_petitionnaire_pays}=  Set Variable If  '${field_value_cerfa['V1E_pays']}'=='${EMPTY}'  France
    ...                                                                                                 ${field_value_cerfa['V1E_pays']}

    &{autre_petitionnaire_personne_morale_dict}=  Create Dictionary
    ...  personne_morale_raison_sociale=${field_value_cerfa['V1R_raison']}

    &{autre_petitionnaire_particulier_dict}=  Create Dictionary
    ...  particulier_civilite=${autre_petitionnaire_particulier_civilite}
    ...  particulier_nom=${field_value_cerfa['V1N_nom']}
    ...  particulier_prenom=${field_value_cerfa['V1P_prenom']}

    # Création du dictionnaire pour le pétitionnaire
    &{dict_autre_petitionnaire}=  Create Dictionary
    ...  om_collectivite=${OM_COLLECTIVITE}
    ...  qualite=${autre_petitionnaire_qualite}
    ...  numero=${field_value_cerfa['V1Z_numero']}
    ...  voie=${field_value_cerfa['V1V_voie']}
    ...  localite=${field_value_cerfa['V1L_localite']}
    ...  code_postal=${field_value_cerfa['V1C_code']}
    ...  bp=${field_value_cerfa['V1B_boite']}
    ...  cedex=${field_value_cerfa['V1X_cedex']}
    ...  pays=${autre_petitionnaire_pays}
    ...  division_territoriale=${field_value_cerfa['V1D_division']}
    ...  lieu_dit=${field_value_cerfa['V1W_lieudit']}

    # Ajout du particulier ou de la personne morale dans le dictionnaire à retourner
    ${items}=  Run Keyword If  '${dict_autre_petitionnaire.qualite}' == 'personne morale'  Get Dictionary Items    ${autre_petitionnaire_personne_morale_dict}
    ...  ELSE  Get Dictionary Items    ${autre_petitionnaire_particulier_dict}
    # Pour chaque couple champ-valeur dans la liste
    :FOR    ${field}    ${value}    IN    @{items}
    \  Set To Dictionary  ${dict_autre_petitionnaire}  ${field}=${value}

    [Return]    ${dict_autre_petitionnaire}

Charger le json et créer le dictionnaire de l'architecte legislation connexe
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire des données techniques
    [Arguments]  ${nom_du_dossier}

    # Mapping de l'architecte

    # E4A_architecte|
    #                 | Si la case architecte est cochée alors on continue sinon c'est jardinier
    # E4J_jardinier |

    # E4B_BP
    # E4C_codepostal
    # E4E1_email
    # E4E2_email
    # E4L_Localite
    # E4N_Nom
    # E4P_prenom
    # E4Q_numero
    # E4T_tel:
    # E4V_voie:
    # E4W_Lieudit
    # E4X_Cedex
    # E5I_inscription
    # E5I_region

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}
    ${archi_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  E4A_architecte
    Return From Keyword If  ${archi_exist}==False
    Return From Keyword If  '${field_value_cerfa['E4A_architecte']}'!='Oui'

    # Création du dictionnaire de l'architecte
    &{dict_architecte_mapping}=  Create Dictionary
    ...  particulier_nom=E4N_Nom
    ...  particulier_prenom=E4P_prenom
    ...  numero=E4Q_numero
    ...  voie=E4V_voie
    ...  lieu_dit=E4W_Lieudit
    ...  localite=E4L_Localite
    ...  code_postal=E4C_codepostal
    ...  bp=E4B_BP
    ...  cedex=E4X_Cedex
    ...  prefixe_email=E4E1_email
    ...  suffixe_email=E4E2_email
    ...  num_inscription=E5I_inscription
    ...  conseil_regional=E5I_region

    &{dict_architecte}=  Create Dictionary

    # On récupère le mapping des données techniques
    ${items_architecte_mapping}=  Get Dictionary Items  ${dict_architecte_mapping}
    # Pour chaque couple champ-valeur du mapping on vérifie que le champ existe dans le json. Si le champ existe il est ajouté dans le dictionnaire qui sera retourné
    :FOR    ${field}    ${value}    IN    @{items_architecte_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_architecte}  ${field}=${field_value_cerfa['${value}']}

    # Gestion du numéro de téléphone
    ${telephone_first_two_char} =  Get Substring  ${field_value_cerfa['E4T_tel']}  0  2
    Run Keyword If  '${telephone_first_two_char}' == '06' or '${telephone_first_two_char}' == '07'   Set To Dictionary  ${dict_architecte}  telephone_mobile=${field_value_cerfa['E4T_tel']}
    ...  ELSE  Set To Dictionary  ${dict_architecte}  telephone_fixe=${field_value_cerfa['E4T_tel']}

    # Gestion de l'adresse email
    Run Keyword If  '${dict_architecte.prefixe_email}'!='${EMPTY}' and '${dict_architecte.suffixe_email}'!='${EMPTY}'  Set To Dictionary  ${dict_architecte}  courriel=${dict_architecte.prefixe_email}@${dict_architecte.suffixe_email}

    Set To Dictionary  ${dict_architecte}  om_collectivite=${OM_COLLECTIVITE}

    [Return]    ${dict_architecte}


Charger le json et créer le dictionnaire du concepteur-paysagiste
    [Documentation]  On récupère les données du cerfa et on retourne le dictionnaire des données techniques
    [Arguments]  ${nom_du_dossier}

    # Mapping de l'architecte

    # E4A_architecte|
    #                 | Si la case architecte est cochée alors on continue sinon c'est jardinier
    # E4J_jardinier |

    # E4B_BP
    # E4C_codepostal
    # E4E1_email
    # E4E2_email
    # E4L_Localite
    # E4N_Nom
    # E4P_prenom
    # E4Q_numero
    # E4T_tel:
    # E4V_voie:
    # E4W_Lieudit
    # E4X_Cedex
    # E5I_inscription
    # E5I_region

    # Lecture du fichier json
    ${json}=  Get file  ${EXECDIR}${/}binary_files${/}final_files${/}${nom_du_dossier}.json
    # Convertion du fichier json en objet python
    ${field_value_cerfa}=  Evaluate  json.loads('''${json}''')  json
    Log Dictionary  ${field_value_cerfa}
    ${paysagiste_exist}=  Run Keyword And Return Status  Dictionary Should Contain Key  ${field_value_cerfa}  E4J_jardinier
    Return From Keyword If  ${paysagiste_exist}==False
    Return From Keyword If  '${field_value_cerfa['E4J_jardinier']}'!='Oui'

    # Création du dictionnaire de l'architecte
    &{dict_pays_concep_mapping}=  Create Dictionary
    ...  particulier_nom=E4N_Nom
    ...  particulier_prenom=E4P_prenom
    ...  numero=E4Q_numero
    ...  voie=E4V_voie
    ...  lieu_dit=E4W_Lieudit
    ...  localite=E4L_Localite
    ...  code_postal=E4C_codepostal
    ...  bp=E4B_BP
    ...  cedex=E4X_Cedex
    ...  prefixe_email=E4E1_email
    ...  suffixe_email=E4E2_email

    &{dict_pays_concep}=  Create Dictionary

    # On récupère le mapping des données techniques
    ${items_pays_concep_mapping}=  Get Dictionary Items  ${dict_pays_concep_mapping}
    # Pour chaque couple champ-valeur du mapping on vérifie que le champ existe dans le json. Si le champ existe il est ajouté dans le dictionnaire qui sera retourné
    :FOR    ${field}    ${value}    IN    @{items_pays_concep_mapping}
    \   ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key  ${field_value_cerfa}  ${value}
    \   Run Keyword If    ${exist} == True    Set To Dictionary  ${dict_pays_concep}  ${field}=${field_value_cerfa['${value}']}

    # Gestion du numéro de téléphone
    ${telephone_first_two_char} =  Get Substring  ${field_value_cerfa['E4T_tel']}  0  2
    Run Keyword If  '${telephone_first_two_char}' == '06' or '${telephone_first_two_char}' == '07'   Set To Dictionary  ${dict_pays_concep}  telephone_mobile=${field_value_cerfa['E4T_tel']}
    ...  ELSE  Set To Dictionary  ${dict_pays_concep}  telephone_fixe=${field_value_cerfa['E4T_tel']}

    # Gestion de l'adresse email
    Run Keyword If  '${dict_pays_concep.prefixe_email}'!='${EMPTY}' and '${dict_pays_concep.suffixe_email}'!='${EMPTY}'  Set To Dictionary  ${dict_pays_concep}  courriel=${dict_pays_concep.prefixe_email}@${dict_pays_concep.suffixe_email}

    Set To Dictionary  ${dict_pays_concep}  om_collectivite=${OM_COLLECTIVITE}

    [Return]    ${dict_pays_concep}


Plat'Au Ajouter le dossier d'instruction
    [Documentation]  Permet de saisir un dossier d'instruction et ses données techniques
    [Arguments]  ${nom_du_dossier}  ${args_petitionnaire_supp}=${EMPTY}  ${args_petitionnaire_supp1}=${EMPTY}

    &{args_petitionnaire}=  Charger le json et créer le dictionnaire du petitionnaire  ${nom_du_dossier}
    &{args_demande}=  Charger le json et créer le dictionnaire de la demande  ${nom_du_dossier}
    &{args_delegataire}=  Charger le json et créer le dictionnaire du délégataire  ${nom_du_dossier}
    &{args_petitionnaire_autre}=  Charger le json et créer le dictionnaire pour un autre pétitionnaire  ${nom_du_dossier}
    &{args_architecte_legi_connexe}=  Charger le json et créer le dictionnaire de l'architecte legislation connexe  ${nom_du_dossier}
    &{args_concepteur_paysagiste}=  Charger le json et créer le dictionnaire du concepteur-paysagiste  ${nom_du_dossier}

    &{args_autres_demandeurs} =  Create Dictionary

    ${length_delegataire}=  Get Length  ${args_delegataire}
    ${length_petitionnaire_autre}=  Get Length  ${args_petitionnaire_autre}
    ${length_petitionnaire_supp}=  Get Length  ${args_petitionnaire_supp}
    ${length_petitionnaire_supp1}=  Get Length  ${args_petitionnaire_supp1}
    ${length_architecte_legi_connexe}=  Get Length  ${args_architecte_legi_connexe}
    ${length_concepteur_paysagiste}=  Get Length  ${args_concepteur_paysagiste}
    Run Keyword If  ${length_delegataire}!=0  Set To Dictionary  ${args_autres_demandeurs}  delegataire=${args_delegataire}
    Run Keyword If  ${length_petitionnaire_autre}!=0  Set To Dictionary  ${args_autres_demandeurs}  proprietaire=${args_petitionnaire_autre}
    Run Keyword If  ${length_petitionnaire_supp}!=0  Set To Dictionary  ${args_autres_demandeurs}  petitionnaire2=${args_petitionnaire_supp}
    Run Keyword If  ${length_petitionnaire_supp1}!=0  Set To Dictionary  ${args_autres_demandeurs}  petitionnaire3=${args_petitionnaire_supp1}
    Run Keyword If  ${length_architecte_legi_connexe}!=0  Set To Dictionary  ${args_autres_demandeurs}  architecte_lc=${args_architecte_legi_connexe}
    Run Keyword If  ${length_concepteur_paysagiste}!=0  Set To Dictionary  ${args_autres_demandeurs}  paysagiste=${args_concepteur_paysagiste}

    ${di_ok}=  Ajouter la nouvelle demande  ${args_demande}  ${args_petitionnaire}  ${args_autres_demandeurs}

    &{donnees_techniques_values}=  Charger le json et créer le dictionnaire des données techniques  ${nom_du_dossier}
    &{donnees_techniques_terrain}=  Charger le json et créer le dictionnaire des données techniques de terrain  ${nom_du_dossier}
    &{donnees_techniques_architecte}=  Charger le json et créer le dictionnaire de l'architecte  ${nom_du_dossier}

    Plat'AU Saisir les données techniques du DI  ${di_ok}  ${donnees_techniques_values}  ${donnees_techniques_terrain}  ${donnees_techniques_architecte}
    Plat'AU Ajouter l'identifiant du dossier dans le CSV  ${nom_du_dossier}  ${di_ok}

    [Return]    ${di_ok}


Plat'Au Ajouter les pièces depuis le dossiers
    [Documentation]  On récupère un dictionnaire contenant le nom de la pièce en champs et le type de pièce en valeur
    ...  pour ensuite les ajouter dans un dossier en particulier à la date du jour.
    [Arguments]  ${nom_du_dossier}  ${dict_pieces}  ${nature_piece}=Initiale  ${date_creation}=${date_ddmmyyyy}

    # On récupère les pièces
    ${items_pieces}=  Get Dictionary Items  ${dict_pieces}
    #
    :FOR    ${uid_upload}    ${type_piece}    IN    @{items_pieces}
    \  &{document_numerise_values}=  Create Dictionary
    \  Set To Dictionary  ${document_numerise_values}  uid_upload=${uid_upload}
    \  Set To Dictionary  ${document_numerise_values}  date_creation=${date_creation}
    \  Set To Dictionary  ${document_numerise_values}  document_numerise_type=${type_piece}
    \  Set To Dictionary  ${document_numerise_values}  document_numerise_nature=${nature_piece}
    \  Ajouter une pièce depuis le dossier d'instruction  ${nom_du_dossier}  ${document_numerise_values}


Plat'AU Ajouter l'identifiant du dossier dans le CSV
    [Documentation]  Ajoute l'identifiant du dossier créé dans le fichier CSV
    ...  ce fichier CSV permet de réutiliser les identifiants des dossier dans les autres séquence
    [Arguments]  ${nom_du_dossier}  ${id_dossier}

    Append To File  binary_files/id_di_dossier.csv  ${nom_du_dossier};${id_dossier}\n

Plat'AU Récupérer l'identifiant du dossier dans le CSV
    [Documentation]  Récupère l'identifiant du dossier en fonction du nom passé en paramètre
    [Arguments]  ${nom_du_dossier}

    ${rc}    ${id_dossier}=  Run and Return RC And Output  grep -w "${nom_du_dossier};[A-Z]*\ [0-9]*\ [0-9]*\ [0-9]*" binary_files/id_di_dossier.csv | cut -d ";" -f 2

    [Return]    ${id_dossier}
