
*** Settings ***
Library                     Collections
Library                     CSVLibrary
Library                     SeleniumLibrary
Library                     String
# Library                     OperatingSystem
Suite Setup  Login to PISTE
Suite Teardown  Close browser


*** Variables ***
${BROWSER}  firefox


*** Test Cases ***

Create all applications from CSV file
    [Documentation]
    @{data}=    read csv file to associative  ${INPUT_FILE_PATH}
    Go To  https://developer.aife.economie.gouv.fr/apps
    FOR    ${collectivite}    IN    @{data}
        Log  ${collectivite}
        # ${app_exists}  Get WebElement  partial link:${collectivite["piste app"]}
        # Log  ${app_exists}
        Create PISTE application  ${collectivite["identifiant"]}  ${collectivite["libellé"]}  ${collectivite["piste app"]}  ${collectivite["siren"]}  ${collectivite["email"]}
    END

Store PISTE application infos
    @{input_data}=    read csv file to associative  ${INPUT_FILE_PATH}
    @{output_data}=  Create List
    FOR    ${collectivite}    IN    @{input_data}
        ${collectivite_infos}=  Copy Dictionary  ${collectivite}
        ${app_infos}=  Get PISTE application infos  ${collectivite["piste app"]}
        Set To Dictionary  ${collectivite_infos}  application UID=${app_infos[0]}  client_id=${app_infos[1]}  client_secret=${app_infos[2]}
        Log  ${collectivite_infos}
        Append To List  ${output_data}  ${collectivite_infos}
    END
    Log  ${output_data}
    ${fieldsname}  Get Dictionary Keys  ${input_data[0]}
    Append To List  ${fieldsname}  application UID  client_id  client_secret
    Csv File From Associative  ${OUTPUT_FILE_PATH}  ${output_data}  ${fieldsname}

*** Keywords ***

Create PISTE application
    [Arguments]    ${entity_id}    ${entity_label}    ${PISTE_app_name}    ${entity_siren}    ${entity_email}
    # Login to PISTE
    Go To  https://developer.aife.economie.gouv.fr/apps

    Click Element    //span[@id="createAppSingleManager"]
    Input Text    //input[@name="apidata[name]"]    ${PISTE_app_name}
    Input Text    //textarea[@name="apidata[description]"]    openADS / ${entity_label}
    Input Text    //input[@name="apidata[email]"]    ${entity_email}
    Input Text    //input[@name="apidata[NumberStructure]"]    ${entity_siren}
    Click Element    xpath=(//div[@class="form-group"])[8]
    Input Text    //input[@name="apidata[ContactName]"]    atReal

    # MTES_PREPROD
    # Click Element    xpath=(//input[@name="apidata[apis][]"])[1]
    # Click Element    xpath=(//input[@name="apidata[apis][]"])[4]

    # MTES
    Click Element    xpath=(//input[@name="apidata[apis][]"])[4]
    Click Element    xpath=(//input[@name="apidata[apis][]"])[8]

    Click Element    //button[@id="application-save-and-list-button"]
    Sleep    5


Go to application
    [Arguments]    ${entity_id}
    # Login to PISTE
    Go To  https://developer.aife.economie.gouv.fr/apps
    Click Link    partial link:openads_${entity_id}

Get PISTE application infos
    [Documentation]    ${entity_id}
    [Arguments]    ${PISTE_app_name}

    Go To  https://developer.aife.economie.gouv.fr/apps
    Click Link    partial link:${PISTE_app_name}

    ${app_url}=    Get Location
    ${base_url}=  Split String  ${app_url}  ?
    ${splitted_url}=  Split String From Right  ${base_url[0]}  /

    ${client_id}=  Get Text  //table[@id="oauth-clients-table"]/*/tr[1]/td[1]
    Click Element    //span[@class="show-oauth-secret-btn"]
    Sleep  2
    ${client_secret}=  Get Text  css:p#oauth-secret-key

    [Return]  ${splitted_url[-1]}  ${client_id}  ${client_secret}

Get PISTE application UID
    [Arguments]    ${PISTE_app_name}

    Go To  https://developer.aife.economie.gouv.fr/apps
    Click Link    partial link:${PISTE_app_name}
    ${app_url}=    Get Location
    ${base_url}=  Split String  ${app_url}  ?
    ${splitted_url}=  Split String From Right  ${base_url[0]}  /

    [Return]  ${splitted_url[-1]}

Get PISTE application credentials
    [Arguments]    ${PISTE_app_name}

    Go To  https://developer.aife.economie.gouv.fr/apps
    Click Link    partial link:${PISTE_app_name}
    ${client_id}=  Get Text  //table[@id="oauth-clients-table"]/*/tr[1]/td[1]
    Click Element    //span[@class="show-oauth-secret-btn"]
    Sleep  2
    ${client_secret}=  Get Text  css:p#oauth-secret-key

    [Return]  ${client_id}  ${client_secret}

Login to PISTE
    Open Browser    https://developer.aife.economie.gouv.fr/se-connecter    ${BROWSER}
    ${PISTE_LOGIN}=  Set variable  %{PISTE_login}
    ${PISTE_PASSWORD}=  Set variable  %{PISTE_password}
    Input Text    //input[@name="username"]    ${PISTE_LOGIN}
    Input Text    //input[@name="password"]    ${PISTE_PASSWORD}
    Click Element    //button[@id="btn-login"]

