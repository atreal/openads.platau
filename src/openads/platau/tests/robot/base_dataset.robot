*** Settings ***
Documentation  Plat'AU - Scénario de base : création, ajout de pièces, changement de compétence, décision

# On inclut les mots-clefs
Resource  resources/resources.robot
Resource  ./keywords_platau.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}pieces_pdf${/}
${OM_COLLECTIVITE}  Marseille
# Use the following line in you test case if you want use another Collectivite
# Set global variable  ${OM_COLLECTIVITE}  Autre collectivite

*** Test Cases ***

Ajouter le dossier basé sur le cerfa DPXXXC1S20ZZ102
    Depuis la page d'accueil  admin  admin
    ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPXXXC1S20ZZ102

    ${pieces} =  Create Dictionary
    ...  DP-DP4-DPXXXC1S20ZZ102.pdf=Plan des façades et des toitures...
    ...  DP-DP5-DPXXXC1S20ZZ102.pdf=Représentation de l’aspect extérieur de la construction faisant apparaître les modifications projet...
    ...  DP-DP6-DPXXXC1S20ZZ102.pdf=Document graphique permettant d’apprécier l’insertion du projet de construction dans son environnem...
    ...  DPXXXC1S20ZZ102-V7.pdf=Formulaire DP 13404 07...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

    #
    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT
    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Retour Autorité Commune


    ${inst_decision}=  Ajouter une instruction au DI et la finaliser  ${NUMERO_DOSSIER}  DECISION DE NON OPPOSITION sans réserves (ACCORD) - DP  01/02/2020
    &{args_instruction} =  Create Dictionary
    ...  date_finalisation_courrier=01/02/2018
    ...  date_envoi_signature=01/02/2018
    ...  date_retour_signature=01/02/2018
    ...  date_retour_rar=01/02/2018
   Modifier le suivi des dates  ${NUMERO_DOSSIER}  DECISION DE NON OPPOSITION sans réserves (ACCORD) - DP  ${args_instruction}


Ajouter le dossier basé sur le cerfa PCMI-PCXXXC2U18YY002
    Depuis la page d'accueil  admin  admin
    ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U18YY002

    ${pieces} =  Create Dictionary
    ...  PCMI-A7-PCXXXC2U18YY002.pdf=Descriptif des moyens mis en oeuvre pour éviter toute atteinte aux parties conservées du bâtiment...
    ...  PCMI-PCXXXC2U18YY002-V7.pdf=Formulaire PCMI 13406 07...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT

    ${pieces} =  Create Dictionary
    ...  PCMI-PCMI1-PCXXXC2U18YY002.pdf=Plan de situation du terrain...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}  Complémentaire  15/01/2018

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en COMMUNE pour ETAT

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  ATTENTE DECISION PREFET  01/02/2018
    ${inst_decision}=  Ajouter une instruction au DI et la finaliser  ${NUMERO_DOSSIER}  ARRETE D'ACCORD PREFET (initial)  01/02/2018
    &{args_instruction} =  Create Dictionary
    ...  date_finalisation_courrier=01/02/2018
    ...  date_envoi_signature=01/02/2018
    ...  date_retour_signature=01/02/2018
    ...  date_retour_rar=01/02/2018
    Modifier le suivi des dates  ${NUMERO_DOSSIER}  ARRETE D'ACCORD PREFET (initial)  ${args_instruction}

