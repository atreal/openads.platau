*** Settings ***
Documentation  Plat'AU - Séquence 0 de la vie d'un dossier - Dossiers (Versement - Dépot - Qualification).

# On inclut les mots-clefs
Resource  resources/resources.robot
Resource  ./keywords_platau.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}pieces_pdf${/}


*** Test Cases ***

# Isolation du contexte pour ces dossier
#     Depuis la page d'accueil  admin  admin
#     # Isolation du contexte Laon
#     &{isolation_values_mars} =  Create Dictionary
#     ...  om_collectivite_libelle=Laon
#     ...  departement=002
#     ...  commune=408
#     ...  insee=02408
#     ...  direction_code=X
#     ...  direction_libelle=Direction de PlatAu Laon
#     ...  direction_chef=Chef
#     ...  division_code=Y
#     ...  division_libelle=Division Y
#     ...  division_chef=Chef
#     ...  guichet_om_utilisateur_nom=Victor Michel
#     ...  guichet_om_utilisateur_email=lmichel@openads-test.fr
#     ...  guichet_om_utilisateur_login=lmichel
#     ...  guichet_om_utilisateur_pwd=lmichel
#     ...  instr_om_utilisateur_nom=Hugues Orange
#     ...  instr_om_utilisateur_email=horange@openads-test.fr
#     ...  instr_om_utilisateur_login=horange
#     ...  instr_om_utilisateur_pwd=horange
#     ...  dossier_autorisation_type_detaille=DECLARATION PREALABLE
#     Plat'AU Isolation d'un contexte  ${isolation_values_mars}
#     Ajouter l'utilisateur depuis le menu  Pierre Jacques  pjacques@openads-test.fr  pjacques  pjacques  CELLULE SUIVI  ${isolation_values_mars.om_collectivite_libelle}
#     Ajouter l'utilisateur depuis le menu  Jean Valjean  jvaljean@openads-test.fr  jvaljean  jvaljean  QUALIFICATEUR  ${isolation_values_mars.om_collectivite_libelle}

#     &{args_affectation} =  Create Dictionary
#     ...  instructeur=${isolation_values_mars.instr_om_utilisateur_nom} (${isolation_values_mars.division_code})
#     ...  om_collectivite=${isolation_values_mars.om_collectivite_libelle}
#     ...  dossier_autorisation_type_detaille=PERMIS DE CONSTRUIRE MAISON INDIVIDUELLE
#     Ajouter l'affectation depuis le menu  ${args_affectation}

##########
# BATCH 01

# Phase 1.1 cerfa échantillon DPMI-DPXXXC2U18YY003
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPMI-DPXXXC2U18YY003

#     ${pieces} =  Create Dictionary
#     ...  DPMI-DP1-DPXXXC2U18YY003.pdf=Plan de situation du terrain...
#     ...  DPMI-DPXXXC2U18YY003-V7.pdf=Formulaire DPMI 13703 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon DPMI-DPXXXC2U18YY005
#     Depuis la page d'accueil  admin  admin

#     &{dict_petitionnaire_supp1}=  Create Dictionary
#     ...  om_collectivite=Laon
#     ...  qualite=particulier
#     ...  particulier_civilite=Madame
#     ...  particulier_nom=César
#     ...  particulier_prenom=Juliette
#     ...  particulier_commune_naissance=Barfleur
#     ...  particulier_departement_naissance=014
#     ...  particulier_pays_naissance=France
#     ...  particulier_date_naissance=09/08/1980
#     ...  numero=4589
#     ...  voie=Carrefour dela liberté
#     ...  localite=Honfleur
#     ...  lieu_dit=Bord-de-mer
#     ...  code_postal=92600
#     ...  bp=445
#     ...  cedex=99

#     &{dict_petitionnaire_supp2}=  Create Dictionary
#     ...  om_collectivite=Laon
#     ...  qualite=particulier
#     ...  particulier_civilite=Monsieur
#     ...  particulier_nom=César
#     ...  particulier_prenom=Antoine
#     ...  particulier_commune_naissance=Paris
#     ...  particulier_departement_naissance=075
#     ...  particulier_pays_naissance=France
#     ...  particulier_date_naissance=15/06/1978
#     ...  numero=78
#     ...  voie=Rue du Colisée
#     ...  localite=Rome
#     ...  code_postal=89600
#     ...  pays=Italie
#     ...  notification=true
#     ...  courriel=antoine@orange.fr

#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPMI-DPXXXC2U18YY005  ${dict_petitionnaire_supp1}  ${dict_petitionnaire_supp2}

#     ${pieces} =  Create Dictionary
#     ...  DPMI-DPXXXC2U18YY005-V7.pdf=Formulaire DPMI 13703 07...
#     ...  DPMI-DPXXXC2U18YY005-petitionnaire supp.pdf=Autre à préciser...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon DPMI-DPXXXC2U18YY008
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPMI-DPXXXC2U18YY008

#     ${pieces} =  Create Dictionary
#     ...  DPMI-DP12-1-DPXXXC2U18YY008.pdf=Document prévu par l’article R. 111-21 du code de la construction et de l’habitation attestant que ...
#     ...  DPMI-DPXXXC2U18YY008-V7.pdf=Formulaire DPMI 13703 07...

#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon DPMI-DPXXXC2U18YY012
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPMI-DPXXXC2U18YY012

#     ${pieces} =  Create Dictionary
#     ...  DPMI-DPXXXC2U18YY012-V7.pdf=Formulaire DPMI 13703 07...
#     ...  DPMI-F8-DPXXXC2U18YY012.pdf=Justification de la date de la destruction, de la démolition ou du sinistre...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY001
#     Depuis la page d'accueil  admin  admin

#     # Création du dictionnaire pour le pétitionnaire
#     &{dict_petitionnaire_supp1}=  Create Dictionary
#     ...  om_collectivite=Laon
#     ...  qualite=personne morale
#     ...  personne_morale_denomination=Transport Charlebourg
#     ...  personne_morale_raison_sociale=Transport Charlebourg
#     ...  personne_morale_siret=98765434567800
#     ...  personne_morale_categorie_juridique=SA
#     ...  personne_morale_civilite=Madame
#     ...  personne_morale_nom=Sheeran
#     ...  personne_morale_prenom=Julie
#     ...  numero=45
#     ...  voie=rue du double
#     ...  localite=Paris
#     ...  code_postal=75013

#     &{dict_petitionnaire_supp2}=  Create Dictionary
#     ...  om_collectivite=Laon
#     ...  qualite=personne morale
#     ...  personne_morale_denomination=Transport Charlebourg
#     ...  personne_morale_raison_sociale=Transport Charlebourg
#     ...  personne_morale_siret=45678098765432
#     ...  personne_morale_categorie_juridique=SA
#     ...  personne_morale_civilite=Monsieur
#     ...  personne_morale_nom=Pelletier
#     ...  personne_morale_prenom=Gilbert
#     ...  numero=67
#     ...  voie=boulevard du bois
#     ...  localite=Levallois
#     ...  code_postal=92600

#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U18YY001  ${dict_petitionnaire_supp1}  ${dict_petitionnaire_supp2}

#     ${pieces} =  Create Dictionary
#     ...  PCMI-PCXXXC2U18YY001-V7.pdf=Formulaire PCMI 13406 07...
#     ...  PCXXXC2U18YY001-petitionnaire supp.pdf=Autre à préciser...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT

# END BATCH_01
##############

##########
# BATCH 02

# Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY002
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U18YY002

#     ${pieces} =  Create Dictionary
#     ...  PCMI-A7-PCXXXC2U18YY002.pdf=Descriptif des moyens mis en oeuvre pour éviter toute atteinte aux parties conservées du bâtiment...
#     ...  PCMI-PCXXXC2U18YY002-V7.pdf=Formulaire PCMI 13406 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY003
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U18YY003

#     ${pieces} =  Create Dictionary
#     ...  PCMI-A8-PCXXXC2U18YY003.pdf=Descriptif des moyens mis en oeuvre pour éviter toute atteinte au patrimoine protégé...
#     ...  PCMI-PCXXXC2U18YY003-V7.pdf=Formulaire PCMI 13406 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY007
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U18YY007

#     ${pieces} =  Create Dictionary
#     ...  PCMI24-PCXXXC2U18YY007.pdf=Copie du contrat ou de la décision judiciaire relative à l’institution de ces servitudes...
#     ...  PCMI-PCXXXC2U18YY007-V7.pdf=Formulaire PCMI 13406 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon PCMI-PCXXXC2U20YY004
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  PCMI-PCXXXC2U20YY004

#     ${pieces} =  Create Dictionary
#     ...  PCMI19-PCXXXC2U20YY004.pdf=Pièces à joindre à une demande de permis de démolir, selon l’annexe ci-jointe...
#     ...  PCMI-PCXXXC2U20YY004-V7.pdf=Formulaire PCMI 13406 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


# Phase 1.1 cerfa échantillon DPXXXC2U18YY104
#     Depuis la page d'accueil  admin  admin
#     ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPXXXC2U18YY104

#     ${pieces} =  Create Dictionary
#     ...  DP-DP9-DPXXXC2U18YY104.pdf=Plan sommaire des lieux indiquant, le cas échéant, les bâtiments de toute nature existant sur le te...
#     ...  DPXXXC2U18YY104-V7.pdf=Formulaire DP 13404 07...
#     Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

#     Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT

# END BATCH_02
##############

##########
# BATCH 03

Phase 1.1 cerfa échantillon DPXXXC2U18YY105
    Depuis la page d'accueil  admin  admin
    ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPXXXC2U18YY105

    ${pieces} =  Create Dictionary
    ...  DP-DP11-DPXXXC2U18YY105.pdf=Notice faisant apparaître les matériaux utilisés et les modalités d’exécution des travaux...
    ...  DPXXXC2U18YY105-V7.pdf=Formulaire DP 13404 07...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


Phase 1.1 cerfa échantillon DPXXXC2U18YY108
    Depuis la page d'accueil  admin  admin
    ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPXXXC2U18YY108

    ${pieces} =  Create Dictionary
    ...  DP-DP17-DPXXXC2U18YY108.pdf=Document graphique faisant apparaître l’état initial et l’état futur de chacun des éléments ou part...
    ...  DPXXXC2U18YY108-V7.pdf=Formulaire DP 13404 07...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT


Phase 1.1 cerfa échantillon DPXXXC2U18YY109
    Depuis la page d'accueil  admin  admin
    ${NUMERO_DOSSIER} =  Plat'Au Ajouter le dossier d'instruction  DPXXXC2U18YY109

    ${pieces} =  Create Dictionary
    ...  DP-F1-DPXXXC2U18YY109.pdf=Certificat fourni par le lotisseur...
    ...  DPXXXC2U18YY109-V7.pdf=Formulaire DP 13404 07...
    Plat'Au Ajouter les pièces depuis le dossiers  ${NUMERO_DOSSIER}  ${pieces}

    Ajouter une instruction au DI  ${NUMERO_DOSSIER}  Changement autorité en ETAT

# END BATCH_03
##############