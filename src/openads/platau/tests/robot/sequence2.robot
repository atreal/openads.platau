*** Settings ***
Documentation  Plat'AU - Séquence 2 de la vie d'un dossier - Incomplétude.

# On inclut les mots-clefs
Resource  resources/resources.robot
Resource  ./keywords_platau.robot

# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}pieces_pdf${/}

*** Test Cases ***

Phase 1.1 cerfa échantillon DPXXXC1S20ZZ103
    Depuis la page d'accueil  admin  admin

    ${DPXXXC1S20ZZ103} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  DPXXXC1S20ZZ103

    ${pieces} =  Create Dictionary
    ...  DP-DP1-DPXXXC1S20ZZ103.pdf=Plan de situation du terrain...
    Plat'Au Ajouter les pièces depuis le dossiers  ${DPXXXC1S20ZZ103}  ${pieces}  Complémentaire  15/01/2020

Phase 1.1 cerfa échantillon DPXXXC2U18YY104
    Depuis la page d'accueil  admin  admin
    ${DPXXXC2U18YY104} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  DPXXXC2U18YY104

    ${pieces} =  Create Dictionary
    ...  DP-DP10-1-DPXXXC2U18YY104-V2.pdf=Plan de masse coté dans les 3 dimensions...
    Plat'Au Ajouter les pièces depuis le dossiers  ${DPXXXC2U18YY104}  ${pieces}  Complémentaire  15/01/2018

Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY001
    Depuis la page d'accueil  admin  admin

    ${PCMI-PCXXXC2U18YY001} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U18YY001

    ${pieces} =  Create Dictionary
    ...  PCMI-PCMI1-PCXXXC2U18YY001.pdf=Plan de situation du terrain...
    Plat'Au Ajouter les pièces depuis le dossiers  ${PCMI-PCXXXC2U18YY001}  ${pieces}  Complémentaire  15/01/2018

Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY002
    Depuis la page d'accueil  admin  admin
    ${PCMI-PCXXXC2U18YY002} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U18YY002

    ${pieces} =  Create Dictionary
    ...  PCMI-PCMI1-PCXXXC2U18YY002.pdf=Plan de situation du terrain...
    Plat'Au Ajouter les pièces depuis le dossiers  ${PCMI-PCXXXC2U18YY002}  ${pieces}  Complémentaire  15/01/2018

Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY003
    Depuis la page d'accueil  admin  admin
    ${PCMI-PCXXXC2U18YY003} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U18YY003

    ${pieces} =  Create Dictionary
    ...  PCMI18-PCXXXC2U18YY003-V2.pdf=Justification du dépôt de la demande de permis de démolir...
    Plat'Au Ajouter les pièces depuis le dossiers  ${PCMI-PCXXXC2U18YY003}  ${pieces}  Complémentaire  15/01/2018

Phase 1.1 cerfa échantillon PCMI-PCXXXC2U20YY004
    Depuis la page d'accueil  admin  admin
    ${PCMI-PCXXXC2U20YY004} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U20YY004

    ${pieces} =  Create Dictionary
    ...  PCMI19-PCXXXC2U20YY004-V2.pdf=Pièces à joindre à une demande de permis de démolir, selon l’annexe ci-jointe...
    ...  PCMI1-PCXXXC2U20YY004.pdf=Plan de situation du terrain...
    ...  PCMI20-PCXXXC2U20YY004-V2.pdf=Accord du gestionnaire du domaine pour engager la procédure d’autorisation d’occupation temporaire ...
    ...  PCMI21-PCXXXC2U20YY004-V1.pdf=Notice faisant apparaître les matériaux utilisés et les modalités d’exécution des travaux...
    # ...  PCMI21-PCXXXC2U20YY004-V1.odt.pdf=Notice faisant apparaître les matériaux utilisés et les modalités d’exécution des travaux...
    ...  PCMI21-PCXXXC2U20YY004-V2.pdf=Notice faisant apparaître les matériaux utilisés et les modalités d’exécution des travaux...
    ...  PCMI3-PCXXXC2U20YY004.pdf=Plan en coupe du terrain et de la construction...
    ...  PCMI4-PCXXXC2U20YY004.pdf=Notice décrivant le terrain et présentant le projet...
    ...  PCMI5-PCXXXC2U20YY004.pdf=Plan des façades et des toitures...
    ...  PCMI6-PCXXXC2U20YY004.pdf=Document graphique permettant d’apprécier l’insertion du projet de construction dans son environnem...
    ...  PCMI7-PCXXXC2U20YY004.pdf=Photographie permettant de situer le terrain dans l’environnement proche...


    Plat'Au Ajouter les pièces depuis le dossiers  ${PCMI-PCXXXC2U20YY004}  ${pieces}  Complémentaire  15/01/2020

Phase 1.1 cerfa échantillon PCMI-PCXXXC2U18YY007
    Depuis la page d'accueil  admin  admin
    ${PCMI-PCXXXC2U18YY007} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U18YY007

    ${pieces} =  Create Dictionary
    ...  PCMI-PCMI1-PCXXXC2U18YY007.pdf=Plan de situation du terrain...
    ...  PCMI-PCMI2-PCXXXC2U18YY007.pdf=Plan de masse des constructions à édifier ou à modifier...
    Plat'Au Ajouter les pièces depuis le dossiers  ${PCMI-PCXXXC2U18YY007}  ${pieces}  Complémentaire  15/01/2018

