*** Settings ***
Documentation  Plat'AU - Séquence 3 de la vie d'un dossier - Décision.

# On inclut les mots-clefs
Resource  resources/resources.robot
Resource  ./keywords_platau.robot

# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}pieces_pdf${/}

*** Test Cases ***
Phase 1.1 cerfa échantillon PCMI-PCXXXC2U20YY004
    Depuis la page d'accueil  admin  admin
    ${PCMI-PCXXXC2U20YY004} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  PCMI-PCXXXC2U20YY004

    ${inst_decision}=  Ajouter une instruction au DI et la finaliser  ${PCMI-PCXXXC2U20YY004}  ARRETE D'ACCORD (sans réserve)  01/02/2020
    &{args_instruction} =  Create Dictionary
    ...  date_finalisation_courrier=01/02/2018
    ...  date_envoi_signature=01/02/2018
    ...  date_retour_signature=01/02/2018
    ...  date_retour_rar=01/02/2018
   Modifier le suivi des dates  ${PCMI-PCXXXC2U20YY004}  ARRETE D'ACCORD (sans réserve)  ${args_instruction}


Phase 1.1 cerfa échantillon DPMI-DPXXXC2U18YY008
    Depuis la page d'accueil  admin  admin
    ${DPMI-DPXXXC2U18YY008} =  Plat'AU Récupérer l'identifiant du dossier dans le CSV  DPMI-DPXXXC2U18YY008

    Ajouter une instruction au DI  ${DPMI-DPXXXC2U18YY008}   Changement autorité en COMMUNE pour ETAT

    Ajouter une instruction au DI  ${DPMI-DPXXXC2U18YY008}  ATTENTE DECISION PREFET  01/02/2018
    ${inst_decision}=  Ajouter une instruction au DI et la finaliser  ${DPMI-DPXXXC2U18YY008}  ARRETE D'ACCORD PREFET (initial)  01/02/2018
    &{args_instruction} =  Create Dictionary
    ...  date_finalisation_courrier=01/02/2018
    ...  date_envoi_signature=01/02/2018
    ...  date_retour_signature=01/02/2018
    ...  date_retour_rar=01/02/2018
    Modifier le suivi des dates  ${DPMI-DPXXXC2U18YY008}  ARRETE D'ACCORD PREFET (initial)  ${args_instruction}


Phase 1.1 repasser les dossiers en compétence Maire pour État
    Depuis la page d'accueil  admin  admin

    @{dossiers} =  Create List
    ...  DP 013001 20 00017
    ...  DP 013055 18 00020
    ...  PC 013055 18 00011
    ...  PC 013055 18 00012
    ...  PC 013055 18 00015
    ...  DP 013001 20 00016
    ...  DP 013001 20 00010
    ...  DP 013055 18 00010

    :FOR  ${dossier}  IN  @{dossiers}
    \  Ajouter une instruction au DI  ${dossier}  Changement autorité en COMMUNE pour ETAT


Phase 1.1 cerfa échantillon application des décisions tacites
    Depuis la page d'accueil  admin  admin
    ${json} =  Set Variable  { "module":"instruction"}
    Vérifier le code retour du web service et retourner son message  Post  maintenance  ${json}  200