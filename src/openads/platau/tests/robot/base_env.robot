*** Settings ***
Documentation  Plat'AU - Environnement de base : collectivité, instructeurs, etc

# On inclut les mots-clefs
Resource  resources/resources.robot
Resource  ./keywords_platau.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Variables ***
${OM_COLLECTIVITE}  Marseille
# Use the following line in you test case if you want use another Collectivite
# Set global variable  ${OM_COLLECTIVITE}  Autre collectivite

*** Test Cases ***

Création d'une collectivité et d'un instructeur
    Depuis la page d'accueil  admin  admin
    # Isolation du contexte Marseille
    &{isolation_values_mars} =  Create Dictionary
    ...  om_collectivite_libelle=Marseille
    ...  departement=013
    ...  commune=055
    ...  insee=13055
    ...  direction_code=M
    ...  direction_libelle=Direction de PlatAu Marseille
    ...  direction_chef=Chef
    ...  division_code=Z
    ...  division_libelle=Division Z
    ...  division_chef=Chef
    ...  guichet_om_utilisateur_nom=Kalo Loyo
    ...  guichet_om_utilisateur_email=loyo@openads-test.fr
    ...  guichet_om_utilisateur_login=loyo
    ...  guichet_om_utilisateur_pwd=loyo
    ...  instr_om_utilisateur_nom=Baton Rouge
    ...  instr_om_utilisateur_email=brouge@openads-test.fr
    ...  instr_om_utilisateur_login=brouge
    ...  instr_om_utilisateur_pwd=brouge
    ...  dossier_autorisation_type_detaille=DECLARATION PREALABLE
    # Plat'AU Isolation d'un contexte  ${isolation_values_mars}
    Ajouter l'utilisateur depuis le menu  Rabi Labi  rlabi@openads-test.fr  rlabi  rlabi  CELLULE SUIVI  ${isolation_values_mars.om_collectivite_libelle}
    Ajouter l'utilisateur depuis le menu  Moineau Leclerc  mleclerc@openads-test.fr  mleclerc  mleclerc  QUALIFICATEUR  ${isolation_values_mars.om_collectivite_libelle}

    &{args_affectation} =  Create Dictionary
    ...  instructeur=${isolation_values_mars.instr_om_utilisateur_nom} (${isolation_values_mars.division_code})
    ...  om_collectivite=${isolation_values_mars.om_collectivite_libelle}
    ...  dossier_autorisation_type_detaille=PERMIS DE CONSTRUIRE MAISON INDIVIDUELLE
    Ajouter l'affectation depuis le menu  ${args_affectation}
