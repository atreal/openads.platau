
*** Settings ***
Documentation  Elements OpenADS du Test d'Intégration 07 pièce en doubles

# On inclut les mots-clefs
Resource  resources/resources.robot
Library  random

# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Test Cases ***
Activation de la configuration service consulté
 # En tant qu'administrateur
    Depuis la page d'accueil  admin  admin

    # Activation du mode service consulté
    Activer le mode service consulté

    # Active option_dossier_commune
    &{param_dossier_commune} =  Create Dictionary
    ...  libelle=option_dossier_commune
    ...  valeur=true
    ...  om_collectivite=agglo
    Gerer le paramètre par WS  ${param_dossier_commune}
    
    # Active option_dossier_saisie_numero_complet
    # /!\ Cette option est nécessaire pour pouvoir avoir des dossiers de consultation
    # rattachée à un même dossier d'autorisation
    &{param_values} =  Create Dictionary
    ...  libelle=option_dossier_saisie_numero_complet
    ...  valeur=true
    ...  om_collectivite=agglo


Création du service UDAP 44
    # En tant qu'administrateur
    Depuis la page d'accueil  admin  admin

    Ajouter la collectivité depuis le menu  UDAP 44  mono

    # Crée le service 
    &{service} =  Create Dictionary
    ...  abrege=44
    ...  libelle=UDAP 44
    ...  edition=Consultation - Demande d'avis
    ...  om_collectivite=UDAP 44
    ...  service_type=openADS
    ...  generate_edition=true
    Ajouter le service depuis le listing  ${service}

    # Parametres pour l'UDAP 44,
    &{om_param} =  Create Dictionary
    ...  libelle=code_entite
    ...  valeur=U44
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=adresse_service
    ...  valeur=DRAC des Pays de la Loire, 1 rue Stanislas Baudry, 44000 Nantes
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=email_service
    ...  valeur=sdap.loire-atlantique@culture.gouv.fr
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=option_module_acteur
    ...  valeur=true
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=param_communes
    ...  valeur=44
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_acteur_service_consulte
    ...  valeur=71Z-Y9O-KWQ
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_acteur_autorite_competente_email
    ...  valeur=support+openads_v_AC@atreal.fr
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=service_type
    ...  valeur=UDAP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=ville_service
    ...  valeur=Nantes
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=libelle_service
    ...  valeur=Unité départementale de l'architecture et du Patrimoine de la Loire-Atlantique
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}

    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_DP
    ...  valeur=DPEP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_DPL
    ...  valeur=DPEP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_DPMI
    ...  valeur=DPEP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PI
    ...  valeur=PIEP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PC
    ...  valeur=PIEP
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PA
    ...  valeur=P
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
    &{om_param} =  Create Dictionary
    ...  libelle=platau_type_demande_initial_PD
    ...  valeur=P
    ...  om_collectivite=UDAP 44
    Gerer le paramètre par WS  ${om_param}
