# -*- coding: utf-8 -*-

import hashlib
import json
import os
import logging
import unittest
import requests
import tempfile
import time
import uuid

from base64 import b64decode, b64encode
from dataclasses import dataclass
from datetime import date, datetime, timedelta
from pypdf import PdfReader

from openads.platau.mapping import PLATAU_MAPPING, OPENADS_MAPPING
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import OAPConfig
from openads.platau.utils import generate_temporary_pdf


class UtilsTestCase(unittest.TestCase):
    """Test utils mathods and fucntions."""

    def setUp(self):
        """."""

    def test_00_generate_temporary_pdf(self):
        """."""
        filename = "DP0130552400023_DP1_1223.pdf"
        now = datetime.now().strftime("%m/%d/%Y à %H:%M:%S")
        tmp_file_path = generate_temporary_pdf(
            filename, f"Pièce {filename} supprimée le {now}"
        )

        # Let's dive into this wonderful PDF file and find some proof everything went perfectly
        reader = PdfReader(tmp_file_path)
        number_of_pages = len(reader.pages)
        # We sholud have one an only page
        self.assertEqual(number_of_pages, 1)

        page = reader.pages[0]
        text = page.extract_text()
        # Filename should be present in the text
        self.assertTrue(filename in text)
