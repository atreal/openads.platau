Process of 'Jalon 1' tests
==========================

Pre-processing
--------------

The pre-processing is composed by two bash scripts :

* extract_cerfa_convert_to_json.sh
* convert_all_files_into_pdf.sh

Step 1 - Extract pdf content and convert into json
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The script extract_cerfa_convert_to_json.sh extract cerfas datas with **poolbox**.
You have to run a local instance of **poolbox**. 

The script has to be run in the 'Jeu d'essai' directory. 

The path is : "Jeu d'essai Plat'AU v1.4.1/Phase 1 - Les dossiers et pièces initiales/Cerfa V6 ou Cerfa V7"

A directory named "results" will be created in  : "Jeu d'essai Plat'AU v1.4.1/Phase 1 - Les dossiers et pièces initiales/"

This directory will contain :

- A sub-directoy wich contain all cerfas turned into base64 file ;
- A sub-directoy wich contain json files returned by poolbox ;
- A sub-directoy named "final_files" wich contain all json used to create 'les dossiers d'instruction' in openADS.

To run the script :

    $ . <file_path>/extract_cerfa_convert_to_json.sh


Step 2 - Convert all attachments into pdf files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The script convert_all_files_into_pdf.sh convert all attachments into pdf files.
You have to install the **libreoffice** library and the **convert** library.

This script has to be runed in the 'Jeu d'essai' directory. The path is : "Jeu d'essai Plat'AU v1.4.1/Phase 1 - Les dossiers et pièces initiales/Cerfa V6 ou Cerfa V7"

The pdf files will be in the "results/pieces_pdf" directory. 

To run the script :

    $ . <file_path>/convert_all_files_into_pdf.sh


Phases of 'Jalon 1'
-------------------

All Phases has to be runed in the same app with a production param and with ".robot" files. 

Phase 1
^^^^^^^

The Phase 1 consist of 'saisie des dossiers', 'le dépôt des pièces' and 'la qualification de ces mêmes dossiers'.

The file sequence0.robot has to be runed with om-tests.

Before that we have to move our two directories (final_files and pieces_pdf) in the directory : "openads/tests/binary_files".

We have to copy sequence0.robot and keywords_platau.robot files into "openads/tests".

To run the file sequence0.robot we have to be in om-tests virtualenv.

Run the command in the directory "openads/tests" :

   $ om-tests -c runone -t sequence0.robot --noinit