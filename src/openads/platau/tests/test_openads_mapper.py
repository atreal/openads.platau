# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from datetime import datetime, timedelta
from pprint import PrettyPrinter


# from openads.platau.tasks import InputTask
# from bravado.exception import HTTPInternalServerError, HTTPBadRequest, HTTPUnauthorized

from openads.platau.tests.base_test import (
    BaseTestCase,
    REMOTE_URL,
    time_me,
    FakeArgs,
    SC_CONFIG_PATH,
)
from openads.platau.conductor import Conductor, OAPConfig
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.openads_mapper import ConsultationMapper

# TODO : use object config
ACTEURS = {
    "13055": {
        "commune": {
            "guichet_unique": "8XP-G2Z-2XP",
            "service_instructeur": "2XN-Z0D-VXJ",
            "autorite_competente": "YW8-DV2-3W3",
        },
        "etat": {
            "service_instructeur": "571-M6E-NXZ",
            "autorite_competente": "3Y1-LPM-4XP",
        },
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
    },
}

DP = {
    "DP": {
        "insee": "13055",
        "acteurs": {
            "guichet_unique": ACTEURS["13055"]["commune"]["guichet_unique"],
            "service_instructeur": ACTEURS["13055"]["commune"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            # "qualification_DI": 8,
            # "decision_DI": 15,
            "consultation": ACTEURS["culture"]["DEV-UDAP44"],
        },
        "external_uids": {
            "dossier_autorisation": "",
            "dossier": "",
            "pieces": {},
            "consultation": "71M-GPG-ER1",
            # "consultation": "",
        },
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    }
}
PC = {
    "PC": {
        "insee": "13055",
        "acteurs": {
            "guichet_unique": ACTEURS["13055"]["commune"]["guichet_unique"],
            "service_instructeur": ACTEURS["13055"]["commune"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            "qualification_DI": 8,
            # "decision_DI": 15,
            "consultation": ACTEURS["culture"]["DEV-UDAP44"],
        },
        "external_uids": {
            "dossier_autorisation": "",
            # "dossier": "71M-GP6-GZ1",
            "dossier": "",
            "pieces": {},
            "consultation": "",
            # "consultation": "71Z-Y9O-KWQ",
        },
        "local_number": "",
        "type": "PC",
        "competence": "commune",
    },
}
PA = {
    "PA": {
        "insee": "13055",
        "acteurs": {
            "guichet_unique": ACTEURS["13055"]["commune"]["guichet_unique"],
            "service_instructeur": ACTEURS["13055"]["commune"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            "qualification_DI": 8,
            # "decision_DI": 15,
            "consultation": ACTEURS["culture"]["DEV-UDAP44"],
        },
        "external_uids": {
            "dossier_autorisation": "",
            # "dossier": "71M-GP6-GZ1",
            "dossier": "",
            "pieces": {},
            "consultation": "",
            # "consultation": "71Z-Y9O-KWQ",
        },
        "local_number": "",
        "type": "PA",
        "competence": "commune",
    },
}


class OpenADSMapperTest(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        self.conf = OAPConfig(SC_CONFIG_PATH)
        self.actor = ACTEURS["culture"]["DEV-UDAP44"]
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)
        self.openads_url = self.conf.openadsapi_url
        self.openads_auth = self.conf.openadsapi_config["auth"]
        self.kwargs = FakeArgs()

    def test_OAMT_build_dataset(self):
        """."""
        self.dummy_backend = PC.copy()
        self._build_dataset()

    def _test_ConsultationMapper(self, type_dossier) -> tuple:
        """XXX : write some assertion to validate general mapping"""
        # Do no create dataset if not needed
        dossier_uid = self.dummy_backend[type_dossier]["external_uids"]["dossier"]
        consultation_uid = self.dummy_backend[type_dossier]["external_uids"][
            "consultation"
        ]
        if not consultation_uid:
            self._build_dataset()
            event = self.app.get_notifications(self.app.calling_actor, raw=False)[0]
            print(
                f"Consultation UID : {event.target_object}\nDossier UID : {event.dossier_uid}"
            )
            dossier_uid = event.dossier_uid
            consultation_uid = event.target_object

        result = self.app.get_consultation_by_uid(consultation_uid, dossier_uid)
        service_consultant = self.app.get_acteur_by_uid(
            result.dossier.consultations[0].idServiceConsultant
        )
        infofiscales = self.app.get_infofiscales(dossier_uid)
        mapped = ConsultationMapper(
            result, service_consultant=service_consultant, infofiscales=infofiscales
        ).render()
        # pp = PrettyPrinter()
        # pp.pprint(result._as_dict())
        # pp.pprint(mapped)
        # print(
        #     f"Donnees techninques : mapped {len(mapped['donnees_techniques'])} properties"
        # )
        # print(f"Dossier : mapped {len(mapped['dossier'])} properties")
        openads_task = self._get_task(
            json_path=f"json_tasks/{type_dossier}_creation_DI.json",
        )

        return (openads_task, mapped)

    def test_ConsultationMapperDP(self):
        """XXX : write some assertion to validate mapping"""
        self.dummy_backend = DP.copy()
        self._test_ConsultationMapper("DP")

    def test_ConsultationMapperPC(self):
        """XXX : write some assertion to validate mapping"""
        self.dummy_backend = PC.copy()
        self._test_ConsultationMapper("PC")

    def test_ConsultationMapperPA(self):
        """Test specifique data mapping on PA.

        There are 2 Demandeurs in openADS : one Personne Morale (PM) as Petitionnaire Principal, one Particulier (P)
        """
        self.dummy_backend = PA.copy()
        openads_task, mapped = self._test_ConsultationMapper("PA")

        openads_demandeurs = [
            value for dummy, value in openads_task["demandeur"].items()
        ]

        ## Demandeurs
        # 1 PM principal + 1 P Representant + 1 P in Plat'AU should be 1 PM + Represente + 1 P in openADS
        self.assertEqual(len(mapped["demandeur"]), len(openads_demandeurs) + 1)

        # Let's check some PM properties
        openads_personne_morale = [
            d for d in openads_demandeurs if d["qualite"] == "personne_morale"
        ][0]
        platau_personne_morale = [
            d for d in mapped["demandeur"] if d["qualite"] == "personne_morale"
        ][0]
        # PM Adresse & Coordonnees should be correctly set
        for prop in (
            "courriel",
            "voie",
            "code_postal",
            "localite",
            "pays",
            "telephone_fixe",
        ):
            self.assertEqual(
                openads_personne_morale[prop], platau_personne_morale[prop]
            )


def _dict_equals(d1, d2):
    """."""
    s1 = set([f"{k}::{v}" for k, v in d1])
    s2 = set([f"{k}::{v}" for k, v in d2])

    return s1 == s2
