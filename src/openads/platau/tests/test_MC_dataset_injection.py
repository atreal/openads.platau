# -*- coding: utf-8 -*-

import csv
import base64
import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from datetime import datetime, timedelta
from flatten_json import flatten
from pprint import PrettyPrinter


from openads.platau.openads_driver import OpenADSDriver
from openads.platau.tests.base_test import (
    BaseTestCase,
    REMOTE_URL,
    time_me,
    get_file_data,
    SC_CONFIG_PATH,
)
from openads.platau.conductor import OAPConfig
from openads.platau.mapper import PlatAUMapper
from openads.platau.tasks import InputTask

from bravado.exception import HTTPInternalServerError, HTTPBadRequest, HTTPUnauthorized

FILE_PATH = os.getenv(
    "commune_by_service_path",
    f"{os.path.dirname(os.path.abspath(__file__))}/binary_files/commune_by_service.csv",
)

PIECE_PATHS = [
    "binary_files/DP_116.pdf",
    # "binary_files/DP_117.pdf"
]


class PlatAUMapperTest(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        self.conf = OAPConfig(SC_CONFIG_PATH)
        self.openads = OpenADSDriver(**self.conf.openadsapi_config)

    def test_inject_Consultation(self):
        """."""
        consultation = self._get_task(
            json_path="json_tasks/openads_injection/mc_create_DI_for_consultation.json"
        )
        piece = self._get_task(
            json_path="json_tasks/openads_injection/mc_add_piece.json"
        )
        today = date.today().strftime("%Y-%m-%d")
        year_2_digits = date.today().strftime("%y")
        with open(FILE_PATH, "r") as f:
            reader = csv.reader(f)
            #  get rid of the key's definition line with `reader.line_num > 1`
            services = {
                row[0]: dict(
                    label=row[1],
                    insee=row[2],
                    commune=row[3],
                    actor_uid=row[4],
                    parcelle=row[5],
                )
                for row in reader
                if reader.line_num > 1
            }

        # for k, v in services.items():
        #     print(f"{k},{v['label']},{str(uuid.uuid1())}")

        for org, service in services.items():

            # XXX search_actors encountered a 15 item limitations
            # Source will be the CSV file only

            # for actor in self.openads.search_actors(role="service_consulte"):
            # actors = self.openads.search_actors(
            #     role="service_consulte", organization=org
            # )

            # if len(actors) != 1:
            #     print(f"\nService {org} unknown. Aborting injection.")
            #     continue
            # else:
            #     actor = actors[0]
            # try:
            #     service = services[actor.organization]
            # except KeyError:
            #     print(f"\nService {actor.organization} unknown. Aborting injection.")
            #     continue

            uid = str(uuid.uuid1())
            dossier_uid = uid
            local_number = (
                f"DP0{service['insee']}{year_2_digits}{uid.split('-')[0][:-3]}"
            )
            dossier_consultation_uid = str(uuid.uuid1())

            # replace Consultation vars with CT ones
            consultation["dossier"]["acteur"] = service["actor_uid"]
            consultation["dossier"]["date_demande"] = today
            consultation["dossier"]["date_depot_mairie"] = today
            consultation["dossier"]["insee"] = service["insee"]
            consultation["dossier"]["terrain_adresse_localite"] = service["commune"]
            consultation["dossier"]["terrain_adresse_code_postal"] = service["insee"]
            consultation["dossier"]["dossier"] = local_number
            consultation["dossier"]["terrain_references_cadastrales"] = (
                service["parcelle"] + ";"
            )

            consultation["external_uids"]["acteur"] = service["actor_uid"]
            consultation["external_uids"]["dossier"] = dossier_uid
            consultation["external_uids"][
                "dossier_consultation"
            ] = dossier_consultation_uid

            consultation["consultation"]["date_consultation"] = today
            consultation["consultation"]["date_emission"] = today
            consultation["consultation"]["date_premiere_consultation"] = today
            consultation["consultation"]["date_production_notification"] = today
            consultation["consultation"][
                "service_consultant_id"
            ] = f"CCC-{service['insee']}"
            consultation["consultation"]["service_consultant_insee"] = service["insee"]
            consultation["consultation"]["service_consultant_libelle"] = service[
                "label"
            ]

            response = self.openads.create_task(
                "create_DI_for_consultation", consultation
            )
            for piece_path in PIECE_PATHS:

                # replace Piece vars with CT ones
                piece["document_numerise"]["date_creation"] = today
                piece["document_numerise"]["nom_fichier"] = piece_path.split("/")[-1]
                piece_path = "{}/{}".format(
                    os.path.dirname(os.path.abspath(__file__)), piece_path
                )
                with open(piece_path, "rb") as f:
                    piece["document_numerise"]["file_content"] = base64.encodebytes(
                        f.read()
                    ).decode("UTF-8")
                if "DP_116" in piece_path:
                    piece["document_numerise"]["document_numerise_type_code"] = "90"
                elif "DP_117" in piece_path:
                    piece["document_numerise"]["document_numerise_type_code"] = "84"
                else:
                    piece["document_numerise"]["document_numerise_type_code"] = "111"

                piece["external_uids"]["acteur"] = service["actor_uid"]
                piece["external_uids"]["dossier"] = dossier_uid
                piece["external_uids"][
                    "dossier_consultation"
                ] = dossier_consultation_uid
                piece["external_uids"]["piece"] = str(uuid.uuid1())

                response = self.openads.create_task("add_piece", piece)
