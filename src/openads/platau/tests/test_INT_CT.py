# -*- coding: utf-8 -*-

import time

from datetime import datetime

from openads.platau.tests.base_test import (
    BaseTestCase,
    FakeArgs,
    CT_CONFIG_PATH,
    CURRENT_PATH,
)
from openads.platau.conductor import Conductor, OAPConfig
from openads.platau.openads_driver import OpenADSDriver
from openads.platau.platau_connector import PlatAUConnector


ACTEURS = {
    "13005": {
        "guichet_unique": "9WK-N3N-XPW",
        "service_instructeur": "25N-YGY-DLW",
        "autorite_competente": "V52-JMJ-NL5",
    },
    "32013": {"guichet_unique": "YX3-G82-PD1", "service_instructeur": "EWV-20J-VRW",},
    "44109": {"guichet_unique": "71M-R43-ZWZ", "service_instructeur": "7XY-2EP-0XZ",},
    "27004": {
        "guichet_unique": "8XP-YMY-2XP",
        "service_instructeur": "2XN-K2K-VXJ",
        "service_instructeur_ddt": "91D-GY5-E14",
    },
    "69243": {
        "guichet_unique": "91K-8O8-7WJ",
        "service_instructeur": "VX2-L2L-E1E",
        "service_instructeur_ddt": "71Z-V34-DWQ",
    },
    "07019": {
        "guichet_unique": "71M-O2O-6WZ",
        "service_instructeur": "E10-VQV-OWZ",
        "service_instructeur_ddt": "MWJ-879-GWP",
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
    },
}

CONSULTATION = {
    "date_consultation": "27/11/2000",
    "service_consulte": None,
    "type_consultation": 1,
    "delai_reponse": 1,
}

DOSSIERS = {
    "00": {
        "collectivite": 2,
        "type_detaille": "PC",
        "terrain": {
            "numero_voie": "20",
            "nom_voie": "boulevard Neuf",
            "lieu_dit": "",
            "code_postal": 13015,
            "localite": "Marseille",
            "references_cadastrales": [
                {"prefixe": "898", "section": "H", "numero": "0014"},
                {"prefixe": "898", "section": "BC", "numero": "0016"},
                {"prefixe": "898", "section": "H", "numero": "15"},
            ],
        },
        "demandeurs": [
            {
                "type_personne": "particulier",
                "typologie": "petitionnaire",
                "nom": "Pierre",
                "prenom": "Martin",
                "adresse": {
                    "numero_voie": "28b",
                    "nom_voie": "Boulevard Liberté",
                    "lieu_dit": "L'Estaque'",
                    "code_postal": 13016,
                    "localite": "Marseille",
                },
                "coordonnees": {"email": "support-atreal@yopmail.com"},
            }
        ],
        "engagement": {
            "lieu": "Marseille",
            "date": datetime.today().strftime("%Y-%m-%d"),
        },
        "pieces": [
            {"path": f"{CURRENT_PATH}/binary_files/DP_116.pdf"},
            {"path": f"{CURRENT_PATH}/binary_files/DP_117.pdf"},
        ],
    }
}

INSTRUCTIONS = {
    "decision": {"id": "97", "name": "ARRETE D'ACCORD (sans réserve)"},
    "incompletude": {"id": "92", "name": "INCOMPLÉTUDE"},
    "completude": {"id": "96", "name": "COMPLETUDE"},
}


class CTIntegrationTest(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        self.dossiers = DOSSIERS.copy()

        # Init openADS connection
        self.openads = OpenADSDriver(**self.conf.openadsapi_config)
        # Init actor and org with default one
        for actor in self.openads.actors:
            if actor.role == "guichet_unique":
                self.organization = actor.organization
                self.actor = actor.uid

        # Init Plat'AU connection
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)

        # self.openads_url = CT_OPENADS_LEGACY_WS[0]
        self.openads_url = self.conf.openadsapi_url

        # self.openads_auth = CT_OPENADS_LEGACY_WS[1]
        self.openads_auth = self.conf.openadsapi_config["auth"]
        self.kwargs = FakeArgs()

    def test_00_init_dossier(self):
        """."""
        dossier_data = self.dossiers["00"]
        dossier_data["collectivite"] = self.organization

        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(state="new", category="platau")
        self.cleanup_tasks(state="pending", category="platau")

        # Let's add a Dossier
        print("Creating a basic Dossier")
        dossier_result = self.openads.create_dossier(dossier_data)

        self.assertIn("numero_dossier", dossier_result)
        numero_dossier = dossier_result["numero_dossier"]

        sync_tasks = []
        async_tasks = []
        for typology in ("creation_DA", "creation_DI", "depot_DI", "qualification_DI"):
            result = self._get_tasks({"typology": typology, "state": "new"})
            # We should have one task of current type
            self.assertEqual(len(result), 1)
            # Dossier should match
            self.assertEqual(result[0]["dossier"], numero_dossier)
            sync_tasks.append(result[0])

        # Let's add some Pieces
        piece_result = self.openads.add_pieces_from_path(
            numero_dossier, dossier_data["pieces"]
        )
        for piece in piece_result["pieces"]:
            self.assertEqual(piece["status"], "OK")

        piece_tasks = self._get_tasks({"typology": "ajout_piece", "state": "new"})
        # We should have one task of current type
        self.assertEqual(len(piece_tasks), len(dossier_data["pieces"]))
        # Dossier should match
        self.assertEqual(result[0]["dossier"], numero_dossier)
        async_tasks.extend(piece_tasks)

        # We run the conductor to handle output tasks
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        # Tasks whithout attachment should be done
        for task in sync_tasks:
            sync_result = self._get_task(task_id=task["task"])
            # Task's state should be 'done'
            self.assertEqual(sync_result["task"]["state"], "done")

        # Last sync task should provide Plat'AU dossier uid
        dossier_uid = sync_result["external_uids"]["dossier"]
        # In Plat'AU, Dossier should exist
        dossier_platau = self.app.get_dossier_by_uid(dossier_uid)
        self.assertEqual(dossier_platau.noLocal, numero_dossier)

        # In Plat'AU, history should mention previous actions
        history = self.app.get_evenements_by_dossier(dossier_uid)
        triggered_events_ids = [
            1,  # libNom="Versement dans Plat’AU d'un dossier"
            2,  # libNom='Occurrence d’un dépôt de dossier'
            3,  # libNom='Occurrence d’une qualification de dossier'
        ]
        platau_events_ids = [event.nomTypeEvenement.idNom for event in history]
        for event_id in triggered_events_ids:
            self.assertIn(event_id, platau_events_ids)

        # Tasks whith attachment should be pending
        for task in async_tasks:
            async_result = self._get_task(task_id=task["task"])
            # Task's state should be 'pending'
            self.assertEqual(async_result["task"]["state"], "pending")

        print("Let's wait 30 seconds")
        time.sleep(30)
        # We run the conductor to handle async notification
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "collectivite", "input", self.kwargs)
        conductor.run()

        # Tasks whith attachment should be done
        for task in async_tasks:
            async_result = self._get_task(task_id=task["task"])
            # Task's state should be 'done'
            self.assertEqual(async_result["task"]["state"], "done")

    def test_01_completude(self):
        """."""
        dossier_data = self.dossiers["00"]
        # Let's get rid of Pieces*
        dossier_data["pieces"] = []
        dossier_data["collectivite"] = self.organization

        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(typology="creation_DI", state="done", category="platau")
        self.cleanup_tasks(typology="completude_DI", state="new", category="platau")
        self.cleanup_tasks(
            typology="lettre_incompletude", state="new", category="platau"
        )
        self.cleanup_tasks(
            typology="lettre_incompletude", state="pending", category="platau"
        )
        self.cleanup_tasks(typology="incompletude_DI", state="new", category="platau")

        # Let's add a Dossier
        print("Creating a basic Dossier")
        dossier_result = self.openads.create_dossier(dossier_data)
        numero_dossier = dossier_result["numero_dossier"]

        # We run the conductor to handle output tasks
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        result = self._get_tasks({"typology": "creation_DI", "state": "done"})
        dossier_uid = result[0]["external_uids"]["dossier"]

        result = self.add_instruction(
            numero_dossier, INSTRUCTIONS["incompletude"]["id"], lettre_type=True
        )

        # XXX - setting Date Notification is unavailable in API for now.
        # Let's do it manually.
        print(
            f"Please add a Date Notification on dossier {numero_dossier}\n"
            "Then press <ENTER> to continue"
        )
        input()
        print("OK, let's continue")

        incompletude_tasks = self._get_tasks(
            {"typology": "incompletude_DI", "state": "new"}
        )
        self.assertEqual(len(incompletude_tasks), 1)
        # Is this the task related to the dossier
        self.assertEqual(incompletude_tasks[0]["external_uids"]["dossier"], dossier_uid)

        # We should have a matching Lettre Petitionnaire task
        lettre_tasks = self._get_tasks(
            {"typology": "lettre_incompletude", "state": "new"}
        )
        self.assertEqual(len(lettre_tasks), 1)
        # Is this the task related to the dossier
        self.assertEqual(lettre_tasks[0]["external_uids"]["dossier"], dossier_uid)

        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        # Incompletude related task should be set has `done`
        incompletude_task = self._get_task(task_id=incompletude_tasks[0]["task"])
        self.assertEqual(incompletude_task["task"]["state"], "done")
        # Lettre Incompletude is async (binary provided) : related task should be set
        # has `pending`
        lettre_task = self._get_task(task_id=lettre_tasks[0]["task"])
        self.assertEqual(lettre_task["task"]["state"], "pending")

        # Related task should be set has `pending`
        tasks = self._get_tasks({"typology": "lettre_incompletude", "state": "pending"})
        self.assertEqual(len(tasks), 1)
        self.assertEqual(
            tasks[0]["external_uids"]["dossier"], dossier_uid,
        )

        # Lets'add a Completude
        result = self.add_instruction(
            numero_dossier, INSTRUCTIONS["completude"]["id"], lettre_type=True
        )

        completude_tasks = self._get_tasks(
            {"typology": "completude_DI", "state": "new"}
        )
        self.assertEqual(len(completude_tasks), 1)
        # Is this the task related to the dossier
        self.assertEqual(completude_tasks[0]["external_uids"]["dossier"], dossier_uid)

        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()
        # Completude related task should be set has `done`
        completude_task = self._get_task(task_id=completude_tasks[0]["task"])
        self.assertEqual(completude_task["task"]["state"], "done")

        # Next stop, retrieve input stream and ensure Lettre Incompletude is OK
        self._waiting_prompt(120)
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "collectivite", "input", self.kwargs)
        conductor.run()

        # Lettre Incompletude should be `done`
        lettre_task = self._get_task(task_id=lettre_tasks[0]["task"])
        self.assertEqual(lettre_task["task"]["state"], "done")

    def test_02_lettre_petitionnaire(self):
        """."""
        pass

    def test_03_decision(self):
        """."""
        dossier_data = self.dossiers["00"]
        # Let's get rid of Pieces*
        dossier_data["pieces"] = []
        dossier_data["collectivite"] = self.organization

        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(typology="creation_DI", state="done", category="platau")
        self.cleanup_tasks(typology="decision_DI", state="new", category="platau")
        self.cleanup_tasks(typology="decision_DI", state="pending", category="platau")
        self.cleanup_tasks(typology="decision_DI", state="done", category="platau")

        # Let's add a Dossier
        print("Creating a basic Dossier")
        dossier_result = self.openads.create_dossier(dossier_data)
        numero_dossier = dossier_result["numero_dossier"]

        # We run the conductor to handle output tasks
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        result = self._get_tasks({"typology": "creation_DI", "state": "done"})
        dossier_uid = result[0]["external_uids"]["dossier"]

        # XXX - we should test a Decision Tacite
        result = self.add_instruction(
            numero_dossier, INSTRUCTIONS["decision"]["id"], lettre_type=True
        )

        tasks = self._get_tasks({"typology": "decision_DI", "state": "new"})
        self.assertEqual(len(tasks), 1)

        # Is this the task related to the dossier
        self.assertEqual(tasks[0]["external_uids"]["dossier"], dossier_uid)

        # We run the conductor. Avis should be injected in Plat'AU
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `pending`
        tasks = self._get_tasks({"typology": "decision_DI", "state": "pending"})
        self.assertEqual(len(tasks), 1)
        self.assertEqual(
            tasks[0]["external_uids"]["dossier"], dossier_uid,
        )

        self._waiting_prompt(120)

        # We run the conductor. Avis should be injected in Plat'AU
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "collectivite", "input", self.kwargs)
        conductor.run()
        # Related task should be set has `done`
        task = self._get_task(task_id=tasks[0]["task"])
        self.assertEqual(task["task"]["state"], "done")

        return {"dossier_uid": dossier_uid, "numero_dossier": numero_dossier}

    def test_04_controle_legalite(self):
        """."""
        data = self.test_03_decision()

        # XXX - currently, we cannot trigger Envoi CL programatically
        print(
            f"Please trigger the Envoi CL on dossier {data['numero_dossier']}\n"
            "Then press <ENTER> to continue"
        )
        input()
        print("OK, let's continue")

        tasks = self._get_tasks({"typology": "envoi_CL", "state": "new"})
        self.assertEqual(len(tasks), 1)

        # Is this the task related to the dossier
        self.assertEqual(tasks[0]["external_uids"]["dossier"], data["dossier_uid"])

        # We run the conductor to handle output tasks
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `done`
        task = self._get_task(task_id=tasks[0]["task"])
        self.assertEqual(task["task"]["state"], "done")

    def test_05_DOC(self):
        """."""
        pass

    def test_05_DAACT(self):
        """."""
        pass

    def test_06_evolution_decision(self):
        """ANNUL & PRO + CL"""
        pass

    def test_50_piece_limite(self):
        """"""
        ## Piece with non-ASCII filename
        dossier_data = self.dossiers["00"].copy()
        dossier_data["collectivite"] = self.organization
        dossier_data["pieces"] = [
            {"path": f"{CURRENT_PATH}/binary_files/DP_116_façade_étages.pdf"},
        ]
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(typology="ajout_piece", state="new", category="platau")
        self.cleanup_tasks(typology="ajout_piece", state="pending", category="platau")

        # Let's add a Dossier
        print("Creating a basic Dossier")
        dossier_result = self.openads.create_dossier(dossier_data)
        numero_dossier = dossier_result["numero_dossier"]

        # Let's add some Pieces
        piece_result = self.openads.add_pieces_from_path(
            numero_dossier, dossier_data["pieces"]
        )
        # Pieces are OK in openADS
        for piece in piece_result["pieces"]:
            self.assertEqual(piece["status"], "OK")

        # Matching tasks are created
        piece_tasks = self._get_tasks({"typology": "ajout_piece", "state": "new"})
        # We should have one task of current type
        self.assertEqual(len(piece_tasks), len(dossier_data["pieces"]))
        # Dossier should match
        self.assertEqual(piece_tasks[0]["dossier"], numero_dossier)

        # We run the conductor to handle output tasks
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "collectivite", "output", self.kwargs)
        conductor.run()

        # Tasks whith attachment should be pending
        for task in piece_tasks:
            async_result = self._get_task(task_id=task["task"])
            # Task's state should be 'pending'
            self.assertEqual(async_result["task"]["state"], "pending")

        print("Let's wait 30 seconds")
        time.sleep(30)
        # We run the conductor to handle async notification
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "collectivite", "input", self.kwargs)
        conductor.run()

        # Tasks whith attachment should be done
        for task in piece_tasks:
            async_result = self._get_task(task_id=task["task"])
            # Task's state should be 'done'
            self.assertEqual(async_result["task"]["state"], "done")

        ## Large Piece
        pass
