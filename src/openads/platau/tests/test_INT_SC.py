# -*- coding: utf-8 -*-

import time

from openads.platau.tests.base_test import (
    BaseTestCase,
    FakeArgs,
    SC_CONFIG_PATH,
)
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.conductor import Conductor, OAPConfig


# Autorite Compétente Auch -> LX6-L20-KEW

ACTEURS = {
    "13005": {"guichet_unique": "8XP-G2Z-2XP", "service_instructeur": "2XN-Z0D-VXJ",},
    "32013": {"guichet_unique": "YX3-G82-PD1", "service_instructeur": "EWV-20J-VRW",},
    "44109": {"guichet_unique": "71M-R43-ZWZ", "service_instructeur": "7XY-2EP-0XZ",},
    "27004": {
        "guichet_unique": "8XP-YMY-2XP",
        "service_instructeur": "2XN-K2K-VXJ",
        "service_instructeur_ddt": "91D-GY5-E14",
    },
    "69243": {
        "guichet_unique": "91K-8O8-7WJ",
        "service_instructeur": "VX2-L2L-E1E",
        "service_instructeur_ddt": "71Z-V34-DWQ",
    },
    "07019": {
        "guichet_unique": "71M-O2O-6WZ",
        "service_instructeur": "E10-VQV-OWZ",
        "service_instructeur_ddt": "MWJ-879-GWP",
    },
    "culture": {
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        # "DEV-CRMH44": "O19-PR9-JWN",
        # "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-SRA-ARA": "MWJ-PZP-7WP",
        # "DEV-UDAP07": "52X-N0L-XJ4",
        # "DEV-UDAP27": "RXQ-MKN-01Q",
        # "DEV-UDAP69": "571-MY8-1Z4",
        # # XP
        "UDAP01": "71Z-ZKM-D1Q",
        # "UDAP14": "71Z-ZKN-D1Q",
        # "UDAP27": "71Z-ZKY-N1Q",
        # "UDAP69": "91K-6LV-N1J",
        # "UDAP76": "71M-M87-R1Z",
        # "UDAP91": "91D-Z27-PX4",
        # "SRA-ARA": "71M-M84-61Z",
        # "SRA-NORMANDIE": "GXG-V9J-LX3",
    },
}

CONSULTATION = {
    "date_consultation": "27/11/2000",
    "service_consulte": None,
    "type_consultation": 1,
    "delai_reponse": 1,
}

DOSSIERS = {
    "A.1": {
        "insee": "32013",
        "acteurs": {
            "guichet_unique": ACTEURS["32013"]["guichet_unique"],
            "service_instructeur": ACTEURS["32013"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [116,],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": True,
            "completude_DI": True,
            "consultation": [ACTEURS["culture"]["DEV-UDAP44"],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "PA",
        "competence": "commune",
    },
    "A.2": {
        "insee": "32013",
        "acteurs": {
            "guichet_unique": ACTEURS["32013"]["guichet_unique"],
            "service_instructeur": ACTEURS["32013"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": False,
            "completude_DI": False,
            "consultation": [ACTEURS["culture"]["DEV-UDAP44"],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "PA",
        "competence": "commune",
    },
    "A.3": {
        "insee": "32013",
        "acteurs": {
            "guichet_unique": ACTEURS["32013"]["guichet_unique"],
            "service_instructeur": ACTEURS["32013"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [],
            # "ajout_piece": [116],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": False,
            "completude_DI": False,
            "consultation": [ACTEURS["culture"]["DEV-SRA-ARA"],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "PA",
        "competence": "commune",
    },
}
INSTRUCTIONS = {
    "pec_metier": {"id": "5006", "name": "Accepté - Dossier Complet"},
    "pec_metier_with_attachment": {
        "id": "5002",
        "name": "Refusé : Pièces manquantes ou inexploitables",
    },
    "avis_consultation": {"id": "5042", "name": "Abords Accord"},
    "prescription": {"id": "", "name": "TEST PRESCRIPTION"},
}


class SCIntegrationTest(BaseTestCase):
    """Integration tests fro Service Consulte.
    Here, we will not focus on the data, but the streams between
    openADS <> openads.platau <> Plat'AU.
    """

    def setUp(self):
        """."""
        self.conf = OAPConfig(SC_CONFIG_PATH)
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()
        self.actor = ACTEURS["culture"]["DEV-UDAP44"]
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
            calling_actor=self.actor,
        )
        self.app.start(self.actor)
        self.openads_url = self.conf.openadsapi_url
        self.openads_auth = self.conf.openadsapi_config["auth"]
        self.kwargs = FakeArgs()

    # def _test_00_SC_get_consultation(self):
    #     """."""
    #     c_uid = "RXQ-4KK-LQW"
    #     cc = self.app.get_consultation_by_uid(c_uid)
    #     # criterions = {"criteresSurConsultations": {"idServiceConsulte": "71Z-ZKY-N1Q"}}
    #     criterions = {
    #         "criteresSurConsultations": {"idServiceConsultant": "591-DK6-X42"}
    #     }
    #     result = self.app.get_consultations(criterions)
    #     for r in result:
    #         c = r.dossier.consultations[0]
    #         print(c.dtConsultation, c.idServiceConsultant, c.idConsultation)

    def _test_00_SC_build_dataset(self):
        """."""
        print("Building dataset")
        self._build_dataset()

    def test_00_SC_status(self):
        """."""
        self.assertTrue(self.app.status())

    def test_01_SC_get_consultation_piece_message(self):
        """Usecase:
            - Dossier, Completude and a Consultation injection
            - Retrieve notifications (consultation, completude)
            - Add matching inpout tasks in openADS queue
            - Check tasks are correctly added
        """

        # SC_Configure_Openads.robot

        # we don't want any existing task messing up with our test case
        # self.cleanup_tasks("create_DI_for_consultation")
        # self.cleanup_tasks("add_piece")
        # self.cleanup_tasks("message")
        self.cleanup_tasks(state="new")
        # Get rid of unwanted notifications
        self.app.get_notifications(self.actor, raw=False)

        # We build a simple dataset : one dossier with one consultation
        dataset = {"A.1": DOSSIERS.copy()["A.1"]}
        self._build_dataset(dataset)

        # Let's create Consultation and pieces in openads
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        # We should have one task of type `create_DI_for_consultation`
        print("Retrieving new tasks of type `create_DI_for_consultation`")
        tasks = self._get_tasks(
            {"typology": "create_DI_for_consultation", "state": "new"}
        )
        self.assertEqual(len(tasks), 1)
        # Let's check the local number
        self.assertEqual(self.dummy_backend["A.1"]["local_number"], tasks[0]["dossier"])

        # We should have two tasks of type `create_message`, one for each Completude event
        print("Retrieving new tasks of type `create_message`")
        tasks = self._get_tasks({"typology": "create_message", "state": "new"})
        self.assertEqual(len(tasks), 2)

        # Re-run conductor to catch late Piece
        print("Running conductor again to catch late Piece")
        conductor.run()

        # We should have `n` task(s) of type `add_piece`
        print("Retrieving new tasks of type `add_piece`")
        n = len(self.dummy_backend["A.1"]["tasks"]["ajout_piece"])
        tasks = self._get_tasks({"typology": "add_piece", "state": "new"})
        self.assertEqual(len(tasks), n)

        # Because we injected dataset straight from the connector, we stored some uids
        # and are able to run few assertions. Although this is not the main objective here.
        # Plat'AU uids should match
        matching_uids = 0
        for task_summary in tasks:
            task = self._get_task(task_id=task_summary["task"])
            for piece_uid in self.dummy_backend["A.1"]["external_uids"]["pieces"]:
                if piece_uid in task['external_uids']['piece']:
                    matching_uids += 1

        self.assertEqual(matching_uids, n)

        print("Running openADS taskmanager")
        response = self.run_openads_taskmanager(mode="SC")
        self.assertEqual(response.status_code, 200)

    def test_02a_SC_add_pec(self):
        """Usecase :
            - Add an Instruction on openADS backend, that generates a PeC output task
            - Run the conductor in output stream mode
            - Check if the openADS output task  has been correctly handled
            - Check Pec is correctly created on Plat'AU
        """
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(state="new")

        # We build a simple dataset : one dossier with one consultation
        actor_sc = ACTEURS["culture"]["DEV-UDAP44"]
        self.dummy_backend = {
            "A.2": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "incompletude_DI": False,
                    "completude_DI": False,
                    "consultation": [actor_sc,],
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "PA",
                "competence": "commune",
            },
        }

        self._build_dataset()

        # Let's create Consultation and pieces in openads
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        # XXX - Dossier  number shoulb retreived properly and dynamically
        dossier_number = self.dummy_backend["A.2"]["local_number"] + "U4401"
        consultation_uid = self.dummy_backend["A.2"]["external_uids"]["consultation"][0]

        response = self.run_openads_taskmanager(mode="SC")
        self.assertEqual(response.status_code, 200)

        # XXX - test and assert Commentaire
        result = self.add_instruction(dossier_number, INSTRUCTIONS["pec_metier"]["id"])

        tasks = self._get_tasks({"typology": "pec_metier_consultation", "state": "new"})
        self.assertEqual(len(tasks), 1)

        # Is this the task related to the consultation
        self.assertEqual(
            tasks[0]["external_uids"]["dossier_consultation"], consultation_uid
        )

        # We run the conductor. Pec should be injected in Plat'AU
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `done`
        tasks = self._get_tasks(
            {"typology": "pec_metier_consultation", "state": "done"}
        )
        self.assertIn(
            consultation_uid,
            [t["external_uids"]["dossier_consultation"] for t in tasks],
        )

        # Pec should be available on Plat'AU
        response = self.app.get_pec_consultation(consultation_uid)
        self.assertIsNotNone(response)
        self.assertIsNone(response.erreur)

        # Are there any PeC ?
        # Consultation should match
        self.assertEqual(response.idConsultation, consultation_uid)

        # we should have one and only PeC
        pecs = response.pecMetiers
        self.assertEqual(len(pecs), 1)

        # Pec should be from the right service
        self.assertEqual(
            pecs[0].idActeurEmetteur, pecs[0].idActeurGenerateur, actor_sc,
        )

    def test_02b_SC_add_pec_with_attachment(self):
        """Usecase :
            - Add an Instruction on openADS backend, that generates a PeC output task
            - Run the conductor in output stream mode
            - Check if the openADS output task  has been correctly handled
            - Check Pec is correctly created on Plat'AU
        """
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks(state="new")

        # We build a simple dataset : one dossier with one consultation
        actor_sc = ACTEURS["culture"]["DEV-UDAP44"]
        self.dummy_backend = {
            "A.2": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "incompletude_DI": False,
                    "completude_DI": False,
                    "consultation": [actor_sc,],
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "PA",
                "competence": "commune",
            },
        }

        self._build_dataset()

        # Let's create Consultation and pieces in openads
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        # XXX - Dossier  number shoulb retreived properly and dynamically
        dossier_number = self.dummy_backend["A.2"]["local_number"] + "U4401"
        consultation_uid = self.dummy_backend["A.2"]["external_uids"]["consultation"][0]

        response = self.run_openads_taskmanager(mode="SC")
        self.assertEqual(response.status_code, 200)

        # XXX - test and assert Commentaire
        result = self.add_instruction(
            dossier_number,
            INSTRUCTIONS["pec_metier_with_attachment"]["id"],
            lettre_type=True,
        )

        tasks = self._get_tasks({"typology": "pec_metier_consultation", "state": "new"})
        # XXX - manual input until Instrcution search and update are implemented
        if len(tasks) == 0:
            print(
                f"Please finalize and update date_signature on Dossier {dossier_number}\n"
                "The press <ENTER> to continue"
            )
            dummy = input()
            print("OK, let's continue")
            tasks = self._get_tasks(
                {"typology": "pec_metier_consultation", "state": "new"}
            )
        self.assertEqual(len(tasks), 1)

        # Is this the task related to the consultation
        self.assertEqual(
            tasks[0]["external_uids"]["dossier_consultation"], consultation_uid
        )

        # We run the conductor. Pec should be injected in Plat'AU
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `pending`, bexasue it has an attachment
        tasks = self._get_tasks(
            {"typology": "pec_metier_consultation", "state": "pending"}
        )

        self.assertIn(
            consultation_uid,
            [t["external_uids"]["dossier_consultation"] for t in tasks],
        )

        wait_seconds = 10
        max_iter = 20
        for i in range(max_iter):

            print(
                f"Wait {wait_seconds} seconds before retrieving tasks from Plat'AU. Try {i}."
            )
            time.sleep(wait_seconds)

            # We run the conductor. Binary ACK should be recieved from Plat'AU
            print("Running conductor in stream input")
            conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
            conductor.run()

            # Related task should be set has `done`
            tasks = self._get_tasks(
                {"typology": "pec_metier_consultation", "state": "done"}
            )
            if tasks is not None and len(tasks) != 0:
                break

        # Pec should be available on Plat'AU
        response = self.app.get_pec_consultation(consultation_uid)
        self.assertIsNotNone(response)
        self.assertIsNone(response.erreur)

        # Are there any PeC ?
        # Consultation should match
        self.assertEqual(response.idConsultation, consultation_uid)

        # we should have one and only PeC
        pecs = response.pecMetiers
        if pecs is None:
            # Plat'AU is busy, let's give another try
            time.sleep(wait_seconds * 3)
            response = self.app.get_pec_consultation(consultation_uid)
            pecs = response.pecMetiers

        self.assertEqual(len(pecs), 1)

        # Pec should be from the right service
        self.assertEqual(
            pecs[0].idActeurEmetteur, pecs[0].idActeurGenerateur, actor_sc,
        )

    def test_03_SC_add_avis(self):
        """Usecase :
            - Add an Instruction on openADS backend, that generates an Avis output task
            - Run the conductor in full cycle stream mode :
                * output stream for Avis injection
                * input stream for binary files confirmation
            - Check if the openADS output task has been correctly handled
            - Check Pec is correctly created on Plat'AU
        """
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks("avis_consultation")

        self.test_02a_SC_add_pec()

        # XXX - Dossier number should be retreived properly and dynamically
        dossier_number = self.dummy_backend["A.2"]["local_number"] + "U4401"
        consultation_uid = self.dummy_backend["A.2"]["external_uids"]["consultation"][0]
        actor_sc = DOSSIERS["A.2"]["tasks"]["consultation"][0]

        result = self.add_instruction(
            dossier_number, INSTRUCTIONS["avis_consultation"]["id"], lettre_type=True
        )

        tasks = self._get_tasks({"typology": "avis_consultation", "state": "new"})
        # XXX - manual input until Instrcution search and update are implemented
        if len(tasks) == 0:
            print(
                f"Please inalize and update date_signature on Dossier {dossier_number}\n"
                "Press <ENTER> to continue"
            )
            dummy = input()
            print("OK, let's continue")
            tasks = self._get_tasks({"typology": "avis_consultation", "state": "new"})

        self.assertEqual(len(tasks), 1)

        # Is this the task related to the consultation
        self.assertEqual(
            tasks[0]["external_uids"]["dossier_consultation"], consultation_uid
        )

        # We run the conductor. Avis should be injected in Plat'AU
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "service_consulte", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `pending`
        tasks = self._get_tasks({"typology": "avis_consultation", "state": "pending"})
        self.assertEqual(len(tasks), 1)
        self.assertEqual(
            consultation_uid, tasks[0]["external_uids"]["dossier_consultation"]
        )

        wait_seconds = 10
        max_iter = 10
        while True:
            # Plat'AU files handling can be lazy. We wait few seconds.
            print(
                f"Plat'AU files handling can be lazy. We wait {wait_seconds} seconds."
            )
            time.sleep(wait_seconds)

            # We run the conductor. Binary ACK should be recieved from Plat'AU
            print("Running conductor in stream input")
            conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
            conductor.run()

            # Related task should be set has `done`
            tasks = self._get_tasks({"typology": "avis_consultation", "state": "done"})
            if len(tasks) == 0:
                max_iter -= 1
                if max_iter == 0:
                    break
                else:
                    continue
            else:
                break

        self.assertEqual(len(tasks), 1)

        self.assertEqual(
            consultation_uid, tasks[0]["external_uids"]["dossier_consultation"]
        )

        # Avis should be available on Plat'AU
        criterions = {"criteresSurConsultations": {"idConsultation": consultation_uid}}
        response = self.app.get_avis(criterions)
        self.assertIsNotNone(response)
        self.assertEqual(len(response), 1)
        self.assertIsNone(response[0].erreur)

        # Weshould have one and only Avis
        self.assertEqual(len(response[0].dossier.avis), 1)
        avis = response[0].dossier.avis[0]

        # Consultation should match
        self.assertEqual(avis.idConsultation, consultation_uid)

        # # Avis should be from the right service
        self.assertEqual(avis.idActeurAuteur, actor_sc)

        # We should have one document
        self.assertEqual(len(avis.documents), 1)

    def test_03_SC_add_prescription(self):
        """Usecase :
            - Add an Instruction on openADS backend, that generates a Prescription output task
            - Run the conductor in full cycle stream mode :
                * output stream for Prescription injection
                * input stream for binary files confirmation
            - Check if the openADS output task has been correctly handled
            - Check Pec is correctly created on Plat'AU
        """
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks("pec_metier_consultation")
        self.cleanup_tasks("prescription")

        # We build a simple dataset : one dossier with one consultation
        actor_sc = ACTEURS["culture"]["DEV-SRA-ARA"]
        self.dummy_backend = {
            "A.3": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [],
                    # "ajout_piece": [116],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "incompletude_DI": False,
                    "completude_DI": False,
                    "consultation": [actor_sc,],
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "PA",
                "competence": "commune",
            }
        }
        self._build_dataset()

        # Let's create Consultation and pieces in openads
        print("Running conductor in stream input")
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        response = self.run_openads_taskmanager(mode="SC")
        self.assertEqual(response.status_code, 200)

        # XXX - Dossier  number should retreived properly and dynamically
        dossier_number = self.dummy_backend["A.3"]["local_number"] + "SRAARA01"
        consultation_uid = self.dummy_backend["A.3"]["external_uids"]["consultation"][0]

        result = self.add_instruction(dossier_number, INSTRUCTIONS["pec_metier"]["id"])

        # We run the conductor. Pec should be injected in Plat'AU
        print("Running conductor in stream output")
        conductor = Conductor(self.conf, "service_consulte", "output", self.kwargs)
        conductor.run()

        result = self.add_instruction(
            dossier_number, INSTRUCTIONS["prescription"]["name"], lettre_type=True
        )

        return

        tasks = self._get_tasks({"typology": "prescription", "state": "new"})
        # XXX - manual input until Instrcution search and update are implemented
        if len(tasks) == 0:
            print(
                f"Please inalize and update date_signature on Dossier {dossier_number}\n"
                "Press <ENTER> to continue"
            )
            dummy = input()
            print("OK, let's continue")
            tasks = self._get_tasks({"typology": "avis_consultation", "state": "new"})

        self.assertEqual(len(tasks), 1)

        # Is this the task related to the consultation
        self.assertEqual(
            tasks[0]["external_uids"]["dossier_consultation"], consultation_uid
        )

        # We run the conductor. Avis should be injected in Plat'AU
        conductor = Conductor(self.conf, "service_consulte", "output", self.kwargs)
        conductor.run()

        # Related task should be set has `pending`
        tasks = self._get_tasks({"typology": "avis_consultation", "state": "pending"})
        self.assertEqual(len(tasks), 1)
        self.assertEqual(
            consultation_uid, tasks[0]["external_uids"]["dossier_consultation"]
        )

        wait_seconds = 10
        max_iter = 10
        print("Calling Conductor in ")
        while True:
            # Plat'AU files handling can be lazy. We wait few seconds.
            print(
                f"Plat'AU files handling can be lazy. We wait {wait_seconds} seconds."
            )
            time.sleep(wait_seconds)

            # We run the conductor. Binary ACK should be received from Plat'AU
            print("Running conductor in stream input")
            conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
            conductor.run()

            # Related task should be set has `done`
            tasks = self._get_tasks({"typology": "avis_consultation", "state": "done"})
            if len(tasks) == 0:
                max_iter -= 1
                if max_iter == 0:
                    break
                else:
                    continue
            else:
                break

        self.assertEqual(len(tasks), 1)

        self.assertEqual(
            consultation_uid, tasks[0]["external_uids"]["dossier_consultation"]
        )

        # Avis should be available on Plat'AU
        criterions = {"criteresSurConsultations": {"idConsultation": consultation_uid}}
        response = self.app.get_avis(criterions)
        self.assertIsNotNone(response)
        self.assertEqual(len(response), 1)
        self.assertIsNone(response[0].erreur)

        # Weshould have one and only Avis
        self.assertEqual(len(response[0].dossier.avis), 1)
        avis = response[0].dossier.avis[0]

        # Consultation should match
        self.assertEqual(avis.idConsultation, consultation_uid)

        # # Avis should be from the right service
        self.assertEqual(avis.idActeurAuteur, actor_sc)

        # We should have one document
        self.assertEqual(len(avis.documents), 1)

    def test_04_SC_no_redundant_piece(self):
        """ If 2 identical Pieces are provided, we should retreive only one of those .
        """
        #########################################
        # First, let's check the nominal use case
        #########################################

        # we don't want any existing task messing up with our test case
        self.cleanup_tasks("create_DI_for_consultation", state="new")
        self.cleanup_tasks("add_piece", state="new")
        # Get rid of unwanted notifications
        self.app.get_notifications(self.actor, raw=False)

        # Let's prepare a regular dataset
        dataset = {
            "0": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116, 117],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "consultation": [ACTEURS["culture"]["DEV-UDAP44"],],
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            }
        }

        # We build a simple dataset : one dossier with one consultation
        self.dummy_backend = dataset
        self._build_dataset()

        # Let's retreive Consultation and Piece in openads
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        # We should have one task of type `create_DI_for_consultation`
        tasks = self._get_tasks(
            {"typology": "create_DI_for_consultation", "state": "new"}
        )
        self.assertEqual(len(tasks), 1)
        # Let's check the local number
        self.assertEqual(self.dummy_backend["0"]["local_number"], tasks[0]["dossier"])

        # Dataset provided 2 different Pieces, but we should have the same number of tasks of type `add_piece`
        n = len(self.dummy_backend["0"]["tasks"]["ajout_piece"])
        tasks = self._get_tasks({"typology": "add_piece", "state": "new"})
        self.assertEqual(len(tasks), n)

        #################################################
        # Now, let's check a usecas with redundant Pieces
        #################################################
        # we don't want any existing task messing up with our test case
        self.cleanup_tasks("create_DI_for_consultation", state="new")
        self.cleanup_tasks("add_piece", state="new")
        # Get rid of unwanted notifications
        self.app.get_notifications(self.actor, raw=False)
        # Let's prepare a dataset with 2 identical Pieces
        dataset = {
            "0": {
                "insee": "32013",
                "acteurs": {
                    "guichet_unique": ACTEURS["32013"]["guichet_unique"],
                    "service_instructeur": ACTEURS["32013"]["service_instructeur"],
                },
                "tasks": {
                    "creation_DA": 6,
                    "creation_DI": 7,
                    "ajout_piece": [116, 116],
                    "depot_dossier": 7,
                    "qualification_DI": 8,
                    "consultation": [ACTEURS["culture"]["DEV-UDAP44"],],
                },
                "external_uids": {
                    "dossier_autorisation": "",
                    "dossier": "",
                    "pieces": {},
                },
                "local_number": "",
                "type": "DP",
                "competence": "commune",
            }
        }

        # We build a simple dataset : one dossier with one consultation
        self.dummy_backend = dataset
        self._build_dataset()

        # Let's retreive Consultation and Piece in openads
        conductor = Conductor(self.conf, "service_consulte", "input", self.kwargs)
        conductor.run()

        # We should have one task of type `create_DI_for_consultation`
        tasks = self._get_tasks(
            {"typology": "create_DI_for_consultation", "state": "new"}
        )
        self.assertEqual(len(tasks), 1)
        # Let's check the local number
        self.assertEqual(self.dummy_backend["0"]["local_number"], tasks[0]["dossier"])

        # Dataset provided 2 identical Pieces, but we should have 1 and 1 only task
        # of type `add_piece`
        n = len(self.dummy_backend["0"]["tasks"]["ajout_piece"])
        tasks = self._get_tasks({"typology": "add_piece", "state": "new"})
        self.assertNotEqual(len(tasks), n)
        self.assertEqual(len(tasks), n - 1)
