# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from random import choice

from openads.platau.tests.base_test import BaseTestCase
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.syncplicity_connector import SyncplicityUploadConnector
from bravado.exception import HTTPInternalServerError, HTTPBadRequest


# Identifiants des communes
# Commune         typeActeur  idCommuneDepot
# --------------------------------------
# Marseille       1           PYW-8EY-132
# Marseille       2           52X-NQL-XJ4
# Marseille       4           471-OV9-XMV
# Aix-en-Provence 1           8MW-J7G-XPR
# Aix-en-Provence 2           RLX-6LR-W9D
# Aix-en-Provence 4           48X-PPV-XPZ

TYPE_ACTEUR = [
    {
        "idNom": 1,
        "libNom": "Guichet unique",
        "dtMiseEnPlace": "2020-01-01",
        "dtObsolescence": None,
    },
    {
        "idNom": 2,
        "libNom": "Service instructeur",
        "dtMiseEnPlace": "2020-01-02",
        "dtObsolescence": None,
    },
    {
        "idNom": 4,
        "libNom": "Autorité compétente",
        "dtMiseEnPlace": "2020-01-04",
        "dtObsolescence": None,
    },
]

DOSSIERS = {
    # "DP0132011800001": {
    #     "tasks": {
    #         "creation_DA": 1,
    #         "creation_DI": 2,
    #         "ajout_piece": [5],
    #         "depot_dossier": 2,
    #         "qualification_DI": 3,
    #         "decision_DI": 14,
    #     },
    #     "external_uids": {
    #         # "dossier_autorisation": "71O-K25-OWM",
    #         "dossier_autorisation": "",
    #         # "dossier": "91K-O3N-LXJ",
    #         "dossier": "",
    #         "document_numerise": "",
    #     },
    #     "acteurs": {
    #         "guichet_unique": "PYW-8EY-132",
    #         # "service_instructeur": "52X-NQL-XJ4", # service instructeur Marseille
    #         "service_instructeur": "571-M6E-NXZ", # DDT-13
    #         # "autorite_competente": "471-OV9-XMV", Autorite competente Marseille
    #         "autorite_competente": "3Y1-LPM-4XP", # Prefet-13
    #     },
    # },
    # "PC0130012000001": {
    #     "tasks": {
    #         "creation_DA": 6,
    #         "creation_DI": 7,
    #         "ajout_piece": [10, 11, 12, 13],
    #         "depot_dossier": 7,
    #         "qualification_DI": 8,
    #         "decision_DI": 15,
    #     },
    #     "external_uids": {
    #         "dossier_autorisation": "",
    #         "dossier": "",
    #         "document_numerise": "",
    #     },
    #     "acteurs": {
    #         "guichet_unique": "8MW-J7G-XPR",
    #         "service_instructeur": "RLX-6LR-W9D",
    #         # "autorite_competente": "48X-PPV-XPZ",
    #         "autorite_competente": "571-M6E-NXZ",
    #     },
    # },
    "DP0132012000001": {
        "tasks": {"creation_DA": 33, "creation_DI": 34,},
        "external_uids": {
            "dossier_autorisation": "Y1L-JRK-4XP",
            "dossier": "",
            "document_numerise": "",
        },
        "acteurs": {
            "guichet_unique": "PYW-8EY-132",
            "service_instructeur": "52X-NQL-XJ4",  # service instructeur Marseille
            "autorite_competente": "471-OV9-XMV",  # Autorite competente Marseille
        },
    }
}


class BeBJ1TestCase(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        super().setUp()
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()
        # print(self.dummy_backend)

    def test_status(self):
        """."""
        self.assertTrue(self.app.status())

    def test_01_creation_DA(self):
        u"""."""
        print("test_creation_DA")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS
        for dossier, values in self.dummy_backend.items():

            task_id = values["tasks"]["creation_DA"]
            task = self._get_task(
                task_id=task_id,
                json_path="binary_files/{}_creation_DA.json".format(dossier),
            )

            projet_uid = self.app.create_projet(task)[0]

            print("Projet created : {}".format(projet_uid))

            # XXX : fail-safe mode
            update_backend = {"dossier": dossier, "typology": "dossier_autorisation"}
            self._update_task(task_id, "done", projet_uid, update_backend)

    def test_02_creation_DI(self):
        u"""."""
        print("test_creation_DI")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS
        for dossier, values in self.dummy_backend.items():

            task_id = values["tasks"]["creation_DI"]
            task = self._get_task(
                task_id=task_id,
                json_path="binary_files/{}_creation_DI.json".format(dossier),
            )

            # XXX : fail-safe mode
            task["external_uids"].update(
                {
                    "dossier_autorisation": values["external_uids"][
                        "dossier_autorisation"
                    ]
                }
            )
            # We provide a unique Dossier local number
            local_number = task["dossier"]["dossier"]
            task["dossier"]["dossier"] = (
                local_number[:-5] + str(uuid.uuid1()).split("-")[0][:-3]
            )

            dossier_uid = self.app.create_dossier(task)[0]

            print("Dossier created : {}".format(dossier_uid))

            # self.app.get_dossier_by_uid(dossier_uid)

            # XXX : fail-safe mode
            update_backend = {"dossier": dossier, "typology": "dossier"}
            self._update_task(task_id, "done", dossier_uid, update_backend)

    def test_03_ajout_piece(self):
        u"""."""
        print("test_ajout_piece")
        print("----------------")

        self.upload_app = SyncplicityUploadConnector()

        for dossier, values in self.dummy_backend.items():

            tasks = []
            # uploaded = []
            for task_id in values["tasks"]["ajout_piece"]:

                task = self._get_task(
                    task_id=task_id,
                    json_path="binary_files/{}_ajout_piece-{}.json".format(
                        dossier, task_id
                    ),
                )

                task["task"]["acteur"] = values["acteurs"]["guichet_unique"]

                file_path = "{}/binary_files/{}-{}.pdf".format(
                    os.path.dirname(os.path.abspath(__file__)), dossier, task_id
                )

                task["document_numerise"]["local_file_path"] = file_path

                # XXX : fail-safe mode
                task["external_uids"].update(
                    {
                        "dossier_autorisation": values["external_uids"][
                            "dossier_autorisation"
                        ],
                        "dossier": values["external_uids"]["dossier"],
                    }
                )
                tasks.append(task)

            piece_uids = self.app.create_piece(tasks)

            # XXX : fail-safe mode / this is b
            for task_id in values["tasks"]["ajout_piece"]:
                update_backend = {"dossier": dossier, "typology": "document_numerise"}
                # XXX - we have no way to attached a notification to a task
                self._update_task(task_id, "done", piece_uids[0], update_backend)

        print("Piece(s) added : {}".format(piece_uids))

        print("----------------")

    def test_04_depot_DI(self):
        """."""
        print("test_depot_DI")
        print("----------------")

        for dossier, values in self.dummy_backend.items():

            # XXX : fail-safe mode. We should have a depot task
            task_id = values["tasks"]["depot_dossier"]
            task = self._get_task(
                task_id=task_id,
                json_path="binary_files/{}_creation_DI.json".format(dossier),
            )

            task["external_uids"].update(
                {
                    "dossier_autorisation": values["external_uids"][
                        "dossier_autorisation"
                    ],
                    "dossier": values["external_uids"]["dossier"],
                }
            )

            result = self.app.depot_dossier(task)

    # def test_05_recherche_acteurs(self):
    #     """."""
    #     print("test_recherche_acteurs")
    #     print("----------------")
    #     self.app.get_acteurs({"nomTypeActeur": 2})
    #     print("----------------")

    def test_06_qualification_DI(self):
        u"""."""
        print("test_qualification_DI")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS

        for dossier, values in self.dummy_backend.items():

            task_id = values["tasks"]["qualification_DI"]
            task = self._get_task(
                task_id=task_id,
                json_path="binary_files/{}_qualification_DI.json".format(dossier),
            )

            # XXX 07/07/2020 - acteurs search with more than 50 results returns a 400 ..
            # if task["dossier"]["autorite_competente_code"] == "ETAT":
            #     dpt = task["dossier"]["insee"][:2]
            #     service_instructeur = self.app.get_DDT_by_departement(dpt)
            # else:
            #     service_instructeur = values["acteurs"]["service_instructeur"]

            service_instructeur = values["acteurs"]["service_instructeur"]

            task["task"]["acteur"] = service_instructeur

            # XXX : fail-safe mode
            task["external_uids"].update(
                {
                    "dossier_autorisation": values["external_uids"][
                        "dossier_autorisation"
                    ],
                    "dossier": values["external_uids"]["dossier"],
                }
            )

            result = self.app.qualification_dossier(task)

            print("Dossier qualified : {}".format(result))
        print("----------------")

    def test_07_decision_DI(self):
        u"""."""
        print("test_desicion_DI")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS
        for dossier, values in self.dummy_backend.items():

            task_id = values["tasks"]["decision_DI"]
            task = self._get_task(
                task_id=task_id,
                json_path="binary_files/{}_decision_DI.json".format(dossier),
            )

            task["task"]["acteur"] = values["acteurs"]["autorite_competente"]

            # XXX : fail-safe mode
            task["external_uids"].update(
                {
                    "dossier_autorisation": values["external_uids"][
                        "dossier_autorisation"
                    ],
                    "dossier": values["external_uids"]["dossier"],
                }
            )

            result = self.app.decision(task)

            print("Decision added : {}".format(result))

        print("----------------")
