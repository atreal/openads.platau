# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import datetime, timedelta
from random import choice

from openads.platau.tests.base_test import BaseTestCase, REMOTE_URL, AUTH, time_me
from openads.platau.platau_connector import PlatAUConnector


ACTEURS = {
    "13055": {"guichet_unique": "8XP-G2Z-2XP", "service_instructeur": "2XN-Z0D-VXJ",},
    "44109": {"guichet_unique": "71M-R43-ZWZ", "service_instructeur": "7XY-2EP-0XZ",},
    "27004": {
        "guichet_unique": "8XP-YMY-2XP",
        "service_instructeur": "2XN-K2K-VXJ",
        "service_instructeur_ddt": "91D-GY5-E14",
    },
    "69243": {
        "guichet_unique": "91K-8O8-7WJ",
        "service_instructeur": "VX2-L2L-E1E",
        "service_instructeur_ddt": "71Z-V34-DWQ",
    },
    "07019": {
        "guichet_unique": "71M-O2O-6WZ",
        "service_instructeur": "E10-VQV-OWZ",
        "service_instructeur_ddt": "MWJ-879-GWP",
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
        #
        "UDAP01": "71Z-ZKM-D1Q",
        "UDAP14": "71Z-ZKN-D1Q",
        "UDAP27": "71Z-ZKY-N1Q",
        "UDAP69": "91K-6LV-N1J",
        "UDAP76": "71M-M87-R1Z",
        "UDAP91": "91D-Z27-PX4",
        "SRA-ARA": "71M-M84-61Z",
        "SRA-NORMANDIE": "GXG-V9J-LX3",
    },
}

CONSULTATION = {
    "date_consultation": "27/11/2000",
    "service_consulte": None,
    "type_consultation": 1,
    "delai_reponse": 1,
}

DOSSIERS = {
    "A.1": {
        "insee": "13055",
        "acteurs": {
            "guichet_unique": ACTEURS["13055"]["guichet_unique"],
            "service_instructeur": ACTEURS["13055"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            "ajout_piece": [116, 117,],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "consultation": [
                ACTEURS["culture"]["DEV-UDAP44"],
                # ACTEURS["culture"]["DEV-UDAP27"],
                # ACTEURS["culture"]["UDAP01"],
                # ACTEURS["culture"]["UDAP14"],
                # ACTEURS["culture"]["UDAP27"],
                # ACTEURS["culture"]["UDAP69"],
                # ACTEURS["culture"]["UDAP76"],
                # ACTEURS["culture"]["UDAP91"],
                # ACTEURS["culture"]["SRA-ARA"],
                # ACTEURS["culture"]["SRA-NORMANDIE"],
            ],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    },
}


class XP1_2Dataset(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()
        self.app = PlatAUConnector()
        self.openads_url = REMOTE_URL
        self.auth = tuple(AUTH)

    def test_xp1_2_status(self):
        """."""
        self.assertTrue(self.app.status())
        self.app = PlatAUConnector()
        self.assertTrue(self.app.status())

    def test_xp1_2_build_dataset(self):
        """."""
        self._build_dataset()

    def test_xp1_2_get_piece(self):
        """."""
        self.app = PlatAUConnector()
        self.app.calling_actor = ACTEURS["culture"]["UDAP91"]
        pieces_1 = self.app.get_pieces_binaries("2XN-PPQ-691")
        pieces_2 = self.app.get_pieces_binaries("LX6-LLG-PYW")
        pieces_3 = self.app.get_pieces_binaries("91K-KKZ-LD1")
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on

    def test_xp1_2_get_notifications_ct(self):
        """."""
        for category, acteurs in ACTEURS.items():
            if category in ("culture", "13055"):
                continue

            print("---------------------------------------------")
            print(f"Notifications for {category} / guichet_unique")
            print(
                self.app.get_notifications(
                    acteurs["guichet_unique"], offset=0, raw=False
                )
            )
            print("---------------------------------------------------")
            print(f"Notifications for {category} / service_instructeur")
            print(
                self.app.get_notifications(
                    acteurs["service_instructeur"], offset=0, raw=False
                )
            )

    def test_xp1_2_get_notifications_mc(self):
        """."""
        self.app = PlatAUConnector()
        for label, actor_id in ACTEURS["culture"].items():

            print("---------------------------------------------")
            print(f"Notifications for {label} : {actor_id}")
            print(self.app.get_notifications(actor_id, offset=0, raw=False))
