# -*- coding: utf-8 -*-

import json
import os
import unittest

from base64 import b64decode, b64encode
from datetime import date
from random import choice

from openads.platau.tests.base_test import BaseTestCase
from openads.platau.platau_connector import PlatAUConnector
from bravado.exception import HTTPInternalServerError, HTTPBadRequest

projet_template = {"nom_projet": "Construction d'une maison ossature bois"}

dossier_template = {
    "boConcerneBatimentHistorique": False,
    "boMaisonIndividuelle": True,
    "boRecoursAUnProfessionnel": False,
    "boRecoursAUnProfessionnelNonOblig": True,
    "dtEngagementAAvoirQualite": date.today(),
    "idCommuneDepot": "13055",
    "idNatureDossier": None,
    "idProjet": "",
    "idTypeDossier": None,
    "lieuEngagementAAvoirQualite": "",
    "noLocal": "",
    "txDescriptifGlobal": "Je souhaite connaître la législation en vigueur",
    "txDescriptifPiecesFournies": "Néant",
}

personne_template = {
    "boEstPersonneMorale": False,
    "boOkTransmissionDemat": True,
    "boOppositionUtilisationDonneesPersonnelles": True,
    "dtNaissance": date.today(),
    "idGenre": 1,
    "libCommuneNaissance": "La Plaine-des-Palmistes",
    "libPaysNaissance": "France",
    "noDepartementNaissance": "974",
    "nom": "DUPONT-DUBOIS",
    "prenom": "Jean-Sébastien",
    "roles": [{"idRole": 1}, {"idRole": 10}]
    # "libDenomination": "Entreprise de conseils informatiques",
    # "libRaisonSociale": "YAML",
    # "libTypeSociete": "SARL",
}

personne_morale_template = {
    "boEstPersonneMorale": True,
    "boOkTransmissionDemat": True,
    "boOppositionUtilisationDonneesPersonnelles": True,
    "idGenre": 2,
    "libDenomination": "MTES",
    "libRaisonSociale": "MINISTERE DE LA TRANSITION ECOLOGIQUE ET SOLIDAIRE",
    "codeCategorieJuridique": "Administration de l'état",
    "siret": "11006801200050",
    "roles": [{"idRole": 1}],
}

terrain_template = {
    "noTerrain": "64",
    "surfTerrainCERFA": 1490,
    "refCadastrales": [
        {
            "noOrdre": 1,
            "prefixeReference": "898",
            "sectionReference": "AH",
            "noReference": "0215",
        }
    ],
}

piece_template = {
    "dtProduction": date.today(),
    "idNaturePiece": 1,
    "idTypePiece": None,
    "noPiece": "1",
    "referenceContenu": "ZRTY34",
}


dossier_complet_template = {
    "annexes": [
        {
            "boAgregationPourRetroCompatibilite": True,
            "idDestinationFiscale": 1,
            "idTypeAnnexe": 3,
            "idTypeFinancementFiscal": 1,
            "surfAnnexe": 10,
        }
    ],
    "batiments": [{"nbPiecesMaison": 5, "niveauMax": 2, "noBatiment": 22}],
    "boAtteinteDuSMD": False,
    "boDivisionAvantAchevement": False,
    "boMaisonIndividuelle": True,
    "boTerrassement": True,
    "dtApproxConstructionBatimentsDemolis": date.today(),
    "dtEngagementAAvoirQualite": date.today(),
    "dtLimiteInstruction": date.today(),
    "idFinalitePrevue": 1,
    "idNatureTravaux": 2,
    "idPrecisionNatTrav": 3,
    "idTypeDemolition": 1,
    "idUsagePrevuLocaux": 1,
    "libAutreNatureTravaux": "Autre",
    "lieuEngagementAAvoirQualite": "Lieu",
    "nbLogementsCrees": 1,
    "nbLogementsCreesDansBatimentCollectif": 0,
    "nbLogementsCreesDansBatimentIndiv": 1,
    "nbLogementsDemolis": 0,
    "nbPlacesStationnementExternes": 2,
    "niveauMaxBatiments": 2,
    "surfConstructibleTotale": 120,
    "surfConstruiteConservee": 0,
    "surfFiscaleAnnexes": 30,
    "surfInstructionAgregees": [
        {
            "idDestinationInstruction": 1,
            "idSousDestinationInstruction": 1,
            "idStatutInformation": 1,
            "idStatutVuDuDossier": 1,
            "surfInstruction": 120,
        }
    ],
    "surfTotaleDesAmenagements": 154,
    "txDescriptifBatimentsProjetes": "Maison ossature bois",
    "txObservations": "Néant",
    "txObservationsMairie": "Rien à dire",
}


class BeBJ0TestCase(BaseTestCase):
    """."""

    def test_01(self):
        u"""Versement  d'un projet."""
        print("test_01")
        print("-------")
        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

    def test_02(self):
        u"""Versement d'un dossier avec incomplet."""
        # In this test case,  we are expecting a Bad Request response,
        # we need to avoid request valdiation
        print("test_02")
        print("-------")
        self.app = PlatAUConnector(
            self.spec_path, validate_requests=False, validate_responses=False
        )

        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        # remote_dossier = self._get_remote_dossier('CU0130552000001')

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "CU0130552000001"
        # 1 == CUa
        dossier["idTypeDossier"] = 1
        # 1 == intiial
        dossier["idNatureDossier"] = 1

        # Dossier is valid, let's invalid that
        dossier["noLocal"] = ""

        try:
            dossier_uid = self.app.create_dossier(dossier)
            print("Dossier created : {}".format(dossier_uid))
        except Exception as e:
            self.assertTrue(type(e) in (HTTPInternalServerError, HTTPBadRequest))
            # import ipdb; ipdb.set_trace()
            self.assertTrue("noLocal: must match" in e.response.json()["message"])
            print(e)
            return
        self.assertTrue("False")

    def test_03(self):
        u"""Versement d'un dossier de type PCMI avec les données minimales."""
        print("test_03")
        print("-------")
        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        # remote_dossier = self._get_remote_dossier('CU0130552000001')

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "CU0130552000001"
        # 1 == CUa
        dossier["idTypeDossier"] = 1
        # 1 == intiial
        dossier["idNatureDossier"] = 1

        dossier_uid = self.app.create_dossier(dossier)[0]
        print("Dossier created : {}".format(dossier_uid))

        # Now we have a Dossier, we sould be able to retreive it
        # r_ == remote
        r_dossier = self.app.get_dossier_by_uid(dossier_uid)
        self.assertEqual(r_dossier.noLocal, dossier["noLocal"])

    def test_04(self):
        """Versement d'un dossier de type PCMI avec une personne physique et une parcelle cadastrale."""
        print("test_04")
        print("-------")
        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        # remote_dossier = self._get_remote_dossier('CU0130552000001')

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "PC0130552000001"
        # 4 == PC
        dossier["idTypeDossier"] = 4
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        personne = personne_template.copy()
        dossier["personnes"] = [personne]
        terrain = terrain_template.copy()
        dossier["terrains"] = [terrain]

        dossier_uid = self.app.create_dossier(dossier)[0]
        print("Dossier created : {}".format(dossier_uid))

        # Now we have dossier, we sould be able to retreive it
        # r_ == remote
        r_dossier = self.app.get_dossier_by_uid(dossier_uid)
        self.assertEqual(r_dossier.noLocal, dossier["noLocal"])

        # r_ == remote
        r_personne = r_dossier.personnes[0]
        self.assertEqual(r_personne.nom, personne["nom"])
        self.assertEqual(r_personne.prenom, personne["prenom"])

        # XXX
        # r_terrain = self.app.get_references_cadastrales(dossier_uid)
        # import ipdb; ipdb.set_trace()
        # self.assertEqual(r_terrain.refCadastrales[0].prefixeReference, terrain['refCadastrales']['prefixeReference'])
        # self.assertEqual(r_terrain.refCadastrales[0].sectionReference, terrain['refCadastrales']['sectionReference'])
        # self.assertEqual(r_terrain.refCadastrales[0].noReference, terrain['refCadastrales']['noReference'])

    def test_05(self):
        u"""Versement d'un dossier de type PCMI avec une personne morale et deux parcelles cadastrales."""
        print("test_05")
        print("-------")
        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        # remote_dossier = self._get_remote_dossier('CU0130552000001')

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "PC0130552000001"
        # 4 == PC
        dossier["idTypeDossier"] = 4
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        personne = personne_morale_template.copy()
        dossier["personnes"] = [personne]
        terrain = terrain_template.copy()
        terrain["refCadastrales"].append(
            {
                "noOrdre": 2,
                "prefixeReference": "898",
                "sectionReference": "0H",
                "noReference": "0215",
            }
        )
        dossier["terrains"] = [terrain]

        dossier_uid = self.app.create_dossier(dossier)[0]
        print("Dossier created : {}".format(dossier_uid))

        # Now we have dossier, we sould be able to retreive it
        # r_ == remote
        r_dossier = self.app.get_dossier_by_uid(dossier_uid)
        self.assertEqual(r_dossier.noLocal, dossier["noLocal"])

        # import ipdb; ipdb.set_trace()
        # r_ == remote
        r_personne = r_dossier.personnes[0]
        self.assertEqual(r_personne.libDenomination, personne["libDenomination"])
        self.assertEqual(r_personne.libRaisonSociale, personne["libRaisonSociale"])
        self.assertEqual(
            r_personne.codeCategorieJuridique, personne["codeCategorieJuridique"]
        )
        self.assertEqual(r_personne.siret, personne["siret"])

    def test_06(self):
        u"""Création d'un dossier de type PCMI avec les données un maximun de données (y compris des surfaces) et de données structurées de pièces constitutives."""
        print("test_06")
        print("-------")
        # import ipdb; ipdb.set_trace(); return

        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "PC0130552000023"
        # 4 == PC
        dossier["idTypeDossier"] = 4
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        demandeur = personne_template.copy()
        codemandeur = personne_template.copy()
        codemandeur["nom"] = "BON"
        codemandeur["prenom"] = "Jean"
        dossier["personnes"] = [demandeur, codemandeur]
        terrain = terrain_template.copy()
        terrain["refCadastrales"].append(
            {
                "noOrdre": 2,
                "prefixeReference": "898",
                "sectionReference": "0H",
                "noReference": "0215",
            }
        )
        dossier["terrains"] = [terrain]

        dossier.update(dossier_complet_template.copy())

        dossier_uid = self.app.create_dossier(dossier)[0]
        print("Dossier created : {}".format(dossier_uid))

        pieces = {"idDossier": dossier_uid, "noVersion": 1, "pieces": []}

        types_piece = self.app.get_nomenclature("TYPE_PIECE")
        for type_piece in range(10):
            piece = piece_template.copy()
            piece["idTypePiece"] = choice(range(103))
            pieces["pieces"].append(piece)
            print(
                "id : {} - type : {}".format(
                    piece["idTypePiece"], types_piece[piece["idTypePiece"]].libNom
                )
            )

        result = self.app.create_piece(pieces)

    def test_07(self):
        u"""Versement d'un dossier de type PCMI avec un demandeur et un co demandeur."""
        print("test_07")
        print("-------")
        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        # remote_dossier = self._get_remote_dossier('CU0130552000001')

        dossier = dossier_template.copy()
        dossier["idProjet"] = projet_uid
        dossier["idProjet"] = projet_uid
        dossier["noLocal"] = "PC0130552000001"
        # 4 == PC
        dossier["idTypeDossier"] = 4
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        demandeur = personne_template.copy()
        codemandeur = personne_template.copy()
        codemandeur["nom"] = "BON"
        codemandeur["prenom"] = "Jean"
        dossier["personnes"] = [demandeur, codemandeur]
        terrain = terrain_template.copy()
        dossier["terrains"] = [terrain]

        dossier_uid = self.app.create_dossier(dossier)[0]
        print("Dossier created : {}".format(dossier_uid))

        # Now we have dossier, we sould be able to retreive it
        # r_ == remote
        r_dossier = self.app.get_dossier_by_uid(dossier_uid)
        self.assertEqual(r_dossier.noLocal, dossier["noLocal"])

        # r_ == remote
        r_demandeur = r_dossier.personnes[0]
        self.assertEqual(r_demandeur.nom, demandeur["nom"])
        self.assertEqual(r_demandeur.prenom, demandeur["prenom"])

        # r_ == remote
        r_codemandeur = r_dossier.personnes[1]
        self.assertEqual(r_codemandeur.nom, codemandeur["nom"])
        self.assertEqual(r_codemandeur.prenom, codemandeur["prenom"])

    def test_08(self):
        u"""Versement en masse de 5 dossiers de type PCMI."""
        print("test_08")
        print("-------")

        dossier = dossier_template.copy()
        # 4 == PC
        dossier["idTypeDossier"] = 4
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        personne = personne_template.copy()
        dossier["personnes"] = [personne]
        terrain = terrain_template.copy()
        dossier["terrains"] = [terrain]

        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        dossiers = []
        for item in range(5):

            dossier["idProjet"] = projet_uid
            dossier["noLocal"] = "PC01305520000{}0".format(item)
            dossiers.append(dossier.copy())

        dossier_uids = self.app.create_dossier(dossiers)
        print("Dossiers created : {}".format(dossier_uids))

        # for uid in dossier_uids:
        #     r_dossier = self.app.get_dossier_by_uid(uid)
        #     self.assertEqual(r_dossier.noLocal, dossiers['noLocal'])

    def test_09(self):
        u"""Versement en masse de 5 dossiers de type DPMI."""
        print("test_09")
        print("-------")

        dossier = dossier_template.copy()
        # 3 == DP
        dossier["idTypeDossier"] = 3
        # 1 == intiial
        dossier["idNatureDossier"] = 1
        personne = personne_template.copy()
        dossier["personnes"] = [personne]
        terrain = terrain_template.copy()
        dossier["terrains"] = [terrain]

        projet_uid = self.app.create_projet(projet_template)[0]
        print("Projet created : {}".format(projet_uid))

        dossiers = []
        for item in range(5):

            dossier["idProjet"] = projet_uid
            dossier["noLocal"] = "PC01305520000{}0".format(item)
            dossiers.append(dossier.copy())

        dossier_uids = self.app.create_dossier(dossiers)
        print("Dossiers created : {}".format(dossier_uids))
