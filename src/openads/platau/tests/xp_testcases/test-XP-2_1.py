# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from random import choice

from openads.platau.tests.base_test import BaseTestCase
from openads.platau.platau_connector import PlatAUConnector
from bravado.exception import HTTPInternalServerError, HTTPBadRequest


# Identifiants
# Organisation      typeActeur  idCommuneDepot
# --------------------------------------
# Laon              1           PYW-8EY-132
# Laon              2           52X-NQL-XJ4
# Laon              4           471-OV9-XMV
# Laon              4           471-OV9-XMV
# DDT-02            13          YX3-QNR-DX8

# TODO : use object config
ACTEURS = {
    "02408": {
        "commune": {
            "guichet_unique": "71Z-59O-YWQ",
            "service_instructeur": "YX3-QR3-NX8",
            "autorite_competente": "GXG-PJ2-OX3",
        },
        "etat": {"service_instructeur": "YX3-QNR-DX8",},
    },
    # "13055": {
    #     "commune": {
    #         "guichet_unique": "8XP-G2Z-2XP",
    #         "service_instructeur": "2XN-Z0D-VXJ",
    #         "autorite_competente": "YW8-DV2-3W3",
    #     },
    #     "etat": {
    #         "service_instructeur": "571-M6E-NXZ",
    #         "autorite_competente": "3Y1-LPM-4XP",
    #     },
    # },
}


# Set this value to target specific Dossier
NUMERO_DOSSIER = "DP0024081800001"

BATCH_01 = {
    "91D-LJP-EW4": ["91K-EDE-41J", "VX2-V8V-OXE"],
    "71M-DZ7-R1Z": ["71M-DZD-Z1Z", "E10-3N3-81Z"],
    "515-J8E-51L": ["KWE-J6J-GX6", "91D-LJL-GW4"],
    "YX3-Q80-ZX8": ["71Z-5D5-KWQ", "GXG-P0D-EX3"],
    "EWV-60V-E1Y": ["RXQ-GQP-21Q", "MWJ-593-LWP"],
}
BATCH_02 = {
    "8XP-33R-2WP": [],
    "YW8-YY0-3W3": [],
    "71O-ZZZ-NWM": [],
    "Y1L-99V-OWP": [],
    "71O-ZZ9-3WM": [],
}
BATCH_03 = {}


class Sequence0(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        super().setUp()

    def test_status(self):
        """."""
        self.assertTrue(self.app.status())

    def test_01_XP21_creation_DA(self):
        u"""."""
        print("test_creation_DA")
        print("----------------")

        criterions = {"typology": "creation_DA", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        external_uids = {}
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            dossier = task["dossier"]

            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)
            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            projet_uid = self.app.create_projet(task)[0]

            print("Projet created : {}".format(projet_uid))
            external_uids[dossier] = projet_uid

            if not self._update_task(task_id, "done", projet_uid):
                raise RuntimeError

        print("Projet(s) created : {}".format(external_uids))

    def test_02_XP21_creation_DI(self):
        u"""."""
        print("test_creation_DI")
        print("----------------")

        criterions = {"typology": "creation_DI", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        external_uids = {}
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            dossier = task["dossier"]

            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uid"]["dossier_autorisation"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            dossier_uid = self.app.create_dossier(task)[0]

            print("Dossier created : {}".format(dossier_uid))
            external_uids[dossier] = dossier_uid

            if not self._update_task(task_id, "done", dossier_uid):
                raise RuntimeError

        print("Dossier(s) created : {}".format(external_uids))

    # def test_02_XP21_get_dossier(self):

    def test_03_XP21_ajout_piece(self):
        u"""."""
        print("test_ajout_piece")
        print("----------------")

        criterions = {"typology": "ajout_piece", "state": "new"}

        # XXX : specific docuemnt_numerise id
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        tasks = self._get_tasks(criterions)
        working_dossiers = set([task["dossier"] for task in tasks])

        # TODO : move to task by task, no need to group tasks by Dossier
        for numero_dossier in working_dossiers:
            curated_tasks = []
            criterions["numero_dossier"] = numero_dossier
            tasks = self._get_tasks(criterions)
            for task in tasks:

                task_id = task["task"]
                print("Working on task : {}".format(task_id))
                task = self._get_task(task_id=task_id)

                # XXX : if  we don't have a DI external UID, we can't add a Piece
                if task["external_uid"]["dossier"] == "":
                    continue

                if not self._update_task(task_id, "pending"):
                    raise RuntimeError

                file_name, file_path = self._get_file(task_id)

                insee = task["dossier"]["insee"]
                task["task"]["acteur"] = ACTEURS[insee]["commune"]["guichet_unique"]
                task["document_numerise"]["local_file_path"] = file_path
                task["document_numerise"]["file_name"] = file_name

                curated_tasks.append(task)

            piece_uids = self.app.create_piece(curated_tasks)

            # XXX : this is baaaad : we presume the order is peacefuly kept ...
            # We should handle file by file creation to have the right external uid for sure
            i = 0
            for task in curated_tasks:
                task_id = task["task"]["task"]
                if not self._update_task(task_id, "done", piece_uids[i]):
                    raise RuntimeError
                i += 1

            print("Piece(s) added : {}".format(piece_uids))

        print("----------------")

    def test_verify_03_XP21_ajout_piece(self):
        """."""
        dossiers = BATCH_01.keys()
        message = ""
        for uid in dossiers:
            result = self.app.get_pieces(uid)
            piece_uids = [piece.idPiece for piece in result]
            message += "Dossier {} : {} piece(s) \n".format(uid, len(result))
            message += "{}\n".format(piece_uids)
        print(message)

    def test_04_XP21_depot_DI(self):
        """."""
        print("test_depot_DI")
        print("-------------")

        criterions = {"typology": "depot_DI", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        for task in self._get_tasks(criterions):

            task_id = task["task"]
            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uid"]["dossier"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            result = self.app.depot_dossier(task)

            if not self._update_task(task_id, "done"):
                raise RuntimeError

            print("Depot OK : {}".format(result))

        print("----------------")

    def test_06_XP21_qualification_DI(self):
        u"""."""
        print("test_qualification_DI")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS

        criterions = {"typology": "qualification_DI", "state": "new"}

        # XXX : specific docuemnt_numarise id
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        errors = []
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uid"]["dossier"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            insee = task["dossier"]["insee"]
            if task["dossier"]["autorite_competente_code"] == "ETAT":
                competence = "etat"
            elif task["dossier"]["autorite_competente_code"] == "ETATMAIRE":
                competence = "etat"
            elif task["dossier"]["autorite_competente_code"] == "COM":
                competence = "commune"
            else:
                raise ValueError("Found an unknown Competence")

            task["task"]["acteur"] = ACTEURS[insee][competence]["service_instructeur"]
            # task["task"]["acteur"] = ACTEURS[insee]["commune"]["service_instructeur"]

            # import ipdb;ipdb.set_trace()

            try:
                result = self.app.qualification_dossier(task)
            except:
                errors.append(task_id)
                continue

            if not self._update_task(task_id, "done"):
                raise RuntimeError

            print("Dossier qualified : {}".format(result))

        print("Error on tasks : {}".format(errors))

        print("----------------")
