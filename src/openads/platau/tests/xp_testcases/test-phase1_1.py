# -*- coding: utf-8 -*-

import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from random import choice

from openads.platau.tests.base_test import BaseTestCase
from openads.platau.platau_connector import PlatAUConnector
from bravado.exception import HTTPInternalServerError, HTTPBadRequest


# Identifiants des communes
# Commune         typeActeur  idCommuneDepot
# --------------------------------------
# Marseille       1           PYW-8EY-132
# Marseille       2           52X-NQL-XJ4
# Marseille       4           471-OV9-XMV
# Aix-en-Provence 1           8MW-J7G-XPR
# Aix-en-Provence 2           RLX-6LR-W9D
# Aix-en-Provence 4           48X-PPV-XPZ

TYPE_ACTEUR = [
    {
        "idNom": 1,
        "libNom": "Guichet unique",
        "dtMiseEnPlace": "2020-01-01",
        "dtObsolescence": None,
    },
    {
        "idNom": 2,
        "libNom": "Service instructeur",
        "dtMiseEnPlace": "2020-01-02",
        "dtObsolescence": None,
    },
    {
        "idNom": 4,
        "libNom": "Autorité compétente",
        "dtMiseEnPlace": "2020-01-04",
        "dtObsolescence": None,
    },
]

# TODO : use object config
ACTEURS = {
    "13055": {
        "commune": {
            "guichet_unique": "8XP-G2Z-2XP",
            "service_instructeur": "2XN-Z0D-VXJ",
            "autorite_competente": "YW8-DV2-3W3",
        },
        "etat": {
            "service_instructeur": "571-M6E-NXZ",
            "autorite_competente": "3Y1-LPM-4XP",
        },
    },
    "13001": {
        "commune": {
            "guichet_unique": "R17-MEJ-9XO",
            "service_instructeur": "YX3-EOP-PX8",
            "autorite_competente": "71Z-YL8-NWQ",
        },
        "etat": {
            "service_instructeur": "571-M6E-NXZ",
            "autorite_competente": "3Y1-LPM-4XP",
        },
    },
}

DOSSIERS = {
    "DP0132012000001": {
        "tasks": {"creation_DA": 33, "creation_DI": 34,},
        "external_uids": {
            "dossier_autorisation": "Y1L-JRK-4XP",
            "dossier": "",
            "document_numerise": "",
        },
        "acteurs": {
            "guichet_unique": "PYW-8EY-132",
            "service_instructeur": "52X-NQL-XJ4",  # service instructeur Marseille
            "autorite_competente": "471-OV9-XMV",  # Autorite competente Marseille
        },
    }
}

# Set this value to target specific Dossier
# NUMERO_DOSSIER = "DP0130012000012"
# NUMERO_DOSSIER = "DP0130012000009"
NUMERO_DOSSIER = "PC0130551809101"


class Sequence0(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        super().setUp()

    def test_status(self):
        """."""
        self.assertTrue(self.app.status())

    def test_01_s0_creation_DA(self):
        u"""."""
        print("test_creation_DA")
        print("----------------")

        criterions = {"typology": "creation_DA", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        external_uids = {}
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            dossier = task["dossier"]

            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)
            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            projet_uid = self.app.create_projet(task)[0]

            print("Projet created : {}".format(projet_uid))
            external_uids[dossier] = projet_uid

            if not self._update_task(task_id, "done", projet_uid):
                raise RuntimeError

        print("Projet(s) created : {}".format(external_uids))

    def test_02_s0_creation_DI(self):
        u"""."""
        print("test_creation_DI")
        print("----------------")

        criterions = {"typology": "creation_DI", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        external_uids = {}
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            dossier = task["dossier"]

            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uids"]["dossier_autorisation"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            dossier_uid = self.app.create_dossier(task)[0]

            print("Dossier created : {}".format(dossier_uid))
            external_uids[dossier] = dossier_uid

            if not self._update_task(task_id, "done", dossier_uid):
                raise RuntimeError

        print("Dossier(s) created : {}".format(external_uids))

    def test_03_s0_ajout_piece(self):
        u"""."""
        print("test_ajout_piece")
        print("----------------")

        criterions = {"typology": "ajout_piece", "state": "new"}

        # XXX : specific docuemnt_numerise id
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        tasks = self._get_tasks(criterions)
        working_dossiers = set([task["dossier"] for task in tasks])

        # TODO : move to task by task, no need to group tasks by Dossier
        for numero_dossier in working_dossiers:
            curated_tasks = []
            criterions["numero_dossier"] = numero_dossier
            tasks = self._get_tasks(criterions)
            for task in tasks:

                task_id = task["task"]
                print("Working on task : {}".format(task_id))
                task = self._get_task(task_id=task_id)

                # XXX : if  we don't have a DI external UID, we can't add a Piece
                if task["external_uids"]["dossier"] == "":
                    continue

                if not self._update_task(task_id, "pending"):
                    raise RuntimeError

                file_name, file_path = self._get_file(task_id)

                insee = task["dossier"]["insee"]
                task["task"]["acteur"] = ACTEURS[insee]["commune"]["guichet_unique"]
                task["document_numerise"]["local_file_path"] = file_path
                task["document_numerise"]["file_name"] = file_name

                curated_tasks.append(task)

            piece_uids = self.app.create_piece(curated_tasks)

            # XXX : this is baaaad : we presume the order is peacefuly kept ...
            # We should handle file by file creation to have the right external uid for sure
            i = 0
            for task in curated_tasks:
                task_id = task["task"]["task"]
                if not self._update_task(task_id, "done", piece_uids[i]):
                    raise RuntimeError
                i += 1

            print("Piece(s) added : {}".format(piece_uids))

        print("----------------")

    def test_verify_03_s0_ajout_piece_initiale(self):
        """."""
        dossiers = (
            "EWV-VJ7-YWY",
            "RXQ-DK9-51Q",
            "Y1L-YO3-LXP",
            "LX6-K3G-PX9",
            "2XN-RMN-VWJ",
            "YX3-045-QX8",
            "R17-J53-8XO",
        )
        message = ""
        for uid in dossiers:
            result = self.app.get_pieces(uid, "Complémentaire")
            piece_uids = [piece.idPiece for piece in result]
            message += "Dossier {} : {} piece(s) complémentaire(s)\n".format(
                uid, len(result)
            )
            message += "{}\n".format(piece_uids)
        print(message)

    def test_verify_03_s0_ajout_piece(self):
        """."""
        dossiers = (
            "EWV-VJ7-YWY",
            "RXQ-DK9-51Q",
            "Y1L-YO3-LXP",
            "LX6-K3G-PX9",
            "2XN-RMN-VWJ",
            "YX3-045-QX8",
            "R17-J53-8XO",
        )
        message = ""
        for uid in dossiers:
            result = self.app.get_pieces(uid, "Complémentaire")
            piece_uids = [piece.idPiece for piece in result]
            message += "Dossier {} : {} piece(s) complémentaire(s)\n".format(
                uid, len(result)
            )
            message += "{}\n".format(piece_uids)
        print(message)

    def test_04_s0_depot_DI(self):
        """."""
        print("test_depot_DI")
        print("-------------")

        criterions = {"typology": "depot_DI", "state": "new"}

        # XXX : specific Dossier
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        for task in self._get_tasks(criterions):

            task_id = task["task"]
            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uids"]["dossier"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            result = self.app.depot_dossier(task)

            if not self._update_task(task_id, "done"):
                raise RuntimeError

            print("Depot OK : {}".format(result))

        print("----------------")

    def test_06_s0_qualification_DI(self):
        u"""."""
        print("test_qualification_DI")
        print("----------------")

        # Get the data from openADS
        # XXX - use data straight from openADS

        criterions = {"typology": "qualification_DI", "state": "new"}

        # XXX : specific docuemnt_numarise id
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        errors = []
        for task in self._get_tasks(criterions):

            task_id = task["task"]
            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX : if  we don't have DA external UID, we can't create a DI
            if task["external_uids"]["dossier"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            insee = task["dossier"]["insee"]
            if task["dossier"]["autorite_competente_code"] == "ETAT":
                competence = "etat"
            elif task["dossier"]["autorite_competente_code"] == "ETATMAIRE":
                competence = "etat"
            elif task["dossier"]["autorite_competente_code"] == "COM":
                competence = "commune"
            else:
                raise ValueError("Found an unknown Competence")

            task["task"]["acteur"] = ACTEURS[insee][competence]["service_instructeur"]
            # task["task"]["acteur"] = ACTEURS[insee]["commune"]["service_instructeur"]

            # import ipdb;ipdb.set_trace()

            try:
                result = self.app.qualification_dossier(task)
            except:
                errors.append(task_id)
                continue

            if not self._update_task(task_id, "done"):
                raise RuntimeError

            print("Dossier qualified : {}".format(result))

        print("Error on tasks : {}".format(errors))

        print("----------------")

    def test_07_s0_decision_DI(self):
        u"""."""
        print("test_decision_DI")
        print("----------------")

        criterions = {"typology": "decision_DI", "state": "new"}

        # XXX : specific document_numarise id
        # criterions["numero_dossier"] = NUMERO_DOSSIER

        for task in self._get_tasks(criterions):

            task_id = task["task"]
            print("Working on task : {}".format(task_id))
            task = self._get_task(task_id=task_id)

            # XXX
            if task["external_uids"]["dossier"] == "":
                continue

            if not self._update_task(task_id, "pending"):
                raise RuntimeError

            # We should provide binary files only if the Decision is not *Tacite*
            if task["instruction"]["tacite"] == "f":
                file_name, file_path = self._get_file(task_id)
                task["instruction"]["local_file_path"] = file_path
                task["instruction"]["file_name"] = file_name

            insee = task["dossier"]["insee"]

            if task["dossier"]["autorite_competente_code"] == "ETAT":
                competence = "etat"
                signataire = "etat"
            elif task["dossier"]["autorite_competente_code"] == "ETATMAIRE":
                competence = "etat"
                signataire = "commune"
            elif task["dossier"]["autorite_competente_code"] == "COM":
                competence = "commune"
                signataire = "commune"
            else:
                raise ValueError("Found an unknown Competence")

            task["task"]["acteur"] = ACTEURS[insee][competence]["autorite_competente"]
            task["task"]["signataire"] = ACTEURS[insee][signataire][
                "autorite_competente"
            ]

            result = self.app.decision(task)
            print("Decision added : {}".format(result))

            if not self._update_task(
                result[0]["task"],
                "done"
                # result[0]["task"], "done", result[0]["decision_uid"] # XXX
            ):
                raise RuntimeError

        print("----------------")

    def test_get_files(self):
        u"""."""
        print("test_decision_DI")
        print("----------------")

        dossier = self.app.get_dossier_by_numero(NUMERO_DOSSIER)
        self.assertTrue(dossier is not None)
        dossier_uid = dossier.idDossier
        pieces = self.app.get_pieces_binaries(dossier_uid)

        print(pieces)

        print("----------------")
