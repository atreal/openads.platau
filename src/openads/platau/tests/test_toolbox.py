import csv
import json
import os
import unittest
import requests
import time
import uuid

from base64 import b64decode, b64encode
from datetime import date
from datetime import datetime, timedelta
from pprint import PrettyPrinter


from openads.platau.tests.base_test import (
    BaseTestCase,
    REMOTE_URL,
    time_me,
    FakeArgs,
    CT_CONFIG_PATH,
    SC_CONFIG_PATH,
)
from openads.platau.base_objects import Actor
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.openads_mapper import ConsultationMapper
from openads.platau.tasks import InputTask
from bravado.exception import HTTPInternalServerError, HTTPBadRequest, HTTPUnauthorized
from openads.platau.conductor import Conductor, OAPConfig


FOLDER_PATH = os.getenv(
    "ct_platau_config_path", "/home/tiazma/Bureau/openADS/culture/Anomalies/TI#81"
)
ACTORS_CSV_PATH = os.getenv("platau_actors_csv_path", "acteurs_platau.csv")


ACTEURS = {
    "13055": {"guichet_unique": "8XP-G2Z-2XP", "service_instructeur": "2XN-Z0D-VXJ",},
    "32013": {"guichet_unique": "YX3-G82-PD1", "service_instructeur": "EWV-20J-VRW",},
    "44109": {"guichet_unique": "71M-R43-ZWZ", "service_instructeur": "7XY-2EP-0XZ",},
    "27004": {
        "guichet_unique": "8XP-YMY-2XP",
        "service_instructeur": "2XN-K2K-VXJ",
        "service_instructeur_ddt": "91D-GY5-E14",
    },
    "69243": {
        "guichet_unique": "91K-8O8-7WJ",
        "service_instructeur": "VX2-L2L-E1E",
        "service_instructeur_ddt": "71Z-V34-DWQ",
    },
    "07019": {
        "guichet_unique": "71M-O2O-6WZ",
        "service_instructeur": "E10-VQV-OWZ",
        "service_instructeur_ddt": "MWJ-879-GWP",
    },
    "culture": {
        "DEV-CRMH44": "O19-PR9-JWN",
        "DEV-SRA-PL": "R17-MRL-8XO",
        "DEV-SRA-ARA": "MWJ-PZP-7WP",
        "DEV-UDAP07": "52X-N0L-XJ4",
        "DEV-UDAP27": "RXQ-MKN-01Q",
        "DEV-UDAP44": "71Z-Y9O-KWQ",
        "DEV-UDAP69": "571-MY8-1Z4",
        # # XP
        "UDAP01": "71Z-ZKM-D1Q",
        "UDAP14": "71Z-ZKN-D1Q",
        "UDAP27": "71Z-ZKY-N1Q",
        "UDAP69": "91K-6LV-N1J",
        "UDAP76": "71M-M87-R1Z",
        "UDAP91": "91D-Z27-PX4",
        "SRA-ARA": "71M-M84-61Z",
        "SRA-NORMANDIE": "GXG-V9J-LX3",
    },
}

CONSULTATION = {
    "date_consultation": "27/11/2000",
    "service_consulte": None,
    "type_consultation": 1,
    "delai_reponse": 1,
}

DOSSIERS = {
    "A.1": {
        "insee": "32013",
        "acteurs": {
            "guichet_unique": ACTEURS["32013"]["guichet_unique"],
            "service_instructeur": ACTEURS["32013"]["service_instructeur"],
        },
        "tasks": {
            "creation_DA": 6,
            "creation_DI": 7,
            # "ajout_piece": [],
            "ajout_piece": [116, 118,],
            # "ajout_piece": [116],
            # "ajout_piece": [116, 117, 118, 119],
            "depot_dossier": 7,
            "qualification_DI": 8,
            "incompletude_DI": True,
            "completude_DI": True,
            "consultation": [ACTEURS["culture"]["UDAP01"],],
        },
        "external_uids": {"dossier_autorisation": "", "dossier": "", "pieces": {},},
        "local_number": "",
        "type": "DP",
        "competence": "commune",
    }
}


class OpenADSToolboxTest(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        # self.conf = OAPConfig(CT_CONFIG_PATH)
        # self.app = PlatAUConnector(
        #     self.conf.platau_credentials,
        #     self.conf.spec_path,
        #     spec_overrides=self.conf.spec_overrides
        # )
        if self.dummy_backend is None:
            self.dummy_backend = DOSSIERS.copy()
        # self.openads_url = self.conf.openadsapi_url
        # self.openads_auth = self.conf.openadsapi_config["auth"]
        # self.kwargs = FakeArgs()

    def test_00_debug(self):
        """."""
        print("Starting debug session")
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on
        print("Closing debug sesssion")

    def test_00_build_dataset(self):
        """."""
        self._build_dataset()

    def test_01_ConsultationMapper_from_files(self):
        """."""
        self.conf = OAPConfig(SC_CONFIG_PATH)
        self.app = PlatAUConnector(self.conf.platau_credentials, self.conf.spec_path,)

        json_payloads = {
            "consultation": None,
            "service_consultant": None,
            "infofiscales": None,
        }
        get_model = self.app.client.get_model

        for item in json_payloads:
            with open(f"{FOLDER_PATH}/{item}.json") as f:
                json_payloads[item] = json.load(f)

        mapped = ConsultationMapper(
            get_model("ReponseConsultationRecherche")(
                **json_payloads["consultation"][0]
            ),
            service_consultant=get_model("ReponseActeurRecherche")(
                **json_payloads["service_consultant"][0]
            ),
            infofiscales=get_model("ReponseInfoFiscalesRecherche")(
                **json_payloads["infofiscales"][0]
            ),
        ).render()
        pp = PrettyPrinter()
        pp.pprint(mapped)
        print(
            f"Donnees techniques : mapped {len(mapped['donnees_techniques'])} properties"
        )
        print(f"Dossier : mapped {len(mapped['dossier'])} properties")

    def test_02_create_acteurs_from_csv(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        self.app = PlatAUConnector(
            self.conf.platau_credentials,
            self.conf.spec_path,
            spec_overrides=self.conf.spec_overrides,
        )
        self.app.start()
        actors_out = []
        role_map = {
            "guichet_unique": "guichetsUniques",
            "service_instructeur": "servicesInstructeurs",
            "autorite_competente": "autoritesCompetentes",
        }
        headers = None
        with open(ACTORS_CSV_PATH, "r", newline="") as f:
            reader = csv.DictReader(f)
            for row in reader:

                actor = Actor(
                    role=row["role"],
                    label=row["label"],
                    email=row["email"],
                    siren=row["siren"],
                )
                if actor.role == "guichet_unique":
                    actor.insee = row["insee"]
                    actor.depot_adau = False

                # SI mono
                elif actor.role == "service_instructeur" and row["insee"] != "":
                    actor.insee = row["insee"]
                    actor.insee_competence = [row["insee"]]

                # SI multi
                elif actor.role == "service_instructeur" and row["insee"] == "":
                    actor.insee_competence = row["insee_competence"].split("/")

                elif actor.role == "autorite_competente":
                    actor.insee = row["insee"]
                    actor.insee_competence = [row["insee"]]

                print(actor)

                result = self.app.create_acteurs([actor])

                prop = role_map[actor.role]
                row["uid"] = getattr(result, prop)[0]["idActeur"]
                actors_out.append(row)

                if headers is None:
                    headers = row.keys()

        with open(ACTORS_CSV_PATH, "w", newline="") as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            for item in actors_out:
                writer.writerow(item)
