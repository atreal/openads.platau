# -*- coding: utf-8 -*-

import os
import base64

from openads.platau.tests.base_test import BaseTestCase, CT_CONFIG_PATH
from openads.platau.publik_connector import PublikConnector
from openads.platau.publik_conductor import PublikConductorConfig


class PublikConnectorTest(BaseTestCase):
    """This test case validate PlatAUConnetcor against current API.
        No interaction with openADS are done.
    """

    def setUp(self):
        """."""
        self.conf = PublikConductorConfig(CT_CONFIG_PATH)
        self.publik = PublikConnector(**self.conf.publik_config)

    def test_00_list_forms(self):
        """."""
        forms = self.publik.list_forms()
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on

    def test_01_create_suivi_demande(self):
        """."""
        demande = {
            "identite_prenom": "Jean",
            "identite_nom": "Dôt",
            "source_depot": "app",
            "coordonnees_courriel": "mbroquet@atreal.fr",
        }
        response = self.publik.create_demande("suivi-de-demande", demande)
        demande_id = f"suivi-de-demande/{response['id']}"
        print(demande_id)
        # demande_url = response["api_url"]

        # response = self.publik.trigger_transition(demande_id, "transferer_demande")

        # fmt: off
        # import ipdb;ipdb.set_trace()
        # fmt: on
        # data = {"recepisse_de_depot": _prepare_attachment("recepisse_de_depot.pdf")}

        # response = self.publik.trigger_transition(
        #     demande_url, "notifier_recepisse", data
        # )
        # print(response)

    def test_02_list_demandes(self):
        """."""
        # criterions = {"filter": "pending", "form_status": "En attente de récépissé"}
        # criterions = {"filter": "1"}
        criterions = {}
        response = self.publik.list_demandes(
            "certificat-urbanisme", criterions=criterions
        )
        print(response)
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on

    def test_02b_list_filtered_demandes(self):
        """."""
        criterions = {"filter": "just_submitted", "full": "on"}
        workflow_criterions = {"status": {"id": "just_submitted"}}
        response = self.publik.list_demandes(
            "pieces_complementaires",
            criterions,
            workflow_criterions=workflow_criterions,
        )
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on
        print(response)

    def test_03_get_demande(self):
        """."""
        response = self.publik.get_demande("certificat-urbanisme", "5")
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on


def _prepare_attachment(file_name):
    """."""
    # file_name = "recepisse_de_depot.pdf"
    file_path = "{}/binary_files/{}".format(
        os.path.dirname(os.path.abspath(__file__)), file_name
    )

    with open(file_path, "rb") as f:
        file_content = f.read()

    return {
        "filename": file_name,
        "b64_content": base64.encodebytes(file_content).decode("utf-8"),
        "content_type": "application/pdf",
    }
