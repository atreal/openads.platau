[
  {
    "amenagements": [
      {
        "attributsAmenagement": [
          {
            "nbPlacesStationnementNonClosesOuNonCouvertes": 20,
            "nomStatutInformation": 1,
            "nomStatutVuDuDossier": 2
          }
        ],
        "boAgregationPourRetroCompatibilite": true,
        "boPrexisteAuDossier": false,
        "nomTypeAmenagement": 12
      }
    ],
    "annexes": [
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 1,
        "nomTypeAnnexe": 1,
        "surfAnnexe": 21
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 2,
        "nomTypeAnnexe": 2,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 3,
        "nomTypeAnnexe": 3,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 4,
        "nomTypeAnnexe": 4,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "libAutreTypeAnnexe": "ggg",
        "noAnnexe": 5,
        "nomTypeAnnexe": 6,
        "surfAnnexe": 0
      }
    ],
    "boAccordUtilisationDonneesNominatives": true,
    "boConcerneBatimentHistorique": true,
    "boMaisonIndividuelle": true,
    "boRecoursAUnProfessionnel": false,
    "boRecoursAUnProfessionnelNonOblig": false,
    "boTerrassement": true,
    "dtEngagementAAvoirQualite": "2020-05-29",
    "equipementsPrives": [
      {
        "noEquipPrive": 1,
        "nomTypeEquipPrive": 1,
        "surfEquipPrive": 25
      }
    ],
    "idCommuneDepot": "44109",
    "idProjet": "ZZZ-ZZZ-ZZZ",
    "infoFiscalesOuStatistiquesAgregees": [
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "surfFiscaleHorsSCC": 20000,
        "surfFiscaleSCC": 5000
      },
      {
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 2,
        "surfFiscaleHorsSCC": 20000,
        "surfFiscaleSCC": 5000
      },
      {
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfaceFiscaleSSCIncluses": 400
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfaceFiscaleSSCIncluses": 500
      },
      {
        "nbLocaux": 50,
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfaceFiscaleSSCIncluses": 100
      }
    ],
    "lieuEngagementAAvoirQualite": "Nantes",
    "noLocal": "DP04410920XX002",
    "nomFinalitesPrevues": [
      2,
      3
    ],
    "nomInfosLegiConnexes": [
      5,
      6,
      7,
      8
    ],
    "nomNatureDossier": 1,
    "nomNaturesTravaux": [
      1,
      2,
      3
    ],
    "nomPrecisionsNatTrav": [
      1,
      2,
      3,
      4
    ],
    "nomTypeDossier": 3,
    "personnes": [
      {
        "adresseCourante": {
          "codePostal": "44230",
          "libTypeVoie": "Rue",
          "noVoie": "123",
          "nomLocalite": "Saint-S�bastien-sur-Loire",
          "nomPays": "France",
          "nomVoie": "Pablo Picasso"
        },
        "adresseFuture": {
          "codePostal": "44000",
          "libTypeVoie": "Avenue",
          "noBoitePostale": "321",
          "noCedex": "98",
          "noVoie": "234",
          "nomDivTerritoriale": "Territoire",
          "nomLieuDit": "Eraudi�re",
          "nomLocalite": "Nantes",
          "nomPays": "France",
          "nomVoie": "des Am�thystes"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": true,
        "coordonnees": [
          {
            "coordonnee": "0123456789",
            "nomTypeCoordonnee": 1
          },
          {
            "coordonnee": "alice.durant@nomail.com",
            "nomTypeCoordonnee": 3
          }
        ],
        "dtNaissance": "1980-04-13",
        "libCommuneNaissance": "Pornic",
        "libPaysNaissance": "France",
        "noDepartementNaissance": "44",
        "nomGenre": 2,
        "noms": [
          "Dupont"
        ],
        "prenoms": [
          "Sophie"
        ],
        "roles": [
          {
            "nomRole": 1
          }
        ]
      },
      {
        "adresseCourante": {
          "libTypeVoie": "rue",
          "noVoie": "345",
          "nomLocalite": " Londres",
          "nomPays": "Royaume Unis",
          "nomVoie": "Main Street"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": false,
        "coordonnees": [
          {
            "coordonnee": "1234567890",
            "indicatifPays": "44",
            "nomTypeCoordonnee": 1
          }
        ],
        "nomGenre": 2,
        "noms": [
          "Durant"
        ],
        "prenoms": [
          "Alice"
        ],
        "roles": [
          {
            "nomRole": 5
          }
        ]
      }
    ],
    "surfFiscaleAnnexes": 208,
    "surfInstructionAgregees": [
      {
        "nomDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 5
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 5
      },
      {
        "nomDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 4
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 4
      },
      {
        "nomDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 2
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 2
      },
      {
        "nomDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 7
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 7
      }
    ],
    "terrains": [
      {
        "adresses": [
          {
            "codePostal": "44109",
            "libTypeVoie": "All�e",
            "noVoie": "678",
            "nomLocalite": "Nantes",
            "nomPays": "France",
            "nomVoie": " C�zanne"
          }
        ],
        "boEstSitueDansUnLotissement": true,
        "noTerrain": "1",
        "refCadastrales": [
          {
            "noOrdre": 1,
            "noReference": "3001",
            "prefixeReference": "101",
            "sectionReference": "21",
            "surfParcelleCERFA": 4001
          },
          {
            "noOrdre": 2,
            "noReference": "3002",
            "prefixeReference": "102",
            "sectionReference": "22",
            "surfParcelleCERFA": 4002
          },
          {
            "noOrdre": 3,
            "noReference": "3003",
            "prefixeReference": "103",
            "sectionReference": "23",
            "surfParcelleCERFA": 4003
          },
          {
            "noOrdre": 4,
            "noReference": "3004",
            "prefixeReference": "104",
            "sectionReference": "24",
            "surfParcelleCERFA": 4004
          },
          {
            "noOrdre": 5,
            "noReference": "3005",
            "prefixeReference": "105",
            "sectionReference": "25",
            "surfParcelleCERFA": 4005
          },
          {
            "noOrdre": 6,
            "noReference": "3006",
            "prefixeReference": "106",
            "sectionReference": "26",
            "surfParcelleCERFA": 4006
          },
          {
            "noOrdre": 7,
            "noReference": "3007",
            "prefixeReference": "107",
            "sectionReference": "27",
            "surfParcelleCERFA": 4007
          },
          {
            "noOrdre": 8,
            "noReference": "3008",
            "prefixeReference": "108",
            "sectionReference": "28",
            "surfParcelleCERFA": 4008
          },
          {
            "noOrdre": 9,
            "noReference": "3009",
            "prefixeReference": "109",
            "sectionReference": "29",
            "surfParcelleCERFA": 4009
          },
          {
            "noOrdre": 10,
            "noReference": "3010",
            "prefixeReference": "110",
            "sectionReference": "30",
            "surfParcelleCERFA": 4010
          },
          {
            "noOrdre": 11,
            "noReference": "3011",
            "prefixeReference": "111",
            "sectionReference": "31",
            "surfParcelleCERFA": 4011
          },
          {
            "noOrdre": 12,
            "noReference": "3012",
            "prefixeReference": "112",
            "sectionReference": "32",
            "surfParcelleCERFA": 4012
          },
          {
            "noOrdre": 13,
            "noReference": "3013",
            "prefixeReference": "113",
            "sectionReference": "33",
            "surfParcelleCERFA": 4013
          },
          {
            "noOrdre": 14,
            "noReference": "3014",
            "prefixeReference": "114",
            "sectionReference": "34",
            "surfParcelleCERFA": 4014
          },
          {
            "noOrdre": 15,
            "noReference": "3015",
            "prefixeReference": "115",
            "sectionReference": "35",
            "surfParcelleCERFA": 4015
          },
          {
            "noOrdre": 16,
            "noReference": "3016",
            "prefixeReference": "116",
            "sectionReference": "36",
            "surfParcelleCERFA": 4016
          }
        ]
      }
    ],
    "txDescriptifGlobal": "Ceci est une courte description du projet ou des travaux",
    "txRenseignementsAFinaliteFiscale": " Renseignements � finalit� fiscale",
    "txsAutresPrecisionsNatTrav": "hhh"
  }
]