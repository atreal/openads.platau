[
  {
    "amenagements": [
      {
        "attributsAmenagement": [
          {
            "nbPlacesStationnementNonClosesOuNonCouvertes": 20,
            "nomStatutInformation": 1,
            "nomStatutVuDuDossier": 2
          }
        ],
        "boAgregationPourRetroCompatibilite": true,
        "boPrexisteAuDossier": false,
        "nomTypeAmenagement": 12
      }
    ],
    "annexes": [
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 1,
        "nomTypeAnnexe": 1,
        "surfAnnexe": 21
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 2,
        "nomTypeAnnexe": 2,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 3,
        "nomTypeAnnexe": 3,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 4,
        "nomTypeAnnexe": 4,
        "surfAnnexe": 0
      },
      {
        "boAgregationPourRetroCompatibilite": true,
        "noAnnexe": 5,
        "nomTypeAnnexe": 6,
        "surfAnnexe": 0
      }
    ],
    "batiments": [
      {
        "nbPiecesMaison": 36,
        "niveauMax": 72,
        "noBatiment": 1
      }
    ],
    "boAccordUtilisationDonneesNominatives": true,
    "boAtteinteDuSMD": true,
    "boConcerneBatimentHistorique": false,
    "boMaisonIndividuelle": true,
    "boRecoursAUnProfessionnel": true,
    "boRecoursAUnProfessionnelNonOblig": true,
    "boTerrassement": false,
    "dtApproxConstructionBatimentsDemolis": "1900-01-01",
    "dtEngagementAAvoirQualite": "2020-05-29",
    "equipementsPrives": [
      {
        "noEquipPrive": 2,
        "nomTypeEquipPrive": 1,
        "surfEquipPrive": 25
      }
    ],
    "idCommuneDepot": "44420",
    "idProjet": "ZZZ-ZZZ-ZZZ",
    "infoFiscalesOuStatistiquesAgregees": [
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "surfFiscaleHorsSCC": 20000,
        "surfFiscaleSCC": 5000
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfaceFiscaleSSCIncluses": 500
      },
      {
        "nbLocaux": 1,
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 1,
        "surfFiscaleHorsSCC": 201,
        "surfFiscaleSCC": 101
      },
      {
        "nbLocaux": 2,
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 2,
        "surfFiscaleHorsSCC": 202,
        "surfFiscaleSCC": 102
      },
      {
        "nbLocaux": 3,
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 3,
        "surfFiscaleHorsSCC": 203,
        "surfFiscaleSCC": 103
      },
      {
        "nbLocaux": 4,
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 4,
        "surfFiscaleHorsSCC": 204,
        "surfFiscaleSCC": 104
      },
      {
        "nbLocaux": 5,
        "nomDestinationFiscale": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "surfFiscaleHorsSCC": 205,
        "surfFiscaleSCC": 105
      },
      {
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 6
      },
      {
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 7
      },
      {
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 8
      },
      {
        "libAutreTypeFinancement": " J'ai gagn� au loto ;-))",
        "nomDestinationFiscale": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 2,
        "nomTypeFinancement": 9
      }
    ],
    "lieuEngagementAAvoirQualite": "La Turballe",
    "nbLogementsCrees": 18,
    "nbLogementsDemolis": 44,
    "noLocal": "PC04410920XX004",
    "nomFinalitesPrevues": [
      2,
      3,
      4,
      5
    ],
    "nomInfosLegiConnexes": [
      5,
      6,
      7,
      8
    ],
    "nomNatureDossier": 1,
    "nomNaturesTravaux": [
      1,
      2
    ],
    "nomPrecisionsNatTrav": [
      1,
      2,
      3
    ],
    "nomTypeDemolition": 1,
    "nomTypeDossier": 4,
    "personnes": [
      {
        "adresseCourante": {
          "codePostal": "44230",
          "libTypeVoie": "Rue",
          "noVoie": "123",
          "nomLocalite": "Saint-S�bastien-sur-Loire",
          "nomPays": "France",
          "nomVoie": "Pablo Picasso"
        },
        "adresseFuture": {
          "codePostal": "44000",
          "libTypeVoie": "Avenue",
          "noBoitePostale": "321",
          "noCedex": "98",
          "noVoie": "234",
          "nomDivTerritoriale": "Territoire",
          "nomLieuDit": "Eraudi�re",
          "nomLocalite": "Nantes",
          "nomPays": "France",
          "nomVoie": "des Am�thystes"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": true,
        "coordonnees": [
          {
            "coordonnee": "0123456789",
            "nomTypeCoordonnee": 1
          },
          {
            "coordonnee": "philippe.durant@nomail.com",
            "nomTypeCoordonnee": 3
          }
        ],
        "dtNaissance": "1980-04-13",
        "libCommuneNaissance": "Pornic",
        "libPaysNaissance": "France",
        "noDepartementNaissance": "44",
        "nomGenre": 1,
        "noms": [
          "Dupont"
        ],
        "prenoms": [
          "Pierre",
          "Jean-Marc",
          "Luc"
        ],
        "roles": [
          {
            "nomRole": 1
          }
        ]
      },
      {
        "adresseCourante": {
          "libTypeVoie": "rue",
          "noVoie": "345",
          "nomLocalite": " Londres",
          "nomPays": "Royaume Unis",
          "nomVoie": "Main Street"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": false,
        "coordonnees": [
          {
            "coordonnee": "1234567890",
            "indicatifPays": "44",
            "nomTypeCoordonnee": 1
          }
        ],
        "nomGenre": 1,
        "noms": [
          "Durant"
        ],
        "prenoms": [
          "Philippe"
        ],
        "roles": [
          {
            "nomRole": 5
          }
        ]
      },
      {
        "adresseCourante": {
          "codePostal": "38000",
          "libTypeVoie": "Square",
          "noVoie": "999",
          "nomLocalite": "Grenoble",
          "nomPays": "France",
          "nomVoie": "Joffre"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": false,
        "coordonnees": [
          {
            "coordonnee": "8765432109",
            "nomTypeCoordonnee": 1
          },
          {
            "coordonnee": "6543210987",
            "nomTypeCoordonnee": 2
          },
          {
            "coordonnee": "s.desmoulins@architectes.fr",
            "nomTypeCoordonnee": 3
          }
        ],
        "noms": [
          "Desmoulins"
        ],
        "prenoms": [
          "Sylvie"
        ],
        "roles": [
          {
            "libCROA": "Rh�ne-Alpes",
            "noInscriptionCROA": "161803",
            "nomRole": 6
          }
        ]
      },
      {
        "adresseCourante": {
          "codePostal": "4020",
          "libTypeVoie": "Boulevard",
          "noBoitePostale": "98",
          "noCedex": "99",
          "noVoie": "456",
          "nomDivTerritoriale": "Wallonnie",
          "nomLieuDit": "Quatres chemins",
          "nomLocalite": "Li�ge",
          "nomPays": "Belgique",
          "nomVoie": "du Jardin"
        },
        "boEstPersonneMorale": false,
        "boOkTransmissionDemat": false,
        "nomGenre": 2,
        "noms": [
          "Duval "
        ],
        "prenoms": [
          "Mathilde "
        ],
        "roles": [
          {
            "nomRole": 10
          }
        ]
      }
    ],
    "puissanceElectriqueRequise": "17",
    "surfInstructionAgregees": [
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 105
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 115
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 125
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 135
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 145
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 155
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 165
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 175
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 185
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 195
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 205
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 215
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 225
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 235
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 245
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 255
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 265
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 275
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 285
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 295
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 1,
        "surfInstruction": 4000
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 104
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 114
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 124
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 134
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 144
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 154
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 164
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 174
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 184
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 194
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 204
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 214
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 224
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 234
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 244
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 254
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 264
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 274
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 284
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 294
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 3,
        "surfInstruction": 3980
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 103
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 113
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 123
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 133
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 143
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 153
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 163
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 173
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 183
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 193
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 203
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 213
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 223
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 233
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 243
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 253
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 263
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 273
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 283
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 293
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 4,
        "surfInstruction": 3960
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 102
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 112
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 122
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 132
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 142
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 152
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 162
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 172
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 182
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 192
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 202
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 212
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 222
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 232
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 242
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 252
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 262
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 272
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 282
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 292
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 6,
        "surfInstruction": 3940
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 101
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 111
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 121
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 131
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 141
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 151
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 161
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 171
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 181
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 191
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 201
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 211
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 221
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 231
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 241
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 251
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 261
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 271
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 281
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 291
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 5,
        "surfInstruction": 3920
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 3,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 109
      },
      {
        "nomDestinationInstruction": 2,
        "nomSousDestinationInstruction": 4,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 119
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 1,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 129
      },
      {
        "nomDestinationInstruction": 1,
        "nomSousDestinationInstruction": 2,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 139
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 5,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 149
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 6,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 159
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 7,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 169
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 8,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 179
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 9,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 189
      },
      {
        "nomDestinationInstruction": 3,
        "nomSousDestinationInstruction": 10,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 199
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 11,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 209
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 12,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 219
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 13,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 229
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 14,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 239
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 15,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 249
      },
      {
        "nomDestinationInstruction": 4,
        "nomSousDestinationInstruction": 16,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 259
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 17,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 269
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 18,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 279
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 19,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 289
      },
      {
        "nomDestinationInstruction": 5,
        "nomSousDestinationInstruction": 20,
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 299
      },
      {
        "nomStatutInformation": 1,
        "nomStatutVuDuDossier": 7,
        "surfInstruction": 4080
      }
    ],
    "terrains": [
      {
        "adresses": [
          {
            "codePostal": "44420",
            "libTypeVoie": "All�e",
            "noVoie": "678",
            "nomLocalite": "La Turballe",
            "nomPays": "France",
            "nomVoie": " C�zanne"
          }
        ],
        "boEstSitueDansUnLotissement": false,
        "boEstSitueDansUneOIN": false,
        "boEstSitueDansUneZAC": false,
        "boExistenceCU": false,
        "noTerrain": "1",
        "refCadastrales": [
          {
            "noOrdre": 1,
            "noReference": "3001",
            "prefixeReference": "101",
            "sectionReference": "21",
            "surfParcelleCERFA": 4001
          },
          {
            "noOrdre": 2,
            "noReference": "3002",
            "prefixeReference": "102",
            "sectionReference": "22",
            "surfParcelleCERFA": 4002
          },
          {
            "noOrdre": 3,
            "noReference": "3003",
            "prefixeReference": "103",
            "sectionReference": "23",
            "surfParcelleCERFA": 4003
          },
          {
            "noOrdre": 4,
            "noReference": "3004",
            "prefixeReference": "104",
            "sectionReference": "24",
            "surfParcelleCERFA": 4004
          },
          {
            "noOrdre": 5,
            "noReference": "3005",
            "prefixeReference": "105",
            "sectionReference": "25",
            "surfParcelleCERFA": 4005
          },
          {
            "noOrdre": 6,
            "noReference": "3006",
            "prefixeReference": "106",
            "sectionReference": "26",
            "surfParcelleCERFA": 4006
          },
          {
            "noOrdre": 7,
            "noReference": "3007",
            "prefixeReference": "107",
            "sectionReference": "27",
            "surfParcelleCERFA": 4007
          },
          {
            "noOrdre": 8,
            "noReference": "3008",
            "prefixeReference": "108",
            "sectionReference": "28",
            "surfParcelleCERFA": 4008
          },
          {
            "noOrdre": 9,
            "noReference": "3009",
            "prefixeReference": "109",
            "sectionReference": "29",
            "surfParcelleCERFA": 4009
          },
          {
            "noOrdre": 10,
            "noReference": "3010",
            "prefixeReference": "110",
            "sectionReference": "30",
            "surfParcelleCERFA": 4010
          },
          {
            "noOrdre": 11,
            "noReference": "3011",
            "prefixeReference": "111",
            "sectionReference": "31",
            "surfParcelleCERFA": 4011
          },
          {
            "noOrdre": 12,
            "noReference": "3012",
            "prefixeReference": "112",
            "sectionReference": "32",
            "surfParcelleCERFA": 4012
          },
          {
            "noOrdre": 13,
            "noReference": "3013",
            "prefixeReference": "113",
            "sectionReference": "33",
            "surfParcelleCERFA": 4013
          },
          {
            "noOrdre": 14,
            "noReference": "3014",
            "prefixeReference": "114",
            "sectionReference": "34",
            "surfParcelleCERFA": 4014
          },
          {
            "noOrdre": 15,
            "noReference": "3015",
            "prefixeReference": "115",
            "sectionReference": "35",
            "surfParcelleCERFA": 4015
          },
          {
            "noOrdre": 16,
            "noReference": "3016",
            "prefixeReference": "116",
            "sectionReference": "36",
            "surfParcelleCERFA": 4016
          }
        ]
      }
    ],
    "txDescriptifGlobal": "Ceci est une courte description du projet ou des travaux",
    "txDescriptifTravauxSurConstructionRestante": "Ceci est une description des travaux effectu�s sur les constructions restantes",
    "txRenseignementsAFinaliteFiscale": " Renseignements � finalit� fiscale"
  }
]