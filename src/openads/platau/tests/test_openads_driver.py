# -*- coding: utf-8 -*-

from openads.platau.tests.base_test import (
    BaseTestCase,
    CT_CONFIG_PATH,
)
from openads.platau.conductor import OAPConfig
from openads.platau.openads_driver import OpenADSDriver


class OpenADSDriverTest(BaseTestCase):
    """This test case validate PlatAUConnetcor against current API.
        No interaction with openADS are done.
    """

    def setUp(self):
        """."""
        self.conf = OAPConfig(CT_CONFIG_PATH)
        self.openads = OpenADSDriver(**self.conf.openadsapi_config)

    def test_00_get_file(self):
        """."""
        aa = self.openads.get_file("6043")
        # fmt: off
        import ipdb;ipdb.set_trace()
        # fmt: on

    def test_00_dossier_exists(self):
        """WIP"""
        # We assume Dossier CU0130552100025P0 exists in the backend
        dossier_id = "CU0130552100026P0"
        result = self.openads.dossier_exists(dossier_id)
        self.assertTrue(result["existe"])
        result = self.openads.dossier_exists(dossier_id, response_format="publik")
        self.assertTrue(result["existe"])

    def test_01_depot_possible(self):
        """WIP"""
        # We assume Dossier CU0130552100025P0 exists in the backend and `dps` is OK
        dossier_id = "CU0130552100025P0"
        type_demande = "dps"
        result = self.openads.depot_possible(
            dossier_id, type_demande, response_format="publik"
        )
        self.assertTrue(result["depot_possible"])
        result = self.openads.depot_possible(dossier_id, type_demande)
        self.assertTrue(result["depot_possible"])
