import argparse
import csv
import logging
import sys

from bravado.exception import HTTPBadRequest
from datetime import datetime
from pathlib import Path

from openads.platau.base_objects import Actor
from openads.platau.conductor import OAPConfig
from openads.platau.platau_connector import PlatAUConnector
from openads.platau.openads_driver import OpenADSDriver
from openads.platau.utils import VALID_EMAIL


log = logging.getLogger(__name__)

BINARY_TASK_MAPPING = {
    "avis_consultation": {
        "OK": 59,
        "KO": 60,
        "external_uid_type": "avis_dossier_consultation",
    },
}

NO_DRY_RUN = ("status", "iplatau", "search_actors", "create_actors")


class PlatAUToolbox(object):
    """."""

    conf: OAPConfig
    connector: PlatAUConnector
    dry_run: bool = False

    def __init__(self, config: OAPConfig, kwargs=None) -> None:
        """."""
        self.conf = config
        # XXX - do we need openADS access at all ?
        self.connector = PlatAUConnector(
            config.platau_credentials,
            config.spec_path,
            spec_overrides=config.spec_overrides,
        )
        if kwargs is not None:
            self.kwargs = kwargs

        if kwargs.dry_run:
            self.dry_run = True

    def status(self):
        """."""
        logging.disable(sys.maxsize)
        print("> Plat'AU status <\n")
        try:
            self.connector.start()
            result = self.connector.status()
        except Exception:
            result = False
        if result:
            print("Plat'AU is UP, everything's fine\n")
        else:
            print("Plat'AU is DOWN, something is going on\n")

    def iplatau(self):
        """Provide a ipdb on Plat'AU"""
        print("> Plat'AU interactive PDB <\n")
        log.info("Starting Plat'AU session")
        platau = self.connector
        print("Use `platau` as main connector")
        actor = self.kwargs.actor
        print("`actor` is set with actor UID")
        platau.calling_actor = actor
        platau.start(actor)

        try:
            from ipdb import set_trace
        except ModuleNotFoundError:
            print("`ipdb` is not available. Using `pdb` instead.")
            from pdb import set_trace
        set_trace()

        log.info("Ending Plat'AU session")

    def search_actors(self):
        """."""
        log.info(f"Searching actors in Plat'AU")
        self.connector.start(self.kwargs.actor)

        criterions = {}
        for criterion, value in self.kwargs.query.items():
            if criterion not in ("idActeur",):
                print(f"Unkown criterion {criterion}")
                return -1
            criterions[criterion] = value.strip('"')

        results = self.connector.get_acteurs(criterions)

        print("--------------")
        print("Search results")
        print("--------------")
        print(results)

        log.info("Actors searching done.")

    def create_actors(self):
        """Create matching actors from a CSV file.
            Info are extracted from file found at `file_path`.
            `file_path` is updated with Plat'AU actor UID.
        """
        print("> Plat'AU actors creation <\n")
        file_path = self.kwargs.file_path
        try:
            f = open(file_path, "a")
        except PermissionError:
            log.error(f"Provided file {file_path} is not writable")
            return
        f.close()

        self.connector.start()
        actors_out = []
        role_map = {
            "guichet_unique": "guichetsUniques",
            "service_instructeur": "servicesInstructeurs",
            "autorite_competente": "autoritesCompetentes",
        }
        headers = None
        result = []
        with open(file_path, "r", newline="") as f:
            reader = csv.DictReader(f)
            for row in reader:

                actor = Actor(
                    role=row["role"],
                    label=row["label"],
                    email=row["email"],
                    siren=row["siren"],
                    organization_label=row["organization_label"],
                )
                if actor.role == "guichet_unique":
                    actor.insee = row["insee"]
                    actor.depot_adau = False
                    actor.extra_properties = (
                        {"sve_cgu_url": row["organization_label"]},
                    )

                # SI mono
                elif actor.role == "service_instructeur" and row["insee"] != "":
                    actor.insee = row["insee"]
                    actor.insee_competence = [row["insee"]]

                # SI multi
                elif actor.role == "service_instructeur" and row["insee"] == "":
                    actor.insee_competence = row["insee_competence"].split("/")

                elif actor.role == "autorite_competente":
                    actor.insee = row["insee"]
                    actor.insee_competence = [row["insee"]]

                print(actor)

                prop = role_map[actor.role]
                try:
                    result = self.connector.create_acteurs([actor])
                    row["uid"] = getattr(result, prop)[0]["idActeur"]
                except Exception as e:
                    print(f"Failed to create {actor}")
                    log.error(e, exc_info=True)
                    row["uid"] = ""

                actors_out.append(row)

                if headers is None:
                    headers = row.keys()

        if len(actors_out) == 0:
            print("No actors to be created")
            return

        with open(file_path, "w", newline="") as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            for item in actors_out:
                writer.writerow(item)

    def sync_actors(self):
        """."""
        log.info("Syncing actors from openADS to Plat'AU")
        openads = OpenADSDriver(**self.conf.openadsapi_config, mode="collectivite")

        extra_properties_by_role = {"guichet_unique": ["sve_cgu_url",]}

        actors = []
        if self.kwargs.filter:
            for criterion, value in self.kwargs.filter.items():

                # Do we have a list of values ?
                value = set(value.split(","))

                # For now, we only support filtering by role, but the following lines
                # should properly handle other criterion.
                if criterion not in ("role",):
                    print(f"Unkown criterion {criterion}")
                    return -1

                actors += [
                    actor
                    for actor in openads.actors
                    if getattr(actor, criterion) in value and actor not in actors
                ]
        else:
            actors = openads.actors

        validated_actors = []
        for actor in actors:

            # Few validations
            if actor.email and not VALID_EMAIL.match(actor.email):
                log.warning("Actor %s does not have a valid email address", actor)
                continue
            # XXX other valdidation to come ?

            # Set some global extra properties retrieved from openADS.
            if actor.role in extra_properties_by_role:
                for prop in extra_properties_by_role[actor.role]:
                    if prop in openads.extra_properties:
                        actor.extra_properties[prop] = openads.extra_properties[prop]

            validated_actors.append(actor)

        if validated_actors and not self.dry_run:
            # We abritrary take the first actor, as we run the sync on unique PISTE application
            self.connector.start(validated_actors[0].uid)
            results = self.connector.update_acteurs(validated_actors, raw=False)

        else:
            results = [
                {"uid": a.uid, "role": a.role, "status": "synchronized"}
                for a in validated_actors
            ]

        print("-----------------------")
        print("Synchronization results")
        print("-----------------------")

        if not results:
            print("No actor synchronized")
        else:
            for actor in results:
                print(
                    f"Actor UID: {actor['uid']} | role: {actor['role']} | status: {actor['status']}"
                )

        log.info("Actors syncing done.")

    def history(self):
        """."""
        log.info(f"Querying Dossier history in Plat'AU")
        self.connector.start(self.kwargs.actor)

        criterions = {}
        for criterion, value in self.kwargs.query.items():
            if criterion not in ("idDossier", "noLocal"):
                print(f"Unkown criterion {criterion}")
                return -1
            criterions[criterion] = value.strip('"')

        if "idDossier" not in criterions:
            criterions["idDossier"] = self.connector.get_dossier_by_numero(
                criterions["noLocal"]
            ).idDossier

        dossier = self.connector.get_dossier_by_uid(criterions["idDossier"])
        if not dossier:
            print(f"No Dossier found with noDossier like {criterions['idDossier']}")
            return

        events = self.connector.get_evenements_by_dossier(criterions["idDossier"])
        if not events:
            print("No results")
            return

        klass = self.connector.client.get_model(events[0].__class__.__name__)

        print("-------")
        print("History")
        print("-------")
        for event in events:
            output = {}
            for prop in klass._properties:

                value = getattr(event, prop)

                if isinstance(value, datetime):
                    output[prop] = value.strftime("%d-%m-%Y %H:%M:%S")

                elif isinstance(value, str):
                    output[prop] = value

                elif value.__class__.__name__ == "Nomenclature":
                    output[prop] = f"{value.idNom} / {value.libNom}"

                else:
                    output[prop] = ""

            for k, v in output.items():
                print(f"{k}: {v}")
            print("--")

    def resync_binary_tasks(self):
        """."""
        task_type = self.kwargs.task_type
        log.info(f"Resyncing binary tasks `{task_type}`")
        self.openads = OpenADSDriver(**self.conf.openadsapi_config)
        self.connector.start(self.kwargs.actor)

        try:
            event_OK, event_KO, object_uid_type = BINARY_TASK_MAPPING[
                task_type
            ].values()
        except KeyError:
            log.error(f"Failed to parse task {task_type} info")
            return -1

        criterions = {"state": "pending", "category": "platau", "stream": "output"}
        criterions["typology"] = task_type

        success = {}
        notfound = []
        error = []
        for task_summary in self.openads.get_tasks(criterions):

            task_id = task_summary["task"]
            try:
                dossier_uid = task_summary["external_uids"]["dossier"]
                object_uid = task_summary["external_uids"][object_uid_type]
            except KeyError:
                log.error(
                    f"Task {task_id} : something went wrong while extracting external_uids"
                )
                error.append(task_id)
                continue

            task_next_state = None
            # We check every Dossier events looking for OK/KO ones
            for event in self.connector.get_evenements_by_dossier(dossier_uid):
                if event.idElementConcerne == object_uid:
                    if event.nomTypeEvenement.idNom == event_OK:
                        task_next_state = "done"
                    elif event.nomTypeEvenement.idNom == event_KO:
                        task_next_state = "error"
                if task_next_state:
                    break

            # We didn't find any related events. For now we do nothing but log it.
            if task_next_state is None:
                log.info(f"Task {task_id} not found in Dossier history")
                notfound.append(task_id)
                continue

            log.info(f"Task {task_id} state should be update to `{task_next_state}`")
            if not self.dry_run:
                # openADS task should be synced accordingly
                if not self.openads.update_task(task_id, state=task_next_state):
                    log.error(
                        f"Unable to update task {task_id} with state `{task_next_state}`"
                    )
                    error.append(task_id)
                else:
                    log.info(
                        f"Task {task_id} state successfully updated to `{task_next_state}`"
                    )
                    success[task_id] = task_next_state
            else:
                success[task_id] = task_next_state

        log.info("resync_binary_tasks results")
        print("--------------------------")
        print("Binary tasks resync result")
        print("--------------------------")
        for task, state in success.items():
            print(f"Task {task} -> `{state}`")
        if error:
            print(f"Errors : {error}")
            log.error("Resync performed with errors.")
        if notfound:
            print(f"Not found : {notfound}")
            log.warning("Resync performed with tasks not found.")
        print("--------------------------")
        log.info("resync_binary_tasks done.")

    def resync_pieces(self):
        """."""
        log.info(f"Resyncing tasks `ajout_piece`")
        openads = OpenADSDriver(**self.conf.openadsapi_config)

        criterions = {"category": "platau", "stream": "output"}
        criterions["typology"] = "ajout_piece"
        tasks_list = []
        for state in ("pending", "done"):
            criterions["state"] = state
            tasks_list.extend(openads.get_tasks(criterions))

        success = {}
        error = []
        dossiers_not_found = []
        pieces_by_dossier = {}
        total_tasks = len(tasks_list)
        treated_tasks = 0
        for task_summary in tasks_list:

            task_id = task_summary["task"]
            task = openads.get_task(task_id)
            try:
                dossier_uid = task["external_uids"]["dossier"]
                piece_uid = task["external_uids"]["piece"]
                collectivite = task["dossier"]["om_collectivite"]
            except Exception:
                log.error(
                    f"Task {task_id} : something went wrong while extracting external_uids"
                    "Moving task to state `canceled`"
                )
                if not openads.update_task(task_id, state="canceled"):
                    log.error(f"Unable to update task {task_id} with state `canceled`")
                error.append(task_id)
                continue

            try:
                actor = openads.search_actors(
                    role="guichet_unique", organization=collectivite
                )[0]
            except Exception:
                log.error(
                    f"Unable to find actor `guichet_unique` for collectivite {collectivite}"
                )
                continue

            self.connector.start(actor.uid)
            # If we did not load Pieces previously, we retreive those
            if dossier_uid not in pieces_by_dossier:

                try:
                    pieces_by_dossier[dossier_uid] = [
                        p.idPiece for p in self.connector.get_pieces(dossier_uid)
                    ]
                except HTTPBadRequest:
                    log.warning(f"Dossier {dossier_uid} doesn't exist in Plat'AU")
                    if dossier_uid not in dossiers_not_found:
                        dossiers_not_found.append(dossier_uid)
                    pieces_by_dossier[dossier_uid] = []
                    continue

            # Let's check if our Piece is present in Plat'AU
            if piece_uid in pieces_by_dossier[dossier_uid]:
                # Piece is already in Plat'AU, but we didn't receive proper ACK.
                # Let's assure it's taken care of
                if task["task"]["state"] == "pending":
                    task_next_state = "done"
                else:
                    # Nothing to do here, eveyrthing's in order
                    continue
            else:
                # Setting the task to 'error' state will allow to rejoin the nominal process
                log.info(
                    f"Piece {piece_uid} not found in Plat'AU. Task {task_id} will be updated."
                )
                task_next_state = "error"

            if not self.dry_run:
                # openADS task should be synced accordingly
                if not openads.update_task(task_id, state=task_next_state):
                    log.error(
                        f"Unable to update task {task_id} with state `{task_next_state}`"
                    )
                    error.append(task_id)
                else:
                    log.info(
                        f"Task {task_id} state successfully updated to `{task_next_state}`"
                    )
                    treated_tasks += 1
                    if dossier_uid in success:
                        success[dossier_uid] = success[dossier_uid] + [
                            {"task": task_id, "state": task_next_state,}
                        ]
                    else:
                        success[dossier_uid] = [
                            {"task": task_id, "state": task_next_state}
                        ]
            else:
                treated_tasks += 1
                if dossier_uid in success:
                    success[dossier_uid] = success[dossier_uid] + [
                        {"task": task_id, "state": task_next_state,}
                    ]
                else:
                    success[dossier_uid] = [{"task": task_id, "state": task_next_state}]

        log.info("resync_pieces results")
        print("--------------------")
        print("Pieces resync result")
        print("--------------------")
        if dossiers_not_found:
            print(f"Dossiers not found in Plat'AU : {dossiers_not_found}")
        print("---")
        print("Tasks updated : ")
        for dossier, tasks in success.items():
            tasks_details = (", ").join(
                [f"{t['task']} -> `{t['state']}`" for t in tasks]
            )
            print(f"Dossier {dossier} : {tasks_details}")
        if error:
            print("---")
            print(f"Tasks with error : {error}")
        print("---")
        print(f"Total tasks / treated tasks: {total_tasks} / {treated_tasks}")
        print(
            f"Total Dossiers / impacted Dossiers: {len(pieces_by_dossier)} / {len(success)}"
        )
        print("--------------------")
        if self.kwargs.log_path:
            log.info(
                "Total Dossiers / impacted Dossiers: "
                f"{len(pieces_by_dossier)} / {len(success)}. "
                f"Total tasks / treated tasks: {total_tasks} / {treated_tasks}"
            )
        log.info("resync_pieces done.")

    def resync_input_streams(self):
        """."""
        log.info(f"Resyncing tasks `ajout_piece`")
        openads = OpenADSDriver(**self.conf.openadsapi_config)

        task_criterions = {"category": "platau", "stream": "output", "state": "done"}
        task_criterions["typology"] = "creation_DI"
        tasks_list = openads.get_tasks(task_criterions)

        # 20,"Occurrence d’une prise en compte métier",20/01/2020
        # 21,"Occurrence d’un avis avec ou sans prescription",21/01/2020
        event_type_by_task_type = {"pec_consultation": 20, "avis_cosnsultation": 21}

        success = {}
        error = []
        dossiers_not_found = []
        dossiers = {}
        total_tasks = len(tasks_list)
        treated_tasks = 0
        for task_summary in tasks_list:

            task_id = task_summary["task"]
            task = openads.get_task(task_id)
            try:
                dossier_uid = task["external_uids"]["dossier"]
                collectivite = task["dossier"]["om_collectivite"]
            except Exception:
                log.error(
                    f"Task {task_id} : something went wrong while extracting external_uids"
                )
                # XXX
                error.append(task_id)
                continue

            try:
                actor = openads.search_actors(
                    role="guichet_unique", organization=collectivite
                )[0]
            except Exception:
                log.error(
                    f"Unable to find actor `guichet_unique` for collectivite {collectivite}"
                )
                continue

            self.connector.start(actor.uid)
            # If we did not load Pieces previously, we retreive those
            if dossier_uid not in dossiers:

                task_next_state = None
                events_criterions = {"criteresSurDossiers": {"idDossier": dossier_uid}}
                # We check every Dossier events looking for OK/KO ones
                for task_type, event_type in event_type_by_task_type:
                    events_criterions["criteresSurEvenements"] = {
                        "nomTypeEvenement": event_type
                    }
                    for event in self.connector.get_evenements(events_criterions):
                        matching_task = self.openads.get_tasks()
                    # EvenementR(hdEvenement=datetime.datetime(2022, 5, 15, 8, 56, 2, 730000, tzinfo=tzoffset(None, 7200)), idActeurGenerateur='RXQ-MKN-01Q', idElementConcerne='E0V-JR7-D90', idEvenement='85P-2J1-GL5', nomTypeEvenement=Nomenclature(idNom=20, libNom='Occurrence d’une prise en compte métier'), nomTypeObjetMetier=Nomenclature(idNom=6, libNom='CONSULTATION')

        #     if piece_uid in pieces_by_dossier[dossier_uid]:
        #         # Piece is already in Plat'AU, but we didn't receive proper ACK.
        #         # Let's assure it's taken care of
        #         if task["task"]["state"] == "pending":
        #             task_next_state = "done"
        #         else:
        #             # Nothing to do here, eveyrthing's in order
        #             continue
        #     else:
        #         # Setting the task to 'error' state will allow to rejoin the nominal process
        #         log.info(
        #             f"Piece {piece_uid} not found in Plat'AU. Task {task_id} will be updated."
        #         )
        #         task_next_state = "error"

        #     if not self.dry_run:
        #         # openADS task should be synced accordingly
        #         if not openads.update_task(task_id, state=task_next_state):
        #             log.error(
        #                 f"Unable to update task {task_id} with state `{task_next_state}`"
        #             )
        #             error.append(task_id)
        #         else:
        #             log.info(
        #                 f"Task {task_id} state successfully updated to `{task_next_state}`"
        #             )
        #             treated_tasks += 1
        #             if dossier_uid in success:
        #                 success[dossier_uid] = success[dossier_uid] + [
        #                     {"task": task_id, "state": task_next_state,}
        #                 ]
        #             else:
        #                 success[dossier_uid] = [
        #                     {"task": task_id, "state": task_next_state}
        #                 ]
        #     else:
        #         treated_tasks += 1
        #         if dossier_uid in success:
        #             success[dossier_uid] = success[dossier_uid] + [
        #                 {"task": task_id, "state": task_next_state,}
        #             ]
        #         else:
        #             success[dossier_uid] = [{"task": task_id, "state": task_next_state}]

        # log.info("resync_pieces results")
        # print("--------------------")
        # print("Pieces resync result")
        # print("--------------------")
        # if dossiers_not_found:
        #     print(f"Dossiers not found in Plat'AU : {dossiers_not_found}")
        # print("---")
        # print("Tasks updated : ")
        # for dossier, tasks in success.items():
        #     tasks_details = (", ").join(
        #         [f"{t['task']} -> `{t['state']}`" for t in tasks]
        #     )
        #     print(f"Dossier {dossier} : {tasks_details}")
        # if error:
        #     print("---")
        #     print(f"Tasks with error : {error}")
        # print("---")
        # print(f"Total tasks / treated tasks: {total_tasks} / {treated_tasks}")
        # print(
        #     f"Total Dossiers / impacted Dossiers: {len(pieces_by_dossier)} / {len(success)}"
        # )
        # print("--------------------")
        # if self.kwargs.log_path:
        #     log.info(
        #         "Total Dossiers / impacted Dossiers: "
        #         f"{len(pieces_by_dossier)} / {len(success)}. "
        #         f"Total tasks / treated tasks: {total_tasks} / {treated_tasks}"
        #     )
        # log.info("resync_pieces done.")

    # def search_events(self, ):
    #     result = platau.client.evenements.evenementsGET(body={"criteresSurDossier":{"idDossier":"ZKD-QM5-JNO"}}, request_options={"headers": {"Id-Acteur-Appelant": "0K8-3QV-WOJ"}})


def main():
    parser = argparse.ArgumentParser(description="Interact with Plat'AU via CLI")
    parser.add_argument(
        "action",
        help="Pick-up the action you want to perfom",
        choices=[
            "status",
            "iplatau",
            "search_actors",
            "create_actors",
            "sync_actors",
            "resync_binary_tasks",
            "resync_pieces",
            "history",
        ],
        type=str,
    )

    parser.add_argument(
        "-c", "--config", help="Config file path", type=str, required=True
    )

    parser.add_argument(
        "-f", "--file_path", help="The source CSV file you want use", type=str,
    )

    parser.add_argument(
        "-a",
        "--actor",
        help="The actor Plat'AU UID you want use to authenticate your request",
        type=str,
    )

    parser.add_argument(
        "-t", "--task_type", help="The type of the task you want to resync", type=str,
    )

    parser.add_argument(
        "-q",
        "--query",
        nargs="+",
        help="The criterions to use when searching. Format: CRITERION_1=VALUE",
        type=str,
    )

    parser.add_argument(
        "--filter",
        nargs="+",
        help="A filter that should be applied on data. Format: FILTER_1=VALUE",
        type=str,
    )

    parser.add_argument(
        "-d",
        "--dry_run",
        help=(
            "Providing this option will try to perfom the action"
            "in dry-run mode (no data will be altered)"
        ),
        action="store_true",
        default=False,
    )

    parser.add_argument(
        "-l", "--log_path", help="Path to an optionnal log file", type=str,
    )

    args = parser.parse_args()
    config_path = Path(args.config)

    if args.dry_run and args.action in NO_DRY_RUN:
        print("Dry-run is not implemented with his action")
        return -1

    if args.actor is None and args.action in (
        "search_actors",
        # "sync_actors",
        "iplatau",
        "history",
    ):
        print("You should provide a valid Plat'AU UID actor")
        return -1

    if args.query is None and args.action in ("search_actors", "history"):
        print("You did not provide any query.")
        return -1

    if args.action == "create_actors" and args.file_path is None:
        print("You should provide a valid CSV file path")
        return -1

    if args.action == "iplatau" and config_path.is_dir():
        print("Action `iplatau` should be used with only one config file.")
        return -1

    if args.action == "resync_binary_tasks" and (
        args.task_type is None or args.actor is None
    ):
        print("You should provide a task type AND a valid Plat'AU UID actor")
        return -1

    log_formater = "%(asctime)s %(levelname)-7.7s [%(name)s] %(message)s"
    args.log_level = logging.INFO
    log_handlers = [logging.StreamHandler()]
    if args.log_path:
        log_handlers.append(logging.FileHandler(args.log_path))
    logging.basicConfig(
        level=args.log_level, format=log_formater, handlers=log_handlers,
    )

    if args.query:
        try:
            args.query = dict(map(lambda x: x.split("="), args.query))
        except ValueError:
            print(
                "Criterions for your query should respect the format: CRITERION_1=VALUE"
            )
            return -1

    if args.filter:
        try:
            args.filter = dict(map(lambda x: x.split("="), args.filter))
        except ValueError:
            print(
                "Criterions for your filter should respect the format: FILTER_1=VALUE"
            )
            return -1

    print(f"Action {args.action} will be perfomed.")
    if args.dry_run:
        print("- `dry-run` is on. No data will be altered")
    if args.log_path:
        print(f"- `log_path` is provided. Log well be writed at {args.log_path}")

    if config_path.is_dir():

        print(
            f"\nConfig path is a directory. Every JSON file within it will used to perform action `{args.action}`"
        )
        config_files = [cf for cf in config_path.iterdir() if ".json" in cf.suffixes]
        conf_list = ("\n").join([cf.name for cf in config_files])
        print(f"Following config files will be used :\n{conf_list}")
        confirm = input("Proceed (y/n) ?")

        if confirm != "y":
            return

        else:
            for config_file in config_files:
                args.config = str(config_file.absolute())
                config = OAPConfig(args.config)

                log.info(f"Applying action with config {config_file.name}")
                toolbox = PlatAUToolbox(config, kwargs=args)
                getattr(toolbox, args.action)()

    else:
        config = OAPConfig(args.config)
        toolbox = PlatAUToolbox(config, kwargs=args)
        getattr(toolbox, args.action)()


if __name__ == "__main__":
    main()
