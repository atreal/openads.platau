import logging
import json
import os
import re
import uuid

from datetime import datetime, date

from openads.platau.mapping import IDEAU_MAPPING
from openads.platau.mapping import OPENADS_MAPPING
from openads.platau.utils import unique_filename

log = logging.getLogger(__name__)

DEFAULT_TYPE_PIECE = 111

# from NATURE_PIECE  "1": {"idNom": 1, "libNom": "Initiale"}, "2": {"idNom": 2, "libNom": "Complémentaire"}
NATURE_PIECE_MAPPING = {
    "dpc": "2",
    "dps": "1",
    "default": "1",
}

FIELDS_BLOCKS = (
    "nature_amenagements",
    "autres_reglementations",
    "situation_juridique",
    "logements_crees",
    "logements_repartition",
    "logements_utilisation",
    "residence_precisions",
    "stationnement_adresses",
    "stationnement_details",
)


class P2OMInvalidDataException(Exception):
    """docstring for P2OMInvalidDataException"""

    pass


class Publik2OpenADSMapper(object):

    mapped: dict = {}
    keys: tuple
    source: dict
    fields: dict
    user: dict
    multi_field_key: str = None

    def __init__(self, source: dict, multi_field_key: str = None) -> None:
        """."""
        self.source = source
        self.fields = source["fields"]
        if multi_field_key is not None:
            self.multi_field_key = multi_field_key
        self.user = source["user"]

    def render(self):
        """."""
        if self.multi_field_key is not None:
            return self.multi_render()

        for k in self.keys:
            mapper = getattr(self, f"_map_{k}", None)
            if mapper is None:
                raise NotImplementedError

            self.mapped[k] = mapper()

        log.debug(f"Mapped data : \n{self.mapped}")

        return self.mapped

    def multi_render(self) -> list:
        """."""
        # multi_files can be None (ie. no Piece attached)
        if not self.fields[self.multi_field_key]:
            return []

        multi_mapped = []
        for field in self.fields[self.multi_field_key]:
            mapped = {}
            for k in self.keys:
                mapper = getattr(self, f"_map_{k}", None)
                if mapper is None:
                    raise NotImplementedError

                mapped[k] = mapper(field)
            multi_mapped.append(mapped)

        log.debug(f"Mapped data : \n{self.mapped}")

        return multi_mapped

    def to_json(self) -> str:
        """."""
        if self.mapped == {}:
            self.render()

        return json.dumps(self.mapped)

    def to_file(self, file_path):
        """."""
        if self.mapped == {}:
            self.render()

        with open(file_path, "w") as f:
            return json.dump(self.mapped, f, indent=2, sort_keys=True, default=str)


class DemandeMapper(Publik2OpenADSMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demandeur",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]

    def _base_map_demandeurs(self, demandeurs: list) -> list:
        """Map a list of Demandeurs from Publik to a openADS one."""
        mapped_demandeurs = []
        for demandeur in demandeurs:

            # XXX : missing mapping

            # Division territoriale / D3D_division

            # Common properties
            mapped_demandeur = {
                "notification": "t",
                "type_demandeur": demandeur.get("type_demandeur", ""),
                "courriel": demandeur.get("coordonnees_courriel", ""),
                "bp": demandeur.get("adresse_boite_postale", ""),
                "cedex": demandeur.get("adresse_cedex", ""),
                "complement": demandeur.get("adresse_complement", ""),
                "lieu_dit": demandeur.get("adresse_lieu_dit", ""),
                "code_postal": demandeur.get("adresse_code_postal", ""),
                "localite": demandeur.get("adresse_localite", ""),
                "numero": demandeur.get("adresse_numero", ""),
                "voie": demandeur.get("adresse_voie", ""),
            }
            mapped_demandeur.update(map_telephone(demandeur["coordonnees_telephone"])),

            # Handle specific properties
            if "identite_raison_sociale" in demandeur:
                # This is a Personne Morale
                mapped_demandeur.update(
                    {
                        "qualite": "personne_morale",
                        "personne_morale_denomination": demandeur.get(
                            "identite_denomination", ""
                        ),
                        "personne_morale_raison_sociale": demandeur.get(
                            "identite_raison_sociale", ""
                        ),
                        "personne_morale_siret": demandeur.get("identite_siret", ""),
                        "personne_morale_categorie_juridique": demandeur.get(
                            "identite_categorie_juridique", ""
                        ),
                        # Hardcoded for now ...
                        "personne_morale_civilite": demandeur.get(
                            "identite_civilite", ""
                        )
                        == "Monsieur"
                        and "4"
                        or "3",
                        "personne_morale_nom": demandeur.get("identite_nom", ""),
                        "personne_morale_prenom": demandeur.get("identite_prenom", ""),
                    }
                )

            else:
                # This is a Particulier
                mapped_demandeur.update(
                    {
                        "qualite": "particulier",
                        # Hardcoded for now ...
                        # "particulier_civilite": demandeur.get("identite_civilite", "")
                        # == "Monsieur"
                        # and "4"
                        # or "3",
                        "particulier_nom": demandeur.get("identite_nom", ""),
                        "particulier_prenom": demandeur.get("identite_prenom", ""),
                        "particulier_date_naissance": demandeur.get(
                            "naissance_date", ""
                        ),
                        "particulier_commune_naissance": demandeur.get(
                            "naissance_commune", ""
                        ),
                        "particulier_departement_naissance": demandeur.get(
                            "naissance_departement", ""
                        ),
                        "particulier_pays_naissance": demandeur.get(
                            "naissance_pays", ""
                        ),
                    }
                )

            mapped_demandeurs.append(mapped_demandeur)

        return mapped_demandeurs

    def _map_demandeur(self) -> dict:
        """."""

        # Petitionnaire Principal mapping
        if self.fields.get("identite_qualite", None) is None:
            raise P2OMInvalidDataException("`identite_qualite` must be set")

        # Let's load the Petitionnaire Principal
        if self.fields["identite_qualite"] == "Un particulier":
            set_demandeur_type(self.fields["identite_particulier_raw"])
            demandeurs = self.fields["identite_particulier_raw"]

        elif self.fields["identite_qualite"] == "Une personne morale":
            set_demandeur_type(self.fields["identite_personne_morale_raw"])
            demandeurs = self.fields["identite_personne_morale_raw"]

        else:
            # No Demandeurs to map
            return []

        # And add the others Demandeurs
        if self.fields.get("autres_demandeurs_particuliers_raw", None) is not None:
            set_demandeur_type(self.fields["autres_demandeurs_particuliers_raw"])
            demandeurs.extend(self.fields["autres_demandeurs_particuliers_raw"])

        if self.fields.get("autres_demandeurs_personnes_morales_raw", None) is not None:
            set_demandeur_type(self.fields["autres_demandeurs_personnes_morales_raw"])
            demandeurs.extend(self.fields["autres_demandeurs_personnes_morales_raw"])

        # Do we have a Mandataire ?
        if self.fields.get("identite_mandataire_particulier_raw", None):
            set_demandeur_type(
                self.fields["identite_mandataire_particulier_raw"], "delegataire"
            )
            demandeurs.extend(self.fields["identite_mandataire_particulier_raw"])

        if self.fields.get("identite_mandataire_personne_morale_raw", None):
            set_demandeur_type(
                self.fields["identite_mandataire_personne_morale_raw"], "delegataire"
            )
            demandeurs.extend(self.fields["identite_mandataire_personne_morale_raw"])

        mapped_demandeurs = self._base_map_demandeurs(demandeurs)

        if len(mapped_demandeurs) >= 1:
            mapped_demandeurs[0]["petitionnaire_principal"] = "t"

        return mapped_demandeurs

    def _map_dossier(self) -> dict:
        """."""
        insee = self.fields.get("code_commune_insee", None)
        if insee is None:
            raise P2OMInvalidDataException("`code_commune_insee` must be set")
        dossier = {
            "depot_electronique": "t",
            "insee": insee,
        }
        #  Terrain
        terrain = self.fields.get("terrain_raw", [])
        if not len(terrain):
            log.debug("No `terrain` to be mapped")
            return dossier
        else:
            terrain = terrain[0]

        dossier.update(
            {
                "terrain_adresse_voie_numero": terrain.get("adresse_numero", ""),
                "terrain_adresse_voie": terrain.get("adresse_voie", ""),
                "terrain_adresse_complement": terrain.get("adresse_complement", ""),
                "terrain_adresse_lieu_dit": terrain.get("adresse_lieu_dit", ""),
                "terrain_adresse_code_postal": terrain.get("adresse_code_postal", ""),
                "terrain_adresse_localite": terrain.get("adresse_localite", ""),
                "terrain_superficie": terrain.get("terrain_superficie", ""),
            }
        )

        # References cadastrales
        references_cadastrales = []
        for ref in self.fields["references_cadastrales_raw"]:
            references_cadastrales.append(
                ref["prefixe"].rjust(3, "0")
                + ref["section"]
                + ref["numero"].rjust(4, "0")
            )
        dossier["terrain_references_cadastrales"] = ";".join(references_cadastrales)
        # terrain_references_cadastrales should always end by a semi-column
        dossier["terrain_references_cadastrales"] += ";"

        return dossier

    def _map_donnees_techniques(self):
        """."""
        d_t = {}
        if "engagement_declarant_raw" in self.fields:
            engagement = self.fields.get("engagement_declarant_raw", "")[0]
            d_t["enga_decla_date"] = engagement["enga_decla_date"]
            d_t["enga_decla_lieu"] = engagement["enga_decla_lieu"]
        else:
            d_t["enga_decla_date"] = self.fields.get("enga_decla_date", "")
            d_t["enga_decla_lieu"] = self.fields.get("enga_decla_lieu", "")

        # # Specific mapping -----------------------------------------------------------
        # It will pop original values from fields, inserting new mappable keys and
        # values

        # Destination / Sous-destination -----------------------------------------------
        for field_name in ("destination", "sous_destination"):

            log.info(f"Mapping {field_name}")

            field = self.fields.pop(f"surfaces_{field_name}_raw", None)
            if not field:
                continue

            for destination in field:
                prefix_cerfa_code = destination[f"{field_name}_structured"][
                    "code_cerfa"
                ]
                for k in destination:
                    # keys will like 'A1', 'B1', etc
                    if k in ("A1", "B1", "C1", "D1", "E1"):
                        # We rebuild the CERFA code
                        self.fields[f"{prefix_cerfa_code}{k}"] = destination[k]
        # ------------------------------------------------------------------------------

        # Surfaces taxables  -----------------------------------------------
        for field_name in ("habitation", "hors_habitation"):

            log.info(f"Mapping {field_name}")

            field = self.fields.pop(f"surfaces_taxables_{field_name}_raw", None)
            if not field:
                continue

            for surface in field:
                try:
                    prefix_cerfa_code = surface["local_structured"]["code_cerfa"]
                    for k in surface:
                        if k in ("L1", "L2", "S1", "S2", "T4", "T5", "U1", "U2"):
                            # We rebuild the CERFA code
                            self.fields[f"{prefix_cerfa_code}{k}"] = surface[k]
                except (AttributeError, KeyError):
                    log.warning(f"Failed to properly map {field_name}")

        # ------------------------------------------------------------------------------

        # Fields blocks ----------------------------------------------------------------
        # We need to flat sub-fields
        for block in FIELDS_BLOCKS:
            field = self.fields.pop(f"{block}_raw", None)
            if not field:
                continue

            try:
                for subfield_name, subfield_value in field[0].items():
                    self.fields[subfield_name] = subfield_value
            except (AttributeError, KeyError):
                log.warning(f"Failed to flat fields block {block}")

        # ------------------------------------------------------------------------------

        # # CERFA-based encoded fields -------------------------------------------------

        # We need to identify fields that will be auto-mapped
        cerfa_encoded_pattern = re.compile(r"^[A-Z][A-Z0-9]{2,}_?[a-zA-Z]*")
        # Not elegant, but we work with PDF encoded form ...
        indecisive_boolean = ("Oui", "Non", "Je ne sais pas")
        # Don't know why, but sometimes we get a string as scientific notation ...
        # Lets' prepare a regex to handle those
        scientific_notation_pattern = re.compile(r"^[0-9]*\.?[0-9]+[eE](\+|\-)[0-9]+$")

        for key, value in self.fields.items():

            # Fields endind, with "_raw" are IDE'AU artefact. We'll ignore those
            if key.endswith("raw"):
                continue

            if isinstance(value, bool):
                value = cast_boolean(value)

            # Handle scientific notation case. ex: "5E+3" should be rendered as 5000
            elif isinstance(value, str) and scientific_notation_pattern.search(value):
                value = int(float(value))

            matched = cerfa_encoded_pattern.search(key)
            if matched is not None:  # We have a CERFA-based encoded field

                normalized_key = key.lower()

                log.info(f"Mapping {normalized_key}")

                # Boolean or indecisive boolean with various CERFA-based encoded field
                # ex : {"t3i_lotoui__t3l_lotnon__t3s_lotnc" : "Oui"}
                if "__" in normalized_key:
                    # {"Oui": t3i_lotoui, "Non": t3l_lotnon, "Je ne sais pas": t3s_lotnc}

                    log.info("This is a boolean / undecisive boolean")

                    individual_keys = normalized_key.split("__")
                    if len(individual_keys) > 3:
                        log.warning(
                            f"Incorrect property found {key}. "
                            "Basic mapping will be applied."
                        )

                    else:
                        dynamic_map = {
                            indecisive_boolean[i]: individual_keys[i]
                            for i in range(len(individual_keys))
                        }
                        try:
                            normalized_key = dynamic_map[value]

                        except KeyError:
                            log.warning(
                                f"Incorrect property found {key}. "
                                "Basic mapping will be applied."
                            )

                # Do we have a mapping or should we use the raw value ?
                mapped_key = IDEAU_MAPPING.get(normalized_key, normalized_key)

                # We may have one CERFA field matching various openADS fields
                # In this  case, the seperator will be a '|'
                if isinstance(value, str) and "|" in IDEAU_MAPPING.get(mapped_key, ""):
                    log.info("This field matches various fields in openADS")
                    # multiple openADS fields
                    #
                    # ex :
                    #   ideau,openads
                    #   c7u_creee,su_cstr_shon_tot|su2_cstr_shon_tot
                    #
                    for k in IDEAU_MAPPING[mapped_key].split("|"):
                        d_t[k] = value

                else:
                    d_t[mapped_key] = value

        return d_t

    def _map_external_uids(self):
        """."""
        external_uids = {
            "demande": self.source["demande_id"],
        }
        try:
            # tracking code is set in workflow treatment property
            code_suivi = self.source["workflow"]["fields"]["code_suivi"]
            external_uids["code-suivi"] = code_suivi
        except KeyError:
            log.warning(
                f"Demande {self.source['demande_id']}: "
                "Could not extract `code_suivi` from demande"
            )

        return external_uids


class DemandeInitialeMapper(DemandeMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "donnees_techniques",
        "demandeur",
        "dossier",
        "external_uids",
    ]

    def _map_dossier(self) -> dict:
        """."""
        dossier = super()._map_dossier()
        # Which DATD should be created ?
        if "dossier_autorisation_type_detaille_code" in self.fields:
            dossier["dossier_autorisation_type_detaille_code"] = self.fields[
                "dossier_autorisation_type_detaille_code"
            ]

        return dossier


class BaseDemandeDossierExistantMapper(DemandeMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demandeur",
        "demande",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]

    def _map_demande(self):
        """."""
        numero_container = self.fields.get("identification_dossier_raw", [])
        if numero_container is None or len(numero_container) != 1:
            raise P2OMInvalidDataException("`numero_dossier` must be set")

        demande = {}
        demande["dossier_initial"] = numero_container[0].get("numero_dossier", None)
        demande["type_demande"] = self.fields.get("type_demande_dossier_existant", None)

        if demande["dossier_initial"] is None or demande["type_demande"] is None:
            raise P2OMInvalidDataException(
                "`dossier_initial` and `type_demande` must be set"
            )

        return demande


class VanillaDemandeDossierExistantMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "demandeur",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]


class DemandePieceCompSuppMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "dossier",
        "external_uids",
    ]


class DemandeTransfertMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "demandeur",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]


class DemandeModificatifMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "demandeur",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]

    def _map_demandeur(self) -> dict:
        """."""
        demandeurs = []

        # And add the others Demandeurs
        if self.fields.get("autres_demandeurs_particuliers_raw", None) is not None:
            set_demandeur_type(self.fields["autres_demandeurs_particuliers_raw"])
            demandeurs.extend(self.fields.get("autres_demandeurs_particuliers_raw"))

        if self.fields.get("autres_demandeurs_personnes_morales_raw", None) is not None:
            set_demandeur_type(self.fields["autres_demandeurs_personnes_morales_raw"])
            demandeurs.extend(
                self.fields.get("autres_demandeurs_personnes_morales_raw")
            )

        mapped_demandeurs = self._base_map_demandeurs(demandeurs)

        if len(mapped_demandeurs) >= 1:
            mapped_demandeurs[0]["petitionnaire_principal"] = "t"

        demandeurs = []
        # Do we have a Mandataire ?
        if self.fields.get("identite_mandataire_particulier_raw", None):
            set_demandeur_type(
                self.fields["identite_mandataire_particulier_raw"], "delegataire"
            )
            demandeurs.extend(self.fields["identite_mandataire_particulier_raw"])

        if self.fields.get("identite_mandataire_personne_morale_raw", None):
            set_demandeur_type(
                self.fields["identite_mandataire_personne_morale_raw"], "delegataire"
            )
            demandeurs.extend(self.fields["identite_mandataire_personne_morale_raw"])

        mapped_demandeurs += self._base_map_demandeurs(demandeurs)

        return mapped_demandeurs


class DemandeDOCMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]

    def _map_donnees_techniques(self):
        """."""
        dt = super()._map_donnees_techniques()
        dt["doc_date"] = self.fields.get("date_ouverture", "")

        if self.fields.get("ensemble_travaux", "") == "La totalité":
            dt["doc_tot_trav"] = "t"
        elif self.fields.get("ensemble_travaux", "") == "Une tranche":
            dt["doc_tranche_trav"] = "t"

        dt["doc_tranche_trav_desc"] = self.fields.get("precision_tranche", "")

        dt["doc_surf"] = self.fields.get("surface", "")
        dt["doc_nb_log"] = self.fields.get("nombre_logements_total", "")
        dt["doc_nb_log_indiv"] = self.fields.get("nombre_logements_individuels", "")
        dt["doc_nb_log_coll"] = self.fields.get("nombre_logements_collectifs", "")

        financement = self.fields.get("nombre_logements_financement", [])

        i = 0
        for row in (
            "doc_nb_log_lls",
            "doc_nb_log_aa",
            "doc_nb_log_ptz",
            "doc_nb_log_autre",
        ):
            dt[row] = financement[i][0] is not None and financement[i][0] or ""
            i += 1

        return dt


class DemandeDAACTMapper(BaseDemandeDossierExistantMapper):
    """."""

    mapped: dict = {}
    keys: tuple = [
        "demande",
        "donnees_techniques",
        "dossier",
        "external_uids",
    ]

    def _map_donnees_techniques(self):
        """."""
        dt = super()._map_donnees_techniques()
        dt["daact_date"] = self.fields.get("date_achevement", "")
        dt["daact_date_chgmt_dest"] = self.fields.get("date_changement_destination", "")

        if self.fields.get("ensemble_travaux", "") == "La totalité":
            dt["daact_tot_trav"] = "t"
        elif self.fields.get("ensemble_travaux", "") == "Une tranche":
            dt["daact_tranche_trav"] = "t"

        dt["daact_tranche_trav_desc"] = self.fields.get("precision_tranche", "")

        dt["daact_surf"] = self.fields.get("surface", "")
        dt["daact_nb_log"] = self.fields.get("nombre_logements_total", "")
        dt["daact_nb_log_indiv"] = self.fields.get("nombre_logements_individuels", "")
        dt["daact_nb_log_coll"] = self.fields.get("nombre_logements_collectifs", "")

        financement = self.fields.get("nombre_logements_financement", [])

        i = 0
        for row in (
            "daact_nb_log_lls",
            "daact_nb_log_aa",
            "daact_nb_log_ptz",
            "daact_nb_log_autre",
        ):
            dt[row] = financement[i][0] is not None and financement[i][0] or ""
            i += 1

        return dt


class CerfaMapper(Publik2OpenADSMapper):
    """
    """

    mapped: dict = {}
    keys: tuple = ["document_numerise", "external_uids"]

    def _map_document_numerise(self):
        """."""
        # _nature = OPENADS_MAPPING["NATURE_PIECE"]
        _type = OPENADS_MAPPING["TYPE_PIECE"]

        # Fallback to 111, Autre a preciser if no Piece type his provided
        cerfa_type_piece_id = self.fields.get("cerfa_type_piece_id", DEFAULT_TYPE_PIECE)
        if not isinstance(cerfa_type_piece_id, int):
            cerfa_type_piece_id = int(cerfa_type_piece_id)
        document_numerise_type_code = _type[cerfa_type_piece_id]

        generated_cerfa = self.source["workflow"]["fields"].get(
            "synthese_demande", None
        )
        if generated_cerfa:
            # A Synthese had been generated, meaning we use it as the CERFA
            result = {
                "date_creation": cast_date(datetime.now()),
                "document_numerise_nature": 1,  # "1": {"idNom": 1, "libNom": "Initiale"},
                "document_numerise_type_code": document_numerise_type_code,
                "nom_fichier": unique_filename(generated_cerfa["filename"]),
                "file_content_type": generated_cerfa["content_type"],
                "file_content": generated_cerfa["content"],
            }

        else:
            # CERFA is provided
            result = {
                "date_creation": cast_date(datetime.now()),
                "document_numerise_nature": 1,  # "1": {"idNom": 1, "libNom": "Initiale"},
                "document_numerise_type_code": document_numerise_type_code,
                "nom_fichier": unique_filename(self.fields["cerfa"]["filename"]),
                "file_content_type": self.fields["cerfa"]["content_type"],
                "file_content": self.fields["cerfa"]["content"],
            }

            # We add a description_type property only if it's valued
            if self.fields.get("cerfa_type_piece_text"):
                result["description_type"] = self.fields.get("cerfa_type_piece_text")

        return result

    def _map_external_uids(self):
        """."""
        external_uids = {
            "demande": self.source["demande_id"],
            # piece uid will never be used and self.fields["cerfa"]["field_id"] is not unique
            "piece": str(uuid.uuid4()),
        }
        return external_uids


class PiecesMapper(Publik2OpenADSMapper):
    """
    """

    mapped: dict = {}
    # keys: tuple = ["document_numerise", "dossier", "external_uids"]
    keys: tuple = ["document_numerise", "external_uids"]

    def _map_document_numerise(self, field):
        """."""
        # _nature = OPENADS_MAPPING["NATURE_PIECE"]
        _type = OPENADS_MAPPING["TYPE_PIECE"]

        try:
            type_piece = field["type_piece_raw"] and _type[int(field["type_piece_raw"])]
        except (KeyError, TypeError):
            type_piece = None

        if not type_piece:
            type_piece = _type[
                DEFAULT_TYPE_PIECE
            ]  # If value is unknown, fallback to "Autre a préciser"

        if "type_demande" in self.source:
            try:
                nature_piece = NATURE_PIECE_MAPPING[self.source["type_demande"]]
            except KeyError:
                nature_piece = NATURE_PIECE_MAPPING["default"]
        else:
            nature_piece = NATURE_PIECE_MAPPING["default"]

        result = {
            "date_creation": cast_date(datetime.now()),
            "document_numerise_nature": nature_piece,
            "document_numerise_type_code": type_piece,
            "nom_fichier": unique_filename(field["fichier"]["filename"]),
            "file_content_type": field["fichier"]["content_type"],
            "file_content": field["fichier"]["content"],
        }

        # We add a description_type property only if it's valued
        if field.get("type_piece_text_autre"):
            result["description_type"] = field.get("type_piece_text_autre")

        return result

    def _map_external_uids(self, field):
        """."""
        external_uids = {
            "demande": self.source["demande_id"],
            "piece": field["fichier"]["field_id"],
        }
        return external_uids


def map_telephone(telephone: str) -> dict:
    """."""
    if telephone is None:
        return {}
    mapped = {}
    # This is not optimal
    if (
        telephone is not None
        and telephone.startswith("06")
        or telephone.startswith("07")
    ):
        mapped["telephone_mobile"] = telephone
    else:
        mapped["telephone_fixe"] = telephone

    return mapped


def set_demandeur_type(obj, value: str = "petitionnaire"):
    """ Internal fucntion to set type_demandeur"""
    if not isinstance(obj, list):
        obj["type_demandeur"] = value
    else:
        for item in obj:
            item["type_demandeur"] = value


def cast_boolean(value: bool, textual: bool = False):
    """Boolean fields in opennADS can be :
        `t`, `f`, `oui`, `non`
        Setting `textual` to True wil use `oui|non` values
    """
    if value is None and not textual:
        return ""
    elif value is None and textual:
        return "nesaispas"
    elif value and not textual:
        return "t"
    elif value and textual:
        return "oui"
    elif not value and not textual:
        return "f"
    elif not value and textual:
        return "non"


def cast_date(value):
    """."""
    if value is None or not (isinstance(value, date) or isinstance(value, datetime)):
        return ""
    else:
        return value.strftime("%Y-%m-%d")


def cast_str(value, max_length: int = None):
    """."""
    casted = ""
    if value is None:
        return ""
    elif not isinstance(value, str):
        casted = str(value).replace("<", "&#60;").replace(">", "&#62;")
    else:
        casted = value.replace("<", "&#60;").replace(">", "&#62;")

    if max_length is not None:
        return casted[:max_length]
    else:
        return casted


def _log(msg, level="debug"):
    """."""
    getattr(log, level)(msg)
    print("{}: {}".format(level, msg))
