# -*- coding: utf-8 -*-
# freely adapted from plone.i18n

import os
import re
import string
import tempfile
import uuid
import unicodedata

from pathlib import Path

try:
    from poolbox.pdf_toolbox import PDFToolBox

    POOLBOX = True

except Exception:
    POOLBOX = False

# Define and compile static regexes
FILENAME_REGEX = re.compile(r"^(.+)\.(\w{,4})$")
IGNORE_REGEX = re.compile(r"['\"]")
NON_WORD_REGEX = re.compile(r"[\W\-]+")
DANGEROUS_CHARS_REGEX = re.compile(r"[!$%&()*+,/:;<=>?@\\^{|}\[\]~`]+")
MULTIPLE_DASHES_REGEX = re.compile(r"\-+")
EXTRA_DASHES_REGEX = re.compile(r"(^\-+)|(\-+$)")
UNDERSCORE_START_REGEX = re.compile(r"(^_+)(.*)$")
VALID_EMAIL = re.compile(
    r"^([\w-]+(?:\.[\w-]+)*(?:\+[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"
)

# Define static constraints
MAX_LENGTH = 50
MAX_FILENAME_LENGTH = 1023

CHAR = {}
NULLMAP = ["" * 0x100]
UNIDECODE_LIMIT = 0x0530

# On OpenBSD string.whitespace has a non-standard implementation
# See http://dev.plone.org/plone/ticket/4704 for details
whitespace = "".join([c for c in string.whitespace if ord(c) < 128])
allowed = string.ascii_letters + string.digits + string.punctuation + whitespace


def normalize_filename(text, max_length=MAX_FILENAME_LENGTH):
    """
    Returns a normalized text.
    """
    text = text.lower()

    # Remove any leading underscores
    m = UNDERSCORE_START_REGEX.match(text)
    if m is not None:
        text = m.groups()[1]

    base = text
    ext = ""

    m = FILENAME_REGEX.match(text)
    if m is not None:
        base = m.groups()[0]
        ext = m.groups()[1]

    base = IGNORE_REGEX.sub("", base)
    base = NON_WORD_REGEX.sub("-", base)
    base = DANGEROUS_CHARS_REGEX.sub("-", base)
    base = MULTIPLE_DASHES_REGEX.sub("-", base)
    base = EXTRA_DASHES_REGEX.sub("", base)

    # Replace non-ascii characters by their ascci equivalent, drop non-equivalent ones.
    base = unicodedata.normalize("NFD", base).encode("ascii", "ignore").decode("utf-8")

    base = cropName(base, maxLength=max_length)

    if ext != "":
        base = base + "." + ext

    return base


def cropName(base, maxLength=MAX_LENGTH):
    baseLength = len(base)

    index = baseLength
    while index > maxLength:
        index = base.rfind("-", 0, index)

    if index == -1 and baseLength > maxLength:
        base = base[:maxLength]

    elif index > 0:
        base = base[:index]

    return base


def unique_filename(filename: str) -> str:
    """."""
    normalized = Path(normalize_filename(filename))

    return f"{normalized.stem}-{str(uuid.uuid4()).split('-')[-1]}{normalized.suffix}"


def generate_temporary_pdf(filename: str, texte: str):
    """."""
    # Poolbox is not available. We just provide aan empty PDF
    if not POOLBOX:
        filepath = tempfile.mkstemp(suffix=".pdf")[1]

        return filepath

    poolbox = PDFToolBox(external_j2_templates=("openads.platau", "templates"))

    data = poolbox.render_pdf_from_j2("deleted_piece.html.j2", {"msg": texte})
    filepath = tempfile.mkstemp(suffix=".pdf")[1]
    with open(filepath, "wb") as f:
        f.write(data)

    return filepath
