import os
import json
import logging
import csv

log = logging.getLogger(__name__)

PLATAU_MAPPING = {}
OPENADS_MAPPING = {}
NOMENCLATURE = {}
IDEAU_MAPPING = {}

current_path = os.path.dirname(os.path.abspath(__file__))

working_path = f"{current_path}/one_to_one"
log.info(f"Loading Nomenclature mapping from {working_path}")
for filename in os.listdir(working_path):

    if filename.endswith(".json"):

        mapping_name = filename.split(".json")[0]

        with open("{}/{}".format(working_path, filename), "r") as fp:
            PLATAU_MAPPING[mapping_name] = json.loads(fp.read())

        # if one value from PLATAU matches many value from openADS, only the last entry
        # will be kept. Meaning, order your values with whished ones at the bottom
        OPENADS_MAPPING[mapping_name] = {
            v["idNom"]: k for k, v in PLATAU_MAPPING[mapping_name].items()
        }

working_path = f"{current_path}/many_to_many"
log.info(f"Loading Nomenclature mapping from {working_path}")
for filename in os.listdir(working_path):

    if filename.endswith(".json"):

        mapping_name = filename.split(".json")[0]

        with open("{}/{}".format(working_path, filename), "r") as fp:
            PLATAU_MAPPING[mapping_name] = json.loads(fp.read())

        # some clean-up
        if isinstance(PLATAU_MAPPING[mapping_name], list):
            for items in PLATAU_MAPPING[mapping_name]:
                if "x-openads-doc" in items:
                    del items["x-openads-doc"]

working_path = f"{current_path}/nomenclature"
log.info(f"Loading Nomenclature from {working_path}")
for filename in os.listdir(working_path):

    if filename.endswith(".csv"):

        nomenclature_name = filename.split(".csv")[0].split("Nomenclature_")[1]

        with open("{}/{}".format(working_path, filename), "r") as fp:
            reader = csv.reader(fp)
            #  get rid of the key's definition line with `reader.line_num > 1`
            NOMENCLATURE[nomenclature_name] = {
                int(row[0]): row[1] for row in reader if reader.line_num > 1
            }

working_path = f"{current_path}/ideau"
log.info(f"Loading mapping from {working_path}")
with open(f"{working_path}/ideau.csv", "r") as fp:
    reader = csv.reader(fp)
    #  get rid of the key's definition line with `reader.line_num > 1`
    IDEAU_MAPPING = {row[0]: row[1] for row in reader if reader.line_num > 1}
