import logging
import re

# from cachetools import cached, TTLCache
from datetime import date, datetime

from openads.platau.mapping import PLATAU_MAPPING

log = logging.getLogger(__name__)


class PlatAUMapper(object):
    def __init__(self, data, model_generator):
        """."""
        self.data = data
        self.model_generator = model_generator

    def map(self, model_name, parent_model_name=None, data_subset=None):
        """."""
        try:
            Model = self.model_generator(model_name)
        except KeyError as e:
            log.error("No model for {}".format(model_name))
            raise e

        instance = Model()

        if data_subset is None:
            data = self.data
        else:
            data = data_subset

        # Handle inherited model
        for inherited_model_name in Model._inherits_from:
            inherited_object = self.map(
                inherited_model_name,
                parent_model_name=parent_model_name,
                data_subset=data_subset,
            )
            # Set inherited_object properties to instance
            for key, value in inherited_object._as_dict().items():
                setattr(instance, key, value)

        # Handle same-level properties
        for prop, attr in Model._properties.items():
            # _log("{} : Mapping property".format(prop))

            attributes = attr.copy()

            # Handle constant, fixed in JSON CDS
            if "x-openads-constant" in attributes:
                setattr(instance, prop, attributes["x-openads-constant"])
                continue

            # Handle computed values
            if "x-openads-computed" in attributes:
                compute = getattr(self, attributes["x-openads-computed"], None)
                if compute is not None:
                    setattr(instance, prop, compute())
                    continue

            # Handle specific mapper
            specific_mapper = getattr(self, "_map_{}".format(prop), None)
            if specific_mapper is not None:
                setattr(instance, prop, specific_mapper(data_subset=data))
                continue

            if "x-openads-mapping" in attributes and isinstance(
                attributes["x-openads-mapping"], dict
            ):
                if parent_model_name in attributes["x-openads-mapping"]:
                    attributes["x-openads-mapping"] = attributes["x-openads-mapping"][
                        parent_model_name
                    ]
                else:
                    _log("{} : No matching attribute".format(prop))
                    continue

            # Handle  object properties
            if "$ref" in attributes and "type" not in attributes:

                sub_model_name = attributes["$ref"].split("/")[-1]

                result = self.map(
                    sub_model_name,
                    parent_model_name=model_name,
                    data_subset=data_subset,
                )
                setattr(instance, prop, result)

            # Basic cases : let's set the value !
            elif "x-openads-mapping" in attributes or (
                "items" in attributes and "x-openads-mapping" in attributes["items"]
            ):
                # Regular mapping : one field in openads == one property in platau

                if "items" in attributes:
                    # array of prop, attributes are one level deeper
                    # PersonneP1.noms case
                    attributes = attributes["items"]
                    array_mode = True
                else:
                    array_mode = False

                mapping_prop = None
                if isinstance(attributes["x-openads-mapping"], str):
                    mapping_prop = attributes["x-openads-mapping"]

                elif isinstance(attributes["x-openads-mapping"], list):

                    for candidat_prop in attributes["x-openads-mapping"]:
                        if candidat_prop not in data:
                            _log("{} : Not the right attribute".format(candidat_prop))
                            continue
                        elif not data[candidat_prop]:
                            _log("{} : No value".format(candidat_prop))
                            continue
                        else:
                            # XXX - we use the first candidat prop found. Is it right ?
                            mapping_prop = candidat_prop
                            break
                    if mapping_prop is None:
                        _log("{} : No matching attribute".format(prop))
                        continue

                # elif isinstance(attributes["x-openads-mapping"], dict):
                #     mapping_prop = attributes["x-openads-mapping"][parent_model_name]

                else:
                    _log("{} : No matching attribute".format(prop))
                    continue

                if mapping_prop not in data:

                    # shall we use a default value ?
                    # boRecoursAUnProfessionnelNonOblig case
                    if "x-openads-default" in attributes:
                        value = _caster(attributes, attributes["x-openads-default"])
                        _log("{} : default value used".format(prop))
                    else:
                        _log("{} : No matching attribute".format(prop))
                        continue

                else:
                    if not data[mapping_prop]:
                        # No value provided
                        _log("{} : No value".format(prop))
                        continue

                    if "x-openads-nomenclature" in attributes:
                        # One to One Nomeclature based value
                        mapping = PLATAU_MAPPING[attributes["x-openads-nomenclature"]]
                        if data[mapping_prop] in mapping:
                            value = mapping[data[mapping_prop]]["idNom"]
                        else:
                            # We didn't find a matching key in the mapping.
                            # This is not a regular situation, a warning should be
                            # thrown.
                            value = None
                            _log(
                                f"No match in {attributes['x-openads-nomenclature']} "
                                f"for property {prop} with value {data[mapping_prop]}",
                                level="warning",
                            )
                    else:
                        # nominal case
                        value = _caster(attributes, data[mapping_prop])

                # One to N case
                if attributes["type"] == "array" or array_mode:
                    value = [value]

                setattr(instance, prop, value)

            elif (
                attributes["type"] == "integer"
                and "x-openads-nomenclature" in attributes
            ):
                # N to One  Nomeclature based value
                # N fields in openads == one monovalued properties in platau

                # Handle object list
                value = []
                for k, v in PLATAU_MAPPING[
                    attributes["x-openads-nomenclature"]
                ].items():
                    if k in data and _caster({"type": "boolean"}, data[k]) is True:
                        value.append(v["idNom"])

                if value == []:
                    _log("{} : no value found".format(prop))
                    continue

                # Only one value is required
                if len(value) != 1:
                    # Platau property is monovalued, and we have multiple values
                    # from openADS. This is not a regular case
                    _log(
                        "{} : multiple values mapeed to a mono platau property".format(
                            prop
                        ),
                        "error",
                    )
                    # We ignore the property
                    continue
                else:
                    value = value[0]

                setattr(instance, prop, value)

            elif (
                attributes["type"] == "array"
                and "x-openads-nomenclature" in attributes["items"]
            ):
                # N to N  Nomeclature based value
                # N fields in openads == one multivalued properties in platau

                # Handle object list
                value = []
                for k, v in PLATAU_MAPPING[
                    attributes["items"]["x-openads-nomenclature"]
                ].items():
                    if k in data and _caster({"type": "boolean"}, data[k]) is True:
                        value.append(v["idNom"])

                if value == []:
                    _log("{} : no value found".format(prop))
                    continue

                setattr(instance, prop, value)

            # Handle object list
            elif attributes["type"] == "array" and "$ref" in attributes["items"]:
                _log("{} is an array of $ref".format(prop))

                # XXX
                if prop not in (
                    "roles",
                    "pieces",
                    "documents",
                    "consultations",
                    "avis",
                    "prescriptions",
                ):
                    _log(
                        "{} : mapping not implemented. Case : array of $ref".format(
                            prop
                        )
                    )
                    continue

                # What model should we use ? i.e #/definitions/MyModelName
                sub_model_name = attributes["items"]["$ref"].split("/")[-1]

                result = self.map(
                    sub_model_name,
                    parent_model_name=model_name,
                    data_subset=data_subset,
                )
                setattr(instance, prop, [result])

            else:
                _log("{} : mapping not implemented".format(prop))
                # _log("No matching attribute for {}".format(prop))
                continue

        # _log(instance)
        return instance

    def _generic_nomenclature_based_mapper(
        self, model_name, prop_name, platau_keys, index_name=None, data_subset=None
    ):
        """Handle mapping of object array, nomemclature based."""
        Model = self.model_generator(model_name)
        result = []

        if data_subset is None:
            data = self.data
        else:
            data = data_subset

        if not isinstance(platau_keys, list):
            platau_keys = [platau_keys]

        i = 1
        for props in PLATAU_MAPPING[prop_name]:

            # make a proper copy, otherwise we'll break the nomenclature
            dict_values = props.copy()
            we_have_a_winner = False

            if "x-openads-condition" in dict_values:

                openads_key = dict_values["x-openads-condition"]
                if openads_key not in data:
                    # no value from openads with this var name
                    continue

                value = data[openads_key]

                # Boolean to fixed struct case : annexe -> garage
                if not _caster({"type": "boolean"}, value):
                    # we can ignore this item
                    continue
                else:
                    # this entry should be kept
                    we_have_a_winner = True

                # we should clean-up
                del dict_values["x-openads-condition"]

            for platau_key in platau_keys:

                if platau_key not in props:
                    # platau key did'nt match any key of props, meaning we ignore it
                    continue
                openads_key = props[platau_key]

                if openads_key not in data:
                    # no value from openads with this var name
                    # we should drop this item to avoid keeping a mapping key
                    del dict_values[platau_key]
                    continue
                value = data[openads_key]

                if value and value != "":
                    # We have a value, let's cast this
                    value = _caster(Model._properties[platau_key], value)
                    # Replace the key by its value
                    dict_values[platau_key] = value
                    we_have_a_winner = True
                else:
                    # empty value from openads with this var name
                    # we should drop this item to avoid keeping a mapping key
                    del dict_values[platau_key]

            if we_have_a_winner:  # and missing_value is False:
                # SHould we add an index ?
                if index_name:
                    dict_values[index_name] = i

                result.append(Model(**dict_values))
                i += 1

        return result

    def _map_adressesStationnementsExternes(self, **kwargs):
        """Handle adressesStationnementsExternes mapping.
        This is a single text field in openADS. An AdresseP1 in Plat'AU.
        """

        AdresseP1 = self.model_generator("AdresseP1")

        adresses = []
        # Only 2 adresses are allowed for Stationnement
        for index in (1, 2):
            # `nomLocalite` is a required property. We'll map the address only is a value is provided
            if ("donnees_techniques.s1la%d_localite" % index) in self.data:
                adresses.append(
                    AdresseP1(
                        noVoie=_caster(
                            AdresseP1._properties["noVoie"],
                            self.data["donnees_techniques.s1na%d_numero" % index],
                        ),
                        nomVoie=_caster(
                            AdresseP1._properties["nomVoie"],
                            self.data["donnees_techniques.s1va%d_voie" % index],
                        ),
                        nomLieuDit=_caster(
                            AdresseP1._properties["nomLieuDit"],
                            self.data["donnees_techniques.s1wa%d_lieudit" % index],
                        ),
                        codePostal=_caster(
                            AdresseP1._properties["codePostal"],
                            self.data["donnees_techniques.s1la%d_localite" % index],
                        ),
                        nomLocalite=_caster(
                            AdresseP1._properties["nomLocalite"],
                            self.data["donnees_techniques.s1pa%d_codepostal" % index],
                        ),
                    )
                )

        return adresses

    def _map_annexes(self, **kwargs):
        """Handle annexes mapping."""
        annexes = self._generic_nomenclature_based_mapper(
            "AnnexeP1",
            "annexes",
            # ["surfAnnexe", "libAutreTypeAnnexe", "x-openads-condition"],
            ["surfAnnexe", "libAutreTypeAnnexe"],
            index_name="noAnnexe",
        )
        for annexe in annexes:
            if annexe.surfAnnexe is None:
                annexe.surfAnnexe = 0
        return annexes

    def _map_amenagements(self, **kwargs):
        """Handle amenagements mapping."""

        AmenagementP1 = self.model_generator("AmenagementP1")
        AttributAmenagP1 = self.model_generator("AttributAmenagP1")

        platau_keys = [
            "hautExhaussements",
            "nbMaxLots",
            "nbMoisInstallationSiSaisonniere",
            "profAffouillement",
            "surfMaxPlancher",
            "surfTravauxAffouillement",
            "txAgeBoisement",
            "txAutresBoisements",
            "txDensiteBoisement",
            "txDossierEtOuAutorisationPrecedent",
            "txEssencesBoisement",
            "txPeriodeDemontageInstallationSiSaisonniere",
            "txPeriodeExploitSiSaisonniere",
            "txQualiteBoisement",
            "txTraitementBoisement",
        ]

        # TODO: cover every needed keys
        amenagement_keys = [
            "nbEmplHabitatLegerLoisir",
            "nbEmplHabitatMobileLoisir",
            "nbMaxEmplHabitatMobileLoisirCaravane",
            "nbMaxEmplHabitatMobileLoisirResidence",
            "nbMaxEmplHabitatMobileLoisirTente",
            "nbMaxPersonnesAccueillies",
            "nbPlacesStationnementNonClosesOuNonCouvertes",
            "nbTotalEmpl",
            "nbUnites",
            "surfBatieAffecteeAuStationnement",
            "surfEmpriseAuSolAffecteeAuStationnement",
            "surfHabitatLegerLoisir",
            "surfTotaleAffecteeAuStationnement",
        ]

        result = []
        for props in PLATAU_MAPPING["amenagements"]:

            # make a proper copy, otherwise we'll break the nomenclature
            amenagements = props.copy()
            amenagement_exists = False

            if "x-openads-condition" in amenagements:

                openads_key = amenagements["x-openads-condition"]
                if openads_key not in self.data:
                    # no value from openads with this var name
                    continue

                value = self.data[openads_key]

                # Boolean to fixed struct case : annexe -> garage
                if not _caster({"type": "boolean"}, value):
                    # we can ignore this item
                    continue

                # this entry should be kept
                amenagement_exists = True
                # we should clean-up
                del amenagements["x-openads-condition"]

            for platau_key in platau_keys:

                if platau_key not in amenagements:
                    # platau key did'nt match any key of props, meaning we ignore it
                    continue

                else:
                    openads_key = amenagements[platau_key]

                    if openads_key not in self.data:
                        # no value from openads with this var name
                        # we should drop this item to avoid keeping a mapping key
                        del amenagements[platau_key]
                        continue

                    value = self.data[openads_key]

                    if value != "":
                        # We have a value, let's cast this
                        value = _caster(AmenagementP1._properties[platau_key], value)
                        # replace the key by its value
                        amenagements[platau_key] = value
                        amenagement_exists = True
                    else:
                        # empty value from openads with this var name
                        # we should drop this item to avoid keeping a mapping key
                        del amenagements[platau_key]

            #  Let's handle attributsAmenagement
            attributsAmenagement = []
            for entry in amenagements.get("attributsAmenagement", []):
                attribut_exists = False

                # make a proper copy, otherwise we'll break the nomenclature
                attributs = entry.copy()

                for amenagement_key in amenagement_keys:

                    if amenagement_key not in attributs:
                        # this is a static value; no mapping is necessary
                        continue

                    openads_key = attributs[amenagement_key]
                    if openads_key not in self.data:
                        # no value from openads with this var name
                        # let's drop the line
                        del attributs[amenagement_key]
                        continue

                    value = self.data[openads_key]
                    # Sometimes a 0 value is provided for no reason
                    if value not in ("", "0"):
                        # We have a value, let's cast and store this
                        attributs[amenagement_key] = _caster(
                            AttributAmenagP1._properties[amenagement_key], value
                        )
                        attribut_exists = True
                        amenagement_exists = True
                    else:
                        del attributs[amenagement_key]

                if attribut_exists:
                    attributsAmenagement.append(AttributAmenagP1(**attributs))

            if amenagement_exists:
                if attributsAmenagement != []:
                    amenagements["attributsAmenagement"] = attributsAmenagement
                else:
                    # We should drop attributes templates we used while mapping
                    if "attributsAmenagement" in amenagements:
                        del amenagements["attributsAmenagement"]

                # Ultra-specific cases
                # - 4 - Terrain de camping
                if amenagements["nomTypeAmenagement"] == 4:
                    amenagements["boPrexisteAuDossier"] = _caster(
                        AmenagementP1._properties["boPrexisteAuDossier"],
                        self.data.get("donnees_techniques.am_exist_agrand"),
                    )

                # - 17 - Coupe de bois
                if amenagements["nomTypeAmenagement"] == 17:
                    type_coupe = []
                    i = 1
                    for lieu in ("bois", "parc", "align"):
                        key = "donnees_techniques.am_coupe_{}".format(lieu)
                        if key in self.data and _caster(
                            {"type": "boolean"}, self.data[key]
                        ):
                            type_coupe.append(i)
                        i += 1

                    # Only one nomTypeLieuConcerne is supported.
                    # if various Lieu are provided, we ignore the property
                    if len(type_coupe) == 1:
                        amenagements["nomTypeLieuConcerne"] = type_coupe[0]

                # Eventually, we may proceed
                result.append(AmenagementP1(**amenagements))

        return result

    def _map_batiments(self, **kwargs):
        """Handle batiments mapping."""
        BatimentP1 = self.model_generator("BatimentP1")
        batiment = self.map("BatimentP1")

        value_exists = False
        for prop in BatimentP1._properties:
            if getattr(batiment, prop) is not None:
                value_exists = True
                break

        if value_exists:
            batiment.noBatiment = 1
            return [batiment]
        else:
            return

    def _map_boMaisonIndividuelle(self, **kwargs):
        """Handle boMaisonIndividuelle specific mapping.
        We have to check dossier_autorisation_type_code.
        """
        if self.data["dossier.dossier_autorisation_type_detaille_code"] in (
            "DPMI",
            "PI",
        ):
            return True
        else:
            return False

    def _map_coordonnees(self, **kwargs):
        """Handle annexes mapping."""
        coordonnees = self._generic_nomenclature_based_mapper(
            "CoordonneeP1",
            "coordonnees",
            # ["coordonnee", "indicatifPays", "x-openads-condition"],
            ["coordonnee", "indicatifPays"],
            data_subset=kwargs["data_subset"],
        )
        return coordonnees

    def _map_infoFiscalesOuStatistiquesAgregees(self, **kwargs):
        """Handle mapping of infosFiscales."""
        return self._generic_nomenclature_based_mapper(
            "InfoFiscOuStatAgrP1",
            "infoFiscalesOuStatistiquesAgregees",
            [
                "surfFiscaleSCC",
                "surfFiscaleHorsSCC",
                "nbLocaux",
                "surfaceFiscaleSSCIncluses",
            ],
        )

    def _map_equipementsPrives(self, **kwargs):
        """Handle equipementsPrives mapping."""
        return self._generic_nomenclature_based_mapper(
            "EquipPriveP1",
            "equipementsPrives",
            ["surfEquipPrive", "nbEquipPrive"],
            index_name="noEquipPrive",
        )

    def _map_equipementsPublics(self, **kwargs):
        """Handle equipementsPublics mapping."""
        return self._generic_nomenclature_based_mapper(
            "EquipPublicP1",
            "equipementsPublics",
            ["libConcessionnaire", "dtPrevuAvantLe"],
        )

    def _map_logementsParType(self, **kwargs):
        """Handle equipementsPrives mapping."""
        return self._generic_nomenclature_based_mapper(
            "LogementParTypeP1", "logementsParType", ["nbLogements",]
        )

    def _map_personnes(self, **kwargs):
        """Handle mapping of personnes property."""
        PersonneP1 = self.model_generator("PersonneP1")
        RoleP1 = self.model_generator("RoleP1")

        personnes = []
        index = 0
        for demandeur in self.data["demandeurs"]:
            personne = self.map("PersonneP1", data_subset=demandeur)
            if "demandeur.petitionnaire_principal" in demandeur and _caster(
                {"type": "boolean"}, demandeur["demandeur.petitionnaire_principal"]
            ):

                for r in personne.roles:
                    if r.nomRole == 1:  # 1,"Pétitionnaire",04/12/2019
                        r.noOrdrePersonneDansRole = 1
                        break

                # Let's handle AdresseFuture for the main Petitionnaire
                personne.adresseFuture = self.map("AdresseP1", "PersonneP1")

                # adresseFuture might be empty. We don't have to provided empty data
                data_exists = False
                for prop in self.model_generator("AdresseP1")._properties:
                    if getattr(personne.adresseFuture, prop, None) is not None:
                        data_exists = True
                        break

                if not data_exists:
                    personne.adresseFuture = None
            else:
                personne.adresseFuture = None

            # XXX - Mapping in CDS is wrong, we override it
            if demandeur["demandeur.qualite"] == "personne_morale":
                personne.boEstPersonneMorale = True
            else:
                personne.boEstPersonneMorale = False

            # Handle specific case
            # --------------------
            # - Splitting personne morale, only if this is a Petitionnaire
            #   (We don't want that for Delegataire)
            representant = None
            if (
                demandeur["demandeur.qualite"] == "personne_morale"
                and demandeur["demandeur.type_demandeur"] == "petitionnaire"
            ):
                # we build the Representant
                representant = PersonneP1(
                    noms=personne.noms,
                    prenoms=personne.prenoms,
                    boEstPersonneMorale=False,
                    nomGenre=personne.nomGenre,
                    boOkTransmissionDemat=personne.boOkTransmissionDemat,
                )
                # specify the right role, and link PersonneMorale with its Representant
                representant.roles = [
                    RoleP1(
                        indexPersonneRepresentee=index,
                        nomRole=PLATAU_MAPPING["ROLE"]["representant"]["idNom"],
                        noOrdrePersonneDansRole=1,
                    )
                ]
                # finally, we reset useless props from PersonneMorale
                personne.noms = personne.prenoms = personne.nomGenre = None

            # - Handle Delegataire relationship / XXX This seems useless
            # we overload the Roles created previuoulsy
            # if demandeur["demandeur.type_demandeur"] == "delegataire":
            #     personne.roles = [
            #         RoleP1(
            #             indexPersonneRepresentee=index_petitionnaire_principal,
            #             nomRole=PLATAU_MAPPING["ROLE"]["delegataire"]["idNom"],
            #         )
            #     ]

            personnes.append(personne)
            if representant is not None:
                personnes.append(representant)

            index += 1

        if "architecte" in self.data:
            architecte = self.map("PersonneP1", data_subset=self.data["architecte"])
            architecte.adresseFuture = None
            personnes.append(architecte)

        return personnes

    def _map_surfInstructionAgregees(self, **kwargs):
        """Handle mapping of surfaces."""
        surfaces = self._generic_nomenclature_based_mapper(
            "SurfInstAgrP1", "surfInstructionAgregees", "surfInstruction"
        )
        # we should get rid off entries with surfInstruction == 0, cause by openADS
        #  internal computing
        curated_surfaces = []
        for surface in surfaces:
            if surface["surfInstruction"] != 0:
                curated_surfaces.append(surface)

        return curated_surfaces

    def _map_terrains(self, **kwargs):
        """Handle mapping of terrains property."""
        terrain = self.map("TerrainP1")

        # Pays doesn't exist in openADS, but, hey, it's a French law based software
        terrain.adresses[0].nomPays = "France"

        # Terrain is monovalued in openADS (and in CERFAs), but platau expects a list,
        # with a number for each terrain. Let's fake that .
        terrain.noTerrain = "1"
        return [terrain]

    def _map_adresses(self, **kwargs):
        """Handle mapping of terrain / adresse property."""

        # Adresse is monovalued in openADS (and in CERFAs), but platau expects a list.
        # Let's fake that.
        return [self.map("AdresseP1", "TerrainP1")]

    def _map_refCadastrales(self, **kwargs):
        """Handle mapping of terrains property."""
        refCadastrales = []
        prefixe = re.compile("^[0-9]{3}")
        numero = re.compile("[0-9]{1,4}$")
        for parcelle in self.data["parcelles"]:

            parcelle["dossier_parcelle.prefixe"] = prefixe.findall(
                parcelle["dossier_parcelle.libelle"]
            )[0]
            parcelle["dossier_parcelle.numero"] = numero.findall(
                parcelle["dossier_parcelle.libelle"]
            )[0]
            # To support section alpha/digit, we substract prefixe and numero
            parcelle["dossier_parcelle.section"] = numero.split(
                prefixe.split(parcelle["dossier_parcelle.libelle"])[1]
            )[0]
            refCadastrales.append(self.map("RefCadastraleP1", data_subset=parcelle))

        return refCadastrales

    def _compute_todays_date(self) -> date:
        """Return today's date"""
        return date.today()


def _caster(attributes, value):
    """."""
    if attributes["type"] == "string":

        if "format" in attributes and attributes["format"] == "date":
            if "-" in value:
                str_date = value.split("-")
                if len(str_date) == 3:
                    #  case "YYYY-mm-dd"
                    date_format = "%Y-%m-%d"
                elif len(str_date) == 2:
                    #  case "YYYY-mm"
                    date_format = "%Y-%m"
            elif "/" in value:
                str_date = value.split("/")
                if len(str_date) == 3:
                    #  case "dd/mn/YYYY"
                    date_format = "%d/%m/%Y"
                elif len(str_date) == 2:
                    #  case "Y"
                    date_format = "%m/%Y"
            elif len(value) == 1:
                #  case "Y"
                date_format = "%Y"
            else:
                return None

            try:
                return datetime.strptime(value, date_format).date()
            except ValueError:
                return None

        elif "format" in attributes and attributes["format"] == "date-time":
            return datetime.strptime(value, "%Y-%m-%d")

        else:
            return value

    elif attributes["type"] == "boolean":
        if value in ("1", "t", "oui", "true", "True", True):
            return True
        elif value in ("0", "f", "non", "false", "False", False):
            return False
        elif value == "nesaispas":
            return None
        # XXX - case _map_txsAutresPrecisionsNatTrav, to be confirmed
        elif isinstance(value, str) and value != "":
            return True
        else:
            return None

    elif attributes["type"] == "integer":
        try:
            return int(value)
        except ValueError:
            log.warning(
                "Encountered a FLOAT value where an INT is expected. "
                "Value will be rounded : %s -> %s",
                value,
                int(float(value)),
            )

        try:
            return int(float(value))
        except ValueError:
            log.warning("Could not cast value %s to integer", value)

        return None

    elif attributes["type"] == "number":
        if "format" in attributes and attributes["format"] == "double":
            return float(value)
        else:
            return None

    else:
        raise NotImplementedError(
            f"Casting {value} in {attributes['type']} is not implemented"
        )


def _log(msg, level="debug"):
    """."""
    getattr(log, level)(msg)
