import argparse
import gc
import logging
import time
import json
import uuid

from bravado.exception import HTTPUnauthorized, HTTPForbidden, HTTPTooManyRequests
from dataclasses import dataclass

from openads.platau import PACKAGE_VERSION
from openads.platau.platau_connector import PlatAUConnector, PlatAUEvent
from openads.platau.celery import task_runner
from openads.platau.tasks import InputTask, OutputTask
from openads.platau.openads_driver import OpenADSDriver

log = logging.getLogger(__name__)

OUTPUT_TASK_ORDER = {
    "collectivite": [
        "creation_DA",
        "creation_DI",
        "modification_DA",
        "modification_DI",
        "ajout_piece",
        "suppression_piece",
        "modification_piece",
        "depot_DI",
        "creation_consultation",
        "qualification_DI",
        "lettre_majoration",
        "lettre_incompletude",
        "completude_DI",
        "incompletude_DI",
        # "pec_metier_consultation",
        # "avis_consultation",
        "decision_DI",
        "envoi_CL",
    ],
    "service_consulte": [
        "pec_metier_consultation",
        "avis_consultation",
        "prescription",
    ],
}

ACTOR_ROLES = {
    "collectivite": [
        "guichet_unique",
        "service_instructeur",
        "autorite_competente",
        "service_instructeur_epci",
        "autorite_competente_epci",
    ],
    "service_consulte": ["service_consulte"],
}


class PlatAUConductorError(Exception):
    pass


@dataclass
class OAPConfig:

    openadsapi_url: str
    openadsapi_username: str
    openadsapi_password: str

    platau_credentials: dict

    log_path: str
    spec_path: str

    api_path: str
    sandbox: bool = False
    notifications_logging: bool = False

    def __init__(self, config_path):
        """."""
        log.info("Loading and parsing openads.platau config")
        try:
            with open(config_path, "r") as f:
                conf = json.load(f)
        except IOError as e:
            log.error(f"Config file not found at {config_path}")
            raise e

        try:
            self.openadsapi_url = conf["openadsapi"]["url"]
            self.openadsapi_username = conf["openadsapi"]["username"]
            self.openadsapi_password = conf["openadsapi"]["password"]

            for key, credentials in conf["piste_apps"].items():
                if "client_id" not in credentials or "client_secret" not in credentials:
                    raise ValueError(
                        "Fail to parse config file. Check your `platau_apps` properties"
                    )

            if len(conf["piste_apps"]) == 1 and "default" not in conf["piste_apps"]:
                raise ValueError(
                    "Fail to parse config file. Check your `piste_apps` properties"
                )

            self.platau_credentials = conf["piste_apps"]

            # organization and api version are set in `api_path`, ex : `mtes_preprod/platau/v5`
            self.api_path = conf["platau"]["api_path"]
            if "sandbox" in conf["platau"]:
                self.sandbox = conf["platau"]["sandbox"]

            self.log_path = conf["paths"]["log"]
            self.spec_path = conf["paths"]["spec"]

            if conf["platau"].get("notifications_logging", False) and conf["paths"].get(
                "notifications", False
            ):
                self.notifications_logging = True
                self.notifications_path = conf["paths"]["notifications"]

        except KeyError as e:
            log.error(f"Fail to parse config file : {e}", exc_info=True)
            raise e

    @property
    def openadsapi_config(self):
        """."""
        return {
            "url": self.openadsapi_url,
            "auth": tuple([self.openadsapi_username, self.openadsapi_password]),
        }

    @property
    def spec_overrides(self):
        """."""
        return {
            "api_path": self.api_path,
            "sandbox": self.sandbox,
        }


class Conductor(object):
    """."""

    conf: OAPConfig
    connector: PlatAUConnector
    mode: str
    stream: str
    asynchronous: bool = False
    log_level: str = logging.DEBUG
    # verbosity: int = 0
    actor: str = None
    offset: int = None
    output_task_order: list
    actor_roles: list
    restrict_to_task: str = None

    def __init__(self, config: OAPConfig, mode: str, stream: str, kwargs=None) -> None:
        """."""
        log.info(f"Running conductor in {mode} mode")
        if kwargs.asynchronous:
            self.asynchronous = True
            log.info("Tasks will be handled asynchronously")

        self.stream = stream
        if self.stream == "input":
            log.info("Streaming tasks from Plat'AU to openADS")
        elif self.stream == "output":
            log.info("Streaming tasks from openADS to Plat'AU")
        else:
            raise ValueError("Stream should be `input` or `output`")

        self.mode = mode
        self.conf = config
        self.actor_roles = ACTOR_ROLES[mode]
        try:
            self.openads = OpenADSDriver(**self.conf.openadsapi_config, mode=mode)
        except Exception as e:
            log.error("Could not initialize proper connection with openADS")
            log.error(e, exc_info=True)
            raise PlatAUConductorError(
                "Could not initialize proper connection with openADS"
            )

        if kwargs.verbosity:
            self.verbosity = kwargs.verbosity

        self.connector = PlatAUConnector(
            config.platau_credentials,
            config.spec_path,
            spec_overrides=config.spec_overrides,
        )

        if self.stream == "output":
            self.output_task_order = OUTPUT_TASK_ORDER[self.mode]

        if kwargs.offset is not None:
            self.offset = kwargs.offset

        if kwargs.actor:
            if kwargs.actor not in [a.uid for a in self.openads.actors]:
                raise ValueError(
                    f"Specified actor {kwargs.actor} is not a known actor in this backend"
                )

            self.actor = kwargs.actor

        if kwargs.task:
            self.restrict_to_task = kwargs.task

    def run(self):
        """."""
        getattr(self, f"_handle_{self.stream}_tasks")()

    def _handle_input_tasks(self):
        """."""
        if self.conf.notifications_logging:
            platau_notifications = []

        for actor in self.openads.actors:
            if actor.role not in self.actor_roles:
                continue

            if self.actor and actor.uid != self.actor:
                continue

            try:
                self.connector.start(credentials_key=actor.uid)
            except Exception as e:
                log.error(
                    "Something went wrong while connecting to Plat'AU with actor %s."
                    "See below for more info.",
                    actor.uid,
                )
                log.error(e, exc_info=True)
                continue

            events = None
            if self.offset is None:
                try:
                    events = self.connector.get_notifications(actor.uid, raw=False)

                except (HTTPUnauthorized, HTTPForbidden) as e:
                    log.error(
                        f"Actor identified by {actor.uid} is not authorized to access Plat'AU"
                    )
                    log.error(e, exc_info=True)

                except HTTPTooManyRequests as e:
                    log.error(
                        f"'429 : Too many requests' catched while retrieving notifications for actor {actor.uid}"
                    )
                    log.error(e, exc_info=True)

                except Exception as e:
                    log.error(
                        f"Failed to get events for actor {actor.uid}. See tracebak below."
                    )
                    log.error(e, exc_info=True)

                if events is None:
                    continue

                # Keep tracks of notifications
                if self.conf.notifications_logging and events:
                    platau_notifications.extend(
                        [
                            event.as_dict(bravado=True, date_as_str=True)
                            for event in events
                        ]
                    )

            else:
                try:
                    events = self.connector.get_notifications(
                        actor.uid, offset=self.offset, raw=False
                    )

                except (HTTPUnauthorized, HTTPForbidden) as e:
                    log.error(
                        f"Actor identified by {actor.uid} is not authorized to access Plat'AU"
                    )
                    log.error(e, exc_info=True)

                except HTTPTooManyRequests as e:
                    log.error(
                        f"'429 : Too many requests' catched while retrieving notifications for actor {actor.uid}"
                    )
                    log.error(e, exc_info=True)

                except Exception as e:
                    log.error(
                        f"Failed to get events for actor {actor.uid}. See tracebak below."
                    )
                    log.error(e, exc_info=True)

                if events is None:
                    continue

            for event in events:

                log.info(f"Preparing task for event : {event}")

                if self.asynchronous:
                    # Task will pickled.
                    # We should pass pickable params (ie. self.connector isn't)
                    try:
                        task = InputTask(
                            event,
                            actor,
                            self.openads,
                            connector_config=self.connector.get_config(),
                        )
                    except NotImplementedError:
                        # For now we ignore not supported event type
                        continue
                    task_runner.delay(task)

                else:
                    try:
                        task = InputTask(
                            event, actor, self.openads, connector=self.connector
                        )
                    except NotImplementedError:
                        # For now we ignore not supported event type
                        continue
                    task.run()

            # In case of huge treatment, we force the garbage collector to do his job
            # and save somme memory.
            gc.collect()

        if self.conf.notifications_logging and platau_notifications:
            with open(self.conf.notifications_path, "w") as fd:
                json.dump(platau_notifications, fd, indent=4)

    def _handle_output_tasks(self):
        """."""
        criterions = {"state": "new", "category": "platau", "stream": "output"}
        for typology in self.output_task_order:
            criterions["typology"] = typology

            try:
                tasks_list = self.openads.get_tasks(criterions)
            except Exception as e:
                log.error(e, exc_info=True)
                log.error(
                    "Failed to retrieved output tasks `%s`", criterions["typology"]
                )
                continue

            if not tasks_list:
                log.info("No task `%s` retrieved from openADS", criterions["typology"])
                continue

            for task_summary in tasks_list:
                task_id = task_summary["task"]

                if self.restrict_to_task and self.restrict_to_task != task_id:
                    # We'll handle only the provided task
                    log.info(
                        "Operation is restricted to task %s. Task %s won't be handled",
                        self.restrict_to_task,
                        task_id,
                    )
                    continue

                if self.asynchronous:
                    # Task will pickled. We should pass pickable params (ie. self.connector isn't)
                    task = OutputTask(
                        self.openads, self.connector.get_config(), task_id
                    )
                    if task.state not in ("error", "canceled"):
                        task_runner.delay(task)

                else:
                    task = OutputTask(self.openads, self.connector, task_id)
                    if task.state not in ("error", "canceled"):
                        task.run()

            # XXX - 26/02/2021 : we encounter a wrong revision Dossier on
            # avis_consultation after a large (~8) pec_consultation transfert
            # This has to be fixed at response level, inspecting code and reason.
            # For now, let's just slow the machine ;)
            # PS : not needed if we are in async mode
            if not self.asynchronous:
                time.sleep(2)

            # In case of huge treatment, we force the garbage collector to do his job
            # and save somme memory.
            gc.collect()

    def replay_input_event(self, event_uid, actor_uid):
        """WIP"""
        log.info(f"Replaying event {event_uid} for actor {actor_uid}")
        actor = self.openads.search_actors(uid=actor_uid)[0]
        self.connector.start(credentials_key=actor.uid)
        self.connector.calling_actor = actor.uid
        event = self.connector.get_evenement_by_uid(event_uid)
        if event is None:
            return

        event = PlatAUEvent(event)
        try:
            task = InputTask(event, actor, self.openads, connector=self.connector)
        except NotImplementedError:
            # For now we ignore not supported event type
            return

        task.run()
        if task.state == "done":
            log.info(
                f"Event {event_uid} / {event.type_label} : successfully handled and replayed"
            )
            return
        elif task.state == "error" or task.state is None:
            log.error(
                f"Event {event_uid} / {event.type_label} : some errors occured during replay"
            )
            return


def main():
    parser = argparse.ArgumentParser(description="Connect openADS with Plat'AU")
    parser.add_argument(
        "-m",
        "--mode",
        help="Define the openADS type : collectivite|service_consulte",
        required=True,
        type=str,
    )

    parser.add_argument(
        "-s",
        "--stream",
        help="Specify the process stream : input|output|fullcycle",
        required=True,
        type=str,
    )

    parser.add_argument(
        "-c", "--config", help="Config file path", type=str, required=True
    )

    parser.add_argument(
        "-a",
        "--asynchronous",
        help="Run the app in asynchronous mode",
        action="store_true",
    )

    parser.add_argument(
        "-l",
        "--log_level",
        help="""Set the log level.
        Value should be in : DEBUG|INFO|WARNING|ERROR""",
    )

    parser.add_argument(
        "--force_log_path",
        type=str,
        help="""Force the log file path to given one.
        Property `paths.log` from config file will be overrided.
        May be useful in outsource's managed env.""",
    )

    parser.add_argument(
        "-t",
        "--actor",
        help="Actor Plat'AU UID. Retrieve notifications only for the specified actor.",
    )

    parser.add_argument(
        "-o",
        "--offset",
        type=int,
        help="""Retrieve notification at specified offset.
        This option must be set along --actor and is only valid with --stream `input`.""",
    )

    parser.add_argument(
        "-e",
        "--event",
        help="""Replay specified event.
        This option must be set along --actor and is only valid with --stream `input`.""",
    )

    parser.add_argument(
        "-k",
        "--task",
        type=str,
        help="""Restrict operation to selected output task, base don provided id.
        Other tasks will be ignored.
        This option must be set along --actor and is only valid with --stream `output`.
        """,
    )

    parser.add_argument(
        "-v", "--verbosity", action="count", help="Increase verbosity ouput."
    )

    args = parser.parse_args()

    config = OAPConfig(args.config)

    # Should we force log file path ?
    if args.force_log_path is not None:
        print(f"Logs will be written at {args.force_log_path}")
        config.log_path = args.force_log_path

    if args.mode not in {"collectivite", "service_consulte"}:
        print("Mode should be 'collectivite' or 'service_consulte'")
        return

    if args.stream not in {"input", "output", "fullcycle"}:
        print("Stream should be 'input', 'output' or 'fullcycle'")
        return

    if args.offset is not None and args.event is not None:
        print("--offset and --event option cannot be used both at the same time.")
        return

    if args.offset is not None or args.event is not None:
        if args.actor is None:
            print("This option must be set along --actor.")
            return
        if args.stream != "input":
            print("This option is only valid with --stream `input`.")
            return

    if args.task is not None:
        if args.stream != "output":
            print("This option is only valid with --stream `output`.")
            return

    # XXX - check if workers are up ?
    if args.asynchronous:
        pass

    log_formater = "%(asctime)s %(levelname)-7.7s [%(name)s] %(message)s"
    if args.log_level:
        if args.log_level not in ("DEBUG", "INFO", "WARNING", "ERROR"):
            print("Log level value should be in : DEBUG|INFO|WARNING|ERROR"),
            return

        args.log_level = getattr(logging, args.log_level.upper())
    else:
        args.log_level = logging.INFO

    # logging.basicConfig(filename=file_path, format=log_formater, level=args.log_level)
    logging.basicConfig(
        level=args.log_level,
        format=log_formater,
        handlers=[logging.FileHandler(config.log_path), logging.StreamHandler(),],
    )

    batch_id = str(uuid.uuid1()).split("-")[0]
    # Display package version and batch ID

    log.info(f"START / {PACKAGE_VERSION} / Batch ID: {batch_id}")

    if args.stream == "fullcycle":
        conductor = Conductor(config, args.mode, "output", kwargs=args)
        conductor.run()
        time.sleep(30)
        conductor = Conductor(config, args.mode, "input", kwargs=args)
        conductor.run()
    else:
        conductor = Conductor(config, args.mode, args.stream, kwargs=args)
        if args.event is not None:
            conductor.replay_input_event(args.event, args.actor)
        else:
            conductor.run()

    log.info(f"END / {PACKAGE_VERSION} / Batch ID: {batch_id}")


if __name__ == "__main__":
    main()
