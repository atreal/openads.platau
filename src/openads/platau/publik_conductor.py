import argparse
import logging
import time
import json
import uuid

from dataclasses import dataclass

from openads.platau import PACKAGE_VERSION
from openads.platau.publik_connector import PublikConnector, PublikError
from openads.platau.publik_tasks import InputTask, OutputTask
from openads.platau.openads_driver import OpenADSDriver

log = logging.getLogger(__name__)

BASE_FORMS = {
    "certificat-urbanisme": "demande_initiale",
    "permis-construire": "demande_initiale",
    "permis-amenager": "demande_initiale",
    "declaration-prealable": "demande_initiale",
    "permis-demolir": "demande_initiale",
    "pieces_complementaires": "dossier_existant",
    "permis-modificatif": "modificatif",
    "permis-transfert": "transfert",
    "permis-prorogation": "dossier_existant",
    "permis-annulation": "dossier_existant",
    "permis-doc": "doc",
    "permis-daact": "daact",
}

WORKFLOW_INIT_STATUS = "just_submitted"

OUTPUT_TASK_ORDER = [
    "notification_recepisse",
    "notification_instruction",
    "notification_decision",
]


class PublikConductorError:
    pass


@dataclass
class PublikConductorConfig:

    openadsapi_url: str
    openadsapi_username: str
    openadsapi_password: str

    publik_url: str
    publik_username: str
    publik_password: str

    log_path: str

    def __init__(self, config_path):
        """."""
        log.info("Loading and parsing openads <> Publik config")
        try:
            with open(config_path, "r") as f:
                conf = json.load(f)
        except IOError as e:
            log.error(f"Config file not found at {config_path}")
            raise e

        try:
            self.openadsapi_url = conf["openadsapi"]["url"]
            self.openadsapi_username = conf["openadsapi"]["username"]
            self.openadsapi_password = conf["openadsapi"]["password"]

            self.publik_url = conf["publik"]["url"]
            self.publik_username = conf["publik"]["username"]
            self.publik_password = conf["publik"]["password"]

            # Base forms are hard-defined in currnet file - see FORMS
            # Additional forms can be handled with `publik.forms` in conf. 
            # Base forms and additionnal forms will be merged
            # Base forms can be ignored when setting `ignore_base_forms` in conf
            # Only forms defined in conf will be retrieved
            ignore_base_forms = conf["publik"].get("ignore_base_forms", None)
            self.forms = {}
            if not ignore_base_forms:
                self.forms = BASE_FORMS
            if "forms" in conf["publik"]:
                self.forms.update(conf["publik"]["forms"])

            self.publik_portal_mode = conf["publik"].get("portal_mode", "mono")
            if self.publik_portal_mode not in ("mono", "multi"):
                msg = "`portal_mode` should be one of [`mono`, `multi`]"
                log.error(msg)
                raise ValueError(msg)

            self.log_path = conf["publik"]["log"]

        except KeyError as e:
            log.error(f"Fail to parse config file : {e}", exc_info=True)
            raise e

    @property
    def openadsapi_config(self):
        """."""
        return {
            "url": self.openadsapi_url,
            "auth": tuple([self.openadsapi_username, self.openadsapi_password]),
        }

    @property
    def publik_config(self):
        """."""
        return {
            "url": self.publik_url,
            "auth": tuple([self.publik_username, self.publik_password]),
        }


class PublikConductor(object):
    """."""

    conf: PublikConductorConfig
    stream: str
    log_level: str = logging.DEBUG

    def __init__(self, config: PublikConductorConfig, stream: str, kwargs=None) -> None:
        """."""
        log.info("Running Publik conductor")

        self.stream = stream
        if self.stream == "input":
            log.info("Streaming tasks from Publik to openADS")
        elif self.stream == "output":
            log.info("Streaming tasks from openADS to Publik")
        else:
            raise ValueError("Stream should be `input` or `output`")

        self.conf = config
        openadsapi_config = self.conf.openadsapi_config
        openadsapi_config["ignore_config"] = True
        try:
            self.openads = OpenADSDriver(**openadsapi_config)
        except Exception as e:
            log.error("Could not initialize proper connection with openADS")
            log.error(e, exc_info=True)
            raise PublikConductorError

        if kwargs.verbosity:
            self.verbosity = kwargs.verbosity

        self.connector = PublikConnector(**self.conf.publik_config)

    def run(self):
        """."""
        getattr(self, f"_handle_{self.stream}_tasks")()

    def _handle_input_tasks(self):
        """."""
        for form_slug, form_type in self.conf.forms.items():
            criterions = {"filter": WORKFLOW_INIT_STATUS}
            fields_criterions = {}
            workflow_criterions = {}
            if self.conf.publik_portal_mode == "multi":

                # In multi_mode, Demandes should be filtered by CT. We based our filter
                # on `openadsapi_login`.

                # We have to retrieve Demande in full mode to apply our own filter
                # XXX : move to publik native filter is possible
                criterions.update({"full": "on"})
                fields_criterions = {"openadsapi_login": self.conf.openadsapi_username}

                # https://gitlab.com/atreal/openads.platau/-/issues/110
                # We should be absolutely sure we only handle Demande with WORKFLOW_INIT_STATUS
                workflow_criterions = {"status": {"id": WORKFLOW_INIT_STATUS}}

            try:
                for demande in self.connector.list_demandes(
                    form_slug,
                    criterions=criterions,
                    fields_criterions=fields_criterions,
                    workflow_criterions=workflow_criterions,
                ):
                    log.info(f"Preparing task for Demande : {demande['id']}")
                    try:
                        task = InputTask(
                            form_slug, form_type, demande, self.openads, self.connector
                        )
                    except Exception as e:
                        log.error(e, exc_info=True)
                        continue

                    task.run()

            except PublikError as e:
                log.error(e, exc_info=True)
                continue

    def _handle_output_tasks(self):
        """."""
        criterions = {"state": "new", "category": "portal"}

        for typology in OUTPUT_TASK_ORDER:
            criterions["typology"] = typology

            tasks = self.openads.get_tasks(criterions)
            if not tasks:
                continue

            for task_summary in tasks:
                task_id = task_summary["task"]

                task = OutputTask(self.openads, self.connector, task_id)
                if task.state == "error":
                    continue
                else:
                    task.run()


def main():
    parser = argparse.ArgumentParser(
        description="Handle transfert between openADS and Publik"
    )

    parser.add_argument(
        "-s",
        "--stream",
        help="Specify the process stream : input|output|fullcycle",
        required=True,
        type=str,
    )

    parser.add_argument(
        "-c", "--config", help="Config file path", type=str, required=True
    )

    parser.add_argument(
        "-l",
        "--log_level",
        help="""Set the log level.
        Value should be in : DEBUG|INFO|WARNING|ERROR""",
    )

    parser.add_argument(
        "--force_log_path",
        type=str,
        help="""Force the log file path to given one.
        Property `paths.log` from config file will be overrided.
        May be useful in outsource's managed env.""",
    )

    parser.add_argument(
        "-v", "--verbosity", action="count", help="Increase verbosity ouput."
    )

    args = parser.parse_args()

    config = PublikConductorConfig(args.config)

    # Should we force log file path ?
    if args.force_log_path is not None:
        print(f"Logs will be written at {args.force_log_path}")
        config.log_path = args.force_log_path

    if args.stream not in {"input", "output", "fullcycle"}:
        print("Stream should be 'input', 'output' or 'fullcycle'")
        return

    log_formater = "%(asctime)s %(levelname)-7.7s [%(name)s] %(message)s"
    if args.log_level:
        if args.log_level not in ("DEBUG", "INFO", "WARNING", "ERROR"):
            print("Log level value should be in : DEBUG|INFO|WARNING|ERROR"),
            return

        args.log_level = getattr(logging, args.log_level.upper())
    else:
        args.log_level = logging.INFO

    # logging.basicConfig(filename=file_path, format=log_formater, level=args.log_level)
    logging.basicConfig(
        level=args.log_level,
        format=log_formater,
        handlers=[logging.FileHandler(config.log_path), logging.StreamHandler(),],
    )

    batch_id = str(uuid.uuid1()).split("-")[0]
    # Display package version and batch ID
    log.info(f"START / {PACKAGE_VERSION} / Batch ID: {batch_id}")

    if args.stream == "fullcycle":
        conductor = PublikConductor(config, "output", kwargs=args)
        conductor.run()
        time.sleep(30)
        conductor = PublikConductor(config, "input", kwargs=args)
        conductor.run()
    else:
        conductor = PublikConductor(config, args.stream, kwargs=args)
        conductor.run()

    log.info(f"END / {PACKAGE_VERSION} / Batch ID: {batch_id}")


if __name__ == "__main__":
    main()
