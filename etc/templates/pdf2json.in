#!/bin/bash

SOURCE_FOLDER=${vars:dataset_source_path}
DEST_FOLDER=${vars:dataset_path}/cerfa
JSON_FOLDER=$DEST_FOLDER/json_files
FINAL_FOLDER=$DEST_FOLDER/final_files

mkdir -p $JSON_FOLDER;
mkdir -p $FINAL_FOLDER;

# Conversion des fichiers en base64
for DOSSIER_TYPE in `ls $SOURCE_FOLDER`
do
    # cd $repo
    WORKING_DIR=$SOURCE_FOLDER/$DOSSIER_TYPE
    for DOSSIER in `ls $WORKING_DIR`
    do
        FILE_NAME=$DOSSIER.pdf
        if [ ! -f $WORKING_DIR/$DOSSIER/$FILE_NAME ]; then
            FILE_NAME=$DOSSIER-V7.pdf
        fi
        echo Converting $WORKING_DIR/$DOSSIER/$FILE_NAME to base64
        touch $JSON_FOLDER/$DOSSIER.json;
        CONTENT_BASE64=`base64 -w 0 $WORKING_DIR/$DOSSIER/$FILE_NAME`;
        echo '{"base64":' > $JSON_FOLDER/$DOSSIER.json;
        echo '"'$CONTENT_BASE64'"' >> $JSON_FOLDER/$DOSSIER.json;
        echo "}" >> $JSON_FOLDER/$DOSSIER.json;
    done
done

# Envoie de la requête curl et récupération des résultats dans un dossier
for FILE in `ls $JSON_FOLDER`
do
    touch $FINAL_FOLDER/$FILE
    echo Extracting fields from $FILE
    curl --header "Content-Type:application/json" --request POST --data @$JSON_FOLDER/$FILE ${vars:poolbox_url}/extract >  $FINAL_FOLDER/$FILE
done

# Apply few specific fixes
for FILE in `ls $FINAL_FOLDER`
do
    FILE_PATH=$FINAL_FOLDER/$FILE
    sed -i 's/{"fields": //' $FILE_PATH
    sed -i 's/}$//' $FILE_PATH
    sed -i 's/topmostSubform\[0\]\.//g' $FILE_PATH
    sed -i 's/Page[0-9]*\[0\]\.//g' $FILE_PATH
    sed -i 's/\[0\]//g' $FILE_PATH
    type_DPMI_or_PCMI=`expr substr $FILE 1 4`
    type_DP=`expr substr $FILE 1 2`
    sed -i 's/\("T5Z[A-Z]2":\ \)"[0-9][0-9]"/\1"ZZ"/g' $FILE_PATH
    sed -i 's/\("T5Z[A-Z]3":\ \)"[A-Z][A-Z][A-Z][A-Z]"/\1"1111"/g' $FILE_PATH
    sed -i 's/\("T2S_section":\ \)"[0-9][0-9]"/\1"AA"/g' $FILE_PATH
    sed -i 's/\("T2N_numero":\ \)"[A-Z][A-Z][A-Z][A-Z]"/\1"1111"/g' $FILE_PATH
    sed -i 's/C7A_surface/W1MA1/g' $FILE_PATH
    sed -i 's/C7K_supprimee/W1MD1/g' $FILE_PATH
    sed -i 's/C7U_creee/W1MB1/g' $FILE_PATH
    if [[ $type_DPMI_or_PCMI == "DPMI" ]]; then
        sed -i 's/C2ZA7_autres/C5ZE6_autresannexes/g' $FILE_PATH
        sed -i 's/F1TD1_Surfacedemolieconstruction/F1TD1_surfacedemolieconstruction/g' $FILE_PATH
        sed -i 's/F9C_code/F9C_codepostal/g' $FILE_PATH
    elif [[ $type_DP == "DP" ]]; then
        sed -i 's/F9L_localit\u00e9/F9L_localite/g' $FILE_PATH
        sed -i 's/F9Q_num\u00ero/F9Q_numero/g' $FILE_PATH
    fi
done

sed -i 's/\\r/ /' $FINAL_FOLDER/PCMI-PCXXXC2U18YY008.json
sed -i 's/"D4K_morale": "Oui"/ "D4K_morale": "Off"/'  $FINAL_FOLDER/DPXXXC2U18YY104.json
sed -i 's/"D1A_naissance": "32131978"/"D1A_naissance": "30121978"/' $FINAL_FOLDER/DPMI-DPXXXC2U18YY008.json
sed -i 's/"D1F_femme": "Oui"/"D1F_femme": "Off"/' $FINAL_FOLDER/PCMI-PCXXXC2U18YY002.json
sed -i 's/"D1N_nom": "Mendes et Cabello"/"D1N_nom": "Mendes"/' $FINAL_FOLDER/PCMI-PCXXXC2U18YY002.json
sed -i 's/"D1P_prenom": "Shawn et Camilla"/"D1P_prenom": "Shawn"/' $FINAL_FOLDER/PCMI-PCXXXC2U18YY002.json

exit 1

