import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, "README.rst")) as f:
    README = f.read()


setup(
    name="openads.platau",
    version="7.2.0rc2.dev0",
    description="openADS <> Plat'AU connector",
    long_description=README,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    keywords="openADS Plat'AU dematerialisation MTES",
    author="atReal",
    author_email="contact@atreal.fr",
    url="https://www.atreal.fr",
    packages=find_packages("src"),
    package_dir={"": "src"},
    namespace_packages=["openads"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "bravado",
        "celery",
        "filemagic",
        "flatten-json",
        "oauthlib",
        "poolbox",
        "requests_oauthlib",
        "zc.buildout",
    ],
    extras_require={
        "dev": [
            "pypdf",
            "webtest",
            "robotsuite",
            "robotframework",
            "robotframework-csvlibrary-py3",
            "robotframework-seleniumlibrary",
            "zest.releaser",
        ]
    },
    entry_points={
        "console_scripts": [
            "platau-conductor = openads.platau.conductor:main",
            "platau-cli = openads.platau.platau_cli:main",
            "publik-conductor = openads.platau.publik_conductor:main",
        ],
    },
)
