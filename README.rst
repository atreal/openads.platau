*****************
Plat'AU connector
*****************

Make it work
============

This is an opinioned way to install the app.
Other ways exist, free free to choose your own.

App structure will be ::

    openads.platau
    ├── app
    │   ├── bin
    │   ├── building-cache.cfg
    │   ├── buildout.cfg
    │   ├── buildout-offline.cfg
    │   ├── dev.cfg
    │   ├── develop-eggs
    │   ├── eggs
    │   ├── etc
    │   ├── HISTORY.rst
    │   ├── MANIFEST.in
    │   ├── parts
    │   ├── patches
    │   ├── pickedversions.cfg
    │   ├── README.rst
    │   ├── setup.py
    │   ├── src
    │   ├── staging.cfg
    │   ├── versions.cfg
    │   └── versions-dev.cfg
    ├── bin
    │   ├── activate
    │   ├── activate.csh
    │   ├── activate.fish
    │   ├── activate.ps1
    │   ├── activate_this.py
    │   ├── activate.xsh
    │   ├── buildout
    │   ├── pip
    │   ├── pip3
    │   ├── pip3.11
    │   ├── pip3.8
    │   ├── python -> python3.8
    │   ├── python3 -> python3.8
    │   ├── python3.8
    │   ├── python-config
    │   └── wheel
    ├── include
    │   └── python3.8 -> /usr/include/python3.8
    └── lib
        └── python3.8


Create and activate a virtual env ::

    $ virtualenv --python=PYTHON_EXE openads.platau
    $ cd openads.platau
    $ . bin/activate


Clone this repository ::

    $ git clone git@gitlab.com:atreal/openads.platau.git app
    $ cd app


Install buildout and build the stuff.
In dev mode, include the `dev.cfg` as config file argument ::

    $ pip install zc.buildout
    $ buildout [-c dev.cfg]

Dependencies
============

If you plan to use celery (optional), you shoul install RabbitMQ broker ::

    sudo install rabbitmq-server


Credentials
===========

Env variables must be set in order to connect properly to MTES API. Add a `.env` at
project root path and provide the mandatory variables ::

    openads.platau/app $ echo -e "export platau_client_id=XXX\nexport platau_client_secret=YYY" > .env

    openads.platau/app $ cat .env
    export platau_client_id=XXX
    export platau_client_secret=YYY


Apply env variables before launching ::

    openads.platau $ source .env


Run the app
===========

configuration file
^^^^^^^^^^^^^^^^^^

A JSON config file should be provided ::

    {
      "openadsapi": {
        "password": "secret",
        "url": "http://api.openads.local:6543",
        "username": "ministereculture"
      },
      "paths": {
        "log": "/home/tiazma/projects/openads.platau/var/log/openads.platau.log",
        "spec": "/home/tiazma/projects/openads.platau/src/openads/platau/specs/platau_5.2_openads.json"
      },
      "piste_apps": {
        "default": {
          "client_id": "PISTE_CLIENT_ID",
          "client_secret": "PISTE_CLIENT_SECRET"
        }
      },
      "platau": {
        "api_path": "/mtes_preprod/platau/v9",
        "sandbox": false
      }
    }

Almost every properties are self-explainatory. For the others :

* *platau.sandbox* : should the used env be a sanbox
* *platau.api_path* : with this value you'll set PISTE organization and API version


collectivite mode - WIP
^^^^^^^^^^^^^^^^^^^^^^^

From openADS to Plat'AU ::

    ./bin/platau-conductor -m collectivite -s ouput -c myconfig.json

From Plat'AU to openADS ::

    ./bin/platau-conductor -m collectivite -s input -c myconfig.json

service_consulte mode - WIP
^^^^^^^^^^^^^^^^^^^^^^^^^^^

From openADS to Plat'AU ::

    ./bin/platau-conductor -m service_consulte -s ouput -c myconfig.json

From Plat'AU to openADS ::

    ./bin/platau-conductor -m service_consulte -s input -c myconfig.json

Going asynchronous
^^^^^^^^^^^^^^^^^^
RabbitMQ is up by default.
Celery worker should be started ::

    ./bin/celery -A openads.platau worker --loglevel=INFO -E

Then run your conductor with the `-a` or `--asynchronous` arg ::

    ./bin/platau-conductor -m service_consulte -s input -c myconfig.jso


Plat'AU CLI
===========

A Plat'AU CLI tool is built and available through ::

    ./bin/platau_cli [options] action

Usage can be known via builtin help ::

    ./bin/platau-cli --help


Developement
============

Buildout should be called with dev.cfg config file ::

    $ buildout -Nvc dev.cfg

Env variable can be set to avoid sending requests to endpoints ::

    export piste_dry_safe=1


Building datasets
^^^^^^^^^^^^^^^^^
You'll need poolbox running somewhere. Default is `http://localhost:6544`. It can be
overwrited in the dev.cfg [vars] section.

Source folder should respetc the current strcuture ::

    /${PATH}/dataset
    └── PCMI
        └── PCMI-PCXXXC2U18YY002
            ├── PCMI14-1-PCXXXC2U18YY002.pdf
            ├── PCMI14-PCXXXC2U18YY002.doc
            ├── PCMI15-PCXXXC2U18YY002.png
            ├── PCMI-A7-PCXXXC2U18YY002.rtf
            └── PCMI-PCXXXC2U18YY002-V7.pdf


After buildout is runned, a script `pdf2json` is available in the `bin` directory.
You may use it to build the JSON versions of the CERFA ::

    ./bin/pdf2json

Generated JSON files are stored in the `var/dataset/cerfa/final_files` directory (or any
other directory you'll overwrite in dev.cfg). Be aware that previous generated files
with same name will be overwrited.

You should convert all the Pieces to pdf. Conversion can be runned with ::

    ./bin/pdfconverter

On buildout first-time run, symlinks to dataset are broken. After building dataset,
you'll have to re-run buidlout command in order to fix this ::

    $ buildout -Nvc dev.cfg


Tests
^^^^^

At this point, you should have an `openmairie.devtools` installed in some virtualenv.
You have to fill your openADS with some data.

Base environnement
~~~~~~~~~~~~~~~~~~

Go to `part/openads/tests` and run the base_env test case. It wil create a Marseille
Collectivite with everything you need to create some Dossiers ::

    ~/openads.platau/parts/openads/tests$ om-tests -c runone -t base_env.robot --noinit

Dossiers injection
~~~~~~~~~~~~~~~~~~

Go to `part/openads/tests` and run the base_dataset test case. It will create
2 Dossiers (1 DP and 1 PCMI) with some attachements ::

    ~/openads.platau/parts/openads/tests$ om-tests -c runone -t base_dataset.robot --noinit

Running tests
~~~~~~~~~~~~~

.. XXX to be fulfilled

Tests are runned with `zope.testrunner`.

Run all tests ::

    $ ./bin/test

Run all tests of specific module ::

    $ ./bin/test -m test_transmission

Run one specific test ::

    $ ./bin/test -t test_05_create_dossier


Update CDS
^^^^^^^^^^

Copy the CDS and add '_openads' in the name :

    ``cp platau_X.Y.json platau_X.Y_openads.json``

Apply pretty JSON formater.

    ``CTRL + ALT + J``

Make sure to put `host` and `info` properties at the top. Check your URL in `host`
and `securityDefinitions` properties.

Apply these rules :

* keys `tags` in endpoint definitions should be dropped : `bravado` use those to override method's names. Trust me, you don't want that.
  Identify those with this regex in your IDE :
``
,
    .*\"tags\": \[
    (.*?)
    .*\]
``
* First-level property `x-components`should be dropped : useless.
* If absent, first-level property `securityDefinitions` should be restored.

Merge mapping from old version to new version with meld :

    ``meld platau_V.W_openads.json platau_X.Y_openads.json``

with this steps :

* Required properties that are no longer required should be dropped. While handling
  such case, don't forget to check if there's any consequence in mappers


Update Nomenclature
^^^^^^^^^^^^^^^^^^^

* Download last Nomenclature archive from Plat'AU wiki
* Unzip it in `tmp` directory
* Copy all CSV files to `mapping` directory ::

    cp /tmp/Nomenclature\ version\ X.Y/*/*.csv openads.platau/src/openads/platau/mapping/nomenclature


Release
^^^^^^^

Copy specified git log range to HISTORY.rst and orgnanize it ::

    $ git log HEAD ^29e54c94dc5542413cf284826eb5450cb91cac7f >> HISTORY.rst

Commit ::

    $ git commit HISTORY.rst -m "chore: prepare release XXX"

Launch the release with zest.releaser ::

    $ ./bin/fullrelease


Plat'AU matching version
========================

- openads.platau < 1 matches Plat'AU 4.8
- openads.platau == 1 matches Plat'AU 5.2
- openads.platau == 2.X matches Plat'AU 6.X
- openads.platau == [3|4].X matches Plat'AU 7.X
- openads.platau == 5.X matches Plat'AU 8.X
- openads.platau == 6.X matches Plat'AU 9.X
- openads.platau == 7.X matches Plat'AU 10.X

Plat'AU matching version
========================
- openads.platau == 6.X matches `python 3.8`
- openads.platau == 7.X matches `python 3.8`


MISC
====

Building offline cache
----------------------

You can build a cache allowing offline buildouting ::

    $ buildout -c building-cache.cfg

A `cache.tgz` will be built, with a `download-cache` and `src` dir fully pre-loaded.
Unarchived in a fresh check-out, it allows to built opeands.api offline ::

    $ buildout -c buildout-offline.cfg



